''MACRO TITLE: AUTO-ALIGN SELECTED ENTITIES LEFT MACRO
'This macro will perform the 'align left' operation (similar
'to the align buttons on the Alignment Toolbar) for all selected
'entities.

'This macro demonstrates more on how to use entity display objects.
'-------------------------------------------------------------------

Sub Main
	Dim MyDiagram As Diagram
	Dim MyModel As Model
	Dim MyEntity As Entity
	Dim MySubModel As SubModel
	Dim MySelObject As SelectedObject
	Dim MyAttribute As AttributeObj
	Dim MyEntityDisplay As EntityDisplay
	Dim LeftMostEntity As EntityDisplay
	
	Dim ID As Integer
	Dim LeftMost As Integer
	Dim xPosition As Integer
	Dim Logical As Boolean
	Dim ObjectName As String
	Dim ObjType As Integer
	
	LeftMost = -1
	
	'Get the current diagram.
	
	Set MyDiagram = DiagramManager.ActiveDiagram
	
	'Get the current model.
	
	Set MyModel = MyDiagram.ActiveModel
	
	'Get the current submodel.
	
	Set MySubModel = MyModel.ActiveSubModel
	
	'Determine if the model is logical or physical.
	
	Logical = MyModel.Logical
	
	'FDetermine the left-most coordinate position is. Iterate 
	'through all the selected entities in the submodel and 
	'found out which entity is the left-most.
	
	For Each MySelObject In MySubModel.SelectedObjects
		
		'Get type of the selected object.
		
		ObjType = MySelObject.Type
		
		'We only want to align entities, so check the
		'object type.
		
		If ObjType = 1 Then
		
			'Get the ID of the selected object.
		
			ID = MySelObject.ID
	
			'Now, get the actual entity object with this ID.
			
			Set MyEntity = MyModel.Entities.Item(ID)
	
			'We need to get the entity name, so that we can
			'get the corresponding entity display object from
			'the submodel.
			
			'If the model is logical, get the entity name. 
			'If it is physical, get the table name.
			
			If Logical = True Then
				ObjectName = MyEntity.EntityName
			Else 
				ObjectName = MyEntity.TableName
			End If
		
			'Get the entity display object from the submodel.
			
			Set MyEntityDisplay = MySubModel.EntityDisplays.Item(ObjectName)
	
			'Get the x coordinate (upper left corner) of the 
			'entity display object.
			
			xpos = MyEntityDisplay.HorizontalPosition
	
			'If this x coordinate is less than any of the
			'x coordinates so far, then assign this x coordinate
			'as the left-most.
			
			If xpos < LeftMost Or LeftMost = -1 Then
				LeftMost = xpos
			End If
			
		End If
		
	Next MySelObject
	
	'Now iterate through all selected entities and align each
	'of the left sides with the smallest x coordinate 
	'found above.
	
	For Each MySelObject In MySubModel.SelectedObjects

		'Get type of the selected object.
		
		ObjType = MySelObject.Type
		
		'We only want to align entities, so check the
		'object type.
		
		If ObjType = 1 Then	
		
			ID = MySelObject.ID
	
			'Now, get the actual entity object with this ID.
			
			Set MyEntity = MyModel.Entities.Item(ID)
	
			'We need to get the entity name, so that we can
			'get the corresponding entity display object from
			'the submodel.
			
			'If the model is logical, get the entity name. 
			'If it is physical, get the table name.
			
			If Logical = True Then
				ObjectName = MyEntity.EntityName
			Else 
				ObjectName = MyEntity.TableName
			End If
		
			'Get the entity display object from the submodel.
			
			Set MyEntityDisplay = MySubModel.EntityDisplays.Item(ObjectName)

			'Align the left side of the entity display object 
			'with the smallest x coordinate found in
			'the previous iteration loop.
			
			MyEntityDisplay.HorizontalPosition = LeftMost	
			
		End If
		
	Next MySelObject
	
End Sub
