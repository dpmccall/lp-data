''MACRO TITLE: AUTO-COLOR ALL ENTITIES WITH FKS MACRO
'This macro will make all entities with foreign keys purple.
'It will first select (highlight) all entities in the current, 
'submodel. Then it will make all selected entities with 
'foreign keys (which, in this case, are all the entities in the 
'submodel) purple.  Finally, it will deselect (unhighlight) all
'entities in the current, active submodel.

'This macro illustrates how to use entity objects, entity display
'objects, and selected objects - and how to distinguish among 
'them.
'------------------------------------------------------------------

Sub Main
	Dim MyDiagram As Diagram
	Dim MyModel As Model
	Dim MyEntityDisplay As EntityDisplay	
	Dim MyEntity As Entity
	Dim MySubModel As SubModel
	Dim MyAttribute As AttributeObj
	Dim MySelObject As SelectedObject
	
	Dim ObjectName As String
	Dim ForeignKey As Boolean
	Dim Logical As Boolean
	
	'Get the current diagram.
	
	Set MyDiagram = DiagramManager.ActiveDiagram
	
	'Get the current model.
	
	Set MyModel = MyDiagram.ActiveModel

	'Determine if the model is logical or physical.
	
	Logical = MyModel.Logical
	
	'Get the current submodel.
	
	Set MySubModel = MyModel.ActiveSubModel
	
	'Iterate through all the entity display objects in the 
	'current SubModel And Select (highlight) all entities in 
	'the SubModel.
	
	For Each MyEntityDisplay In MySubModel.EntityDisplays
	
		'In order to select (highlight) an entity object in 
		'a submodel, we need to pass in the actual ID of the 
		'entity object (not the entity display object) to 
		'the 'Add' function of the SelectedObjects collection.
		
		'In order to get the ID of the actual entity object,
		'we get the name of the entity from the entity
		'display object, and then pass it to the 'Item' method
		'of the Entities collection.  This will allow us to get
		'the actual entity object.
		
		ObjectName = MyEntityDisplay.Name
		
		Set MyEntity = MyModel.Entities.Item(ObjectName)
		
		ID = MyEntity.ID
	
		'Now we actually add the entity to the SelectedObjects
		'collection.  This will highlight the entity on the screen.
		'The first parameter to 'Add' is the type.  In this case,
		'type is 1 (entity).  The second parameter is the ID of
		'the entity.
		
		MySubModel.SelectedObjects.Add(1, ID)
	
	Next MyEntityDisplay
	
	'Now, we iterate through all the selected entities in
	'the submodel and set the entity background color of
	'selected entities with foreign keys to red.
	
	For Each MySelObject In MySubModel.SelectedObjects
	
		'Get the object type - we are only concerned
		'with entities.
		
		ObjType = MySelObject.Type
		
		If ObjType = 1 Then
		
			' Get the ID for the selected object.
		
			ID = MySelObject.ID
		
			' Now get the actual entity object with this ID. 
			' The model contains the collection of all the
			' entities.
		
			Set MyEntity = MyModel.Entities.Item(ID)

			'We need to the name of the entity.  We can use the
			'name to get the entity display object from the submodel.
			
			If Logical = True Then
		
				' If the model is logical, get the entity 
				' name.

				ObjectName = MyEntity.EntityName
			Else
		
				' If the model is physical, get the table 
				' name.
	
				ObjectName = MyEntity.TableName
			End If
			
			'Iterate through all the attributes in the entity
			'and see if there are any foreign keys.
			
			For Each MyAttribute In MyEntity.Attributes
		
				'Determine if the attribute is a foreign key.
			
				ForeignKey = MyAttribute.ForeignKey
				
				If ForeignKey = True Then
				
					'If the attribute is a foreign key, then we want
					'to get the entity display object from the
					'submodel and make the entity display object
					'red.
				
					'In order to get the entity display object from
					'the submodel, we pass in the entity name to the
					'EntityDisplays 'Item' function.
				
					'Note: We cannot pass in the ID of the entity object
					'because entity display objects and entity objects
					'use different sets of IDs.
			
					If Logical = True Then
		
						' If the model is logical, get the entity 
						' name.
	
						ObjectName = MyEntity.EntityName
					Else
		
						' If the model is physical, get the table 
						' name.
	
						ObjectName = MyEntity.TableName
					End If
				
					Set MyEntityDisplay = MySubModel.EntityDisplays.Item(ObjectName)
				
					'Change the color of local, non-key attributes.
					'We want the color to be purple.
	

					'Each of the three primary colors can have a 
					'value from 0-255.
					'using the RGB function we can mix blue and red
					'to make purple

					MyEntityDisplay.BackgroundColor = RGB(255, 0, 255)


					'Once we have changed the color of the entity
					'display object, we don't have to continue to
					'iterate through the attributes, so just exit
					'the iteration loop.
				
					Exit For		
				
				End If
			
			Next MyAttribute
			
		End If			
		
	Next MySelObject

	'Now we want to iterate through all the entity display
	'objects in the active submodel and deselect (unhighlight)
	'all entities in the submodel.
	
	For Each MyEntityDisplay In MySubModel.EntityDisplays
		
		'In order to select (highlight) an entity object in 
		'a submodel, we need to pass In the actual ID of the 
		'entity object (not the entity display object) to 
		'the 'Add' function of the SelectedObjects collection.
		
		'In order to get the ID of the actual entity object,
		'we first get the name of the entity from the entity
		'display object, and then pass that to the 'Item' method
		'of the Entities collection.  This will allow us to get
		'the actual entity object.

		ObjectName = MyEntityDisplay.Name
		
		Set MyEntity = MyModel.Entities.Item(ObjectName)
		
		ID = MyEntity.ID

		'Now we actually add the entity to the SelectedObjects
		'collection.  This will highlight the entity on the screen.
		'The first parameter to 'Remove' is the type.  In this case,
		'type is 1 (entity).  The second parameter is the ID of
		'the entity.

		MySubModel.SelectedObjects.Remove(1, ID)
	
	Next MyEntityDisplay
End Sub
