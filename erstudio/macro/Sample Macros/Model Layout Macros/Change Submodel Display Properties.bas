Attribute VB_Name = "Module1"
'MACRO TITLE: CHANGE SUBMODEL DISPLAY PROPERTIES
'This macro will change several display properties of the
'currently active submodel.  In particular, it will change
'the font and color for non-key, non-inherited attributes.
'----------------------------------------------------------------

Sub Main()
        Dim MyDiagram As Diagram
        Dim MyModel As Model
        Dim MySubModel As SubModel
        Dim MyERSFont As ERSFont
        Dim MyEntdisp As EntityDisplay
        Dim MyAttribute As AttributeObj
        Dim MyEntity As Entity


        'Get the currently active diagram.
        
        Set MyDiagram = DiagramManager.ActiveDiagram
        
        'Get the currently active model.
        
        Set MyModel = MyDiagram.ActiveModel
        
        'Get the currently active submodel.
        
        Set MySubModel = MyModel.ActiveSubModel
        
        'Get the font object for local, non-key attributes.
        
        Set MyERSFont = MySubModel.AttributeFont
        
		'Set various properties of the font object.
        MyERSFont.Bold = True
        MyERSFont.Underline = True

        
		'Change the color of inherited, non-key attributes.
        'We want the color to be purple.

        'In hexadecimal, the color format is:
        '0x00BBGGRR (RR - red, GG - green, BB - blue)
        
        'Each of the three primary colors can have a value from
        '0-255.
        
        'So, in hex, purple is 0x00FF00FF (255 for blue and
        '255 For red).  In decimal, 0x00FF00FF converts to 16711935.

        MySubModel.InheritedNonKeyColor = 16711935

        
		'in this next example we want to set the attribute font to italic and
		'the color to blue if the attribute has a DATE data type.  So we need
		'to Loop through the Entities of the submodel and then the attributes
		'of the entity


		For Each MyEntdisp In MySubModel.EntityDisplays

			Set MyEntity = MyModel.Entities.Item(MyEntdisp.ParentEntity.ID)

			For Each MyAttribute In MyEntity.Attributes

				If InStr(ucase(MyAttribute.CompositeDatatype),"DATE") Then

					Set MyERSFont = MyAttribute.Font
					MyERSFont.Italic = True
					MyERSFont.Bold = True
					MyAttribute.Color = 16711680  ' this will make them blue

				End If

			Next

		 Next
        
        'Now we want to change the display setting of the current
        'submodel to full attributes, show domain and show datatype

        MySubModel.EntityDisplayFormat = 1
        MySubModel.ShowEntityDataType = True
        MySubModel.ShowEntityDomain = True

        'Set the zoom factor to %50
        
        MySubModel.ZoomFactor = 50
        
End Sub
