'#Language "WWB-COM"

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Macro: Resize Lasso Selection.bas
'
' Purpose: This macro can be used for formatting, where it allows all or selected entities
'          to be set to the same specified length and width, including to the largest/smallest size.
'
' Date: 05/21/2015
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


Option Explicit
Enum EObjectSelection
	AllObjects
	SelectedObjects
End Enum
Type EDefaultSize
	maxWidth As Boolean
	maxHeight As Boolean
	minWidth As Boolean
	minHeight As Boolean
End Type
Dim theDiagram As Diagram
Dim theModel As Model
Dim theSubModel As SubModel
Sub Main
	Begin Dialog UserDialog 420,399,"Entity View  Size Setting",.DiagFunc ' %GRID:10,7,1,1
		GroupBox 10,49,370,133,"Size Setting",.grpSizeSetting
		Text 20,70,100,14,"Width",.txtHorizontalSize
		Text 20,126,90,14,"Height",.txtVerticalSize
		TextBox 100,70,140,21,.edHorizontalSize
		TextBox 100,126,140,21,.edVerticalSize
		GroupBox 10,196,260,70,"Object Types",.grpObjectTypes
		CheckBox 20,224,140,14,"Entities / Tables",.ckEntity
		CheckBox 20,245,90,14,"Views",.ckView
		GroupBox 10,280,260,70,"Object Selection",.GroupBox3
		OptionGroup .grpObjectSelection
			OptionButton 20,301,120,14,"All Objects",.opAllObjects
			OptionButton 20,322,170,14,"Selected Objects",.opSelectedObjects
		OKButton 30,371,90,21
		CancelButton 140,371,90,21
		PushButton 140,371,90,21,"Exit",.btnExit
		PushButton 20,14,170,21,"Size of Selected Object",.pbSizeOfSelectedObject
		OptionGroup .grpDefWidth
			OptionButton 20,98,110,14,"Max Width",.opMaxWidth
			OptionButton 130,98,100,14,"Min Width",.opMinWidth
		OptionGroup .grpDefHeight
			OptionButton 20,154,100,14,"Max Height",.opMaxHeight
			OptionButton 140,154,100,14,"Min Height",.opMinHeight
		CheckBox 260,70,100,14,"Use Width",.ckDefWidth
		CheckBox 260,126,100,14,"Use Height",.ckDefHeight
	End Dialog
	Dim dlg As UserDialog

	If Dialog(dlg) = 0 Then End

End Sub

Rem See DialogFunc help topic for more information.
Private Function DiagFunc(DlgItem$, Action%, SuppValue?) As Boolean

	Dim SHorizontalSize As String
	Dim SVerticalSize As String
	Dim horizontalSize As Integer
	Dim verticalSize As Integer
	Dim objSel As EObjectSelection
	Dim processTables As Boolean
	Dim processViews As Boolean
	Dim defSize As EDefaultSize
	Dim defWidth As Boolean
	Dim defHeight As Boolean

	Select Case Action%
		Case 1
			If (DiagramManager.DiagramCount = 0) Then
				MsgBox("Please open a diagram before running macro.", vbExclamation)
				End
			End If

			Set theDiagram = DiagramManager.ActiveDiagram
			Set theModel = theDiagram.ActiveModel
			Set theSubModel = theModel.ActiveSubModel


			DlgVisible("Cancel", False)
			DlgValue("grpObjectSelection", 0)
			DlgValue("ckEntity", True)
			DlgValue("ckView", True)
			DlgValue("ckDefWidth", True)
			DlgValue("ckDefHeight", True)
			DlgEnable("opMaxWidth", False)
			DlgEnable("opMinWidth", False)
			DlgEnable("opMaxHeight", False)
			DlgEnable("opMinHeight", False)

		Case 2
			Select Case DlgItem$
				Case "ckDefWidth"
					defWidth = DlgValue("ckDefWidth")
					If defWidth Then
						DlgEnable("edHorizontalSize", True)
						DlgEnable("opMaxWidth", False)
						DlgEnable("opMinWidth", False)
					Else
						DlgEnable("edHorizontalSize", False)
						DlgEnable("opMaxWidth", True)
						DlgEnable("opMinWidth", True)
					End If
					DiagFunc = True
					Exit Function

				Case "ckDefHeight"

					defHeight = DlgValue("ckDefHeight")
					If defHeight Then
						DlgEnable("edVerticalSize", True)
						DlgEnable("opMaxHeight", False)
						DlgEnable("opMinHeight", False)
					Else
						DlgEnable("edVerticalSize", False)
						DlgEnable("opMaxHeight", True)
						DlgEnable("opMinHeight", True)
					End If
					DiagFunc = True
					Exit Function


				Case "OK"
					SHorizontalSize = Trim(DlgText("edHorizontalSize"))
					If (SHorizontalSize <> "") And (Not IsNumeric(SHorizontalSize)) Then
						MsgBox ("Please enter a numerical value for horizontal size.", vbExclamation)
						DiagFunc = True
						Exit Function
					End If

					SVerticalSize = Trim(DlgText("edVerticalSize"))
					If (SVerticalSize <> "") And (Not IsNumeric(SVerticalSize)) Then
						MsgBox ("Please enter a numerical value for vertical size.", vbExclamation)
						DiagFunc = True
						Exit Function
					End If

					If SHorizontalSize = "" Then
						horizontalSize = -1
					Else
						horizontalSize = CInt(SHorizontalSize)
					End If

					If SVerticalSize = "" Then
						verticalSize = -1
					Else
						verticalSize = CInt(SVerticalSize)
					End If

					defWidth = (Not DlgValue("ckDefWidth"))
					defHeight = (Not DlgValue("ckDefHeight"))
					With defSize
						If (DlgValue("grpDefHeight") = 0) Then
							.maxHeight = True
							.minHeight = False
						Else
							.maxHeight = False
							.minHeight = True
						End If

						If (DlgValue("grpDefWidth") = 0) Then
							.maxWidth = True
							.minWidth = False
						Else
							.maxWidth = False
							.minWidth = True
						End If
					End With

					objSel = IIf(DlgValue("grpObjectSelection") = 0, AllObjects, SelectedObjects)
					processTables = CBool(DlgValue("ckEntity"))
					processViews = CBool(DlgValue("ckView"))

					DiagramManager.EnableScreenUpdateEx(False, True)
					If (objSel = EObjectSelection.SelectedObjects) And (theSubModel.SelectedObjects.Count = 0) Then
						MsgBox("You have chosen selected objects but no objects are selected.", vbExclamation)
						DiagFunc = True
						Exit Function
					End If

					SetSize(horizontalSize, verticalSize, objSel, processTables, processViews, defWidth, defHeight, defSize)
					DiagramManager.EnableScreenUpdateEx(True, True)
					MsgBox("Done.", vbInformation)

					DiagFunc = True
					Exit Function
				Case "pbSizeOfSelectedObject"
					If (GetSizeOfSelected(horizontalSize, verticalSize)) Then
					 	DlgText("edHorizontalSize", CStr(horizontalSize))
						DlgText("edVerticalSize", CStr(verticalSize))
					End If
					DiagFunc = True
					Exit Function
				Case "btnExit"
					DiagFunc = False
					Exit Function
				Case "Cancel"
					DiagFunc = False
					Exit Function
			End Select
		Case 3 ' TextBox or ComboBox text changed
		Case 4 ' Focus changed
		Case 5 ' Idle
			Rem Wait .1 : DiagFunc = True ' Continue getting idle actions
		Case 6 ' Function key
	End Select
End Function
Function SetSize(ByVal horizontalSize As Integer, ByVal verticalSize As Integer, ByVal objSel As EObjectSelection, ByVal processTables As Boolean, ByVal processViews As Boolean, ByVal defWidth As Boolean, ByVal defHeight As Boolean, ByVal defSize As EDefaultSize)
	Dim so As SelectedObject
	Dim theEntityDisplay As EntityDisplay
	Dim theViewDisplay As ViewDisplay
	Dim theEntity As Entity
	Dim theView As View
	Dim nWidth As Integer
	Dim nHeigth As Integer
	Dim maxWidth As Integer
	Dim minWidth As Integer
	Dim maxHeight As Integer
	Dim minHeight As Integer


	nWidth = -1
	nHeigth = -1

	GetMaxMinSize(objSel, maxWidth, minWidth, maxHeight, minHeight)
	If defWidth Then
		nWidth = IIf(defSize.maxWidth, maxWidth, minWidth)
	Else
		nWidth = horizontalSize
	End If
	If defHeight Then
		nHeigth = IIf(defSize.maxHeight, maxHeight, minHeight)
	Else
		nHeigth = verticalSize
	End If

	If (processTables) Then
		If (objSel = AllObjects) Then
			For Each theEntityDisplay In theSubModel.EntityDisplays
				With theEntityDisplay
					.HorizontalSize = IIf(nWidth <> -1, nWidth, horizontalSize)
					.VerticalSize = IIf(nHeigth <> -1, nHeigth, verticalSize)
				End With
			Next theEntityDisplay
		Else
			For Each so In theSubModel.SelectedObjects
				If (so.Type = 1) Then
					Set theEntity = theModel.Entities(so.ID)
					Set theEntityDisplay = theSubModel.EntityDisplays(theEntity.EntityName)
					With theEntityDisplay
						.HorizontalSize = IIf(nWidth <> -1, nWidth, horizontalSize)
						.VerticalSize = IIf(nHeigth <> -1, nHeigth, verticalSize)
					End With
				End If
			Next so
		End If
	End If

	If (processViews) Then
		If (objSel = AllObjects) Then
			For Each theViewDisplay In theSubModel.ViewDisplays
				With theViewDisplay
					.HorizontalSize = IIf(nWidth <> -1, nWidth, horizontalSize)
					.VerticalSize = IIf(nHeigth <> -1, nHeigth, verticalSize)
				End With
			Next theViewDisplay
		Else
			For Each so In theSubModel.SelectedObjects
				If (so.Type = 16) Then
					Set theView = theModel.Views(so.ID)
					Set  theViewDisplay = theSubModel.ViewDisplays(theView.Name)
					With theViewDisplay
						.HorizontalSize = IIf(nWidth <> -1, nWidth, horizontalSize)
						.VerticalSize = IIf(nHeigth <> -1, nHeigth, verticalSize)
					End With
				End If
			Next so
		End If
	End If
End Function
Function GetSizeOfSelected(ByRef horizontalSize As Integer, ByRef verticalSize As Integer) As Boolean
	Dim theDiagram As Diagram
	Dim theModel As Model
	Dim theSubModel As SubModel
	Dim so As SelectedObject
	Dim theEntityDisplay As EntityDisplay
	Dim theViewDisplay As ViewDisplay
	Dim theEntity As Entity
	Dim theView As View

	GetSizeOfSelected = False

	Set theDiagram = DiagramManager.ActiveDiagram
	Set theModel = theDiagram.ActiveModel
	Set theSubModel = theModel.ActiveSubModel

	If (theSubModel.SelectedObjects.Count = 0) Then
		MsgBox("No object is selected. Please select an object first.", vbExclamation)
		Exit Function
	End If

	horizontalSize = -1
	verticalSize = -1
	For Each so In theSubModel.SelectedObjects
		Select Case so.Type
			Case 1
				Set theEntity = theModel.Entities(so.ID)
				Set theEntityDisplay = theSubModel.EntityDisplays(theEntity.EntityName)
				With theEntityDisplay
					horizontalSize = .HorizontalSize
					verticalSize = .VerticalSize
				End With
				Exit For
			Case 16
				Set theView = theModel.Views(so.ID)
				Set  theViewDisplay = theSubModel.ViewDisplays(theView.Name)
				With theViewDisplay
					horizontalSize = .HorizontalSize
					verticalSize = .VerticalSize
				End With
				Exit For
		End Select
	Next so

	If (horizontalSize = -1) And (verticalSize = -1) Then
		MsgBox("No entity or view has been selected.", vbExclamation)
		Exit Function
	Else
		GetSizeOfSelected = (MsgBox( _
			"Horizontal Size: " + CStr(horizontalSize) + vbCrLf + _
			"Vertical Size: " + CStr(verticalSize), _
			vbYesNoCancel, "Size of Selected") = vbYes)
	End If
End Function
Function GetMaxMinSize(ByVal objSel As EObjectSelection, ByRef maxWidth As Integer, ByRef minWidth As Integer, ByRef maxHeight As Integer, ByRef minHeight As Integer)
	Dim so As SelectedObject
	Dim theEntityDisplay As EntityDisplay
	Dim theViewDisplay As ViewDisplay
	Dim theEntity As Entity
	Dim theView As View


	Set theDiagram = DiagramManager.ActiveDiagram
	Set theModel = theDiagram.ActiveModel
	Set theSubModel = theModel.ActiveSubModel

	maxWidth = -1
	minWidth = -1
	maxHeight = -1
	minHeight = -1

	If (objSel = EObjectSelection.SelectedObjects) Then
		For Each so In theSubModel.SelectedObjects
			Select Case so.Type
				Case 1
					Set theEntity = theModel.Entities(so.ID)
					Set theEntityDisplay = theSubModel.EntityDisplays(theEntity.EntityName)
					With theEntityDisplay
						If (maxWidth = -1) Then maxWidth = .HorizontalSize
						If (minWidth = -1) Then minWidth = .HorizontalSize
						If (maxHeight = -1) Then maxHeight = .VerticalSize
						If (minHeight = -1) Then minHeight = .VerticalSize

						maxWidth = IIf(maxWidth > .HorizontalSize, maxWidth, .HorizontalSize)
						minWidth = IIf(minWidth < .HorizontalSize, minWidth, .HorizontalSize)
						maxHeight = IIf(maxHeight > .VerticalSize, maxHeight, .VerticalSize)
						minHeight = IIf(minHeight < .VerticalSize, minHeight, .VerticalSize)
					End With
					Exit For
				Case 16
					Set theView = theModel.Views(so.ID)
					Set  theViewDisplay = theSubModel.ViewDisplays(theView.Name)
					With theViewDisplay
						If (maxWidth = -1) Then maxWidth = .HorizontalSize
						If (minWidth = -1) Then minWidth = .HorizontalSize
						If (maxHeight = -1) Then maxHeight = .VerticalSize
						If (minHeight = -1) Then minHeight = .VerticalSize
						maxWidth = IIf(maxWidth > .HorizontalSize, maxWidth, .HorizontalSize)
						minWidth = IIf(minWidth < .HorizontalSize, minWidth, .HorizontalSize)
						maxHeight = IIf(maxHeight > .VerticalSize, maxHeight, .VerticalSize)
						minHeight = IIf(minHeight < .VerticalSize, minHeight, .VerticalSize)
					End With
					Exit For
			End Select
		Next so
	Else
		For Each theEntityDisplay In theSubModel.EntityDisplays
			With theEntityDisplay
				If (maxWidth = -1) Then maxWidth = .HorizontalSize
				If (minWidth = -1) Then minWidth = .HorizontalSize
				If (maxHeight = -1) Then maxHeight = .VerticalSize
				If (minHeight = -1) Then minHeight = .VerticalSize
				maxWidth = IIf(maxWidth > .HorizontalSize, maxWidth, .HorizontalSize)
				minWidth = IIf(minWidth < .HorizontalSize, minWidth, .HorizontalSize)
				maxHeight = IIf(maxHeight > .VerticalSize, maxHeight, .VerticalSize)
				minHeight = IIf(minHeight < .VerticalSize, minHeight, .VerticalSize)
			End With
		Next theEntityDisplay
		For Each theViewDisplay In theSubModel.ViewDisplays
			With theViewDisplay
				If (maxWidth = -1) Then maxWidth = .HorizontalSize
				If (minWidth = -1) Then minWidth = .HorizontalSize
				If (maxHeight = -1) Then maxHeight = .VerticalSize
				If (minHeight = -1) Then minHeight = .VerticalSize
				maxWidth = IIf(maxWidth > .HorizontalSize, maxWidth, .HorizontalSize)
				minWidth = IIf(minWidth < .HorizontalSize, minWidth, .HorizontalSize)
				maxHeight = IIf(maxHeight > .VerticalSize, maxHeight, .VerticalSize)
				minHeight = IIf(minHeight < .VerticalSize, minHeight, .VerticalSize)
			End With
		Next theViewDisplay
	End If
End Function
