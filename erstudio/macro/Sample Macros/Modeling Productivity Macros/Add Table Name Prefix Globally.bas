' 'MACRO TITLE: ADD TABLE NAME PREFIX GLOBALLY MACRO
' This macro lets the user add a prefix string to the names
' of all entities in the model.  The macro opens a
' dialog box prompting the user for the prefix.  If the current
' active model is logical, it will assign the prefix to
' all entity names.   If the current model is physical, it will 
' assign the prefix to all table names.
'------------------------------------------------------------------

Sub Main
	Dim MyDiagram As Diagram
	Dim MyModel As Model
	Dim MyEntity As Entity
	Dim ObjectName As String
	Dim NewObjectName As String
	Dim Prefix As String
	Dim Logical As Boolean
	
	' Create the dialog box.  This dialog box was
	' created using the Sax Basic Dialog Editor.
	
	Begin Dialog UserDialog 260,112 ' %GRID:10,7,1,1
		TextBox 30,28,210,21,.Prefix
		Text 30,7,160,14,"Entity Name Prefix:",.Text1
		OKButton 80,70,90,21
	End Dialog
	
	Dim dlg As UserDialog
	
	' Run the dialog box.
	
	Dialog dlg
	
	' Get the prefix from the dialog box.
	
	Prefix = dlg.Prefix
	
	' Get the current diagram.
	
	Set MyDiagram = DiagramManager.ActiveDiagram
	
	' Get the current model.
	
	Set MyModel = MyDiagram.ActiveModel
	
	' Determine if the current model is logical or physical.
	
	Logical = MyModel.Logical
	
	' Iterate through all entities in the current model.
	
	For Each MyEntity In MyModel.Entities
		
		If Logical = True Then
		
			' If the model is logical, get the entity 
			' name, add the prefix to the entity name and
			' set the new name back into the entity.

			ObjectName = MyEntity.EntityName
			NewObjectName = Prefix + ObjectName
			MyEntity.EntityName = NewObjectName
		Else
		
			' If the model is physical, get the table 
			' name, add the prefix to the table name and
			' set the new name back into the entity.
		
			ObjectName = MyEntity.TableName
			NewObjectName = Prefix + ObjectName
			MyEntity.TableName = NewObjectName
		End If
	
	Next MyEntity
	
End Sub
