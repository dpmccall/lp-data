' MACRO TITLE: 	CONVERT CASE NAME
' AUTHOR:		Mervyn Courtney
' DATE:			30/10/2003
' This macro converts the case of Attributes, Columns and Entty/Table names for the highlighted tables/Entities
' in the active SubModel to all upper or lower case.  Works in Logical or Physical models

Sub Main
	' Declare the variables.

	Dim d  As Diagram
	Dim m  As Model
	Dim sm As SubModel
	Dim o  As SelectedObject
	Dim id As Integer
	Dim e  As Entity
	Dim a As AttributeObj

	' Do the normal stuff.

	Set d = DiagramManager.ActiveDiagram
	Set m = d.ActiveModel
	Set sm = m.ActiveSubModel

	
	' Prompt the user to make a choice for upper/lower case.
	
	Begin Dialog UserDialog 310,161,"Case Selection" ' %GRID:10,7,1,1
		OKButton 110,126,90,21
		GroupBox 10,7,140,105,"Convert",.GroupBox1
		CheckBox 20,21,110,14,"Entity",.chkEntity
		CheckBox 20,63,100,14,"Columns",.chkColumn
		CheckBox 20,84,90,14,"Attributes",.chkAttr
		GroupBox 160,7,130,105,"Case Conversion",.GroupBox2
		OptionGroup .optCase
			OptionButton 170,28,100,14,"&Upper Case",.OptionButton1
			OptionButton 170,56,100,14,"&Lower Case",.OptionButton2
			OptionButton 170,84,110,14,"&Proper Case",.OptionButton3
		CheckBox 20,42,100,14,"Table",.chkTable
	End Dialog
	Dim dlg As UserDialog
	Dialog dlg

	DiagramManager.EnableScreenUpdate(False)

	' You need the submodel when determining what's selected.
	' Iterate through all selected objects and process the only entities.

	For Each o In sm.SelectedObjects
		If o.Type = 1 Then		' Entities are Type 1 objects.
			
			' Get the entity.

			id = o.ID
			Set e = m.Entities.Item(id)

			' Make the conversion.
			
			If dlg.optCase = 0 Then
				If dlg.chkEntity = 1 Then
					e.EntityName = StrConv(e.EntityName, vbUpperCase)
				End If
				If dlg.chkTable = 1 Then
					e.TableName = StrConv(e.TableName, vbUpperCase)
				End If
				If dlg.chkColumn = 1 Then
					For Each a In e.Attributes
						a.ColumnName =StrConv(a.ColumnName,vbUpperCase)
						If a.HasRoleName = True Then
							a.RoleName = StrConv(a.ColumnName,vbUpperCase)
						End If
					Next
				End If
				If dlg.chkAttr = 1 Then
					For Each a In e.Attributes
						a.AttributeName =StrConv(a.AttributeName,vbUpperCase)
						If a.HasLogicalRoleName = True Then
							a.LogicalRoleName = StrConv(a.LogicalRoleName,vbUpperCase)
						End If
					Next
				End If
			ElseIf dlg.optCase = 1 Then
				If dlg.chkEntity = 1 Then
					e.EntityName = StrConv(e.EntityName, vbLowerCase)
				End If
				If dlg.chkTable = 1 Then
					e.TableName = StrConv(e.TableName, vbLowerCase)
				End If
				If dlg.chkColumn = 1 Then
					For Each a In e.Attributes
						a.ColumnName =StrConv(a.ColumnName,vbLowerCase)
						If a.HasRoleName = True Then
							a.RoleName = StrConv(a.RoleName,vbLowerCase)
						End If
					Next
				End If
				If dlg.chkAttr = 1 Then
					For Each a In e.Attributes
						a.AttributeName =StrConv(a.AttributeName,vbLowerCase)
						If a.HasLogicalRoleName = True Then
							a.LogicalRoleName = StrConv(a.LogicalRoleName,vbLowerCase)
						End If
					Next
				End If
			ElseIf dlg.optCase = 2 Then
				If dlg.chkEntity = 1 Then
					e.EntityName = ProperCase(e.EntityName)
				End If
				If dlg.chkTable = 1 Then
					e.TableName = ProperCase(e.TableName)
				End If
				If dlg.chkColumn = 1 Then
					For Each a In e.Attributes
						a.ColumnName =ProperCase(a.ColumnName)
						If a.HasRoleName = True Then
							a.RoleName = ProperCase(a.RoleName)
						End If
					Next
				End If
				If dlg.chkAttr = 1 Then
					For Each a In e.Attributes
						a.AttributeName =ProperCase(a.AttributeName)
						If a.HasLogicalRoleName = True Then
							a.LogicalRoleName = ProperCase(a.LogicalRoleName)
						End If
					Next
				End If
			End If
		End If
	Next

	DiagramManager.EnableScreenUpdate(True)

End Sub

Function ProperCase (myString As String)
	Dim I As Integer
	Dim myWork, myChar, myOut As String

	myWork = myString
	myWork = StrConv(myWork,vbLowerCase)
	myOut = ""

	For I = 1 To Len(myWork)
		myChar = Mid(myWork,I,1)

		If I = 1 Then							'capitalize the first char
			myChar = StrConv(myChar,vbUpperCase)
			myOut = myOut & myChar
		ElseIf myChar = " " Then
			myOut = myOut & myChar
			I = I +1
			myChar = Mid(myWork,I,1)
			myChar = StrConv(myChar,vbUpperCase)
			myOut = myOut & myChar
		Else
			myOut = myOut & myChar
		End If
	Next I

	ProperCase = myOut

End Function
