'TITLE:  LOOP THROUGH CURRENT SUBMODEL
'DESCRIPTION:  This macro demonstrates how to loop through the objects
'	of the current submodel.

'ER/Studio Variables
Dim diag As Diagram
Dim mdl As Model
Dim submdl As SubModel

Dim ent As Entity
Dim entdisp As EntityDisplay

Dim rel As Relationship
Dim reldisp As RelationshipDisplay

Dim vw As View
Dim vwdisp As ViewDisplay



Sub Main

	Set diag = DiagramManager.ActiveDiagram
	Set mdl = diag.ActiveModel
	Set submdl = mdl.ActiveSubModel


	'loop through the entities
	For Each entdisp In submdl.EntityDisplays

		Set ent = entdisp.ParentEntity

		'insert entity operations here
		'and/or insert other loops for attributes or indexes
		'use "ent.<entity property>"

	Next entdisp

	'loop throgh the relationships
	For Each reldisp In submdl.RelationshipDisplays

		Set rel = reldisp.ParentRelationship

		'can insert relationship operations here
		'use "rel.<relationship property>"

	Next reldisp

	'loop throgh the relationships
	For Each vwdisp In submdl.ViewDisplays

		Set vw = vwdisp.ParentView

		'can insert view operations here
		'use "vw.<view property>"

	Next vwdisp

	
End Sub
