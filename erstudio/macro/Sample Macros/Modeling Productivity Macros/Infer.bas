' Macro Title: Infer.bas
' 
' This macro attempts to replicate the Infer operation that is available in ER/Studio during reverse engineering and importing of SQL files.
'
' ******************************************************************************
' Note: This code is not that which is used internally by ER/Studio application.
'*******************************************************************************
'  
' Macro tries to perform the infer operations based on the following rules documented in ER/Studio on-line Help > User's Guide:
'
' � Infer Referential Integrity: ER/Studio DA can infer referential Integrity when none is declared in the database. Click the options that follow
' to create relationships between entities in your diagram.
'
' � Infer Primary Keys: If selected, ER/Studio DA infers primary keys from unique indexes on a table. If more than one unique index exists on a
' table, ER/Studio DA chooses the index with the fewest columns.
'
' � Infer Foreign Keys from Indexes: If selected, ER/Studio DA infers foreign keys from indexes. When inferring foreign keys from indexes, 
' ER/Studio DA looks for indexes whose columns match the names, datatype properties, and column sequences of a primary key. If the �child� index is
' a primary key index, it must contain more columns than the �parent� primary key. In this case, an identifying relationship is created. 
'
' � Infer Foreign Keys from Names: If selected and your database contains foreign keys, ER/Studio DA infers foreign keys from names. When inferring
' foreign keys from names, ER/Studio DA looks for columns matching the names and datatype properties of a primary key. In this case, a
' non-identifying relationship is created. ER/Studio DA cannot infer relationships where the child column has a role name, instead create a
' non-identifying relationship and then designate the correct role-named column using the Edit Rolename function; right-click the relationship and
' then click Edit Rolename.
'
' � Infer Domains: If selected, ER/Studio DA infers domains from the columns in the database. ER/Studio DA creates a domain for each unique
' combination of a column name and its associated datatype properties. Duplicate domains with an underscore and number suffix indicate that columns
' with the same name but different datatypes were found in the database. This can alert you of how standardized the columns are in a database. You
' can use macros to consolidate domains and preserve the bindings from the inferred domains. Domains will be placed in the Local Data Dictionary in a
' domain folder named "Inferred Domains".
'
'
' Note: To infer domains, this macro makes use of Windows Scripting Dictionary object.
'
' Version: 1.0
'****************************************************************************************************************************************************

Option Explicit
Option Base 1

Enum ETableSelection
	SelectedOnly
	AllTables
End Enum

Const Identifying As Integer =  0
Const NonIdentifying As Integer = 1
Const NonSpecific As Integer = 2
Const NonIdentifying_Optional As Integer = 3
Const DOMAIN_FOLDER_NAME As String = "Inferred Domains"
Const MAX_LONG As Long =  2147483647
Sub Main
	Dim disableScreenUpdate As Boolean
	Dim optionSelected As Boolean
	Dim tableSelection As ETableSelection
	Dim theDiagram As Diagram
	Dim theModel As Model

	Debug.Clear

	Begin Dialog UserDialog 400,336,"Infer" ' %GRID:10,7,1,1
		GroupBox 20,119,360,154,"Infer Options",.GroupBox1
		CheckBox 40,147,190,14,"Infer Primary Key",.ckInferPK
		OKButton 100,308,90,21
		CancelButton 210,308,90,21
		GroupBox 20,21,360,77,"Table Selection",.GroupBox2
		OptionGroup .ogTableSelection
			OptionButton 30,42,230,14,"Selected Tables",.opSelectedTables
			OptionButton 30,70,240,14,"All Tables",.opAllTables
		CheckBox 40,175,230,14,"Infer Foreign Keys from Indexes",.ckInferFKFromIndex
		CheckBox 40,203,230,14,"Infer Foreign Keys from Names",.ckInferFKFromNames
		CheckBox 40,231,190,14,"Infer Domains",.ckInferDomains
	End Dialog

	Dim dlg As UserDialog

	' Set initial table selection option to Selected Tables Only
	dlg.ogTableSelection = 0


	' Enable error handling
	On Error GoTo errHandler

	' Check that a diagram is open before running.
	Set theDiagram = DiagramManager.ActiveDiagram
	If (theDiagram Is Nothing) Then
		MsgBox "Please open a diagram before running this macro.", vbExclamation
		Exit Sub
	End If

	' Process current model
	Set theModel = theDiagram.ActiveModel

	' Display dialog and process if Ok is pressed.
	StartDialog:
	If (Dialog(dlg) = -1) Then

		' Save user table selection option
		If dlg.ogTableSelection = 0 Then

			If (theModel.ActiveSubModel.SelectedObjects.Count = 0) Then
				MsgBox("You must first select tables.", vbExclamation)
				GoTo StartDialog ' Display the dialog again, to give user option of selecting all tables or exiting.
			End If

			tableSelection = SelectedOnly

		Else

			If (theModel.Entities.Count = 0) Then
				MsgBox("There are no tables in this model. Exiting.", vbInformation)
				Exit Sub
			End If

			tableSelection = AllTables

		End If


		optionSelected = ((dlg.ckInferPK = 1) Or (dlg.ckInferFKFromIndex = 1) Or (dlg.ckInferFKFromNames = 1) Or (dlg.ckInferDomains = 1))

		' Disable screen updating to improve performance
		If (optionSelected) Then
			DiagramManager.EnableScreenUpdateEx(False, True)
			disableScreenUpdate = True
		End If


		' Infer primary keys
		If (dlg.ckInferPK = 1) Then
			doInferPK(theModel, tableSelection)
		End If

		' Infer foreign keys from indexes
		If (dlg.ckInferFKFromIndex = 1) Then
			InferFKFromIndex(theModel, tableSelection)
		End If

		' Infer foreing keys from names
		If (dlg.ckInferFKFromNames = 1) Then
			InferFKFromNames(theModel, tableSelection)
		End If

		' Infer Domains
		If (dlg.ckInferDomains = 1) Then
			InferDomain(theDiagram, theModel)
		End If

		If (disableScreenUpdate) Then DiagramManager.EnableScreenUpdateEx(True, True)

		If (optionSelected) Then MsgBox("Infer Complete.", vbInformation)

	End If

	Exit Sub


' Error Handler
errHandler:
	' Reenable screen updating
	If (disableScreenUpdate) Then DiagramManager.EnableScreenUpdateEx(True, True)

	' Display Error message
	MsgBox(Err.Description, vbCritical)
End Sub

' This routine checks for ER/Studio errors and raises them as VB macro errors.
Function CheckOp()
	Dim errorCode As Integer

	Err.Clear

	errorCode = DiagramManager.GetLastErrorCode()
	If (errorCode <> 0) Then
		Err.Raise Number := vbObjectError + errorCode, _
			Source := "Macro error handler", _
			Description := DiagramManager.GetLastErrorString()
	End If
End Function

Function GetIndexOf(collection() As Integer, value As Integer) As Integer
	Dim nCount As Integer
	Dim I As Integer

	GetIndexOf = -1

	nCount = GetSize(collection)
	If (nCount = 0) Then
		Exit Function
	End If

	For I = LBound(collection) To nCount
		If (collection(I) = value) Then
			GetIndexOf = I
			Exit Function
		End If
	Next I

End Function

Function GetSize(collection As Variant) As Integer
	Dim upperBound As Integer

	Err.Clear
	On Error Resume Next
	upperBound = UBound(collection)
	If (Err.Number = 0) Then
		GetSize = upperBound
	Else
		Err.Clear
		GetSize = 0
	End If

End Function

Function GetAttribute(theEntity As Entity, nameValue As String) As AttributeObj
	Dim theAttribute As AttributeObj

	Set GetAttribute = Nothing

	Set theAttribute = theEntity.Attributes(nameValue)
	If (Not theAttribute Is Nothing) Then
		Set GetAttribute = theAttribute
		Exit Function
	End If

	For Each theAttribute In theEntity.Attributes
		If (StrComp(theAttribute.AttributeName, nameValue, vbUseCompareOption) = 0) Or _
			(StrComp(theAttribute.ColumnName, nameValue, vbUseCompareOption) = 0) Or _
			(StrComp(theAttribute.LogicalRoleName, nameValue, vbUseCompareOption) = 0) Or _
			(StrComp(theAttribute.RoleName, nameValue, vbUseCompareOption) = 0) Then
			Set GetAttribute = theAttribute
			Exit Function
		End If
	Next theAttribute

End Function

Function ExistRelation(theModel As Model, theParentEntity As Entity, theChildEntity As Entity) As Boolean
	Dim theRel As Relationship

	ExistRelation = False

	For Each theRel In theModel.Relationships
		If (theRel.ParentEntity.ID = theParentEntity.ID) And (theRel.ChildEntity.ID = theChildEntity.ID) Then
			ExistRelation = True
			Exit Function
		End If
	Next theRel
	
End Function

Function GetPKIndex(theEntity As Entity) As Index
	Dim theIndex As Index

	Set GetPKIndex = Nothing

	If (theEntity Is Nothing) Then
		Exit Function
	End If

	If (theEntity.Indexes Is Nothing) Then
		Exit Function
	ElseIf (theEntity.Indexes.Count = 0) Then
			Exit Function
	End If

	For Each theIndex In theEntity.Indexes
		If (theIndex.IsPK) Then
			Set GetPKIndex = theIndex
			Exit Function
		End If
	Next theIndex

End Function

Function HasPK(theEntity As Entity) As Boolean
	Dim theIndex As Index
	Dim theAttribute As AttributeObj

	' A table qualifies as having a "Primary Key" if there is either a) PK index declared in ER/Studio
	' Or one of the columns in the table is marked as a primary key column.
	
	HasPK = False

	For Each theIndex In theEntity.Indexes
		If (theIndex.IsPK) Then
			HasPK = True
			Exit Function
		End If
	Next theIndex

	For Each theAttribute In theEntity.Attributes
		If (theAttribute.PrimaryKey) Then
			HasPK = True
			Exit Function
		End If
	Next theAttribute

End Function

Function HasUniqueIndex(theEntity As Entity) As Boolean
	Dim theIndex As Index

	' A table is said to have unique index if there is a) an existing unique index or b) a Unique Constraint Index
	HasUniqueIndex = False

	For Each theIndex In theEntity.Indexes
		If (theIndex.Unique) Or (theIndex.IsUniqueConstraint) Then
			HasUniqueIndex = True
			Exit Function
		End If
	Next theIndex

End Function

Function HasFK(theEntity As Entity) As Boolean
	Dim theAttribute As AttributeObj

	HasFK = False

	For Each theAttribute In theEntity.Attributes
		If (theAttribute.ForeignKey) Then
			HasFK = True
			Exit Function
		End If
	Next theAttribute

End Function

' Infer Primary Keys
Function doInferPK(theModel As Model, ByVal tableSelection As ETableSelection)
	Dim theSubModel As SubModel
	Dim theEntity As Entity
	Dim so As SelectedObject

	Set theSubModel = theModel.ActiveSubModel

	Select Case tableSelection
		Case SelectedOnly
			For Each so In theSubModel.SelectedObjects
				If (so.Type = 1) Then
					Set theEntity = theModel.Entities(so.ID)
					InferTablePk(theModel, theEntity)
				End If
			Next so
		Case AllTables
			For Each theEntity In theModel.Entities
				InferTablePk(theModel, theEntity)
			Next theEntity
	End Select
End Function

Function InferTablePk(theModel As Model, theEntity As Entity)

	' Infer Primary Keys: If selected, infer primary keys from unique indexes on a table.
	' If more than one unique index exists on a table, choose the index with the fewest columns.

	Const MaxInteger As Integer = 32767

	Dim tmpIndex As Index
	Dim theIndex As Index
	Dim theIndexColumn As IndexColumn
	Dim theAttribute As AttributeObj
	Dim indexColumnCount As Integer

	If (theEntity.Indexes.Count = 0) Then
		Exit Function
	End If

	Set theIndex = Nothing
	indexColumnCount = MaxInteger
	For Each tmpIndex In theEntity.Indexes
		If ((tmpIndex.KeyType = "A") Or (tmpIndex.Unique)) Then
			If (tmpIndex.IndexColumns.Count < indexColumnCount) Then
				Set theIndex = tmpIndex
				indexColumnCount = theIndex.IndexColumns.Count
			End If
		End If
	Next tmpIndex

	If (Not theIndex Is Nothing) Then
		For Each theIndexColumn In theIndex.IndexColumns
			Set theAttribute = theEntity.Attributes(theIndexColumn.ColumnName)
			If (Not theAttribute Is Nothing) Then
				theAttribute.PrimaryKey = True
			End If
		Next theIndexColumn
	End If
End Function

Function InferFKFromIndex(theModel As Model, ByVal tableSelection As ETableSelection)

	' Infer Foreign Keys from Indexes: If selected, infer foreign keys from Indexes.
	' When inferring foreign keys from Indexes, ER/Studio DA looks For Indexes whose columns match the names, 
	' datatype properties, And column sequences of a primary key. If the �child� Index Is a primary key Index,
	' it must contain more columns than the �parent� primary key. In this Case, an Identifying Relationship Is created.
	Dim candidateParentTables() As Integer
	Dim candidateChildTables() As Integer

	' There must be at least 2 tables in the model to perform the operation, creating relations.
	If (theModel.Entities.Count < 2) Then
		Exit Function
	End If

	' Retrieve a list of candidate parent and child tables from the model based on user selection.
	GetCandidateInferFKIndexTables(theModel, candidateParentTables(), candidateChildTables(), tableSelection)

	InferFKFromChildPKIndex(theModel, candidateParentTables(), candidateChildTables())
	InferFKFromChildIndex(theModel, candidateParentTables(), candidateChildTables())

	Erase candidateParentTables
	Erase candidateChildTables
End Function

Function GetCandidateInferFKIndexTables(theModel As Model, candidateParentTables() As Integer, candidateChildTables() As Integer, tableSelection As ETableSelection)
	Dim theSubModel As SubModel
	Dim theEntity As Entity
	Dim so As SelectedObject

	Set theSubModel = theModel.ActiveSubModel

	' Select those tables that could be parent or child tables based on user selection
	Select Case tableSelection
		Case SelectedOnly
			For Each so In theSubModel.SelectedObjects
				If (so.Type = 1) Then
					Set theEntity = theModel.Entities(so.ID)
					GetCandidateInferIndexEntity(theEntity, candidateParentTables(), candidateChildTables())
				End If
			Next so
		Case AllTables
			For Each theEntity In theModel.Entities
				GetCandidateInferIndexEntity(theEntity, candidateParentTables(), candidateChildTables())
			Next theEntity
	End Select

End Function

Function GetCandidateInferIndexEntity(theEntity As Entity, candidateParentTables() As Integer, candidateChildTables() As Integer)

	' If table has PK then it could potentially be parent or child
	If HasPK(theEntity) Then
		AddNoDupToCollection(candidateParentTables(), theEntity.ID)
		AddNoDupToCollection(candidateChildTables(), theEntity.ID)
	End If

	' If table has unique index it's a potential child table 
	If HasUniqueIndex(theEntity) Then
		AddNoDupToCollection(candidateChildTables(), theEntity.ID)
	End If

End Function

Function AddNoDupToCollection(collection() As Integer, value As Integer)
	Dim nCount As Integer

	' Automatically grows the array and adds entry to the end of the list.

	nCount = GetSize(collection)
	If (nCount = 0) Then
		nCount = 1
	Else
		If (GetIndexOf(collection(), value) <> -1) Then
			Exit Function
		End If

		nCount = nCount + 1
	End If

	ReDim Preserve collection(nCount)
	collection(nCount) = value

End Function

Function InferFKFromChildPKIndex(theModel As Model, candidateParentTables() As Integer, candidateChildTables() As Integer)

	If (GetSize(candidateParentTables()) = 0) Or (GetSize(candidateChildTables()) = 0) Then
		Exit Function
	End If

	Dim I As Integer
	Dim J As Integer
	Dim theParentEntity As Entity
	Dim theChildEntity As Entity
	Dim theParentPKIndex As Index
	Dim theChildPKIndex As Index
	Dim theParentIndexCol As IndexColumn
	Dim theChildIndexCol As IndexColumn
	Dim theParentCol As AttributeObj
	Dim theChildCol As AttributeObj
	Dim indexSeq As Integer
	Dim theRel As Relationship

	For I = LBound(candidateParentTables) To UBound(candidateParentTables)

		Set theParentEntity = theModel.Entities(candidateParentTables(I))
		Set theParentPKIndex = GetPKIndex(theParentEntity)

		For J = LBound(candidateChildTables) To UBound(candidateChildTables)

			Set theChildEntity = theModel.Entities(candidateChildTables(J))

			If (theParentEntity.ID = theChildEntity.ID) Then
				GoTo ContinueJLoop
			End If

			Set theChildPKIndex = GetPKIndex(theChildEntity)
			If (theChildPKIndex Is Nothing) Then
				GoTo ContinueJLoop
			End If

			If (theChildPKIndex.IndexColumns.Count <= theParentPKIndex.IndexColumns.Count) Then
				GoTo ContinueJLoop
			End If

			For indexSeq = 1 To theParentPKIndex.IndexColumns.Count

				For Each theParentIndexCol In theParentPKIndex.IndexColumns

					If (theParentIndexCol.SequenceNo <> indexSeq) Then
						GoTo ContinueParentIndexSeqLoop
					End If

					For Each theChildIndexCol In theChildPKIndex.IndexColumns

						If (theChildIndexCol.SequenceNo <> indexSeq) Then
							GoTo ContinueChildIndexSeqLoop
						End If

						If (StrComp(theParentIndexCol.ColumnName, theChildIndexCol.ColumnName, vbUseCompareOption) = 0) Then

							Set theParentCol = GetAttribute(theParentEntity, theParentIndexCol.ColumnName)
							Set theChildCol = GetAttribute(theChildEntity, theChildIndexCol.ColumnName)

							If (theParentCol.Datatype = theChildCol.Datatype) And _
								(theParentCol.DataLength = theChildCol.DataLength) And _
								(theParentCol.DataScale = theChildCol.DataScale) Then

								If (Not ExistRelation(theModel, theParentEntity, theChildEntity)) Then
									Set theRel = theModel.Relationships.AddWithUnification(theParentEntity.ID, theChildEntity.ID, Identifying)
									CheckOp
								End If
							End If
						End If

						ContinueChildIndexSeqLoop:
					Next theChildIndexCol

					ContinueParentIndexSeqLoop:
				Next theParentIndexCol

			Next indexSeq

			ContinueJLoop:
		Next J
	Next I


End Function
Function InferFKFromChildIndex(theModel As Model, candidateParentTables() As Integer, candidateChildTables() As Integer)

	If (GetSize(candidateParentTables()) = 0) Or (GetSize(candidateChildTables()) = 0) Then
		Exit Function
	End If

	Dim I As Integer
	Dim J As Integer
	Dim theParentEntity As Entity
	Dim theChildEntity As Entity
	Dim theParentPKIndex As Index
	Dim theChildIndex As Index
	Dim theParentIndexCol As IndexColumn
	Dim theChildIndexCol As IndexColumn
	Dim theParentCol As AttributeObj
	Dim theChildCol As AttributeObj
	Dim indexSeq As Integer
	Dim theRel As Relationship

	For I = LBound(candidateParentTables) To UBound(candidateParentTables)

		Set theParentEntity = theModel.Entities(candidateParentTables(I))
		Set theParentPKIndex = GetPKIndex(theParentEntity)

		For J = LBound(candidateChildTables) To UBound(candidateChildTables)

			Set theChildEntity = theModel.Entities(candidateChildTables(J))

			If (theParentEntity.ID = theChildEntity.ID) Then
				GoTo ContinueJLoop
			End If

			For Each theChildIndex In theChildEntity.Indexes

				If (theChildIndex.IsPK) Or (theChildIndex.IndexColumns.Count <> theParentPKIndex.IndexColumns.Count) Then
					GoTo ContinueChildIndexLoop
				End If

				For indexSeq = 1 To theParentPKIndex.IndexColumns.Count

					For Each theParentIndexCol In theParentPKIndex.IndexColumns

						If (theParentIndexCol.SequenceNo <> indexSeq) Then
							GoTo ContinueParentIndexSeqLoop
						End If

						For Each theChildIndexCol In theChildIndex.IndexColumns

							If (theChildIndexCol.SequenceNo <> indexSeq) Then
								GoTo ContinueChildIndexSeqLoop
							End If

							If (StrComp(theParentIndexCol.ColumnName, theChildIndexCol.ColumnName, vbUseCompareOption) = 0) Then

								Set theParentCol = GetAttribute(theParentEntity, theParentIndexCol.ColumnName)
								Set theChildCol = GetAttribute(theChildEntity, theChildIndexCol.ColumnName)

								If (theParentCol.Datatype = theChildCol.Datatype) And _
									(theParentCol.DataLength = theChildCol.DataLength) And _
									(theParentCol.DataScale = theChildCol.DataScale) Then

									If (Not ExistRelation(theModel, theParentEntity, theChildEntity)) Then
										Set theRel = theModel.Relationships.AddWithUnification(theParentEntity.ID, theChildEntity.ID, Identifying)
										CheckOp
									End If
								End If

							End If


							ContinueChildIndexSeqLoop:
						Next theChildIndexCol

						ContinueParentIndexSeqLoop:
					Next theParentIndexCol
					
				Next indexSeq

				continueChildIndexLoop:
			Next theChildIndex

			continueJLoop:
		Next J

	Next I


End Function

Function InferFKFromNames(theModel As Model, tableSelection As ETableSelection)

	' Infer Foreign Keys from Names:
	' If selected and your database contains foreign keys, ER/Studio DA infers foreign keys from names. 
	' When inferring foreign keys from names, ER/Studio DA looks for columns matching the names and datatype
	' properties of a primary key. In this case, a non-identifying relationship is created. ER/Studio DA cannot
	' infer relationships where the child column has a role name, instead create a non-identifying relationship and
	' then designate the correct role-named column using the Edit Rolename function; right-click the relationship
	' and then click Edit Rolename.


	Dim candidateParentTables() As Integer
	Dim candidateChildTables() As Integer

	' There must be at least 2 tables in the model to perform the operation, creating relations.
	If (theModel.Entities.Count < 2) Then
		Exit Function
	End If

	' Retrieve a list of candidate parent and child tables from the model based on user selection.
	GetCandidateInferFKNameTables(theModel, candidateParentTables(), candidateChildTables(), tableSelection)

	InferFKFromFKColumnNames(theModel, candidateParentTables(), candidateChildTables())

	Erase candidateParentTables
	Erase candidateChildTables

End Function

Function GetCandidateInferFKNameTables(theModel As Model, candidateParentTables() As Integer, candidateChildTables() As Integer, ByVal tableSelection As ETableSelection)
	Dim theSubModel As SubModel
	Dim theEntity As Entity
	Dim so As SelectedObject

	Set theSubModel = theModel.ActiveSubModel

	' Select those tables that could be parent or child tables based on user selection
	Select Case tableSelection
		Case SelectedOnly
			For Each so In theSubModel.SelectedObjects
				If (so.Type = 1) Then
					Set theEntity = theModel.Entities(so.ID)
					GetCandidateInferNameEntity(theEntity, candidateParentTables(), candidateChildTables())
				End If
			Next so
		Case AllTables
			For Each theEntity In theModel.Entities
				GetCandidateInferNameEntity(theEntity, candidateParentTables(), candidateChildTables())
			Next theEntity
	End Select

End Function

Function GetCandidateInferNameEntity(theEntity As Entity, candidateParentTables() As Integer, candidateChildTables() As Integer)
	' If table has PK then it could potentially be parent or child
	If HasPK(theEntity) Then
		AddNoDupToCollection(candidateParentTables(), theEntity.ID)
	End If

	' If table has foreign key columns it's a potential child table
	If HasFK(theEntity) Then
		AddNoDupToCollection(candidateChildTables(), theEntity.ID)
	End If

End Function

Function InferFKFromFKColumnNames(theModel As Model, candidateParentTables() As Integer, candidateChildTables() As Integer)

	If (GetSize(candidateParentTables()) = 0) Or (GetSize(candidateChildTables()) = 0) Then
		Exit Function
	End If

	Dim I As Integer
	Dim J As Integer
	Dim theParentEntity As Entity
	Dim theChildEntity As Entity
	Dim theParentPKIndex As Index
	Dim theParentIndexCol As IndexColumn
	Dim theParentCol As AttributeObj
	Dim theChildCol As AttributeObj
	Dim theRel As Relationship
	Dim matchCount As Integer

	For I = LBound(candidateParentTables) To UBound(candidateParentTables)

		Set theParentEntity = theModel.Entities(candidateParentTables(I))
		Set theParentPKIndex = GetPKIndex(theParentEntity)

		For J = LBound(candidateChildTables) To UBound(candidateChildTables)

			Set theChildEntity = theModel.Entities(candidateChildTables(J))

			If (theParentEntity.ID = theChildEntity.ID) Then
				GoTo ContinueJLoop
			End If


			matchCount = 0
			For Each theParentIndexCol In theParentPKIndex.IndexColumns

				If (matchCount = theParentPKIndex.IndexColumns.Count) Then
					Exit For
				End If

				Set theParentCol = GetAttribute(theParentEntity, theParentIndexCol.ColumnName)
				Set theChildCol = GetAttribute(theChildEntity, theParentIndexCol.ColumnName)

				If (Not theChildCol Is Nothing) Then
					If (theChildCol.ForeignKey) And _
						(theParentCol.Datatype = theChildCol.Datatype) And _
						(theParentCol.DataLength = theChildCol.DataLength) And _
						(theParentCol.DataScale = theChildCol.DataScale) Then

						matchCount = matchCount + 1
					End If
				End If

			Next theParentIndexCol

			If (matchCount = theParentPKIndex.IndexColumns.Count) And (Not ExistRelation(theModel, theParentEntity, theChildEntity)) Then
				Set theRel = theModel.Relationships.AddWithUnification(theParentEntity.ID, theChildEntity.ID, NonIdentifying)
				CheckOp
			End If

		ContinueJLoop:
		Next J

	Next I

End Function
Function InferDomain(theDiagram As Diagram, theModel As Model)

	Const BinaryCompare = 0
	Const TextCompare = 1

	Dim theDictionary As Dictionary
	Dim theDomainFolder As DomainFolder
	Dim theDomain As Domain
	Dim theEntity As Entity
	Dim theAttribute As AttributeObj
	Dim counter As Long
	Dim nameOfDomain As String
	Dim MyDictionary As Object


	' A collection to store names for lookup. Domains collection does not implement case-insensitive search and this
	' is to avoid looping over domains to check for duplicates.
	Set MyDictionary = CreateObject("Scripting.Dictionary")
	If (MyDictionary Is Nothing) Then
		MsgBox("Cannot access [Scripting.Dictionary] object. Infer Domain will be skipped.", vbExclamation)
		Exit Function
	End If
	MyDictionary.CompareMode = vbTextCompare

	' Domains inferred are placed in the Local Dictionary
	Set theDictionary = theDiagram.Dictionary

	' Inferred domains are placed in their own folder
	Set theDomainFolder = theDictionary.DomainFolders( DOMAIN_FOLDER_NAME )
	If (theDomainFolder Is Nothing) Then
		Set theDomainFolder = theDictionary.DomainFolders.Add( DOMAIN_FOLDER_NAME, "Domains" )
		CheckOp
	End If


	' Store existing domain names
	For Each theDomain In theDictionary.Domains
		MyDictionary.Add(theDomain.Name, theDomain.ColumnName)
	Next theDomain


	For Each theEntity In theModel.Entities

		For Each theAttribute In theEntity.Attributes

			' Skip attributes that are bound to domains
			If (Not theDictionary.Domains(theAttribute.DomainId) Is Nothing) Then
				GoTo continueNextAttribute
			End If

			nameOfDomain = theAttribute.ColumnName

			Set theDomain = theDictionary.Domains(nameOfDomain)

			If (Not theDomain Is Nothing) Then

				If 	(theDomain.Datatype <> theAttribute.Datatype) Or _
					(theDomain.DataLength <> theAttribute.DataLength) Or _
					(theDomain.DataScale <> theAttribute.DataScale) Or _
					(theDomain.Nullable <> IsNullable(theAttribute)) Then

					' Duplicate Domains With an underscore And number suffix indicate that columns With the same Name
					' but different datatypes were found In the database.
					nameOfDomain = GetUnqiueDomainName(MyDictionary, theAttribute)
					AddDomain(nameOfDomain, theAttribute, theDictionary, MyDictionary)
					
				End If

			ElseIf ( Not MyDictionary.Exists(nameOfDomain) ) Then

				AddDomain(nameOfDomain, theAttribute, theDictionary, MyDictionary)

			End If
			
		continueNextAttribute:
		Next theAttribute

	Next theEntity

End Function
Function AddDomain(nameOfDomain As String, theAttribute As AttributeObj, theDictionary As Dictionary, MyDictionary As Object)
	Dim theDomain As Domain

	Set theDomain = theDictionary.Domains.AddEx( nameOfDomain, theAttribute.AttributeName, theAttribute.ColumnName, DOMAIN_FOLDER_NAME )
	CheckOp

	SetDomainPropertyUsingAttribute(theDomain, theAttribute)

	theAttribute.DomainId = theDomain.ID

	MyDictionary.Add(nameOfDomain, theAttribute.ColumnName)
End Function
Function IsNullable(theAttribute As AttributeObj) As Boolean
	IsNullable = (theAttribute.NullOption = "NULL")
End Function

Function SetDomainPropertyUsingAttribute(theDomain As Domain, theAttribute As AttributeObj)

	theDomain.Datatype = theAttribute.Datatype
	theDomain.DataLength = theAttribute.DataLength
	theDomain.DataScale = theAttribute.DataScale
	theDomain.Nullable = IsNullable(theAttribute)

End Function
Function GetUnqiueDomainName(MyDictionary As Object, theAttribute As AttributeObj) As String
	Dim counter As Long
	Dim nameOfDomain As String
	Dim columnName As String

	GetUnqiueDomainName = ""

	columnName = theAttribute.ColumnName

	For counter = 1 To MAX_LONG

		nameOfDomain = columnName & "_" & CStr(counter)

		If (Not MyDictionary.Exists(nameOfDomain)) Then

			GetUnqiueDomainName = nameOfDomain
			Exit Function

		End If

	Next counter

End Function
Function DomainExists(theDictionary As Dictionary, domainName As String, Optional argDomain As Domain = Nothing) As Boolean
	Dim theDomain As Domain

	For Each theDomain In theDictionary.Domains
		DomainExists = (StrComp(theDomain.Name, domainName, vbTextCompare) = 0)
		If DomainExists Then
			Set argDomain = theDomain
			Exit Function
		End If
	Next theDomain
End Function
