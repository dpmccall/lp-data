'MACRO TITLE: DOMAIN BINDINGS 
'This macro brings up a dialog box that shows all attributes bounded to 
'a selected domain.  It also allows the user to unbind specific attributes
'from the selected domain.  The dialog box contains two list boxes and two buttons.  
'The first list box, "Domains", shows all the domains in the diagram. The second list 
'box, "Bounded Attributes", shows all the attributes bounded to the selected 
'domain in the "Domains" list box. There is also an "OK" button and an "UnBind" button.
'The user can press the "UnBind" button to unbind selected attributes in the "Bounded
'Attributes" list box from the selected domain in the "Domains" list box.
'----------------------------------------------------------------------------------------


'These are global variables.  They are delcared here.

	'This array is used to populate the "Bounded Attributes"
	'list box in the dialog box.

	Dim AttributeArray() As String

	'This array is used to populate the "Domains" list box
	'in the dialog box.
	 
	Dim DomainArray() As String

	'This integer is used to keep track of the current selection
	'in the "Bounded Attributes" list box.

	Dim CurAttrIndex As Integer
'SetupAttributeArray.
'This function will initialize the attribute array with the
'names of all attributes bounded to the selected domain given
'by the DomainIndex parameter.

Function SetupAttributeArray(DomainIndex)
	Dim MyModel As Model
	Dim MyDiagram As Diagram
	Dim MyDictionary As Dictionary
	Dim MyDomain As Domain
	Dim MyAttribute As AttributeObj
	Dim MyEntity As Entity
	
	Dim i As Integer
	Dim DomainName As String
	Dim Logical As Boolean
	Dim DomainID As Integer
	Dim ID As Integer
	Dim EntityName As String
	Dim AttributeName As String
	
	'Get the currently active diagram.
		
	Set MyDiagram = DiagramManager.ActiveDiagram

	'Get the data dictionary

	Set MyDictionary = MyDiagram.Dictionary
	
	'Set the array size to be big enough for
	'the 0th index.  This is the initial size.

	ReDim AttributeArray(0) As String
			
	i = 0
	
	'Get the domain name from the given index.
			
	DomainName = DomainArray(DomainIndex)
	
	'Get the domain object with this domain name.

	Set MyDomain = MyDictionary.Domains.Item(DomainName)
	
	'Get the Id of the domain.

	ID = MyDomain.ID
	
	'Now we iterate through all models, all entities,
	'and all attributes.  We check if each attribute's
	'domain id matches the selected domain id.  If it
	'does, then we add the attribute to the attribute
	'array (this array will be used to update the "Bounded
	'Attributes" list box).

	For Each MyModel In MyDiagram.Models

		'See whether the model is logical or not.
		'We need to know this so that we can get
		'the correct names (entity and attribute names
		'in the logical model and table and column names
		'in a physical model).

		Logical = MyModel.Logical
		
		'Iterate through all entities in the model.
				
		For Each MyEntity In MyModel.Entities
			
			'Iterate through all attributes in the entity.
				
			For Each MyAttribute In MyEntity.Attributes
	
				If Not(MyAttribute.ForeignKey) Then
					'Get the domain id the attribute is bounded
					'to (if any).
	
					DomainID = MyAttribute.DomainId
					
					'If the domain id matches the selected domain id,
					'then get the attribute name and add it to the
					'attribute array.
							
					If DomainID = ID Then
		
						'If the model is logical, then we get the
						'entity name and attribute name.  If the
						'model is physical, we get the table name
						'and the column name.
	
						If Logical = True Then
							EntityName = MyEntity.EntityName
							AttributeName = MyAttribute.LogicalRoleName
						Else
							EntityName = MyEntity.TableName							
							AttributeName = MyAttribute.RoleName
						End If
	
						'Now we create the full label.  The format 
						'is "Model Name -- Entity Name -- Attribute Name".
	
						Label = MyModel.Name + "  --  " + EntityName + "  --  " + AttributeName
								
						'Add this label to the attribute array.  This array
						'will be used to populate the "Bounded Attributes"
						'list box.
	
						AttributeArray(i) = Label
						
						'Increment the array size by 1.
								
						i = i + 1
	
						'Make the attribute array big enough to fit the
						'new array size (while preserving all the previous
						'entries set thus far).
	
						ReDim Preserve AttributeArray(i) As String
						
					End If
				End If
			Next MyAttribute
					
		Next MyEntity
				
	Next MyModel
	
End Function
	
	
Sub Main
	Dim MyDiagram As Diagram
	Dim MyDictionary As Dictionary
	Dim MyDomain As Domain
	Dim i As Integer
	Dim DomainsExist As Boolean
	Dim Count As Integer


	CurAttrIndex = -1
	DomainsExist = False
	i = 0
	
	'Get the currently active diagram.

	Set MyDiagram = DiagramManager.ActiveDiagram

	'Get the data dictionary.

	Set MyDictionary = MyDiagram.Dictionary
	
	'Get the number of domains in the data dictionary.

	Count = MyDictionary.Domains.Count
	
	'Resize the domains array to be big enough to
	'fit all the domain names in the data dictionary.

	ReDim DomainArray(Count) As String
	
	'Now iterate through all the domains in the data
	'dictionary, get each domain name and put it in the
	'domains array.

	For Each MyDomain In MyDictionary.Domains

		DomainsExist = True	
		DomainArray(i) = MyDomain.Name
		
		i = i + 1
		
	Next MyDomain

	'If there are valid domains in the data dictionary,
	'then we will initialize the attributes array
	'with all the attributes bounded to the first domain
	'in the "Domains" list box.   

	If DomainsExist = True Then
		CurAttrIndex = 0
		SetupAttributeArray(0)
	End If
	
	'Now create the dialog box.  The dialog box will use the
	'domains array to populate the "Domains" list box, and
	'it will use the attributes array to populate the "Bounded
	'Attributes" list box.

	Begin Dialog UserDialog 940,315,"Domain Bindings",.MyDialogFunction ' %GRID:10,7,1,1
		ListBox 20,42,230,203,DomainArray(),.DomainBox
		ListBox 300,42,620,203,AttributeArray(),.AttributeBox
		PushButton 530,266,110,28,"UnBind",.UnBind
		Text 30,21,90,14,"Domains",.Text1
		Text 310,21,400,14,"Bounded Attributes (Format: Model -- Entity -- Attribute)",.Text2
		OKButton 290,266,100,28
	End Dialog

	Dim dlg As UserDialog
	
	Dialog dlg
		
End Sub


Rem See DialogFunc help topic for more information.

'MyDialogFunction
'This function was created by the Sax Basic dialog editor.
'It provides a way to handle certain events that happen in the dialog box.  
'Please read the Sax Basic help (DialogFunc help topic) for more details
'about its overall use.  This function has been modified to specifically
'handle selections done in the "Domains" list box and in the "Bounded
'Attributes" list box.  It has also been modified to handle the pressing
'of the "UnBind" button. 

Private Function MyDialogFunction(DlgItem$, Action%, SuppValue&) As Boolean

	Dim MyModel As Model
	Dim MyDiagram As Diagram
	Dim MyAttribute As AttributeObj
	Dim MyEntity As Entity

	Dim EntityName As String
	Dim AttributeName As String
	Dim Idx As Integer
	Dim PrvIdx As Integer
	Dim Length As Integer
	Dim Label As String
	
	'Get the currently active diagram.

	Set MyDiagram = DiagramManager.ActiveDiagram
	
	Select Case Action%
	Case 1 ' Dialog box initialization.
		
	Case 2 ' Value changing or button pressed.
	
		If DlgItem = "DomainBox" Then

			'If the user is selecting a different domain in 
			'the "Domains" list box, then we want re-initialize 
			'the "Bounded Attributes" list box to contain all the attributes 
			'bounded to the new selection in the "Domains" list box.
			'SuppValue contains the index of the new domain selection
			'in the "Domains" list box.

			CurAttrIndex = -1
			SetupAttributeArray(SuppValue)			
			
			DlgListBoxArray("AttributeBox", AttributeArray())
		
		ElseIf DlgItem = "AttributeBox" Then

			'If the user is selecting a different attribute in
			'the "Bounded Attributes" list box, then we want to
			'save the new selection.  

			CurAttrIndex = SuppValue
			
		ElseIf DlgItem = "UnBind" Then

			'If the user presses the "UnBind" button, then we
			'want to unbind the currently selected attribute in
			'the "Bounded Attributes" list box from the currently
			'selected domain in the "Domains" list box.
			
			'First, check that there is a valid attribute selected.
						
			If CurAttrIndex <> -1 Then

				'Get the actual label which corresponds to the
				'currently selected attribute index.

				Label = AttributeArray(CurAttrIndex)
			
				'See whether the currently selected attribute is
				'already unbounded.  If it is, then do nothing.
		
				Idx = InStr(1, Label, "(*Unbound*)")
	
				If Idx = 0 Then

					'The attribute is validly bounded, so
					'we must get the model, entity, and attribute
					'names from the label so that we can get each
					'object.

					'First, get the model name.

					Idx = InStr(1, Label, "  --  ")
					ModelName = Left(Label, Idx - 1)
	
					'Second, get the entity name.

					PrvIdx = Idx + 6
					Idx = InStr(PrvIdx, Label, "  --  ")
					Length = Idx - PrvIdx
					EntityName = Mid(Label, PrvIdx, Length)
			
					'Last, get the attribute name.

					Length = Len(Label)
					Length = Length - Idx - 5
					AttributeName = Right(Label, Length)
		
					'Now, get the model object from the model name.

					Set MyModel = MyDiagram.Models(ModelName)

					'Get the entity from the entity name.

					Set MyEntity = MyModel.Entities.Item(EntityName)

					'Get the attribute from the attribute name.

					Set MyAttribute = MyEntity.Attributes.Item(AttributeName)
			
					'Unbind the attribute from the selected domain by setting
					'the attribute's domain id to 0.

					MyAttribute.DomainId = 0
	
					'Append the word "Unbounded" to the label.

					Label = Label + "   (*Unbound*)"
				
					'Update the label in the attributes array for
					'the current attribute selected.

					AttributeArray(CurAttrIndex) = Label
			
					'Now update the "Bounded Attributes" list box with
					'the updated attributes array.

					DlgListBoxArray("AttributeBox", AttributeArray())
				
				End If
		
			Else
			
				'If there is no valid attribute selected in the "Bounded
				'Attributes" list box then put up an error message.

				MsgBox("Please select a bounded attribute to unbind.")
				
			End If

			'The function must return true when handling the pressing
			'of the "UnBind" button or else the dialog box will close.
						
			MyDialogFunction = True
						
		End If		
				
	Case 3 ' TextBox or ComboBox text changed
	Case 4 ' Focus changed		
	Case 5 ' Idle
		Rem MyDialogFunction = True ' Continue getting idle actions
	Case 6 ' Function key

	End Select

End Function
