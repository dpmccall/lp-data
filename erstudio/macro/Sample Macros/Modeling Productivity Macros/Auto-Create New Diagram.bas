'MACRO TITLE: AUTO-CREATE A NEW DIAGRAM
'This macro creates a new diagram.  The new diagram will
'contain an IBM DB/2 physical model and six entities in the
'physical model.
'----------------------------------------------------------------

Sub Main
	Dim MyDiagram As Diagram
	Dim MyModel As Model
	Dim MyEntity As Entity

	' Create a new diagram.
	
	Set MyDiagram = DiagramManager.NewDiagram
	
	' A new diagram will automatically have a logical.
	' model - which is also the currently, active model.
	
	Set MyModel = MyDiagram.ActiveModel

	' Add 6 entities to the model.
	
	MyModel.Entities.Add(100, 100)
	MyModel.Entities.Add(300, 100)
	MyModel.Entities.Add(500, 100)
	MyModel.Entities.Add(100, 400)
	MyModel.Entities.Add(300, 400)
	MyModel.Entities.Add(500, 400)
	
	'Create a physical model with the IBM DB/2 database
	'platform type.
	
	MyDiagram.Models.Add("MyPhysicalModel", 32)
	
	'Get the physical model.
	
	Set MyModel = MyDiagram.Models("MyPhysicalModel")
	
	'Get the entity, 'Entity1.'
	
	Set MyEntity = MyModel.Entities.Item("Entity1")

	'Add a primary key attribute to 'Entity1.'
	
	MyEntity.Attributes.Add("PKAttribute1", True)

	'Now add relationships to connect all the six entities
	'together.
	
	MyModel.Relationships.Add("Entity1", "Entity5", 0)
	MyModel.Relationships.Add("Entity5", "Entity3", 0)
	MyModel.Relationships.Add("Entity3", "Entity6", 0)
	MyModel.Relationships.Add("Entity6", "Entity2", 0)
	MyModel.Relationships.Add("Entity2", "Entity4", 0)

End Sub
