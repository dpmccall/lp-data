' Macro Title: Create Index on FK Columns.bas
'
' This macro creates index on foreign key columns, provided such an index does not exist.
'
' Version: 1.0
'*****************************************************************************************

Option Explicit
Option Base 1

Sub Main
	Dim theDiagram As Diagram
	Dim theModel As Model
	Dim theSubModel As SubModel
	Dim theEntity As Entity
	Dim so As SelectedObject

	Debug.Clear

	Begin Dialog UserDialog 340,133,"Create Index on Foreign Key Columns" ' %GRID:10,7,1,1
		GroupBox 20,14,300,70,"Table Selection",.Group1
		OptionGroup .TableSelection
			OptionButton 40,35,130,14,"Selected Tables",.rbSelectTables
			OptionButton 40,56,130,14,"All Tables",.rbAllTables
		OKButton 60,105,90,21
		CancelButton 190,105,80,21
	End Dialog
	Dim dlg As UserDialog


	On Error GoTo errHandler

	dlg.TableSelection = 0

	If (Dialog(dlg) = -1) Then

		Set theDiagram = DiagramManager.ActiveDiagram
		If (theDiagram Is Nothing) Then
			Exit Sub
		End If

		Set theModel = theDiagram.ActiveModel
		Set theSubModel = theModel.ActiveSubModel

		DiagramManager.EnableScreenUpdateEx(False, True)


		Select Case dlg.TableSelection
			Case 0
				For Each so In theSubModel.SelectedObjects
					If (so.Type = 1) Then
						Set theEntity = theModel.Entities(so.ID)
						ProcessTable(theModel, theEntity)
					End If
				Next so
			Case 1
				For Each theEntity In theModel.Entities
					ProcessTable(theModel, theEntity)
				Next theEntity
		End Select

		DiagramManager.EnableScreenUpdateEx(True, True)

		MsgBox "Index Creation Complete!", vbInformation

	End If

	Exit Sub

errHandler:
	DiagramManager.EnableScreenUpdateEx(True, True)
	MsgBox Err.Description, vbCritical
End Sub
Function ProcessTable(theModel As Model, theEntity As Entity)
	Dim colNames() As String
	Dim indexColNames() As String
	Dim I As Integer
	Dim theAttribute As AttributeObj
	Dim foreignKeyExists As Boolean

	If (theEntity.Attributes.Count = 0) Then
		Exit Function
	End If

	foreignKeyExists = False

	For Each theAttribute In theEntity.Attributes

		If (theAttribute.ForeignKey) Then


			ReDim colNames(1)
			ReDim indexColNames(1)

			indexColNames(1) = theAttribute.ColumnName ' Index Column Objects stored column names
			                                           ' But index creation requires attribute names if
			                                           ' index is in logical model.

			If theModel.Logical Then
				colNames(1) = theAttribute.AttributeName
			Else
				colNames(1) = theAttribute.ColumnName
			End If

			If (Not ExistsIndex(theEntity, indexColNames)) Then
	
				CreateIndex(theEntity, colNames())
			End If

		End If

	Next theAttribute


	Erase colNames
	Erase indexColNames

End Function
Function ExistsIndex(theEntity As Entity, colNames() As String) As Boolean
	Dim theIndex As Index
	Dim theIndexColumn As IndexColumn
	Dim I As Integer
	Dim nMatch As Integer


	ExistsIndex = False

	For Each theIndex In theEntity.Indexes

		nMatch = 0
		For Each theIndexColumn In theIndex.IndexColumns

			For I = LBound(colNames) To UBound(colNames)

				If (colNames(I) = theIndexColumn.ColumnName) Then
					nMatch = nMatch + 1
					Exit For
				End If

			Next I

		Next theIndexColumn

		If (nMatch = UBound(colNames)) And (nMatch = theIndex.IndexColumns.Count) Then

			ExistsIndex = True
			Exit Function

		End If

	Next theIndex

End Function
Function CreateIndex(theEntity As Entity, colNames() As String) As Index
	Dim I As Integer
	Dim theIndex As Index
	Dim indexName As String
	Dim tmpIndex As Index

	indexName = "ndx_" & CStr(Now)

	If (UBound(colNames) > 1) Then

		Set theIndex = theEntity.Indexes.Add(colNames(LBound(colNames)), indexName)
		CheckOp
		indexName = ""
		For I = LBound(colNames) + 1 To UBound(colNames)
			theIndex.IndexColumns.Add colNames(I)
			indexName = colNames(I) & "_"
		Next I
		indexName = Left(indexName, Len(indexName) - 1)
	Else

		Set theIndex = theEntity.Indexes.Add(colNames(LBound(colNames)), indexName)
		CheckOp
		indexName = colNames(LBound(colNames))

	End If

	indexName = "ndx_" & indexName
	Set tmpIndex = theEntity.Indexes(indexName)
	If (tmpIndex Is Nothing) Then
		theIndex.Name = indexName
	Else
		I = theEntity.Indexes.Count
		Do
			indexName = indexName & CStr(I)
			Set tmpIndex = theEntity.Indexes(indexName)
			If (tmpIndex Is Nothing) Then
				theIndex.Name = indexName
				Exit Do
			Else
				I = I + 1
			End If
		Loop While True
	End If

	Set CreateIndex = theIndex

End Function
Function CheckOp()
	Dim errorCode As Integer

	Err.Clear

	errorCode = DiagramManager.GetLastErrorCode()
	If (errorCode <> 0) Then
		Err.Raise Number := vbObjectError + errorCode, _
			Source := "Macro error handler", _
			Description := DiagramManager.GetLastErrorString()
	End If
End Function
