' Macro: Generate Constraints and Indexes.bas
'
' Purpose: This macro can be used to generate constraints and indexes.
'
' Date: 9/19/2014
'
'============================================================================================
Enum EObjSelection
	EAll
	ESelected
End Enum

Type userSelection
	objSelection As EObjSelection
	GenPK As Boolean
	GenFK As Boolean
	GenIndex As Boolean
	GenTableConstraint As Boolean
	GenColumnConstraint As Boolean
	GenOwner As Boolean
	QuoteNames As Boolean
End Type

Dim openDelim As String
Dim closeDelim As String
Dim	StmtTerm As String

Option Explicit
Sub Main()



	Begin Dialog UserDialog 710,413,"Generate Constraints and Indexes",.DialogFunc ' %GRID:10,7,1,1
		Text 10,14,110,14,"Output Directory:",.txtDirectory
		TextBox 130,14,430,21,.edDir
		PushButton 580,14,120,21,"Choose Directory",.btnChooseDir
		GroupBox 10,56,550,70,"Object Selection",.GroupBox1
		OptionGroup .grpObjectSelection
			OptionButton 40,77,150,14,"All Objects",.opAll
			OptionButton 40,98,190,14,"Selected Objects",.opSelected
		GroupBox 10,140,680,133,"Generation Options",.GroupBox2
		CheckBox 30,161,190,14,"Primary Key",.ckPK
		CheckBox 30,182,150,14,"Foreign Key",.ckFK
		CheckBox 30,203,120,14,"Indexes",.ckIndexes
		CheckBox 30,224,250,14,"Table Level Constraints",.ckTableConstraint
		CheckBox 30,245,270,14,"Column Level Constraints",.ckColumnConstraint
		GroupBox 10,287,540,70,"General Options",.GroupBox3
		CheckBox 30,308,200,14,"Generate Owner",.ckOwner
		CheckBox 30,329,300,14,"Enclose names in quotes/brackets",.ckQuote
		PushButton 250,378,90,21,"Ok",.btnOk
		CancelButton 360,378,90,21
	End Dialog
    Dim dlg As UserDialog

    Debug.Clear
    If (Dialog(dlg) = -1) Then End

End Sub
Private Function DialogFunc(DlgItem$, Action%, SuppValue&) As Boolean
        Dim sDir As String

        Select Case Action%
        Case 1 ' Dialog box initialization

			sDir = GetUserMyDocumentDirectory()
			DlgText "edDir", sDir
			DlgValue "grpObjectSelection", 0
			DlgValue "ckPK", 1
			DlgValue "ckFK", 1
			DlgValue "ckIndexes", 1
			DlgValue "ckTableConstraint", 1
			DlgValue "ckColumnConstraint", 1
			DlgValue "ckOwner", 1
			DlgValue "ckQuote", 1

        Case 2 ' Value changing or button pressed
                Select Case DlgItem$
                        Case "btnChooseDir"

							sDir = ShellBrowseForFolder()
							If (sDir <> "") Then DlgText("edDir", sDir)

                            DialogFunc = True
                            Exit Function

                        Case "btnOk"

                            sDir = DlgText("edDir")
                            If Len(sDir) = 0 Then
                                    MsgBox "You must specify a directory."
                                    DialogFunc = True
                                    Exit Function
                            ElseIf Not DirExists(sDir) Then
                                    MsgBox "Specified directory does not exist."
                                    DialogFunc = True
                                    Exit Function
                            End If

							Dim theUserSelection As userSelection
							With theUserSelection
								.objSelection = IIf(DlgValue("grpObjectSelection") = 0, EAll, ESelected)
								.GenPK = CBool(DlgValue("ckPK"))
								.GenFK = CBool(DlgValue("ckFK"))
								.GenIndex = CBool(DlgValue("ckIndexes"))
								.GenTableConstraint = CBool(DlgValue("ckTableConstraint"))
								.GenColumnConstraint = CBool(DlgValue("ckColumnConstraint"))
								.GenOwner = CBool(DlgValue("ckOwner"))
								.QuoteNames =  CBool(DlgValue("ckQuote"))

								If .QuoteNames Then
									If (InStr(UCase(DiagramManager.ActiveDiagram.ActiveModel.DatabasePlatform), "MICROSOFT") > 0) Then
										openDelim = "["
										closeDelim = "]"
									Else
										openDelim = """"
										closeDelim = """"
									End If
								End If

								If (InStr(UCase(DiagramManager.ActiveDiagram.ActiveModel.DatabasePlatform), "MICROSOFT") > 0) Then
									StmtTerm = "GO"
								Else
									StmtTerm = ";"
								End If

							End With

                            doGenerate(sDir, theUserSelection)
                            DialogFunc = True
                            Exit Function
                End Select
        End Select
End Function
Function DirExists(ByVal DName As String) As Boolean
        Dim sDummy As String

        On Error Resume Next

        If Right(DName, 1) <> "\" Then
                DName = DName & "\"
        End If
        sDummy = Dir$(DName & "*.*", vbDirectory)
        DirExists = Not (sDummy = "")
End Function
Function CheckOp() As String
	Dim errorCode As Integer

	CheckOp = ""
	errorCode = DiagramManager.GetLastErrorCode()
	If (errorCode <> 0) Then
		CheckOp = DiagramManager.GetLastErrorString()
	End If
End Function
Function GetUserMyDocumentDirectory() As String
	' http://msdn.microsoft.com/en-us/library/0ea7b5xe.aspx
	Dim WshShell As Object
	Set WshShell = CreateObject("WScript.Shell")
	GetUserMyDocumentDirectory = WshShell.SpecialFolders("MyDocuments")
End Function
Function ShellBrowseForFolder() As String

	Const BIF_RETURNONLYFSDIRS As Long = &H1

    Dim objShell As Object
    Dim objFolder As Object

    Set objShell = CreateObject("shell.application")
    Set objFolder = objShell.BrowseForFolder(0, "SQL File Folder",  BIF_RETURNONLYFSDIRS, "")
    If (Not objFolder Is Nothing) Then
    	ShellBrowseForFolder = objFolder.Items.Item.Path
    Else
    	ShellBrowseForFolder = ""
    End If
End Function
Function doGenerate(ByVal sDir As String, theUserSelection As userSelection)
	Dim theDiagram As Diagram
	Dim theModel As Model
	Dim theSubModel As SubModel
	Dim so As SelectedObject
	Dim theEntity As Entity
	Dim SQLFilePath As String
	Dim sqlFileHandle As Long

	On Error GoTo errorHandler


	Set theDiagram = DiagramManager.ActiveDiagram
	Set theModel = theDiagram.ActiveModel
	Set theSubModel = theModel.ActiveSubModel

	' Open SQL File
	SQLFilePath = sDir
	If (Right(SQLFilePath, 1) <> "\") Then SQLFilePath = SQLFilePath + "\"
	SQLFilePath = SQLFilePath + GetSQLFileName(theDiagram, theModel)
	sqlFileHandle = FreeFile
	Open SQLFilePath For Output As #sqlFileHandle


	Select Case theUserSelection.objSelection
		Case EAll
			For Each theEntity In theModel.Entities
				If theUserSelection.GenPK Then doGenPK(sqlFileHandle, theEntity, theUserSelection)
				If theUserSelection.GenFK Then doGenFK(sqlFileHandle, theEntity, theUserSelection)
				If theUserSelection.GenIndex Then doGenIndex(sqlFileHandle, theEntity, theUserSelection)
				If theUserSelection.GenTableConstraint Then doGenTableConstraint(sqlFileHandle, theEntity, theUserSelection)
				If theUserSelection.GenColumnConstraint Then doGenColumnConstraint(sqlFileHandle, theEntity, theUserSelection)
			Next theEntity
		Case ESelected
			For Each so In theSubModel.SelectedObjects
				If (so.Type = 1) Then
					Set theEntity = theModel.Entities(so.ID)
					If theUserSelection.GenPK Then doGenPK(sqlFileHandle, theEntity, theUserSelection)
					If theUserSelection.GenFK Then doGenFK(sqlFileHandle, theEntity, theUserSelection)
					If theUserSelection.GenIndex Then doGenIndex(sqlFileHandle, theEntity, theUserSelection)
					If theUserSelection.GenTableConstraint Then doGenTableConstraint(sqlFileHandle, theEntity, theUserSelection)
					If theUserSelection.GenColumnConstraint Then doGenColumnConstraint(sqlFileHandle, theEntity, theUserSelection)
				End If
			Next so
	End Select

	Close #sqlFileHandle

	MsgBox("Operation completed." & vbCrLf & "File: " & SQLFilePath, vbInformation)
	Exit Function

errorHandler:
	Reset
	MsgBox(Err.Description, vbCritical)
End Function
Function doGenPK(ByVal sqlFileHandle As Long, theEntity As Entity, theUserSelection As userSelection)
	Dim I As Integer
	Dim theAttribute As AttributeObj
	Dim columnNames As String
	Dim sqlStmt As String
	Dim pkConstraintName As String
	Dim theIndex As Index


	pkConstraintName = ""
	For Each theIndex In theEntity.Indexes
		If (theIndex.IsPK) Then
			pkConstraintName = theIndex.Name
			Exit For
		End If
	Next theIndex
	If pkConstraintName = "" Then Exit Function

	columnNames = ""
	For I = 1 To theEntity.Attributes.Count
		For Each theAttribute In theEntity.Attributes
			If ((theAttribute.SequenceNumber = I) And (theAttribute.PrimaryKey)) Then
				columnNames = columnNames + openDelim + theAttribute.ColumnName + closeDelim + ","
				Exit For
			End If
		Next theAttribute
	Next I

	If columnNames <> "" Then
		columnNames = Mid(columnNames, 1, Len(columnNames) - 1)
		sqlStmt = "ALTER TABLE " & _
			IIf(theUserSelection.GenOwner And Len(theEntity.Owner) > 0, openDelim & theEntity.Owner & closeDelim & ".", "") & _
			openDelim & theEntity.TableName & closeDelim & _
			" ADD CONSTRAINT " & openDelim & pkConstraintName & closeDelim & " PRIMARY KEY (" & columnNames & ")" & vbCrLf & StmtTerm
		Print #sqlFileHandle, sqlStmt
	End If
End Function
Function doGenFK(ByVal sqlFileHandle As Long, theEntity As Entity, theUserSelection As userSelection)
	Dim theRelationship As Relationship
	Dim sqlStmt As String
	Dim parentColumnNames As String
	Dim childColumnNames As String
	Dim I As Integer
	Dim theFKColPair As FKColumnPair

	If (theEntity.ChildRelationships.Count = 0) Then Exit Function

	For Each theRelationship In theEntity.ChildRelationships
		If (theRelationship.FKColumnPairs.Count = 0) Then GoTo continueLoop

		parentColumnNames = ""
		childColumnNames = ""
		For I = 1 To theRelationship.FKColumnPairs.Count
			For Each theFKColPair In theRelationship.FKColumnPairs
				If (theFKColPair.SequenceNo = I) Then
					parentColumnNames = parentColumnNames + openDelim + theFKColPair.ParentAttribute.ColumnName + closeDelim + ","
					childColumnNames = childColumnNames + openDelim + theFKColPair.ChildAttribute.ColumnName + closeDelim + ","
					Exit For
				End If
			Next theFKColPair
		Next I
		parentColumnNames = Mid(parentColumnNames, 1, Len(parentColumnNames) - 1)
		childColumnNames = Mid(childColumnNames, 1, Len(childColumnNames) - 1)


		sqlStmt = "ALTER TABLE " &  _
			IIf(theUserSelection.GenOwner And Len(theEntity.Owner) > 0, openDelim & theEntity.Owner & closeDelim & ".", "") & _
			openDelim & theEntity.TableName & closeDelim & _
			" ADD CONSTRAINT " & openDelim & theRelationship.Name & closeDelim & " Foreign KEY (" & childColumnNames & ")" & _
			" REFERENCES " & openDelim & theRelationship.ParentEntity.TableName & closeDelim & "(" & parentColumnNames & ")" &  vbCrLf & StmtTerm
		Print #sqlFileHandle, sqlStmt

		continueLoop:
	Next theRelationship
End Function
Function doGenIndex(ByVal sqlFileHandle As Long, theEntity As Entity, theUserSelection As userSelection)
	Dim theIndex As Index
	Dim theIndexColumn As IndexColumn
	Dim sqlStmt As String
	Dim I As Integer
	Dim columnNames As String

	If (theEntity.Indexes.Count = 0) Then Exit Function

	For Each theIndex In theEntity.Indexes
		sqlStmt = "CREATE " & IIf(theIndex.Unique, "UNIQUE " , "") & "INDEX ON " & _
			IIf(theUserSelection.GenOwner And Len(theIndex.Owner) > 0, openDelim & theIndex.Owner & closeDelim & ".", "") & _
			IIf(theUserSelection.GenOwner And Len(theEntity.Owner) > 0, openDelim & theEntity.Owner & closeDelim & ".", "") & _
			openDelim & theEntity.TableName & closeDelim & "("

		columnNames = ""
		For I = 1 To theIndex.IndexColumns.Count
			For Each theIndexColumn In theIndex.IndexColumns
				If (theIndexColumn.SequenceNo = I) Then
					columnNames = columnNames + openDelim & theIndexColumn.ColumnName + closeDelim + ","
					Exit For
				End If
			Next theIndexColumn
		Next I
		columnNames = Mid(columnNames, 1, Len(columnNames) - 1)

		sqlStmt = sqlStmt + columnNames + ")" + vbCrLf + StmtTerm
		Print #sqlFileHandle, sqlStmt

	Next theIndex
End Function
Function doGenTableConstraint(ByVal sqlFileHandle As Long, theEntity As Entity, theUserSelection As userSelection)
	Dim theConstraint As TableCheckConstraint
	Dim sqlStmt As String

	If (theEntity.TableCheckConstraints.Count = 0) Then Exit Function

	For Each theConstraint In theEntity.TableCheckConstraints
		sqlStmt = "ALTER TABLE " & _
			IIf(theUserSelection.GenOwner And Len(theEntity.Owner) > 0, openDelim & theEntity.Owner & closeDelim & ".", "") & _
			openDelim & theEntity.TableName & closeDelim & " ADD CONSTRAINT " & _
			openDelim & theConstraint.ConstraintName & closeDelim & " CHECK (" & theConstraint.ConstraintText & ")" & vbCrLf & StmtTerm
		Print #sqlFileHandle, sqlStmt
	Next theConstraint
End Function
Function doGenColumnConstraint(ByVal sqlFileHandle As Long, theEntity As Entity, theUserSelection As userSelection)
	Dim theAttribute As AttributeObj
	Dim colConstraint As String
	Dim sqlStmt As String

	For Each theAttribute In theEntity.Attributes
		colConstraint = theAttribute.CheckConstraint
		If colConstraint = "" Then
			GoTo continueLoop
		Else
			colConstraint = Replace(colConstraint, "@var", theAttribute.ColumnName)
		End If

		sqlStmt = "ALTER TABLE " & _
			IIf(theUserSelection.GenOwner And Len(theEntity.Owner) > 0, openDelim & theEntity.Owner & closeDelim & ".", "") & _
			openDelim &	theEntity.TableName & closeDelim & " ADD CONSTRAINT " & openDelim & theAttribute.CheckConstraintName & closeDelim & _
				" CHECK (" & colConstraint & ")" & vbCrLf & StmtTerm
			Print #sqlFileHandle, sqlStmt

		continueLoop:
	Next theAttribute
End Function
Function GetSQLFileName(theDiagram As Diagram, theModel As Model)
	Dim fileName As String
	Dim modelName As String

	modelName = theModel.Name + "_" + CStr(Now)
	modelName = Replace(modelName,"/","_")
	modelName = Replace(modelName,"\","_")
	modelName = Replace(modelName, ":" , "_" )
	modelName = Replace(modelName,"*","_")
	modelName = Replace(modelName,"?","_")
	modelName = Replace(modelName,"""","_")
	modelName = Replace(modelName,"<","_")
	modelName = Replace(modelName,">","_")
	modelName = Replace(modelName,"|","_")
	modelName = Replace(modelName,",","_")

	fileName = Left(theDiagram.FileName, InStrRev(theDiagram.FileName, "."))

	GetSQLFileName = fileName + modelName + ".sql"
End Function
