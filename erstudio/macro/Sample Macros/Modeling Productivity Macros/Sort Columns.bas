' Macro Title: Sort Columns.bas
'
' This macro will sort attributes/columns alphabetically.
'
' Version: 1.0
'*****************************************************************************

Option Explicit

Enum ESortNames
	AttributeNames
	ColumnNames
End Enum

Sub Main

	Begin Dialog UserDialog 210,217,"Sort Columns" ' %GRID:10,7,1,1
		GroupBox 20,14,180,70,"Table Selection",.Group1
		OptionGroup .EntitySelection
			OptionButton 40,35,130,14,"Selected Tables",.rbSelectItems
			OptionButton 40,56,130,14,"All Tables",.rbAllItems
		OKButton 20,189,90,21
		CancelButton 130,189,70,21
		GroupBox 20,98,180,77,"Sort Selection",.GroupBox1
		OptionGroup .SortSelection
			OptionButton 40,119,130,14,"Attribute Name",.opAttributeName
			OptionButton 40,147,130,14,"Column Name",.opColumnName
	End Dialog
	Dim dlg As UserDialog


	Dim theDiagram As Diagram
	Dim theModel As Model
	Dim theSubModel As SubModel
	Dim theEntity As Entity
	Dim so As SelectedObject
	Dim sortBy As ESortNames

	Debug.Clear


	Set theDiagram = DiagramManager.ActiveDiagram
	Set theModel = theDiagram.ActiveModel
	Set theSubModel = theModel.ActiveSubModel

	dlg.EntitySelection = 0
	dlg.SortSelection = 0

	If Dialog(dlg) = -1 Then
		DiagramManager.EnableScreenUpdateEx(False, True)

		If (dlg.SortSelection = 0) Then
			sortBy = AttributeNames
		Else
			sortBy = ColumnNames
		End If

		Select Case dlg.EntitySelection
			Case 0
				'loop through selected objects
				For Each so In theSubModel.SelectedObjects
					'Only concern with Entities type 1 selected object
					If so.Type = 1 Then
						Set theEntity = theModel.Entities.Item(so.ID)
						SortColumns(theEntity, sortBy)
					End If
				Next
			Case 1
				For Each theEntity In theModel.Entities
					SortColumns(theEntity, sortBy)
				Next theEntity
		End Select
		DiagramManager.EnableScreenUpdateEx(True, True)

		MsgBox "Done", vbInformation
	End If

End Sub
Function SortColumns(theEntity As Entity, sortBy As ESortNames)
	Dim theAttribute As AttributeObj
	Dim theAttrArray()
	Dim I As Integer
	Dim nPos As Integer

	If (theEntity.Attributes.Count < 2) Then
		Exit Function
	End If

	ReDim theAttrArray(theEntity.Attributes.Count)

	I = 0
	For Each theAttribute In theEntity.Attributes
		Select Case sortBy
			Case AttributeNames
				theAttrArray(I) = theAttribute.AttributeName
			Case columnnames
				theAttrArray(I) = theAttribute.ColumnName
		End Select
		I = I + 1
	Next theAttribute

	SortArray(theAttrArray)

	nPos = 1
	For I = LBound(theAttrArray) To UBound(theAttrArray)
		For Each theAttribute In theEntity.Attributes
			If (theAttribute.AttributeName = theAttrArray(I) And sortBy = ESortNames.AttributeNames) _
				Or (theAttribute.ColumnName = theAttrArray(I) And sortBy = ESortNames.ColumnNames) Then
				theAttribute.SequenceNumber = nPos
				nPos = nPos + 1
			End If
		Next theAttribute
	Next I

End Function
Public Sub SortArray(ByRef TheArray As Variant)
	Dim lLoop As Integer
	Dim lLoop2 As Integer
	Dim str1 As String
	Dim str2 As String

    For lLoop = 0 To UBound(TheArray)
       For lLoop2 = lLoop To UBound(TheArray)
            If UCase(TheArray(lLoop2)) < UCase(TheArray(lLoop)) Then
                str1 = TheArray(lLoop)
                str2 = TheArray(lLoop2)
                TheArray(lLoop) = str2
                TheArray(lLoop2) = str1
            End If
        Next lLoop2
    Next lLoop
End Sub
