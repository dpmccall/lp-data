' MACRO TITLE: ADD BASE ATTRIBUTES TO PERSON ENTITY
' This macro adds base attributes to selected entities, which 
' represent people. It will also introduce a primary key based
' upon the entity's name.
'----------------------------------------------------------------

Sub Main
    Dim MyDiagram As Diagram
	Dim MyModel As Model
	Dim MySubModel As SubModel
	Dim MyDictionary As Dictionary
	Dim MyDomain As Domain
	Dim MyEntity As Entity
	Dim MyAttribute As AttributeObj
	Dim MyPrimaryKey As Index
	Dim AttributeName As String
	Dim PrimaryKeyName As String
	Dim EntityName As String
	Dim Logical As Boolean
	Dim MySelObject As SelectedObject
	Dim NameID As Integer
	Dim CreateDateID As Integer
	Dim CreatedByID As Integer
	Dim ModifiedDateID As Integer
	Dim ModifiedByID As Integer
	Dim DomainID As Integer
    Dim ID As Integer
	Dim ObjType As Integer
	Dim MyIndex As Index

    ' Get the current diagram.
	
	Set MyDiagram = DiagramManager.ActiveDiagram
	
	' Get the current model.
	
	Set MyModel = MyDiagram.ActiveModel
	
	'Get the current submodel.
	
	Set MySubModel = MyModel.ActiveSubModel
	
	' Get the current Data Dictionary.
	
	Set MyDictionary = MyDiagram.Dictionary

    ' Determine Domains.

    Set MyDomain = MyDictionary.Domains.Item("Name")
    NameID = MyDomain.ID
	
    Set MyDomain = MyDictionary.Domains.Item("CreateDate")
    CreateDateID = MyDomain.ID
	
    Set MyDomain = MyDictionary.Domains.Item("CreatedBy")
    CreatedByID = MyDomain.ID
	
    Set MyDomain = MyDictionary.Domains.Item("ModifiedDate")
    ModifiedDateID = MyDomain.ID
	
    Set MyDomain = MyDictionary.Domains.Item("ModifiedBy")
    ModifiedByID = MyDomain.ID
	
	' Determine if the current model is logical or physical.
	
	Logical = MyModel.Logical

	' Iterate through all the selected objects in the current
	' model.
	
	For Each MySelObject In MySubModel.SelectedObjects

		'Get the object type - we are only concerned
		'with entities.
		
		ObjType = MySelObject.Type
		
		If ObjType = 1 Then
		
			' Get the ID for the selected object.
		
			ID = MySelObject.ID
		
			' Get the actual entity object with this ID. 
			' The model contains the collection of all the
			' entities.
		
			Set MyEntity = MyModel.Entities.Item(ID)
	
			If Logical = True Then
		
				' If the model is logical: 

				' Get the entity name.

				EntityName = MyEntity.EntityName
				
				' Set name of PK and PK attribute.

				AttributeName = EntityName + " ID"
				PrimaryKeyName = EntityName + " PK"
				
				' Add the PK attribute								

                Set MyAttribute = MyEntity.Attributes.Add(AttributeName, True)
				
				' Bind the ID domain to the PK attribute.

				MyAttribute.DomainId = DomainID
				
				' Get ID for PK, using the kluge that it is the only index.
				 'Set MyPrimaryKey = MyEntity.Indexes.ID
				
				' Go through the collection of indexes in the entity and
				' find the PK index

				For Each MyIndex In MyEntity.Indexes
					If (MyIndex.IsPK = True) Then
						Set MyPrimaryKey = MyIndex
						Exit For
					End If
				Next 'MyIndex

				' Set the PK name.
				MyPrimaryKey.Name = PrimaryKeyName

				' First Name
	            Set MyAttribute = MyEntity.Attributes.Add("First Name", False)
				MyAttribute.DomainId = NameID

                ' Last Name
	            Set MyAttribute = MyEntity.Attributes.Add("Last Name", False)
				MyAttribute.DomainId = NameID

				' CreateDate
	            Set MyAttribute = MyEntity.Attributes.Add("Create Date", False)
				MyAttribute.DomainId = CreateDateID

				' CreatedBy
	            Set MyAttribute = MyEntity.Attributes.Add("Created By", False)
				MyAttribute.DomainId = CreatedByID

				' ModifiedDate
	            Set MyAttribute = MyEntity.Attributes.Add("Modified Date", False)
				MyAttribute.DomainId = ModifiedDateID

				' CreatedBy
	            Set MyAttribute = MyEntity.Attributes.Add("Modified By", False)
				MyAttribute.DomainId = ModifiedByID
			End If
			
		End If
		
	Next MySelObject
	
End Sub
