' Macro Title: Move-Up Key Columns.bas
'
' This macro can be used to move primary or foreign key columns to the top of column list.
'
' Version: 1.0
'******************************************************************************************


Option Explicit
Sub Main


	Begin Dialog UserDialog 340,217,"Move-Up Key Columns" ' %GRID:10,7,1,1
		GroupBox 20,14,300,70,"Table Selection",.Group1
		OptionGroup .TableSelection
			OptionButton 40,35,130,14,"Selected Tables",.rbSelectTables
			OptionButton 40,56,130,14,"All Tables",.rbAllTables
		OKButton 60,189,90,21
		CancelButton 190,189,80,21
		CheckBox 30,105,300,14,"Move FK Columns",.ckFKTop
		CheckBox 30,140,240,14,"Move Primary Key Columns",.ckPKTop
	End Dialog
	Dim dlg As UserDialog


	dlg.TableSelection = 0
	dlg.ckFKTop = 1
	dlg.ckPKTop = 1

	Dim selectedTablesOnly As Boolean
	Dim moveFKCols As Boolean
	Dim movePKCols As Boolean

	If Dialog(dlg) = -1 Then

		selectedTablesOnly = (dlg.TableSelection = 0)
		moveFKCols = (dlg.ckFKTop = 1)
		movePKCols = (dlg.ckPKTop = 1)

		If ((movePKCols) Or (moveFKCols)) Then

			doMove(selectedTablesOnly, moveFKCols, movePKCols)

		End If

	End If

End Sub
Function doMove(ByVal selectedTablesOnly As Boolean, ByVal moveFKCols As Boolean, ByVal movePKCols As Boolean)
	Dim theDiagram As Diagram
	Dim theModel As Model
	Dim theSubModel As SubModel
	Dim so As SelectedObject
	Dim theEntity As Entity


	Set theDiagram = DiagramManager.ActiveDiagram
	If (theDiagram Is Nothing)  Then
		Exit Function
	End If

	Set theModel = theDiagram.ActiveModel
	Set theSubModel = theModel.ActiveSubModel

	DiagramManager.EnableScreenUpdateEx(False, True)

	If selectedTablesOnly Then

		For Each so In theSubModel.SelectedObjects
			If (so.Type = 1) Then
				Set theEntity = theModel.Entities(so.ID)
				MoveColumns(theEntity, moveFKCols, movePKCols)
			End If

		Next so
	Else

		For Each theEntity In theModel.Entities
				MoveColumns(theEntity, moveFKCols, movePKCols)
		Next theEntity

	End If

	DiagramManager.EnableScreenUpdateEx(True, True)

	MsgBox "Done.", vbInformation

End Function
Function MoveColumns(theEntity As Entity, ByVal moveFKCols As Boolean, ByVal movePKCols As Boolean)
	Dim theAttr As AttributeObj
	Dim ColNo As Integer
	Dim FirstAvailColPos As Integer

	If (movePKCols) Then
		FirstAvailColPos = GetFirstAvailableColPos(theEntity)
		For ColNo = FirstAvailColPos To theEntity.Attributes.Count
			For Each theAttr In theEntity.Attributes
				If (theAttr.SequenceNumber = ColNo) And (theAttr.PrimaryKey) Then
					theAttr.SequenceNumber = FirstAvailColPos
					FirstAvailColPos = FirstAvailColPos + 1
					Exit For
				End If
			Next theAttr
		Next ColNo
	End If

	If (moveFKCols) Then
		FirstAvailColPos = GetFirstAvailableColPos(theEntity)
		For ColNo = FirstAvailColPos To theEntity.Attributes.Count
			For Each theAttr In theEntity.Attributes
				If (theAttr.SequenceNumber = ColNo) And (theAttr.ForeignKey) Then
					theAttr.SequenceNumber = FirstAvailColPos
					FirstAvailColPos = FirstAvailColPos + 1
					Exit For
				End If
			Next theAttr
		Next ColNo
	End If


End Function
Function GetFirstAvailableColPos(theEntity As Entity) As Integer
	Dim LastAvailColPos As Integer
	Dim ColNo As Integer
	Dim theAttr As AttributeObj

	GetFirstAvailableColPos = 1

	LastAvailColPos = 1
	For ColNo = 1 To theEntity.Attributes.Count
		For Each theAttr In theEntity.Attributes
			If (theAttr.SequenceNumber = ColNo) Then
				'If (Not theAttr.PrimaryKey) Then
					LastAvailColPos = ColNo
					GoTo exitLoop
				'End If
			End If
		Next theAttr
	Next ColNo
	exitLoop:


	GetFirstAvailableColPos = LastAvailColPos
End Function
