''MACRO TITLE: AUTO-CONVERT DATATYPE (VARCHAR->TEXT)
'This macro will iterate through all the selected entities
'and change the datatypes for all attributes which use 
'VARCHAR to TEXT.
'----------------------------------------------------------------

Sub Main
	Dim MyDiagram As Diagram
	Dim MyModel As Model
	Dim MyEntity As Entity
	Dim MySubModel As SubModel
	Dim MySelObject As SelectedObject
	Dim MyAttribute As AttributeObj
	
	Dim EntityName As String
	Dim NewEntityName As String
	Dim Prefix As String
	Dim Datatype As String
	Dim ID As Integer
	Dim ObjType As Integer

	' Get the current diagram.
	
	Set MyDiagram = DiagramManager.ActiveDiagram
	
	' Get the current model.
	
	Set MyModel = MyDiagram.ActiveModel
	
	' Get the current submodel.
	
	Set MySubModel = MyModel.ActiveSubModel
	
	' Iterate through all the selected objects in the
	' submodel.
	
	For Each MySelObject In MySubModel.SelectedObjects
	
		' We are only concerned about entities.
		
		ObjType = MySelObject.Type
		
		If ObjType = 1 Then
	
			' Get the ID of the selected object.
			
			ID = MySelObject.ID
			
			' Get the actual entity object with this ID.
		
			Set MyEntity = MyModel.Entities.Item(ID)
		
			' Iterate through all attributes in this entity.
			
			For Each MyAttribute In MyEntity.Attributes
			
				' Get the datatype of the attribute.
				
				Datatype = MyAttribute.Datatype
				
				' If the datatype is "VARCHAR", then change
				' it to "TEXT".
				
				If Datatype = "VARCHAR" Then
					MyAttribute.Datatype = "TEXT"
				End If
			
			Next MyAttribute				
	
		End If
		
	Next MySelObject
	
End Sub
