'MACRO TITLE: AUTO-CREATE DATA DICTIONARY
'The purpose of this macro is to create a user defined data dictionary quickly. The macro
'can be used as a template to create user-defined or business-specific data dictionaries. This macro
'can be inserted into ER/Studio's ERSBasicHandlers system (specifically in the 
'"CreateDiagramHandler(CurDiagram As Object)" section of ERSBasicHandlers) and if the Create Handlers 
'option is checked on in ER/Studio's Automation Interface Options, this Data Dictionary will be created 
'and populated any time a user creates a new diagram.
'------------------------------------------------------------------------------------------
Sub Main()
        Dim MyDiagram As Diagram
        Dim MyModel As Model
        Dim MyDomain As Domain
        Dim MyDictionary As Dictionary
        Dim ID As Integer

        'Create a new diagram.
        
        Set MyDiagram = DiagramManager.ActiveDiagram
        
        'Get the data dictionary.
        
        Set MyDictionary = MyDiagram.Dictionary
        
        'Get the current model (in this case, the logical model).
        
        Set MyModel = MyDiagram.ActiveModel
        
        'ID domain.
        
        Set MyDomain = MyDictionary.Domains.Add("ID", "ID")
        MyDomain.Datatype = "INTEGER"
    MyDomain.Identity = True
    MyDomain.IdentitySeed = 1
    MyDomain.IdentityIncrement = 1

        'ObjectID domain.
        
        Set MyDomain = MyDictionary.Domains.Add("ObjectID", "Object ID")
        MyDomain.Datatype = "INTEGER"
        MyDomain.Nullable = False

        'Name domain.
        
        Set MyDomain = MyDictionary.Domains.Add("Name", "Name")
        MyDomain.Datatype = "VARCHAR"
        MyDomain.DataLength = 30
        MyDomain.Nullable = False

        'LongName domain.
        
        Set MyDomain = MyDictionary.Domains.Add("LongName", "Name")
        MyDomain.Datatype = "VARCHAR"
        MyDomain.DataLength = 254
        MyDomain.Nullable = False

        'Owner domain.
        
        Set MyDomain = MyDictionary.Domains.Add("Owner", "Owner")
        MyDomain.Datatype = "VARCHAR"
        MyDomain.DataLength = 30
        MyDomain.Nullable = True

        'Description domain.
        
        Set MyDomain = MyDictionary.Domains.Add("Description", "Description")
        MyDomain.Datatype = "VARCHAR"
        MyDomain.DataLength = 254
        MyDomain.Nullable = True

        'Comments domain.
        
        Set MyDomain = MyDictionary.Domains.Add("Comments", "Comments")
        MyDomain.Datatype = "VARCHAR"
        MyDomain.DataLength = 254
        MyDomain.Nullable = True

        'FilePath domain.
        
        Set MyDomain = MyDictionary.Domains.Add("FilePath", "File Path")
        MyDomain.Datatype = "VARCHAR"
        MyDomain.DataLength = 254
        MyDomain.Nullable = True

        'Role Name domain.
        
        Set MyDomain = MyDictionary.Domains.Add("RoleName", "Role Name")
        MyDomain.Datatype = "VARCHAR"
        MyDomain.DataLength = 254
        MyDomain.Nullable = True

        'ValueString domain.
        
        Set MyDomain = MyDictionary.Domains.Add("ValueString", "Value")
        MyDomain.Datatype = "VARCHAR"
        MyDomain.DataLength = 254
        MyDomain.Nullable = False

        'ValueShortString domain.
        
        Set MyDomain = MyDictionary.Domains.Add("ValueShortString", "Value")
        MyDomain.Datatype = "VARCHAR"
        MyDomain.DataLength = 30
        MyDomain.Nullable = False

        'ValueNumber domain.
        
        Set MyDomain = MyDictionary.Domains.Add("ValueNumber", "Value")
        MyDomain.Datatype = "INTEGER"
        MyDomain.Nullable = False

        'URL domain.
        
        Set MyDomain = MyDictionary.Domains.Add("URL", "URL")
        MyDomain.Datatype = "VARCHAR"
        MyDomain.DataLength = 254
        MyDomain.Nullable = True

        'Quantity domain.
        
        Set MyDomain = MyDictionary.Domains.Add("Quantity", "Quantity")
        MyDomain.Datatype = "INTEGER"
        MyDomain.Nullable = False

        'SequenceNbr domain.
        
        Set MyDomain = MyDictionary.Domains.Add("SequenceNbr", "Sequence Nbr")
        MyDomain.Datatype = "INTEGER"
        MyDomain.Nullable = False

        'ListOrder domain.
        
        Set MyDomain = MyDictionary.Domains.Add("ListOrder", "List Order")
        MyDomain.Datatype = "INTEGER"
        MyDomain.Nullable = False

        'Min domain.
        
        Set MyDomain = MyDictionary.Domains.Add("Min", "Min")
        MyDomain.Datatype = "INTEGER"
        MyDomain.Nullable = False

        'Max domain.
        
        Set MyDomain = MyDictionary.Domains.Add("Max", "Max")
        MyDomain.Datatype = "INTEGER"
        MyDomain.Nullable = True

        'Epoch domain.
        
        Set MyDomain = MyDictionary.Domains.Add("Epoch", "Epoch")
        MyDomain.Datatype = "INTEGER"
        MyDomain.Nullable = False
        MyDomain.DeclaredDefault = 1

        'Percent domain.
        
        Set MyDomain = MyDictionary.Domains.Add("Percent", "Percent")
        MyDomain.Datatype = "NUMERIC"
        MyDomain.DataLength = 5
        MyDomain.DataScale = 3
        MyDomain.Nullable = False

        'Is domain.
        
        Set MyDomain = MyDictionary.Domains.Add("Is", "Is")
        MyDomain.Datatype = "BIT"
        MyDomain.Nullable = False

        'IsDefault domain.
        
        Set MyDomain = MyDictionary.Domains.Add("IsDefault", "Is Default")
        MyDomain.Datatype = "BIT"
        MyDomain.Nullable = False

        'IsCurrentVersion domain.
        
        Set MyDomain = MyDictionary.Domains.Add("IsCurrentVersion", "Is Current Version")
        MyDomain.Datatype = "BIT"
        MyDomain.Nullable = False

        'IsModified domain.
        
        Set MyDomain = MyDictionary.Domains.Add("IsModified", "Is Modified")
        MyDomain.Datatype = "BIT"
        MyDomain.Nullable = False

        'IsRequired domain.
        
        Set MyDomain = MyDictionary.Domains.Add("IsRequired", "Is Required")
        MyDomain.Datatype = "BIT"
        MyDomain.Nullable = False

        'IsSystemRequired domain.
        
        Set MyDomain = MyDictionary.Domains.Add("IsSystemRequired", "Is System Required")
        MyDomain.Datatype = "BIT"
        MyDomain.Nullable = False

        'CreateDate.
        
        Set MyDomain = MyDictionary.Domains.Add("CreateDate", "Create Date")
        MyDomain.Datatype = "DATETIME"
        MyDomain.Nullable = False
        MyDomain.DeclaredDefault = "getdate()"

        'CreatedBy.
        
        Set MyDomain = MyDictionary.Domains.Add("CreatedBy", "Created By")
        MyDomain.Datatype = "VARCHAR"
        MyDomain.DataLength = 30
        MyDomain.Nullable = False
        MyDomain.DeclaredDefault = "user_name()"

        'ModifiedDate.
        
        Set MyDomain = MyDictionary.Domains.Add("ModifiedDate", "Modified Date")
        MyDomain.Datatype = "DATETIME"
        MyDomain.Nullable = False
        MyDomain.DeclaredDefault = "getdate()"

        'ModifiedBy.
        
        Set MyDomain = MyDictionary.Domains.Add("ModifiedBy", "Modified By")
        MyDomain.Datatype = "VARCHAR"
        MyDomain.DataLength = 30
        MyDomain.Nullable = False
        MyDomain.DeclaredDefault = "user_name()"

        'IPAddress.
        
        Set MyDomain = MyDictionary.Domains.Add("IPAddress", "IP Address")
        MyDomain.Datatype = "CHAR"
        MyDomain.DataLength = 15
        MyDomain.Nullable = False

        'Subnet.
        
        Set MyDomain = MyDictionary.Domains.Add("Subnet", "Subnet")
        MyDomain.Datatype = "CHAR"
        MyDomain.DataLength = 15
        MyDomain.Nullable = False

        'Datetime.
        
        Set MyDomain = MyDictionary.Domains.Add("Datetime", "Datetime")
        MyDomain.Datatype = "DATETIME"
        MyDomain.Nullable = True

        'RowTimestamp.
        
        Set MyDomain = MyDictionary.Domains.Add("RowTimestamp", "RowTimestamp")
        MyDomain.Datatype = "TIMESTAMP"
        MyDomain.Nullable = True

        'Datetype.
        
        Set MyDomain = MyDictionary.Domains.Add("Datatype", "Data Type")
        MyDomain.Datatype = "VARCHAR"
        MyDomain.DataLength = 30
        MyDomain.Nullable = True

End Sub