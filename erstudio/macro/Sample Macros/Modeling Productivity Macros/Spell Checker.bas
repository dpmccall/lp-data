' MACRO TITLE: Spell Checker.BAS
'
' This script can be used to check spelling in the documentation fields (i.e., Definition, Note,
' Description, Text)of Entities, attribites, views, packages, procedures, functions, triggers,
' shapes in ER/Studio Models.
' This script makes use of the SpellCheck functionality that is present in Microsoft Word.
' Microsoft Word will need To be installed On your machine to run this script successfully.
' When running the spell checker ER/Stuio is automatically minimized. Once the spell check
' is complete the ER/Studio window will be restored to a normal state.
'
' Version: 1.0
'======================================================================================

Global Const SW_HIDE = 0
Global Const SW_SHOWNORMAL = 1
Global Const SW_SHOWMINIMIZED = 2
Global Const SW_SHOWMAXIMIZED = 3
Global Const wdWindowStateMaximize = 1
Global Const wdWindowStateMinimize = 2
Global Const wdWindowStateNormal = 0

Declare Function ShowWindow Lib "user32" _
    Alias "ShowWindow" (ByVal hwnd As Long, _
          ByVal nCmdShow As Long) As Long

Declare Function GetActiveWindow Lib "user32" Alias "GetActiveWindow"  () As Long

Dim theDiagram As Diagram
Dim theModel As Model
Dim theEnt As Entity
Dim theAttr As AttributeObj
Dim theView As View
Dim theSubModel As SubModel
Dim theShapeDisplay As ShapeDisplay
Dim theShape As Shape
Dim thePackage As Package
Dim theProcedure As Procedure
Dim theTrigger As Trigger
Dim theSynonym As OracleSynonym
Dim theSequence As OracleSequence
Dim objWord As Object
Dim objDocument As Object

Sub Main
	Begin Dialog UserDialog 290,399,"ER/Studio Spell Checker" ' %GRID:10,7,1,1
		GroupBox 10,7,270,210,"Elements to check",.grpElements
		CheckBox 20,28,90,14,"Entity",.ckEntity
		CheckBox 20,49,90,14,"Attribute",.ckAttribute
		CheckBox 20,70,90,14,"View",.ckView
		CheckBox 20,91,90,14,"Shapes",.ckShapes
		CheckBox 20,112,110,14,"Procedure",.ckProcedure
		CheckBox 20,133,90,14,"Trigger",.ckTrigger
		CheckBox 20,154,90,14,"Synonym",.ckSynonym
		CheckBox 20,175,110,14,"Sequence",.ckSequence
		CheckBox 20,196,90,14,"Packages",.ckPackages
		GroupBox 10,224,270,63,"Properties to check",.grpProperty
		CheckBox 20,245,90,14,"Definition",.ckDefinition
		CheckBox 20,266,90,14,"Note",.ckNote
		GroupBox 10,294,270,49,"Options",.grpOptions
		CheckBox 20,315,180,14,"Grammar Check",.ckGrammarCheck
		OKButton 20,371,150,21,.SpellCheck
		CancelButton 190,371,90,21
	End Dialog

	Dim dlg As UserDialog

	dlg.ckEntity = 1
	dlg.ckAttribute = 1
	dlg.ckView = 1
	dlg.ckShapes = 1
	dlg.ckProcedure = 1
	dlg.ckTrigger = 1
	dlg.ckSynonym = 1
	dlg.ckSequence = 1
	dlg.ckPackages = 1
	dlg.ckDefinition = 1
	dlg.ckNote = 1


	result = Dialog(dlg)
	If (result = 0) Then
		Exit Sub
	End If

	Set theDiagram = DiagramManager.ActiveDiagram
	If theDiagram Is Nothing Then
		MsgBox "You must first open a model file."
		Exit Sub
	End If

	' Connect to Word
	Set objWord = CreateObject("Word.Application")

	If objWord Is Nothing Then
		MsgBox "Could not create Word object."
		Exit Sub
	End If

	Set theModel = theDiagram.ActiveModel
	ShowWindow GetActiveWindow(), SW_SHOWMINIMIZED

	

	objWord.Visible = True
	objWord.WindowState = wdWindowStateMaximize
    Set objDocument = objWord.Documents.Add()

	If (dlg.ckEntity = 1) Then
		If Not theModel.Entities Is Nothing Then
			For Each theEnt In theModel.Entities
				If (dlg.ckDefinition = 1) Then
					If Len(theEnt.Definition) > 0 Then
					    With objDocument
					        .Select
					        .Range.Text = theEnt.Definition
					        .CheckSpelling , True, True
					        If (dlg.ckGrammarCheck = 1) Then
					        	.CheckGrammar
					        End If
					        theEnt.Definition = Replace(.Range.Text, vbCr , vbCrLf)
					        .Range.Delete
					    End With
		   			End If
				End If
				If (dlg.ckNote = 1) Then
					If Len(theEnt.Note) > 0 Then
					    With objDocument
					        .Select
					        .Range.Text = theEnt.Note
					        .CheckSpelling , True, True
					        If (dlg.ckGrammarCheck = 1) Then
					        	.CheckGrammar
					        End If
					        theEnt.Note = Replace(.Range.Text, vbCr, vbCrLf)
					        .Range.Delete
					    End With
		   			End If
				End If
			Next theEnt
		End If
	End If

	If (dlg.ckAttribute = 1) Then
		For Each theEnt In theModel.Entities
			If (dlg.ckDefinition = 1) Then
				If Not theEnt.Attributes Is Nothing Then
					For Each theAttr In theEnt.Attributes
						If Len(theAttr.Definition) > 0 Then
						    With objDocument
						        .Select
						        .Range.Text = theAttr.Definition
						        .CheckSpelling , True, True
						        If (dlg.ckGrammarCheck = 1) Then
						        	.CheckGrammar
						        End If
						        theAttr.Definition = Replace(.Range.Text, vbCr, vbCrLf)
						        .Range.Delete
						    End With
		   				End If
					Next theAttr
				End If
			End If
			If (dlg.ckNote = 1) Then
				If Not theEnt.Attributes Is Nothing Then
					For Each theAttr In theEnt.Attributes
						If Len(theAttr.Notes) > 0 Then
						    With objDocument
						        .Select
						        .Range.Text = theAttr.Notes
						        .CheckSpelling , True, True
						        If (dlg.ckGrammarCheck = 1) Then
						        	.CheckGrammar
						        End If
						        theAttr.Notes = Replace(.Range.Text, vbCr, vbCrLf)
						        .Range.Delete
						    End With
						End If
					Next theAttr
				End If
			End If
		Next theEnt
	End If

	If (dlg.ckView = 1) Then
		If (dlg.ckDefinition = 1) Then
			If Not theModel.Views Is Nothing Then
				For Each theView In theModel.Views
					If Len(theView.Definition) > 0 Then
					    With objDocument
					        .Select
					        .Range.Text = theView.Definition
					        .CheckSpelling , True, True
					        If (dlg.ckGrammarCheck = 1) Then
					        	.CheckGrammar
					        End If
					        theView.Definition = Replace(.Range.Text, vbCr, vbCrLf)
					        .Range.Delete
					    End With
			   		End If
		   		Next theView
		   	End If
		End If
		If (dlg.ckNote = 1) Then
			For Each theView In theModel.Views
				If Len(theView.Notes) > 0 Then
				    With objDocument
				        .Select
				        .Range.Text = theView.Notes
				        .CheckSpelling , True, True
				        If (dlg.ckGrammarCheck = 1) Then
				        	.CheckGrammar
				        End If
				        theView.Notes = Replace(.Range.Text, vbCr, vbCrLf)
				        .Range.Delete
				    End With
	   			End If
	   		Next theView
		End If
	End If

	If (dlg.ckShapes = 1) Then
		If Not theModel.SubModels Is Nothing Then
			For Each theSubModel In theModel.SubModels
				If Not theSubModel.ShapeDisplays Is Nothing Then
					For Each theShapeDisplay In theSubModel.ShapeDisplays
						Set theShape = theShapeDisplay.ParentShape
						If Len(theShape.Text) > 0 Then
						    With objDocument
						        .Select
						        .Range.Text = theShape.Text
						        .CheckSpelling , True, True
						        If (dlg.ckGrammarCheck = 1) Then
						        	.CheckGrammar
						        End If
						        theShape.Text = Replace(.Range.Text, vbCr, vbCrLf)
						        .Range.Delete
						    End With
				   		End If
					Next theShapeDisplay
				End If
			Next theSubModel
		End If
	End If

	If (dlg.ckProcedure = 1) Then
		If Not theModel.Procedures Is Nothing Then
			For Each theProcedure In theModel.Procedures
				If Len(theProcedure.Description) > 0 Then
				    With objDocument
				        .Select
				        .Range.Text = theProcedure.Description
				        .CheckSpelling , True, True
				        If (dlg.ckGrammarCheck = 1) Then
				        	.CheckGrammar
				        End If
				        theProcedure.Description = Replace(.Range.Text, vbCr, vbCrLf)
				        .Range.Delete
				    End With
			   	End If
			Next theProcedure
		End If
	End If

	If (dlg.ckTrigger = 1) Then
		If Not theModel.Entities Is Nothing Then
			For Each theEnt In theModel.Entities
				If Not theEnt.Triggers Is Nothing Then
					For Each theTrigger In theEnt.Triggers
						If Len(theTrigger.Description) > 0 Then
						    With objDocument
						        .Select
						        .Range.Text = theTrigger.Description
						        .CheckSpelling , True, True
						        If (dlg.ckGrammarCheck = 1) Then
						        	.CheckGrammar
						        End If
						        theTrigger.Description = Replace(.Range.Text, vbCr, vbCrLf)
						        .Range.Delete
						    End With
				   		End If
					Next theTrigger
				End If
			Next theEnt
		End If
	End If
	
	If (dlg.ckSynonym = 1) Then
		If Not theModel.OracleSynonyms Is Nothing Then
			For Each theSynonym In theModel.OracleSynonyms
				If Len(theSynonym.Description) > 0 Then
				    With objDocument
				        .Select
				        .Range.Text = theSynonym.Description
				        .CheckSpelling , True, True
				        If (dlg.ckGrammarCheck = 1) Then
				        	.CheckGrammar
				        End If
				        theSynonym.Description = Replace(.Range.Text, vbCr, vbCrLf)
				        .Range.Delete
				    End With
			   	End If
			Next theSynonym
		End If
	End If

	If (dlg.ckSequence = 1) Then
		If Not theModel.OracleSequences Is Nothing Then
			For Each theSequence In theModel.OracleSequences
				If Len(theSequence.Description) > 0 Then
				    With objDocument
				        .Select
				        .Range.Text = theSequence.Description
				        .CheckSpelling , True, True
				        If (dlg.ckGrammarCheck = 1) Then
				        	.CheckGrammar
				        End If
				        theSequence.Description = Replace(.Range.Text, vbCr, vbCrLf)
				        .Range.Delete
				    End With
			   	End If
			Next theSequence
		End If
	End If

	If (dlg.ckPackages = 1) Then
		If Not theModel.Packages Is Nothing Then
			For Each thePackage In theModel.Packages
				If Len(thePackage.Description) > 0 Then
				    With objDocument
				        .Select
				        .Range.Text = thePackage.Description
				        .CheckSpelling , True, True
				        If (dlg.ckGrammarCheck = 1) Then
				        	.CheckGrammar
				        End If
				        thePackage.Description = Replace(.Range.Text, vbCr, vbCrLf)
				        .Range.Delete
				    End With
			   	End If
			Next thePackage
		End If
	End If

	ExitWord
	MsgBox "Done"
	ShowWindow GetActiveWindow(), SW_SHOWMAXIMIZED
End Sub
Sub ExitWord
	objDocument.Saved = True
	objDocument.Close
    objWord.Quit
    Set objDocument = Nothing
    Set objWord = Nothing
End Sub
