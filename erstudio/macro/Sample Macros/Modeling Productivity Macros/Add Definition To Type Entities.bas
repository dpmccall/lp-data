'MACRO TITLE: ADD DEFINITION TO TYPE ENTITIES
'This macro will add a definition to selected entities. The definition
'applied to the selected entity will also include the object's name automatically,
'as in "My definition text + entity name +(s)"
'----------------------------------------------------------------

Sub Main()
    Dim MyDiagram As Diagram
        Dim MyModel As Model
        Dim MySubModel As SubModel
        Dim MyEntity As Entity
        Dim Logical As Boolean
        Dim MySelObject As SelectedObject
        Dim ID As Integer
        Dim ObjType As Integer
        Dim EntityName As String
        Dim BaseEntityName As String
        Dim StartPosition As Integer

    ' Set the context: active Diagram, Model, Submodel
        
        Set MyDiagram = DiagramManager.ActiveDiagram
        Set MyModel = MyDiagram.ActiveModel
        Set MySubModel = MyModel.ActiveSubModel
        
        ' Determine if the current model is logical or physical.
        
        Logical = MyModel.Logical

        ' Iterate through all the selected objects in the current
        ' model.
        
        For Each MySelObject In MySubModel.SelectedObjects

                'Get the object type - we are only concerned
                'with entities.
                
                ObjType = MySelObject.Type
                
                If ObjType = 1 Then
                
                        ' Get the ID for the selected object.
                
                        ID = MySelObject.ID
                
                        ' Get the actual entity object with this ID.
                        ' The model contains the collection of all the
                        ' entities.
                
                        Set MyEntity = MyModel.Entities.Item(ID)
        
                        If Logical = True Then
                
                                ' If the model is logical:
                    
                                ' Get the entity name.

                                EntityName = MyEntity.EntityName
                                
                ' Strip out Type.

                StartPosition = InStr(EntityName, "Type")

                If StartPosition = 0 Then
                   StartPosition = Len(EntityName) + 1
                End If

                BaseEntityName = Trim(Left(EntityName, StartPosition - 1))

                ' Apply the definition.

                MyEntity.Definition = "Control table that classifies " + BaseEntityName + "(s)"

                        End If
                        
                End If
                
        Next MySelObject
        
End Sub
