'MACRO TITLE: AUTO-CREATE DATA DICTIONARY AND BOUND DOMAIN MACRO
'This macro will create a new diagram, then create a rule, default
'and domain in the data dictionary. Then it will bind the rule and 
'default to the domain. Next, it will then create an entity and an 
'attribute. Finally, it will bind the domain to the attribute.
'-------------------------------------------------------------------

Sub Main
	Dim MyDiagram As Diagram
	Dim MyModel As Model
	Dim MyEntity As Entity
	Dim MyRule As Rule
	Dim MyDefault As Default
	Dim MyDomain As Domain
	Dim MyAttribute As AttributeObj
	Dim MyDictionary As Dictionary
	Dim ID As Integer

	'Create a new diagram.
	
	Set MyDiagram = DiagramManager.NewDiagram
	
	'Get the data dictionary.
	
	Set MyDictionary = MyDiagram.Dictionary
	
	'Get the current model (in this case, the logical model).
	
	Set MyModel = MyDiagram.ActiveModel
	
	'Create a default with a value of 1.
	
	Set MyDefault = MyDictionary.Defaults.Add("MyDefault", "1")
	
	'Create a rule with the constraint of "> 0".
	
	Set MyRule = MyDictionary.Rules.Add("MyRule", "@var > 0")
	
	'Create a domain.
	
	Set MyDomain = MyDictionary.Domains.Add("MyDomain", "MyDomain")
	
	'Set the datatype for the domain as integer.
	
	MyDomain.Datatype = "INTEGER"

	'Bind the default to the domain by setting the default's
	'ID into the domain's DefaultId property.
	
	ID = MyDefault.ID
	MyDomain.DefaultId = ID
	
	'Bind the rule to the domain by setting the rule's
	'ID into the domain's RuleId property.

	ID = MyRule.ID
	MyDomain.RuleId = ID

	'Create a new entity in the model.
	
	Set MyEntity = MyModel.Entities.Add(100, 100)
	
	MyEntity.EntityName = "MyEntity"
	MyEntity.TableName = "MyEntity"
	
	'Create a new attribute in the entity.
	
	Set MyAttribute = MyEntity.Attributes.Add("MyAttribute", False)

	'Bind the domain to the attribute by setting the domain's
	'ID into the attribute's DomainId property.
	
	ID = MyDomain.ID
	MyAttribute.DomainId = ID
	
End Sub
