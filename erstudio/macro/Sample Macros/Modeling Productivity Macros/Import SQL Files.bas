' Macro: Import SQL File.bas
'
' Purpose: This macro can batch import SQL files located in specified directory.
'
' Usage:
'
'   Directory:         Specify the location of SQL files. Use Choose Directory button to select folder.
'   File Extension(s): Specify the extension of files to be processed. Separate multiple extensions
'                      using ";"
'   Database Platform: Specify the database platform of these files. Note that it is assumed that all
'                      SQL files located In the folder To be processed are For the specified platform.
'
'   Date: 8/30/2014
'
'============================================================================================

Option Explicit
Sub Main()

	Dim dbPlatforms() As String
	LoadDBPlatformList(dbPlatforms())


	Begin Dialog UserDialog 750,273,"Import SQL File",.FolderPathDialogFunc ' %GRID:10,7,1,1
		Text 70,14,60,14,"&Directory:",.txtDirectory
		TextBox 160,14,410,21,.edDir
		PushButton 580,14,120,21,"Choose Directory",.btnChooseDir
		Text 30,56,110,14,"File Extension(s):",.txtFileExtensions
		TextBox 160,56,400,21,.edFileExtensions
		Text 20,91,120,14,"Database Platform:",.txtDatabasePlatform
		DropListBox 160,98,410,70,dbPlatforms(),.cbDatabasePlatform,2
		CheckBox 40,182,540,14,"Save and Close Diagram After Import",.ckSaveAndClose
		PushButton 260,231,90,21,"Ok",.btnOk
		CancelButton 380,231,90,21
	End Dialog
    Dim dlg As UserDialog



    Debug.Clear
    If (Dialog(dlg) = -1) Then End

End Sub
Private Function FolderPathDialogFunc(DlgItem$, Action%, SuppValue&) As Boolean
        Dim n As Integer
        Dim retval As Long  ' return value
        Dim fileExtensions As String
        Dim dbPlatform As String
        Dim sDir As String
        Dim saveClose As Boolean

        Select Case Action%
        Case 1 ' Dialog box initialization

			DlgText "edFileExtensions", "*.sql;*.ddl"
			DlgValue "ckSaveAndClose", 1
			DlgValue "cbDatabasePlatform", -1

        Case 2 ' Value changing or button pressed
                Select Case DlgItem$
                        Case "btnChooseDir"

							sDir = ShellBrowseForFolder()
							If (sDir <> "") Then DlgText("edDir", sDir)

                            FolderPathDialogFunc = True
                            Exit Function

                        Case "btnOk"

                            sDir = DlgText("edDir")
                            If Len(sDir) = 0 Then
                                    MsgBox "You must specify a directory."
                                    FolderPathDialogFunc = True
                                    Exit Function
                            ElseIf Not DirExists(sDir) Then
                                    MsgBox "Specified directory does not exist."
                                    FolderPathDialogFunc = True
                                    Exit Function
                            End If

							fileExtensions = DlgText("edFileExtensions")
							dbPlatform = DlgText("cbDatabasePlatform")
							saveClose = CBool(DlgValue("ckSaveAndClose"))

                            doImport(sDir, fileExtensions, dbPlatform, saveClose)
                            FolderPathDialogFunc = True
                            Exit Function
                End Select
        End Select
End Function
Function DirExists(ByVal DName As String) As Boolean
        Dim sDummy As String

        On Error Resume Next

        If Right(DName, 1) <> "\" Then
                DName = DName & "\"
        End If
        sDummy = Dir$(DName & "*.*", vbDirectory)
        DirExists = Not (sDummy = "")
End Function
Function doImport(ByVal sDir As String, ByVal fileExtensions As String, ByVal dbPlatform As String, ByVal saveClose As Boolean)

	Dim arrFileExtensions() As String
	Dim logFile As String
	Dim logFileHandle As Long
	Dim I As Integer
	Dim nCount As Integer
	Dim SQLFilePath As String
	Dim SQLFileExt As String
	Dim SQLFileName As String
	Dim SQLFile As String
	Dim dirExt As String
	Dim logEntry As String
	Dim theDiagram As Diagram
	Dim dm1FileName As String


	' Get extension of files to be processed
	arrFileExtensions = Split(fileExtensions, ";")
	Err.Clear
	On Error Resume Next
	nCount = UBound(arrFileExtensions)
	If (Err.Number > 0) Then Exit Function

	' Set error handler
	On Error GoTo errorHandler

	' Location of SQL files
	SQLFilePath = sDir
	If (Right(SQLFilePath, 1) <> "\") Then SQLFilePath = SQLFilePath + "\"

	' Create and open Log file
	logFile = GetUserMyDocumentDirectory()
	If (Right(logFile, 1) <> "\") Then logFile = logFile & "\"
	logFile = logFile & "Import_SQL_Log.txt"
	logFileHandle = FreeFile
	Open logFile For Output As #logFileHandle

	' Process SQL files
	For I = LBound(arrFileExtensions) To UBound(arrFileExtensions)

		' Process files with specified extension
		SQLFileName = Dir$(SQLFilePath + arrFileExtensions(I))

		While (SQLFileName <> "")

			SQLFile = SQLFilePath + SQLFileName

			Set theDiagram = DiagramManager.ImportSQLFile(SQLFile, Trim(dbPlatform))
			logEntry = CheckOp()
			If (logEntry <> "") Then
				logEntry = "Error Processing file: " + SQLFile + vbCrLf + "Error: " + logEntry + vbCrLf
				Print #logFileHandle, logEntry
			End If


			If (Not theDiagram Is Nothing) And (saveClose) Then
				dm1FileName = Mid(SQLFileName, 1, InStrRev(SQLFileName, ".")) + "dm1"

				theDiagram.SaveFile(SQLFilePath + dm1FileName)
				logEntry = CheckOp()
				If (logEntry <> "") Then Print #logFileHandle, logEntry

				DiagramManager.CloseDiagram(dm1FileName)
				logEntry = CheckOp()
				If (logEntry <> "") Then Print #logFileHandle, logEntry
			End If

			SQLFileName$ = Dir$()
		Wend
	Next I

	Close #logFileHandle
	MsgBox("Operation Complete." + vbCrLf + "Log File: " + logFile, vbInformation)

	Exit Function

errorHandler:
	MsgBox(Err.Description, vbCritical)
End Function
Function CheckOp() As String
	Dim errorCode As Integer

	CheckOp = ""
	errorCode = DiagramManager.GetLastErrorCode()
	If (errorCode <> 0) Then
		CheckOp = DiagramManager.GetLastErrorString()
	End If
End Function
Function GetUserMyDocumentDirectory() As String
	' http://msdn.microsoft.com/en-us/library/0ea7b5xe.aspx
	Dim WshShell As Object
	Set WshShell = CreateObject("WScript.Shell")
	GetUserMyDocumentDirectory = WshShell.SpecialFolders("MyDocuments")
End Function
Function LoadDBPlatformList(dbPlatforms() As String)

	ReDim dbPlatforms(0 To 80)

	dbPlatforms(0)= "Microsoft SQL Server 4.x "
	dbPlatforms(1)= "Sybase System 10.x "
	dbPlatforms(2)= "MS SQL SERVER 6.x "
	dbPlatforms(3)= "Sybase Watcom SQL "
	dbPlatforms(4)= "Informix Online "
	dbPlatforms(5)= "Informix SE "
	dbPlatforms(6)= "Oracle 7.x "
	dbPlatforms(7)= "IBM DB2 Common Server "
	dbPlatforms(8)= "InterBase "
	dbPlatforms(9)= "Sybase ASE 11.0 "
	dbPlatforms(10)= "Sybase SQL Anywhere 5.x "
	dbPlatforms(11)= "Microsoft Access "
	dbPlatforms(12)= "Microsoft Access 2.0 "
	dbPlatforms(13)= "Microsoft Access 95 "
	dbPlatforms(14)= "Microsoft Visual FoxPro 2.x "
	dbPlatforms(15)= "Microsoft Visual FoxPro 3.x "
	dbPlatforms(16)= "Microsoft Access 97 "
	dbPlatforms(17)= "Microsoft Visual FoxPro 5.x "
	dbPlatforms(18)= "IBM DB2 UDB 5.x "
	dbPlatforms(19)= "Microsoft SQL SERVER 7.x "
	dbPlatforms(20)= "Oracle 8.X "
	dbPlatforms(21)= "Sybase ASA 6.0 "
	dbPlatforms(22)= "Sybase ASE 11.9 "
	dbPlatforms(23)= "Sybase ASE 11.5 "
	dbPlatforms(24)= "IBM DB2 UDB 6.x "
	dbPlatforms(25)= "IBM DB2 UDB for OS/390 6.x "
	dbPlatforms(26)= "IBM DB2 UDB for OS/390 5.x "
	dbPlatforms(27)= "Microsoft SQL Server 2000 "
	dbPlatforms(28)= "Microsoft Access 2000 "
	dbPlatforms(29)= "Sybase ASE 12.0 "
	dbPlatforms(30)= "IBM DB2 UDB 7.x "
	dbPlatforms(31)= "Hitachi HiRDB "
	dbPlatforms(32)= "IBM DB2 UDB for OS/390 7.x "
	dbPlatforms(33)= "Oracle 9i "
	dbPlatforms(34)= "IBM DB2 AS/400 4.x "
	dbPlatforms(35)= "Sybase Adaptive Server IQ 12.5 "
	dbPlatforms(36)= "Generic DBMS "
	dbPlatforms(37)= "Sybase ASE 12.5 "
	dbPlatforms(38)= "Sybase ASA 7.0 "
	dbPlatforms(39)= "Sybase ASA 8.0 "
	dbPlatforms(40)= "NCR Teradata V2R4 "
	dbPlatforms(41)= "MySQL 3.x "
	dbPlatforms(42)= "IBM DB2 UDB 8.x "
	dbPlatforms(43)= "MySQL 4.x "
	dbPlatforms(44)= "NCR Teradata V2R5 "
	dbPlatforms(45)= "IBM DB2 AS/400 5.x "
	dbPlatforms(46)= "Sybase ASA 9.0 "
	dbPlatforms(47)= "Informix 9.x "
	dbPlatforms(48)= "Oracle 10g "
	dbPlatforms(49)= "Microsoft SQL Server 2005 "
	dbPlatforms(50)= "IBM DB2 UDB for OS/390 8.x "
	dbPlatforms(51)= "Generic ODBC "
	dbPlatforms(52)= "NCR Teradata V2R6 "
	dbPlatforms(53)= "PostgreSQL 8.0 "
	dbPlatforms(54)= "MySQL 5.x "
	dbPlatforms(55)= "IBM DB2 UDB 9.x "
	dbPlatforms(56)= "Sybase ASE 15 "
	dbPlatforms(57)= "Oracle 11g "
	dbPlatforms(58)= "IBM DB2 for OS/390 9.x "
	dbPlatforms(59)= "Sybase ASA 10.0 "
	dbPlatforms(60)= "InterBase 2007 "
	dbPlatforms(61)= "Microsoft SQL Server 2008 "
	dbPlatforms(62)= "InterBase 2009 "
	dbPlatforms(63)= "NCR Teradata 12.0 "
	dbPlatforms(64)= "Netezza 4.6 "
	dbPlatforms(65)= "Netezza 5.0 "
	dbPlatforms(66)= "NCR Teradata 13.x "
	dbPlatforms(67)= "Netezza 6.0 "
	dbPlatforms(68)= "Microsoft SQL Server 2012 "
	dbPlatforms(69)= "IBM DB2 UDB for OS/390 10.x "
	dbPlatforms(70)= "PostgreSQL 9.x  "
	dbPlatforms(71)= "GreenPlum 4.x "
	dbPlatforms(72)= "NCR Teradata 14.0 "
	dbPlatforms(73)= "Netezza 7.0 "
	dbPlatforms(74)= "IBM DB2 LUW 10.x "
	dbPlatforms(75)= "InterBase XE  "
	dbPlatforms(76)= "InterBase XE3 "
	dbPlatforms(77)= "Firebird 1.5 "
	dbPlatforms(78)= "Firebird 2.x "
	dbPlatforms(79)= "Oracle 12c "
	dbPlatforms(80)= "Microsoft SQL Server 2014"
End Function
Function ShellBrowseForFolder() As String

	Const BIF_RETURNONLYFSDIRS As Long = &H1

    Dim objShell As Object
    Dim objFolder As Object

    Set objShell = CreateObject("shell.application")
    Set objFolder = objShell.BrowseForFolder(0, "SQL File Folder",  BIF_RETURNONLYFSDIRS, "")
    If (Not objFolder Is Nothing) Then
    	ShellBrowseForFolder = objFolder.Items.Item.Path
    Else
    	ShellBrowseForFolder = ""
    End If
End Function
