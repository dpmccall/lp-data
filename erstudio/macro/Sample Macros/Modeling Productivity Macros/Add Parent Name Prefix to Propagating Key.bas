'MACRO TITLE: ADD PARENT NAME PREFIX TO PROPAGATING KEY MACRO
'This macro will add the entity name as a prefix to all attribute
'role names. It demonstrates how to use the FKColumnPair object.
'-----------------------------------------------------------------

Sub Main
	Dim MyDiagram As Diagram
	Dim MyModel As Model
	Dim MyEntity As Entity
	Dim MyRelationship As Relationship
	Dim MyFKColumnPair As FKColumnPair
	Dim ParentAttribute As AttributeObj
	Dim ChildAttribute As AttributeObj
	
	Dim Logical As Boolean
	Dim RoleName As String
	Dim NewRoleName As String
	Dim Prefix As String
	Dim ObjectName As String

	'Get the current diagram.
	
	Set MyDiagram = DiagramManager.ActiveDiagram
	
	'Get the current model.
	
	Set MyModel = MyDiagram.ActiveModel

	'Determine if the model is logical or physical.
	
	Logical = MyModel.Logical

	'Iterate through all entities in the model.
	
	For Each MyEntity In MyModel.Entities
	
		'If the model is logical, then get the entity
		'name. Otherwise, get the table name.
		
		If Logical = True Then
			ObjectName = MyEntity.EntityName
		Else
			ObjectName = MyEntity.TableName
		End If
		
		'Now iterate through all the parent relationships
		'of the entity.
		
		For Each MyRelationship In MyEntity.ParentRelationships
		
			'Iterate through all the foreign key column pairs in
			'the relationship.
			
			For Each MyFKColumnPair In MyRelationship.FKColumnPairs
			
				'Get the child attribute of the column pair.
				
				Set ChildAttribute = MyFKColumnPair.ChildAttribute

				'Get the role name of the child attribute.
				
				RoleName = ChildAttribute.RoleName
				
				'Add the entity name to the beginning of the role
				'name.  Update the child attribute to have the
				'new role name.
				
				NewRoleName = ObjectName + "_" + RoleName
				ChildAttribute.RoleName = NewRoleName
				
			Next MyFKColumnPair
			
		Next MyRelationship		
		
	Next MyEntity
	
End Sub
