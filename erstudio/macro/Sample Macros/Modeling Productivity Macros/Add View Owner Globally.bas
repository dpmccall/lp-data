'TITLE:  ADD VIEW OWNER GLOBALLY
'DESCRIPTION:  For physical models, this will update the owner field
'	in the view editor for all tables in the active model.  The
'	active model must be a physical model.


Sub Main

	Dim diag As Diagram
	Dim mdl As Model
	Dim vw As View
	Dim so As SelectedObject


	Set diag = DiagramManager.ActiveDiagram
	Set mdl = diag.ActiveModel


	If mdl.Logical = False Then


		Begin Dialog UserDialog 340,147,"Set Owner For all Views" ' %GRID:10,7,1,1
			TextBox 120,42,150,21,.Owner
			Text 50,49,50,14,"Owner:",.Text1
			OKButton 80,98,90,21
			CancelButton 210,98,90,21
		End Dialog
		Dim dlg As UserDialog
		

		If Dialog(dlg) = -1 Then

			'loop through selected objects
			For Each vw In mdl.Views

				'update table owner from dialog input
				vw.Owner = dlg.Owner

			Next

		End If

	Else

		MsgBox("Active model must be a physical model.",vbOkOnly,"ERROR!")

	End If


End Sub
