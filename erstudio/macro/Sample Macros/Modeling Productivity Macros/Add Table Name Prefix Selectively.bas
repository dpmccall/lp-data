' 'MACRO TITLE: ADD TABLE NAME PREFIX SELECTIVELY MACRO
' This macro lets the user add a prefix string to the names of 
' selected entities in the model.  The macro opens a dialog to 
' prompting the user for the prefix.  If the current model is 
' logical, it assigns the prefix to selected entity names.  If
' the current model is physical, it will assign the prefix to 
' selected table names.
'-------------------------------------------------------------------

Sub Main
	Dim MyDiagram As Diagram
	Dim MyModel As Model
	Dim MyEntity As Entity
	Dim MySubModel As SubModel
	Dim MySelObject As SelectedObject
	Dim MyEntities As Entities
	Dim EntityName As String
	Dim NewEntityName As String
	Dim Prefix As String
	Dim ID As Integer
	Dim ObjType As Integer
	Dim Logical As Boolean
	
	' Create the dialog box.  This dialog box was
	' created using the Sax Basic Dialog Editor.

	Begin Dialog UserDialog 300,105 ' %GRID:10,7,1,1
		Text 40,14,160,14,"Entity Name Prefix:",.Text1
		TextBox 40,35,220,21,.Prefix
		OKButton 100,70,90,21
	End Dialog
	
	Dim dlg As UserDialog
	
	' Run the dialog box.
	
	Dialog dlg
	
	' Get the prefix from the dialog box.
	
	Prefix = dlg.Prefix
	
	' Get the current diagram.
	
	Set MyDiagram = DiagramManager.ActiveDiagram
	
	' Get the current model.

	Set MyModel = MyDiagram.ActiveModel

	'Determine if the model is logical or physical.
	
	Logical = MyModel.Logical
	
	' Get the current submodel.  Only submodels contain
	' selected objects.
	
	Set MySubModel = MyModel.ActiveSubModel

	' Iterate through all the selected objects in the current
	' submodel.
	
	For Each MySelObject In MySubModel.SelectedObjects
	
		'Get the object type - we are only concerned
		'with entities.
		
		ObjType = MySelObject.Type
		
		If ObjType = 1 Then
		
			' Get the ID for the selected object.
		
			ID = MySelObject.ID
		
			' Get the actual entity object with this ID. 
			' The model contains the collection of all the
			' entities.
		
			Set MyEntity = MyModel.Entities.Item(ID)
	
			If Logical = True Then
		
				' If the model is logical, get the entity 
				' name, add the prefix to the entity name and
				' set the new name back into the entity.

				ObjectName = MyEntity.EntityName
				NewObjectName = Prefix + ObjectName
				MyEntity.EntityName = NewObjectName
			Else
		
				' If the model is physical, get the table 
				' name, add the prefix to the table name and
				' set the new name back into the entity.
	
				ObjectName = MyEntity.TableName
				NewObjectName = Prefix + ObjectName
				MyEntity.TableName = NewObjectName
			End If
			
		End If
		
	Next MySelObject
	
End Sub
