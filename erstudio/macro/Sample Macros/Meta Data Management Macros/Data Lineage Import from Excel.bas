'TITLE:  DATA LINEAGE IMPORT FROM EXCEL.BAS
'DESCRIPTION:  This macro imports data lineage from Excel.  The data lineage mappings
'	will be imported into the active physical model.  The first 6 columns of the sheet are used
'	to look up the column in the model for the mapping.  The columns need to exist in the model.
'	Otherwise the mappings will not be imported.  The mappings start in column 8.  What is
'	selected in the macro UI determines what type mappings are imported.  The order is:

'			1 - Direct Source Mappings
'			2 - Secondary Source Mappings
'			3 - Direct Target Mappings
'			4 - Secondary Target Mappings

'	if all are selected the start columns for the  mappings will be:

'			Direct Source Mappings (8)
'			Secondary Source Mappings (12)
'			Direct Target Mappings (20)
'			Secondary Target Mappings (26)

'	To get a sample of the spreadsheet, run the macro and select "Get Sample Sheet" or
'	run the "Data Lineage Export to Excel.bas" macro with all mappings selected.

' CREATE DATE:  7/8/2006
'
' LAST UPDATE:  4/21/2014
' VERSION 1.2
' - Fixed the macro so as to locate  table and column names referenced by either name. 
' - Writes log to user's My Document's directory since root of C has restricted access.
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


'ER/Studio Variables
Dim diag As Diagram
Dim mdl As Model
Dim ent As Entity
Dim attr As AttributeObj
Dim dmcl As DataMovementColumnLink
Dim dmml As DataMovementModelLink
Dim dlsrc As DataLineageSource
Dim dltab As DataLineageTable
Dim dlcol As DataLineageColumn



' Dim MS Excel variables.
	
Dim Excel As Object
Dim curRow As Integer
Dim curCol As Integer
Dim clrBack As Variant
Dim clrFore As Variant
Dim clrTitleBack As Variant
Dim clrTitleFore As Variant

Public Const CLR_WHITE = RGB(255, 255, 255)
Public Const CLR_BLACK = RGB(0, 0, 0)
Public Const CLR_GREY = RGB(192, 192, 192)
Public Const CLR_TEAL = RGB(0, 128, 128)

Type OptionSet
	chbx As Boolean
	startnum As Integer
End Type

Dim ImportOptions (0 To 3) As OptionSet



Sub Main
	
	Dim dom As Domain
	Dim Excel As Object
	Dim errorStr_Log As String


	Debug.Clear

	Set Excel = Nothing
	Set diag = DiagramManager.ActiveDiagram

	Begin Dialog UserDialog 630,357,"Import Data Lineage From Excel",.DialogFunc ' %GRID:10,7,1,1
		Text 30,21,210,14,"Path to binding info spreadsheet:",.Text1
		TextBox 70,42,440,21,.Path
		OKButton 350,308,110,28
		CancelButton 490,308,110,28
		PushButton 530,42,60,21,"Browse",.Browse
		TextBox 150,77,360,21,.diagram_name_txt
		TextBox 150,112,360,21,.filename_txt
		Text 40,84,100,14,"Active Diagram",.Text2
		Text 70,119,60,14,"Filename",.Text4
		CheckBox 40,259,320,21,"Log errors",.log_errors_chbx
		PushButton 50,301,180,35,"Get Sample",.getsample
		GroupBox 30,147,570,98,"Select Mappings to Import",.GroupBox1
		CheckBox 70,175,200,14,"Direct Source",.ds_chbx
		CheckBox 70,203,220,14,"Secondary Source",.ss_chbx
		CheckBox 330,175,180,14,"Direct Target",.dt_chbx
		CheckBox 330,203,150,14,"Secondary Target",.st_chbx
	End Dialog


	Dim dlg As UserDialog

	'initialize dialog defaults, 0 = unchecked, 1 = checked
	dlg.log_errors_chbx = 1

	clrBack = CLR_WHITE
	clrFore = CLR_BLACK
	clrTitleBack = CLR_GREY
	clrTitleFore = CLR_BLACK


	'start_dialog:
	
	'start dialog
	If (Dialog(dlg) = -1) Then


		On Error GoTo errorHandler


		'initialize dialog option array
		ImportOptions(0).chbx = dlg.ds_chbx
		ImportOptions(1).chbx = dlg.ss_chbx
		ImportOptions(2).chbx = dlg.dt_chbx
		ImportOptions(3).chbx = dlg.st_chbx

		ImportOptions(0).startnum = 8
		ImportOptions(1).startnum = 8
		ImportOptions(2).startnum = 8
		ImportOptions(3).startnum = 8

		If ImportOptions(0).chbx Then
			ImportOptions(1).startnum = ImportOptions(0).startnum + 4
			ImportOptions(2).startnum = ImportOptions(0).startnum + 4
			ImportOptions(3).startnum = ImportOptions(0).startnum + 4
		End If

		If ImportOptions(1).chbx Then
			ImportOptions(2).startnum = ImportOptions(1).startnum + 4
			ImportOptions(3).startnum = ImportOptions(1).startnum + 4
		End If

		If ImportOptions(2).chbx Then
			ImportOptions(3).startnum = ImportOptions(2).startnum + 4
		End If

		Debug.Print ImportOptions(0).chbx & " " & ImportOptions(1).chbx & " " & ImportOptions(2).chbx & " " & ImportOptions(3).chbx & " "
		Debug.Print ImportOptions(0).startnum & " " & ImportOptions(1).startnum & " " & ImportOptions(2).startnum & " " & ImportOptions(3).startnum & " "


		'initialize excel object and make visible
		Set Excel = CreateObject("Excel.Application")

		Excel.workbooks.open dlg.Path
	

		'define excel variables
		Dim sheet As Object
		Dim range As Object
	
		'range variables for loop
		Dim binding_count As Integer
		Dim start_range As Integer
	
		'get sheet info from excel object
		Set sheet = Excel.worksheets(1)
		Set range = sheet.usedrange
	
		'get count for loop
		binding_count = range.rows.Count
		start_range = 3			'ignore first three rows of the sheet.

		'input diagram and model name in error log
		errorStr_Log = "Diagram Name:       " & diag.ProjectName & vbCrLf
		errorStr_Log = errorStr_Log & "File Name:          " & diag.FileName & vbCrLf & vbCrLf & vbCrLf & vbCrLf

	
		For i = start_range To binding_count

			'make sure that the right data is initialized from the spread sheet.
			'model name is first column (1)
			'table name is second column (2)
			'column name is third column (3)

			Dim entity_name As String
			Dim attribute_name As String
			Dim model_name As String

			Dim ds_mapping_description As String
			Dim ds_mapping_logic As String
			Dim ds_columns As String


			Dim dt_mapping_description As String
			Dim dt_mapping_logic As String
			Dim dt_columns As String


			Dim ss_mapping_description As String
			Dim ss_mapping_logic As String
			Dim ss_columns As String


			Dim st_mapping_description As String
			Dim st_mapping_logic As String
			Dim st_columns As String


			'initialize string variables with data from spread sheet.
			model_name = Trim(range.cells(i,1).Value)
			entity_name = Trim(range.cells(i, 2).Value)
			attribute_name = Trim(range.cells(i, 3).Value)

			'direct source mappings
			ds_columns = Trim(range.cells(i, ImportOptions(0).startnum).Value)
			ds_mapping_logic = Trim(range.cells(i, ImportOptions(0).startnum + 1).Value)
			ds_mapping_description = Trim(range.cells(i, ImportOptions(0).startnum + 2).Value)

			'secondary source mappings
			ss_columns = Trim(range.cells(i, ImportOptions(1).startnum).Value)
			ss_mapping_logic = Trim(range.cells(i, ImportOptions(1).startnum + 1).Value)
			ss_mapping_description = Trim(range.cells(i, ImportOptions(1).startnum + 2).Value)

			'direct target mappings
			dt_columns = Trim(range.cells(i, ImportOptions(2).startnum).Value)
			dt_mapping_logic = Trim(range.cells(i, ImportOptions(2).startnum + 1).Value)
			dt_mapping_description = Trim(range.cells(i, ImportOptions(2).startnum + 2).Value)
			
			'secondary target mappings
			st_columns = Trim(range.cells(i, ImportOptions(3).startnum).Value)
			st_mapping_logic = Trim(range.cells(i, ImportOptions(3).startnum + 1).Value)
			st_mapping_description = Trim(range.cells(i, ImportOptions(3).startnum + 2).Value)


			Set mdl = diag.Models.Item(model_name)

			If mdl Is Nothing Then

				errorStr_Log = errorStr_Log & "Row < " & i & " > -  Model  <" & model_name & "> does not exist." & vbCrLf & vbCrLf

			Else

				Set ent = GetEntity(mdl, entity_name)

				If ent Is Nothing Then

					'log missing entity in the error string
					errorStr_Log = errorStr_Log & "Row < " & i & " > -  Table  <" & entity_name & "> does not exist." & vbCrLf & vbCrLf

				Else

					Set attr = GetAttribute(ent, attribute_name)

					If attr Is Nothing Then

						'log missing attribute in the error string
						errorStr_Log = errorStr_Log & "Row < " & i & " > -  Column  <" & entity_name & "." & attribute_name & "> does not exist." & vbCrLf & vbCrLf
	
					Else

						
						'dim parsing variables
						Dim comma_position As Integer
						Dim dot_position As Integer
						Dim mappings As String
						Dim mdl_name As String
						Dim tab_name As String
						Dim col_name As String

						Dim src_col_id As Long
						Dim src_tab_id As Long
						Dim src_mdl As Model
						Dim src_ent As Entity
						Dim src_attr As AttributeObj


						'Import direct source mappings

						If ImportOptions(0).chbx Then

							mappings = ds_columns

							If mappings <> "" Then

								Do  'need to parse mapping string
		
									'get model name
									dot_position = InStr(mappings,".")
									mdl_name = Left(mappings, dot_position - 1)
									mappings = Right(mappings, Len(mappings) - dot_position)
		
									'get table name
									dot_position = InStr(mappings,".")
									tab_name = Left(mappings, dot_position - 1)
									mappings = Right(mappings, Len(mappings) - dot_position)
		
									comma_position = InStr(mappings,",")
		
									'move to next mapping
									If comma_position <> 0 Then
										col_name = Left(mappings, comma_position - 1)
										mappings = Right(mappings, Len(mappings) - comma_position)
									Else
										col_name = mappings
									End If

									mdl_name = Trim(mdl_name)
									tab_name = Trim(tab_name)
									col_name = Trim(col_name)

									'does a model link as a direct source exist
									Set dmml = mdl.DataMovementModelLinks.Item(mdl_name)

									If dmml Is Nothing Then

										If IsPhysicalModel(mdl_name) = False And IsSourceModel(mdl_name) = False Then

											Set dlsrc = diag.DataLineageSources.Add(mdl_name, 1)
											Set dmml = mdl.DataMovementModelLinks.Add(dlsrc.Name, False, False)
											
											Set dltab = dlsrc.DataLineageTables.Add(tab_name)
											Set dlcol = dltab.DataLineageColumns.Add(col_name, False)

											src_tab_id = dltab.ID
											src_col_id = dlcol.ID

										Else


											Set dmml = mdl.DataMovementModelLinks.Add(mdl_name, False, False)

											'set it to a physical model
											Set src_mdl = diag.Models.Item(dmml.SrcTrgtModelName)

											'if source model in nothing that means the source is a data lineage source/target
											If src_mdl Is Nothing Then

												Set dlsrc = diag.DataLineageSources.Item(dmml.SrcTrgtModelName)

												Set dltab = dlsrc.DataLineageTables.Item(tab_name)

												'if the data lineage table object is nothing then it needs to be created
												If dltab Is Nothing Then

													Set dltab = dlsrc.DataLineageTables.Add(tab_name)

												End If

												Set dlcol = dltab.DataLineageColumns.Item(col_name)

												'if the data lineage column object is nothing then it needs to be created
												If dlcol Is Nothing Then

													Set dlcol = dltab.DataLineageColumns.Add(col_name, False)

												End If

												src_tab_id = dltab.ID
												src_col_id = dlcol.ID

											Else

												Set src_ent = src_mdl.Entities.Item(tab_name)
												Set src_attr = src_ent.Attributes.Item(col_name)

												If src_ent Is Nothing Or src_attr Is Nothing Then

													errorStr_Log = errorStr_Log & "Row < " & i & " > -  Table  <" & tab_name & "> or Column <" & col_name & "> does not exist in physical model referenced as a direct source." & vbCrLf & vbCrLf

												Else
												
													src_tab_id = src_ent.ID
													src_col_id = src_attr.ID

												End If

											End If

										End If

										'add column link
										attr.DataMovementColumnLinks.AddV2(src_col_id, src_tab_id, False, False)

									Else 'model link exists...

										'set it to a physical model
										Set src_mdl = diag.Models.Item(dmml.SrcTrgtModelName)

										'if source model in nothing that means the source is a data lineage source/target
										If src_mdl Is Nothing Then

											Set dlsrc = diag.DataLineageSources.Item(dmml.SrcTrgtModelName)

											Set dltab = dlsrc.DataLineageTables.Item(tab_name)

											'if the data lineage table object is nothing then it needs to be created
											If dltab Is Nothing Then

												Set dltab = dlsrc.DataLineageTables.Add(tab_name)

											End If

											Set dlcol = dltab.DataLineageColumns.Item(col_name)

											'if the data lineage column object is nothing then it needs to be created
											If dlcol Is Nothing Then

												Set dlcol = dltab.DataLineageColumns.Add(col_name, False)

											End If

											src_tab_id = dltab.ID
											src_col_id = dlcol.ID


										Else

											Set src_ent = src_mdl.Entities.Item(tab_name)

											If src_ent Is Nothing Then

												errorStr_Log = errorStr_Log & "Row < " & i & " > -  Table <" & tab_name & "> does not exist in physical model referenced as a direct source." & vbCrLf & vbCrLf

											Else

												Set src_attr = src_ent.Attributes.Item(col_name)

												If src_attr Is Nothing Then

													errorStr_Log = errorStr_Log & "Row < " & i & " > -  Column <" & tab_name & ">.<" & col_name & "> does not exist in physical model referenced as a direct source." & vbCrLf & vbCrLf

												Else
												
													src_tab_id = src_ent.ID
													src_col_id = src_attr.ID

												End If

											End If

										End If

										'add column link
										attr.DataMovementColumnLinks.AddV2(src_col_id, src_tab_id, False, False)
										

									End If ' check for existence of model link

								Loop While comma_position


							End If 'mappings exist check

							'import logic and description
							attr.SourceDirectTransformationDescription = ds_mapping_description
							attr.SourceDirectTransformationLogic = ds_mapping_logic


						End If ' direct source mappings option checked

						If ImportOptions(1).chbx Then

							mappings = ss_columns

							If mappings <> "" Then

							Do

								'get model name
								dot_position = InStr(mappings,".")
								mdl_name = Left(mappings, dot_position - 1)
								mappings = Right(mappings, Len(mappings) - dot_position)

								'get table name
								dot_position = InStr(mappings,".")
								tab_name = Left(mappings, dot_position - 1)
								mappings = Right(mappings, Len(mappings) - dot_position)

								comma_position = InStr(mappings,",")

								'move to next mapping
								If comma_position <> 0 Then
									col_name = Left(mappings, comma_position - 1)
									mappings = Right(mappings, Len(mappings) - comma_position)
								Else
									col_name = mappings
								End If

									mdl_name = Trim(mdl_name)
									tab_name = Trim(tab_name)
									col_name = Trim(col_name)


									'does a model link as a direct source exist
									Set dmml = mdl.DataMovementModelLinks.Item(mdl_name)

									If dmml Is Nothing Then

										If IsPhysicalModel(mdl_name) = False And IsSourceModel(mdl_name) = False Then

											Set dlsrc = diag.DataLineageSources.Add(mdl_name, 1)
											Set dmml = mdl.DataMovementModelLinks.Add(dlsrc.Name, False, True)
											
											Set dltab = dlsrc.DataLineageTables.Add(tab_name)
											Set dlcol = dltab.DataLineageColumns.Add(col_name, False)

											src_tab_id = dltab.ID
											src_col_id = dlcol.ID

										Else


											Set dmml = mdl.DataMovementModelLinks.Add(mdl_name, False, True)

											'set it to a physical model
											Set src_mdl = diag.Models.Item(dmml.SrcTrgtModelName)
	
											'if source model in nothing that means the source is a data lineage source/target
											If src_mdl Is Nothing Then
	
												Set dlsrc = diag.DataLineageSources.Item(dmml.SrcTrgtModelName)
	
												Set dltab = dlsrc.DataLineageTables.Item(tab_name)
	
												'if the data lineage table object is nothing then it needs to be created
												If dltab Is Nothing Then
	
													Set dltab = dlsrc.DataLineageTables.Add(tab_name)
	
												End If
	
												Set dlcol = dltab.DataLineageColumns.Item(col_name)
	
												'if the data lineage column object is nothing then it needs to be created
												If dlcol Is Nothing Then
	
													Set dlcol = dltab.DataLineageColumns.Add(col_name, False)
	
												End If

												src_tab_id = dltab.ID
												src_col_id = dlcol.ID

											Else

												Set src_ent = src_mdl.Entities.Item(tab_name)

												If src_ent Is Nothing Then
	
													errorStr_Log = errorStr_Log & "Row < " & i & " > -  Table <" & tab_name & "> does not exist in physical model referenced as a secondary source." & vbCrLf & vbCrLf
	
												Else
	
													Set src_attr = src_ent.Attributes.Item(col_name)
	
													If src_attr Is Nothing Then
	
														errorStr_Log = errorStr_Log & "Row < " & i & " > -  Column <" & tab_name & ">.<" & col_name & "> does not exist in physical model referenced as a secondary source." & vbCrLf & vbCrLf
	
													Else
													
														src_tab_id = src_ent.ID
														src_col_id = src_attr.ID
	
													End If
	
												End If

											End If

										End If

										'add column link
										attr.DataMovementColumnLinks.AddV2(src_col_id, src_tab_id, False, True)

									Else 'model link exists...

										'set it to a physical model
										Set src_mdl = diag.Models.Item(dmml.SrcTrgtModelName)

										'if source model in nothing that means the source is a data lineage source/target
										If src_mdl Is Nothing Then

											Set dlsrc = diag.DataLineageSources.Item(dmml.SrcTrgtModelName)

											Set dltab = dlsrc.DataLineageTables.Item(tab_name)

											'if the data lineage table object is nothing then it needs to be created
											If dltab Is Nothing Then

												Set dltab = dlsrc.DataLineageTables.Add(tab_name)

											End If

											Set dlcol = dltab.DataLineageColumns.Item(col_name)

											'if the data lineage column object is nothing then it needs to be created
											If dlcol Is Nothing Then

												Set dlcol = dltab.DataLineageColumns.Add(col_name, False)

											End If

											src_tab_id = dltab.ID
											src_col_id = dlcol.ID

										Else

											Set src_ent = src_mdl.Entities.Item(tab_name)

											If src_ent Is Nothing Then

												errorStr_Log = errorStr_Log & "Row < " & i & " > -  Table <" & tab_name & "> does not exist in physical model referenced as a secondary source." & vbCrLf & vbCrLf

											Else

												Set src_attr = src_ent.Attributes.Item(col_name)

												If src_attr Is Nothing Then

													errorStr_Log = errorStr_Log & "Row < " & i & " > -  Column <" & tab_name & ">.<" & col_name & "> does not exist in physical model referenced as a secondary source." & vbCrLf & vbCrLf

												Else
												
													src_tab_id = src_ent.ID
													src_col_id = src_attr.ID

												End If

											End If

										End If

										'add column link
										attr.DataMovementColumnLinks.AddV2(src_col_id, src_tab_id, False, True)

									End If ' check for existence of model link

							Loop While comma_position

							End If 'mappings exist check

							'import logic and description
							attr.SourceSecondaryTransformationDescription = ss_mapping_description
							attr.SourceSecondaryTransformationLogic = ss_mapping_logic

						End If ' secondary source mappings option checked


						If ImportOptions(2).chbx Then

							mappings = dt_columns

							If mappings <> "" Then

							Do

								'get model name
								dot_position = InStr(mappings,".")
								mdl_name = Left(mappings, dot_position - 1)
								mappings = Right(mappings, Len(mappings) - dot_position)

								'get table name
								dot_position = InStr(mappings,".")
								tab_name = Left(mappings, dot_position - 1)
								mappings = Right(mappings, Len(mappings) - dot_position)

								comma_position = InStr(mappings,",")

								'move to next mapping
								If comma_position <> 0 Then
									col_name = Left(mappings, comma_position - 1)
									mappings = Right(mappings, Len(mappings) - comma_position)
								Else
									col_name = mappings
								End If

									mdl_name = Trim(mdl_name)
									tab_name = Trim(tab_name)
									col_name = Trim(col_name)

									'does a model link as a direct source exist
									Set dmml = mdl.DataMovementModelLinks.Item(mdl_name)

									If dmml Is Nothing Then

										If IsPhysicalModel(mdl_name) = False And IsSourceModel(mdl_name) = False Then

											Set dlsrc = diag.DataLineageSources.Add(mdl_name, 1)
											Set dmml = mdl.DataMovementModelLinks.Add(dlsrc.Name, True, False)
											
											Set dltab = dlsrc.DataLineageTables.Add(tab_name)
											Set dlcol = dltab.DataLineageColumns.Add(col_name, False)

											src_tab_id = dltab.ID
											src_col_id = dlcol.ID

										Else


											Set dmml = mdl.DataMovementModelLinks.Add(mdl_name, True, False)

											'set it to a physical model
											Set src_mdl = diag.Models.Item(dmml.SrcTrgtModelName)
	
											'if source model in nothing that means the source is a data lineage source/target
											If src_mdl Is Nothing Then
	
												Set dlsrc = diag.DataLineageSources.Item(dmml.SrcTrgtModelName)
	
												Set dltab = dlsrc.DataLineageTables.Item(tab_name)
	
												'if the data lineage table object is nothing then it needs to be created
												If dltab Is Nothing Then
	
													Set dltab = dlsrc.DataLineageTables.Add(tab_name)
	
												End If
	
												Set dlcol = dltab.DataLineageColumns.Item(col_name)
	
												'if the data lineage column object is nothing then it needs to be created
												If dlcol Is Nothing Then
	
													Set dlcol = dltab.DataLineageColumns.Add(col_name, False)
	
												End If

												src_tab_id = dltab.ID
												src_col_id = dlcol.ID

											Else

												Set src_ent = src_mdl.Entities.Item(tab_name)

												If src_ent Is Nothing Then
	
													errorStr_Log = errorStr_Log & "Row < " & i & " > -  Table <" & tab_name & "> does not exist in physical model referenced as a direct target." & vbCrLf & vbCrLf
	
												Else
	
													Set src_attr = src_ent.Attributes.Item(col_name)
	
													If src_attr Is Nothing Then
	
														errorStr_Log = errorStr_Log & "Row < " & i & " > -  Column <" & tab_name & ">.<" & col_name & "> does not exist in physical model referenced as a direct target." & vbCrLf & vbCrLf
	
													Else
													
														src_tab_id = src_ent.ID
														src_col_id = src_attr.ID
	
													End If
	
												End If

											End If

										End If

										'add column link
										attr.DataMovementColumnLinks.AddV2(src_col_id, src_tab_id, True, False)

									Else 'model link exists...

										'set it to a physical model
										Set src_mdl = diag.Models.Item(dmml.SrcTrgtModelName)

										'if source model in nothing that means the source is a data lineage source/target
										If src_mdl Is Nothing Then

											Set dlsrc = diag.DataLineageSources.Item(dmml.SrcTrgtModelName)

											Set dltab = dlsrc.DataLineageTables.Item(tab_name)

											'if the data lineage table object is nothing then it needs to be created
											If dltab Is Nothing Then

												Set dltab = dlsrc.DataLineageTables.Add(tab_name)

											End If

											Set dlcol = dltab.DataLineageColumns.Item(col_name)

											'if the data lineage column object is nothing then it needs to be created
											If dlcol Is Nothing Then

												Set dlcol = dltab.DataLineageColumns.Add(col_name, False)

											End If

											src_tab_id = dltab.ID
											src_col_id = dlcol.ID

										Else

											Set src_ent = src_mdl.Entities.Item(tab_name)

											If src_ent Is Nothing Then

												errorStr_Log = errorStr_Log & "Row < " & i & " > -  Table <" & tab_name & "> does not exist in physical model referenced as a direct target." & vbCrLf & vbCrLf

											Else

												Set src_attr = src_ent.Attributes.Item(col_name)

												If src_attr Is Nothing Then

													errorStr_Log = errorStr_Log & "Row < " & i & " > -  Column <" & tab_name & ">.<" & col_name & "> does not exist in physical model referenced as a direct target." & vbCrLf & vbCrLf

												Else
												
													src_tab_id = src_ent.ID
													src_col_id = src_attr.ID

												End If

											End If

										End If

										'add column link
										attr.DataMovementColumnLinks.AddV2(src_col_id, src_tab_id, True, False)

									End If ' check for existence of model link

							Loop While comma_position

							End If 'mappings exist check

							'import logic and description
							attr.TargetDirectTransformationDescription = dt_mapping_description
							attr.TargetDirectTransformationLogic = dt_mapping_logic


						End If ' direct target mappings option checked

						If ImportOptions(3).chbx Then

							mappings = st_columns

							If mappings <> "" Then

							Do

								'get model name
								dot_position = InStr(mappings,".")
								mdl_name = Left(mappings, dot_position - 1)
								mappings = Right(mappings, Len(mappings) - dot_position)

								'get table name
								dot_position = InStr(mappings,".")
								tab_name = Left(mappings, dot_position - 1)
								mappings = Right(mappings, Len(mappings) - dot_position)

								comma_position = InStr(mappings,",")

								'move to next mapping
								If comma_position <> 0 Then
									col_name = Left(mappings, comma_position - 1)
									mappings = Right(mappings, Len(mappings) - comma_position)
								Else
									col_name = mappings
								End If

									mdl_name = Trim(mdl_name)
									tab_name = Trim(tab_name)
									col_name = Trim(col_name)


									'does a model link as a direct source exist
									Set dmml = mdl.DataMovementModelLinks.Item(mdl_name)

									If dmml Is Nothing Then

										If IsPhysicalModel(mdl_name) = False And IsSourceModel(mdl_name) = False Then

											Set dlsrc = diag.DataLineageSources.Add(mdl_name, 1)
											Set dmml = mdl.DataMovementModelLinks.Add(dlsrc.Name, True, True)
											
											Set dltab = dlsrc.DataLineageTables.Add(tab_name)
											Set dlcol = dltab.DataLineageColumns.Add(col_name, False)

											src_tab_id = dltab.ID
											src_col_id = dlcol.ID

										Else


											Set dmml = mdl.DataMovementModelLinks.Add(mdl_name, True, True)

											'set it to a physical model
											Set src_mdl = diag.Models.Item(dmml.SrcTrgtModelName)
	
											'if source model in nothing that means the source is a data lineage source/target
											If src_mdl Is Nothing Then
	
												Set dlsrc = diag.DataLineageSources.Item(dmml.SrcTrgtModelName)
	
												Set dltab = dlsrc.DataLineageTables.Item(tab_name)
	
												'if the data lineage table object is nothing then it needs to be created
												If dltab Is Nothing Then
	
													Set dltab = dlsrc.DataLineageTables.Add(tab_name)
	
												End If
	
												Set dlcol = dltab.DataLineageColumns.Item(col_name)
	
												'if the data lineage column object is nothing then it needs to be created
												If dlcol Is Nothing Then
	
													Set dlcol = dltab.DataLineageColumns.Add(col_name, False)
	
												End If

												src_tab_id = dltab.ID
												src_col_id = dlcol.ID

											Else

												Set src_ent = src_mdl.Entities.Item(tab_name)

												If src_ent Is Nothing Then
	
													errorStr_Log = errorStr_Log & "Row < " & i & " > -  Table <" & tab_name & "> does not exist in physical model referenced as a secondary target." & vbCrLf & vbCrLf
	
												Else
	
													Set src_attr = src_ent.Attributes.Item(col_name)
	
													If src_attr Is Nothing Then
	
														errorStr_Log = errorStr_Log & "Row < " & i & " > -  Column <" & tab_name & ">.<" & col_name & "> does not exist in physical model referenced as a secondary target." & vbCrLf & vbCrLf
	
													Else
													
														src_tab_id = src_ent.ID
														src_col_id = src_attr.ID
	
													End If
	
												End If

											End If

										End If

										'add column link
										attr.DataMovementColumnLinks.AddV2(src_col_id, src_tab_id, True, True)

									Else 'model link exists...

										'set it to a physical model
										Set src_mdl = diag.Models.Item(dmml.SrcTrgtModelName)

										'if source model in nothing that means the source is a data lineage source/target
										If src_mdl Is Nothing Then

											Set dlsrc = diag.DataLineageSources.Item(dmml.SrcTrgtModelName)

											Set dltab = dlsrc.DataLineageTables.Item(tab_name)

											'if the data lineage table object is nothing then it needs to be created
											If dltab Is Nothing Then

												Set dltab = dlsrc.DataLineageTables.Add(tab_name)

											End If

											Set dlcol = dltab.DataLineageColumns.Item(col_name)

											'if the data lineage column object is nothing then it needs to be created
											If dlcol Is Nothing Then

												Set dlcol = dltab.DataLineageColumns.Add(col_name, False)

											End If

											src_tab_id = dltab.ID
											src_col_id = dlcol.ID

										Else

											Set src_ent = src_mdl.Entities.Item(tab_name)

											If src_ent Is Nothing Then

												errorStr_Log = errorStr_Log & "Row < " & i & " > -  Table <" & tab_name & "> does not exist in physical model referenced as a secondary target." & vbCrLf & vbCrLf

											Else

												Set src_attr = src_ent.Attributes.Item(col_name)

												If src_attr Is Nothing Then

													errorStr_Log = errorStr_Log & "Row < " & i & " > -  Column <" & tab_name & ">.<" & col_name & "> does not exist in physical model referenced as a secondary target." & vbCrLf & vbCrLf

												Else
												
													src_tab_id = src_ent.ID
													src_col_id = src_attr.ID

												End If

											End If

										End If

										'add column link
										attr.DataMovementColumnLinks.AddV2(src_col_id, src_tab_id, True, True)

									End If ' check for existence of model link

							Loop While comma_position

							End If 'mappings exist check

							'import logic and description
							attr.TargetSecondaryTransformationDescription = st_mapping_description
							attr.TargetSecondaryTransformationLogic = st_mapping_logic

						End If ' secondary target mappings option checked



					End If 'attribute existence check

				End If	  'entity existence check

			End If		  'model existence check

		Next
	
		Excel.workbooks.Close
		Excel.Quit()

		If (dlg.log_errors_chbx = 1) Then

			Dim theLog As String
			Dim fileHandle As Long
			
			fileHandle = FreeFile
			
			errorLogFile = GetUserMyDocumentDirectory()
			If Right(errorLogFile, 1) <> "\" Then errorLogFile = errorLogFile + "\"
			errorLogFile = errorLogFile + "mappings_binding_log.txt"
			
			Open errorLogFile For Output As #fileHandle
			Print #fileHandle, errorStr_Log
			Close #fileHandle

		End If

		Dim msgStr As String
		msgStr = "Import of source and target mappings complete."
		If (dlg.log_errors_chbx = 1) Then
			msgStr = msgStr + vbCrLf + "Log written: " + errorLogFile
		End If
		MsgBox(msgStr,vbOkOnly,"ERStudio")

	End If

	Exit Sub


errorHandler:
	If (Excel <> Nothing) Then Excel.Quit()
	MsgBox(Err.Description, vbCritical)
End Sub

Function IsPhysicalModel( mName As String ) As Boolean

	Dim m As Model

	Set m = diag.Models.Item(mName)

	If m Is Nothing Then

		IsPhysicalModel = False

	Else

		IsPhysicalModel = True

	End If

End Function

Function IsSourceModel ( sName As String ) As Boolean

	Dim s As DataLineageSource

	Set s = diag.DataLineageSources.Item(sName)

	If s Is Nothing Then

		IsSourceModel = False

	Else

		IsSourceModel = True

	End If

End Function



Sub PrintSampleSheet (  )

	curRow = 3
	curCol = 1

    'initialize column width
    With Excel
		.columns("A").columnWidth = 12
		.columns("B:C").columnWidth = 20
		.columns("D").columnWidth = 10
		.columns("E").columnWidth = 7
		.columns("F").columnWidth = 10
	End With

	'Print Physical Model Header
	PrintCell "Physical Model Information", curRow - 1, curCol + 2, 0, 0, clrFore, clrBack, 14, False

	PrintCell "Model Name", curRow, curCol, 0, 0, clrFore, clrBack, 12, False
	PrintCell "Mandatory - must match info in dm1", curRow + 1, curCol, 0, 1, clrFore, clrBack, 10, False
	PrintCell "Table Name", curRow, curCol, 0, 0, clrFore, clrBack, 12, False
	PrintCell "Mandatory - must match info in dm1", curRow + 1, curCol, 0, 1, clrFore, clrBack, 10, False
	PrintCell "Column Name", curRow, curCol, 0, 0, clrFore, clrBack, 12, False
	PrintCell "Mandatory - must match info in dm1", curRow + 1, curCol, 0, 1, clrFore, clrBack, 10, False
	PrintCell "DataType", curRow, curCol, 0, 1, clrFore, clrBack, 12, False
	PrintCell "Null?", curRow, curCol, 0, 1, clrFore, clrBack, 12, False
	PrintCell "Primary?", curRow, curCol, 0, 1, clrFore, clrBack, 12, False

	Excel.columns(curCol).columnWidth = .5
	curCol = curCol + 1

		'print header for direct source mappings
		PrintCell "Direct Source Mappings", curRow - 1, curCol, 0, 0, clrFore, clrBack, 14, False

		PrintCell "Direct Source(s)", curRow, curCol, 0, 0, clrFore, clrBack, 12, False
		PrintCell "<model>.<table>.<column> separated by commas", curRow + 1, curCol, 0, 1, clrFore, clrBack, 10, False
		PrintCell "Direct Source Logic", curRow, curCol, 0, 1, clrFore, clrBack, 12, False
		PrintCell "Direct Source Description", curRow, curCol, 0, 1, clrFore, clrBack, 12, False

		Excel.columns(curCol).columnWidth = .5
		curCol = curCol + 1


		'print header for secondary source mappings
		PrintCell "Secondary Source Mappings", curRow - 1, curCol, 0, 0, clrFore, clrBack, 14, False

		PrintCell "Secondary Source(s)", curRow, curCol, 0, 0, clrFore, clrBack, 12, False
		PrintCell "<model>.<table>.<column> separated by commas", curRow + 1, curCol, 0, 1, clrFore, clrBack, 10, False
		PrintCell "Secondary Source Logic", curRow, curCol, 0, 1, clrFore, clrBack, 12, False
		PrintCell "Secondary Source Description", curRow, curCol, 0, 1, clrFore, clrBack, 12, False

		Excel.columns(curCol).columnWidth = .5
		curCol = curCol + 1


		'print header for direct target mappings
		PrintCell "Direct Target Mappings", curRow - 1, curCol, 0, 0, clrFore, clrBack, 14, False

		PrintCell "Direct Target(s)", curRow, curCol, 0, 0, clrFore, clrBack, 12, False
		PrintCell "<model>.<table>.<column> separated by commas", curRow + 1, curCol, 0, 1, clrFore, clrBack, 10, False
		PrintCell "Direct Target Logic", curRow, curCol, 0, 1, clrFore, clrBack, 12, False
		PrintCell "Direct Target Description", curRow, curCol, 0, 1, clrFore, clrBack, 12, False

		Excel.columns(curCol).columnWidth = .5
		curCol = curCol + 1

		'print header for secondary target mappings
		PrintCell "Secondary Target Mappings", curRow - 1, curCol, 0, 0, clrFore, clrBack, 14, False

		PrintCell "Secondary Target(s)", curRow, curCol, 0, 0, clrFore, clrBack, 12, False
		PrintCell "<model>.<table>.<column> separated by commas", curRow + 1, curCol, 0, 1, clrFore, clrBack, 10, False
		PrintCell "Secondary Target Logic", curRow, curCol, 0, 1, clrFore, clrBack, 12, False
		PrintCell "Secondary Target Description", curRow, curCol, 0, 1, clrFore, clrBack, 12, False

		Excel.columns(curCol).columnWidth = .5
		curCol = curCol + 1

	SetFreezeAndFilter Excel
	Excel.sheets("sheet1").columns.autofit
	Excel.Range("A2:V3").interior.Color = clrTitleBack


End Sub


Sub SetFreezeAndFilter ( wb As Object )

	With wb
	    .Columns("H:H").Select
	    .ActiveWindow.FreezePanes = True
	    .Rows("3:3").AutoFilter
	End With

End Sub

' Print a cell

Sub PrintCell(value As String, row As Integer, col As Integer, rowInc As Integer, colInc As Integer, clrFore As Variant, clrBack As Variant, szFont As Integer, bBold As Boolean)
	
	'sample
	'	PrintCell ent.EntityName, curRow, curCol, 0, 1, clrFore, clrBack, 10, False

	Excel.Cells(row, col).Value = value

	Excel.Cells(row, col).Font.Bold = bBold
	Excel.Cells(row, col).Font.Color = clrFore
	Excel.Cells(row, col).Font.Size = szFont

	curRow = curRow + rowInc
	curCol = curCol + colInc

End Sub

Rem See DialogFunc help topic for more information.
Private Function DialogFunc(DlgItem$, Action%, SuppValue&) As Boolean
	Select Case Action%
	Case 1 ' Dialog box initialization

		'make diagram and file name text blocks read only
		DlgEnable "diagram_name_txt", False
		DlgEnable "filename_txt", False

		'populate the diagram and file name text blocks with the appropiate data.
		DlgText "diagram_name_txt", diag.ProjectName
		DlgText "filename_txt", diag.FileName

	Case 2 ' Value changing or button pressed


		If DlgItem = "Browse" Then
			'browse to excel file if used pushes browse button.  Put path in text box.
			DlgText "path", GetFilePath(,"xls;xlsx",,"Open SpreadSheetle", 0)
			DialogFunc = True
		ElseIf DlgItem = "OK" And DlgText("path") = "" Then
			'don't exit dialog if a path is not specified
			MsgBox("Please enter a valid path.",,"Error!")
			DialogFunc = True
		ElseIf DlgItem = "getsample" Then
			Set Excel = CreateObject("Excel.Application")
			Excel.Visible = True
			Excel.Workbooks.Add
			PrintSampleSheet
			DialogFunc = True
		End If


		Rem DialogFunc = True ' Prevent button press from closing the dialog box
	Case 3 ' TextBox or ComboBox text changed
	Case 4 ' Focus changed
	Case 5 ' Idle
		Rem DialogFunc = True ' Continue getting idle actions
	Case 6 ' Function key
	End Select
End Function
Function GetEntity(theModel As Model, ByRef entityName As String) As Entity
	Dim theEntity As Entity

	Set theEntity = theModel.Entities(entityName)
	If (Not theEntity Is Nothing) Then
		Set GetEntity = theEntity
		Exit Function
	Else
		For Each theEntity In theModel.Entities
			If (StrComp(theEntity.EntityName, entityName, vbTextCompare) = 0) Or _
				(StrComp(theEntity.TableName, entityName, vbTextCompare) = 0) Then
				Set GetEntity = theEntity
				Exit Function
			End If
		Next theEntity
	End If
End Function
Function GetAttribute(theEntity As Entity, ByRef attributeName As String) As AttributeObj
	Dim theAttribute As AttributeObj

	Set theAttribute = theEntity.Attributes(attributeName)
	If (Not theAttribute Is Nothing) Then
		Set GetAttribute = theAttribute
		Exit Function
	Else
		For Each theAttribute In theEntity.Attributes
			If (StrComp(theAttribute.AttributeName, attributeName, vbTextCompare) = 0) Or _
				(StrComp(theAttribute.ColumnName, attributeName, vbTextCompare) = 0) Then
				Set GetAttribute = theAttribute
				Exit Function
			End If
		Next theAttribute
	End If
End Function
Function GetUserMyDocumentDirectory() As String
	' http://msdn.microsoft.com/en-us/library/0ea7b5xe.aspx
	Dim WshShell As Object
	Set WshShell = CreateObject("WScript.Shell")
	GetUserMyDocumentDirectory = WshShell.SpecialFolders("MyDocuments")
End Function
