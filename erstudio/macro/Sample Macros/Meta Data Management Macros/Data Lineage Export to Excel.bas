
'MACRO TITLE: DATA LINEAGE EXPORT TO EXCEL.BAS
' This macro will output the data lineage information to excel.  If
' the format of the spreadsheet is unchanged you can update the
' mappings and import them back into the model.
' REQUIREMENT: You must have MS Excel 97 or later installed

'CREATE DATE:  6/7/2006
'LAST UPDATE:  6/9/2006
'VERSION:  1.0



' dim dialog variables
Dim selected_object_loop As Boolean    'if true, macro loops through selected objects
									   'if false, macro loops through all objects

Type OptionSet
	chbx As Boolean
	startnum As Integer
End Type

Dim ExportOptions (0 To 3) As OptionSet



' Dim MS Excel variables.
	
Dim Excel As Object
Dim curRow As Integer
Dim curCol As Integer
Dim clrBack As Variant
Dim clrFore As Variant
Dim clrTitleBack As Variant
Dim clrTitleFore As Variant

	
' Dim ER/Studio variables.
	
Dim diag As Diagram
Dim mdl As Model
Dim submdl As SubModel
Dim so As SelectedObject
Dim ent As Entity
Dim attr As AttributeObj
Dim rel As Relationship
Dim entdisplay As EntityDisplay
Dim dmcl As DataMovementColumnLink
Dim dmml As DataMovementModelLink



Public Const CLR_WHITE = RGB(255, 255, 255)
Public Const CLR_BLACK = RGB(0, 0, 0)
Public Const CLR_GREY = RGB(192, 192, 192)
Public Const CLR_TEAL = RGB(0, 128, 128)

Sub Main

	' Init the ER/Studio variables.
	
	Set diag = DiagramManager.ActiveDiagram
	Set mdl = diag.ActiveModel
	Set submdl = mdl.ActiveSubModel

	curRow = 3
	curCol = 1

	Debug.Clear

	' Prompt the user.
	
	Begin Dialog UserDialog 690,287,"Data Lineage Export Options",.ExportHandler ' %GRID:10,7,1,1
		Text 20,7,150,14,"Diagram Information:",.diaginfotxt
		GroupBox 330,119,340,91,"Choose Export Scope",.GroupBox3
		OptionGroup .object_sel
			OptionButton 360,147,130,14,"All Objects",.OptionButton3
			OptionButton 360,175,140,14,"Selected Objects",.OptionButton4
		OKButton 340,231,160,35
		CancelButton 520,231,150,35
		TextBox 20,28,650,70,.diag_info,1
		GroupBox 20,119,290,147,"Choose Mappings to Export",.GroupBox1
		CheckBox 50,147,240,21,"Direct Source Mappings",.directsrc_chbx
		CheckBox 50,175,230,21,"Secondary Source Mappings",.Secondsrc_chbx
		CheckBox 50,203,230,14,"Direct Target Mappings",.directtrgt_chbx
		CheckBox 50,231,210,14,"Secondary Target Mappings",.Secondtrgt_chbx
	End Dialog
	Dim dlg As UserDialog


	'initialize the dialog info text block.
	Dim sel_objects As String
	Dim mdl_type As String

	If entitiesSelected = False Then
		sel_objects = "NO"
	Else
		sel_objects = "YES"
	End If

	If mdl.Logical = True Then
		mdl_type = "LOGICAL"
	Else
		mdl_type = "PHYSICAL"
	End If

	'populate text box in the dialog with some information about what will be exported
	dlg.diag_info = "Diagram:  " & diag.ProjectName & vbCrLf & "Model Name:  " & mdl.Name & vbCrLf & "Model Type:  " & mdl_type & vbCrLf & "Active Model:  " & submdl.Name & vbCrLf & "Selected Objects:  " & sel_objects



	clrBack = CLR_WHITE
	clrFore = CLR_BLACK
	clrTitleBack = CLR_GREY
	clrTitleFore = CLR_BLACK

	
	' Print the spread sheet.

	If Dialog(dlg) = -1 Then

		'initialize dialog option array
		ExportOptions(0).chbx = dlg.directsrc_chbx
		ExportOptions(1).chbx = dlg.secondsrc_chbx
		ExportOptions(2).chbx = dlg.directtrgt_chbx
		ExportOptions(3).chbx = dlg.Secondtrgt_chbx

		ExportOptions(0).startnum = 8
		ExportOptions(1).startnum = 8
		ExportOptions(2).startnum = 8
		ExportOptions(3).startnum = 8

		If ExportOptions(0).chbx Then
			ExportOptions(1).startnum = ExportOptions(0).startnum + 4
			ExportOptions(2).startnum = ExportOptions(0).startnum + 4
			ExportOptions(3).startnum = ExportOptions(0).startnum + 4
		End If

		If ExportOptions(1).chbx Then
			ExportOptions(2).startnum = ExportOptions(1).startnum + 4
			ExportOptions(3).startnum = ExportOptions(1).startnum + 4
		End If

		If ExportOptions(2).chbx Then
			ExportOptions(3).startnum = ExportOptions(2).startnum + 4
		End If

		Debug.Print ExportOptions(0).chbx & " " & ExportOptions(1).chbx & " " & ExportOptions(2).chbx & " " & ExportOptions(3).chbx & " "
		Debug.Print ExportOptions(0).startnum & " " & ExportOptions(1).startnum & " " & ExportOptions(2).startnum & " " & ExportOptions(3).startnum & " "


		' Start Excel and make it visible.
		
		Set Excel = CreateObject("Excel.Application")
	'	Excel.Visible = True
		Excel.Workbooks.Add
	

	    PrintColumnHeader
	    
	    PrintData

		Excel.sheets("sheet1").columns.autofit
		
		MsgBox("Data Lineage export complete.",vbOkOnly)

		Excel.Visible = True

	End If 'dialog

End Sub

Sub SetFreezeAndFilter ( wb As Object )

	With wb
	    .Columns("H:H").Select
	    .ActiveWindow.FreezePanes = True
	    .Rows("3:3").AutoFilter
	End With

End Sub

Sub PrintData


	If selected_object_loop = False Then

		'loop through all objects in the model
		For Each entdisplay In submdl.EntityDisplays

			Set ent = entdisplay.ParentEntity

			For j = 1 To ent.Attributes.Count
		
				For Each attr In ent.Attributes
			
						If attr.SequenceNumber = j Then
				
								udfPrintAttributeInfo ()

								udfPrintAttributeDataLineage(attr, ExportOptions())

								curRow = curRow + 1
								curCol = 1
	

	
						End If  'sequence number check
	
					Next attr
	
				Next j
	
	
			curCol = 1

		Next entdisplay

	Else

	    'Only report on selected entities
		For Each so In submdl.SelectedObjects
	
			If so.Type = 1 Then
	
			Set ent = mdl.Entities.Item(so.ID)
	
				For j = 1 To ent.Attributes.Count
		
					For Each attr In ent.Attributes
			
						If attr.SequenceNumber = j Then
				

								udfPrintAttributeInfo ()

								udfPrintAttributeDataLineage(attr, ExportOptions())

								curRow = curRow + 1
								curCol = 1
	

						End If  'sequence number check
	
					Next attr
	
				Next j

			
			curCol = 1
	
	
	        End If 'so ID check
	
		Next so

	End If ' selected object check



End Sub

Function udfPrintAttributeInfo ()

		'print attribute stuff
		PrintCell mdl.Name, curRow, curCol, 0, 1, clrFore, clrBack, 10, False
		PrintCell ent.TableName, curRow, curCol, 0, 1, clrFore, clrBack, 10, False
		PrintCell attr.ColumnName, curRow, curCol, 0, 1, clrFore, clrBack, 10, False
		PrintCell udfDatatype(attr), curRow, curCol, 0, 1, clrFore, clrBack, 10, False
		PrintCell udfIsNull(attr), curRow, curCol, 0, 1, clrFore, clrBack, 10, False
		PrintCell udfIsPrimary(attr), curRow, curCol, 0, 1, clrFore, clrBack, 10, False

		curCol = curCol + 1

End Function

Function udfIsNull ( currAttr As AttributeObj ) As String

	If currAttr.NullOption = "NULL" Then

		udfIsNull = "Y"

	Else

		udfIsNull = "N"

	End If

End Function

Function udfIsPrimary ( currAttr As AttributeObj ) As String

	If currAttr.PrimaryKey Then

		udfIsPrimary = "Y"

	Else

		udfIsPrimary = "N"

	End If

End Function

Function udfDatatype ( currAttr As AttributeObj ) As String
	
	Dim dt As String

	dt = Replace(currAttr.CompositeDatatype, " NULL", "")
	dt = Replace(dt, " NOT", "")

	udfDatatype =  dt

End Function


Function udfPrintAttributeDataLineage ( currAttr As AttributeObj, LineageOptions As Variant)

	If LineageOptions(0).chbx Then
		
		PrintCell currAttr.SourceDirectTransformationLogic, curRow, LineageOptions(0).startnum + 1, 0, 0, clrFore, clrBack, 10, False
		PrintCell currAttr.SourceDirectTransformationDescription, curRow, LineageOptions(0).startnum + 2, 0, 0, clrFore, clrBack, 10, False

	End If

	If LineageOptions(1).chbx Then

		PrintCell currAttr.SourceSecondaryTransformationLogic, curRow, LineageOptions(1).startnum + 1, 0, 0, clrFore, clrBack, 10, False
		PrintCell currAttr.SourceSecondaryTransformationDescription, curRow, LineageOptions(1).startnum + 2, 0, 0, clrFore, clrBack, 10, False

	End If

	If LineageOptions(2).chbx Then

		PrintCell currAttr.TargetDirectTransformationLogic, curRow, LineageOptions(2).startnum + 1, 0, 0, clrFore, clrBack, 10, False
		PrintCell currAttr.TargetDirectTransformationDescription, curRow, LineageOptions(2).startnum + 2, 0, 0, clrFore, clrBack, 10, False

	End If

	If LineageOptions(3).chbx Then

		PrintCell currAttr.TargetSecondaryTransformationLogic, curRow, LineageOptions(3).startnum + 1, 0, 0, clrFore, clrBack, 10, False
		PrintCell currAttr.TargetSecondaryTransformationDescription, curRow, LineageOptions(3).startnum + 2, 0, 0, clrFore, clrBack, 10, False

	End If

	If LineageOptions(0).chbx Or LineageOptions(1).chbx Or LineageOptions(2).chbx Or LineageOptions(3).chbx Then

		Dim ds_column_str As String

		Dim ss_column_str As String

		Dim dt_column_str As String

		Dim st_column_str As String

		DiagramManager.EnableScreenUpdate False
	
		For Each dmcl In currAttr.DataMovementColumnLinks
		
			If dmcl.IsDirectSource = True  Then
			'Direct Source Mappings
	
				ds_column_str = ds_column_str & dmcl.SrcTrgtModelName & "." & dmcl.SrcTrgtTableName & "." & dmcl.SrcTrgtColumnName
				ds_column_str = ds_column_str & ",  "

			End If
	
			If dmcl.IsSecondarySource = True Then
			'Secondary Source Mappings
	
				ss_column_str = ss_column_str & dmcl.SrcTrgtModelName & "." & dmcl.SrcTrgtTableName & "." & dmcl.SrcTrgtColumnName
				ss_column_str = ss_column_str & ",  "

			End If

			If dmcl.IsDirectTarget = True Then
			'Direct Target Mappings

				dt_column_str = dt_column_str & dmcl.SrcTrgtModelName & "." & dmcl.SrcTrgtTableName & "." & dmcl.SrcTrgtColumnName
				dt_column_str = dt_column_str & ",  "

			End If

            If dmcl.IsSecondaryTarget = True Then
			'Secondary Target Mappings

				st_column_str = st_column_str & dmcl.SrcTrgtModelName & "." & dmcl.SrcTrgtTableName & "." & dmcl.SrcTrgtColumnName
				st_column_str = st_column_str & ",  "
	
			End If
	
		Next

		'remove last comma
		If Len(ds_column_str) <> 0 Then
			ds_column_str = Left(ds_column_str, Len(ds_column_str)-3)
		End If

		If Len(ss_column_str) <> 0 Then
			ss_column_str = Left(ss_column_str, Len(ss_column_str)-3)
		End If

		If Len(dt_column_str) <> 0 Then
			dt_column_str = Left(dt_column_str, Len(dt_column_str)-3)
		End If

		If Len(st_column_str) <> 0 Then
			st_column_str = Left(st_column_str, Len(st_column_str)-3)
		End If

		If LineageOptions(0).chbx Then

			PrintCell ds_column_str, curRow, LineageOptions(0).startnum, 0, 0, clrFore, clrBack, 10, False

		End If

		If LineageOptions(1).chbx Then

			PrintCell ss_column_str, curRow, LineageOptions(1).startnum, 0, 0, clrFore, clrBack, 10, False

		End If

		If LineageOptions(2).chbx Then

			PrintCell dt_column_str, curRow, LineageOptions(2).startnum, 0, 0, clrFore, clrBack, 10, False

		End If

		If LineageOptions(3).chbx Then

			PrintCell st_column_str, curRow, LineageOptions(3).startnum, 0, 0, clrFore, clrBack, 10, False

		End If

		DiagramManager.EnableScreenUpdate True

	End If

End Function



' Print the column header.  Only print headers when value is true in options array.

Sub PrintColumnHeader (  )

    'initialize column width
    With Excel
		.columns("A").columnWidth = 12
		.columns("B:C").columnWidth = 20
		.columns("D").columnWidth = 10
		.columns("E").columnWidth = 7
		.columns("F").columnWidth = 10
	End With

	'Print Physical Model Header
	PrintCell "Physical Model Information", curRow - 1, curCol + 2, 0, 0, clrFore, clrBack, 14, False

	PrintCell "Model Name", curRow, curCol, 0, 1, clrFore, clrBack, 12, False
	PrintCell "Table Name", curRow, curCol, 0, 1, clrFore, clrBack, 12, False
	PrintCell "Column Name", curRow, curCol, 0, 1, clrFore, clrBack, 12, False
	PrintCell "DataType", curRow, curCol, 0, 1, clrFore, clrBack, 12, False
	PrintCell "Null?", curRow, curCol, 0, 1, clrFore, clrBack, 12, False
	PrintCell "Primary?", curRow, curCol, 0, 1, clrFore, clrBack, 12, False

	Excel.columns(curCol).columnWidth = .5
	curCol = curCol + 1

	If ExportOptions(0).chbx Then

		'print header for direct source mappings
		PrintCell "Direct Source Mappings", curRow - 1, curCol, 0, 0, clrFore, clrBack, 14, False

		PrintCell "Direct Source(s)", curRow, curCol, 0, 1, clrFore, clrBack, 12, False
		PrintCell "Direct Source Logic", curRow, curCol, 0, 1, clrFore, clrBack, 12, False
		PrintCell "Direct Source Description", curRow, curCol, 0, 1, clrFore, clrBack, 12, False

		Excel.columns(curCol).columnWidth = .5
		curCol = curCol + 1

	End If

	If ExportOptions(1).chbx Then

		'print header for secondary source mappings
		PrintCell "Secondary Source Mappings", curRow - 1, curCol, 0, 0, clrFore, clrBack, 14, False

		PrintCell "Secondary Source(s)", curRow, curCol, 0, 1, clrFore, clrBack, 12, False
		PrintCell "Secondary Source Logic", curRow, curCol, 0, 1, clrFore, clrBack, 12, False
		PrintCell "Secondary Source Description", curRow, curCol, 0, 1, clrFore, clrBack, 12, False

		Excel.columns(curCol).columnWidth = .5
		curCol = curCol + 1

	End If

	If ExportOptions(2).chbx Then

		'print header for direct target mappings
		PrintCell "Direct Target Mappings", curRow - 1, curCol, 0, 0, clrFore, clrBack, 14, False

		PrintCell "Direct Target(s)", curRow, curCol, 0, 1, clrFore, clrBack, 12, False
		PrintCell "Direct Target Logic", curRow, curCol, 0, 1, clrFore, clrBack, 12, False
		PrintCell "Direct Target Description", curRow, curCol, 0, 1, clrFore, clrBack, 12, False

		Excel.columns(curCol).columnWidth = .5
		curCol = curCol + 1

	End If

	If ExportOptions(3).chbx Then

		'print header for secondary target mappings
		PrintCell "Secondary Target Mappings", curRow - 1, curCol, 0, 0, clrFore, clrBack, 14, False

		PrintCell "Secondary Target(s)", curRow, curCol, 0, 1, clrFore, clrBack, 12, False
		PrintCell "Secondary Target Logic", curRow, curCol, 0, 1, clrFore, clrBack, 12, False
		PrintCell "Secondary Target Description", curRow, curCol, 0, 1, clrFore, clrBack, 12, False

		Excel.columns(curCol).columnWidth = .5
		curCol = curCol + 1

	End If


	SetFreezeAndFilter Excel

	Excel.Range("A2:V3").interior.Color = clrTitleBack

	curRow = curRow + 1
	curCol = 1
End Sub

Function entitiesSelected() As Boolean

	Dim selObj As SelectedObject
	
	If submdl.SelectedObjects.Count > 0 Then
		For Each selObj In submdl.SelectedObjects
			If selObj.Type=1 Then
				entitiesSelected = True
				Exit Function
			End If
		Next
	End If

	entitiesSelected = False

End Function

' Print a cell

Sub PrintCell(value As String, row As Integer, col As Integer, rowInc As Integer, colInc As Integer, clrFore As Variant, clrBack As Variant, szFont As Integer, bBold As Boolean)
	
	'sample
	'	PrintCell ent.EntityName, curRow, curCol, 0, 1, clrFore, clrBack, 10, False

	Excel.Cells(row, col).Value = value

	Excel.Cells(row, col).Font.Bold = bBold
	Excel.Cells(row, col).Font.Color = clrFore
	Excel.Cells(row, col).Font.Size = szFont

	curRow = curRow + rowInc
	curCol = curCol + colInc

End Sub

Rem See DialogFunc help topic for more information.
Private Function ExportHandler(DlgItem$, Action%, SuppValue&) As Boolean
	Select Case Action%
	Case 1 ' Dialog box initialization



			If entitiesSelected = False Then

				DlgEnable "OptionButton4", False
				selected_object_loop = False
			
			Else
				
				DlgEnable "optionbutton4", True
				DlgValue "object_sel", 1
				selected_object_loop = True

			End If

	Case 2 ' Value changing or button pressed

		If DlgItem = "object_sel" Then

			selected_object_loop = DlgValue("object_sel")

			Debug.Print DlgValue("object_sel")
			Debug.Print "selected_object_loop = " & selected_object_loop

		End If



		Rem ExportHandler = True ' Prevent button press from closing the dialog box
	Case 3 ' TextBox or ComboBox text changed
	Case 4 ' Focus changed
	Case 5 ' Idle
		Rem ExportHandler = True ' Continue getting idle actions
	Case 6 ' Function key
	End Select
End Function

