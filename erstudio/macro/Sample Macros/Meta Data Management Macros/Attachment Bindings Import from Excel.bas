' Macro Title: Attachment Bindings Import from Excel.bas
'
' This macro can be used to import submodel, entity and attribute attachment bindings from Excel.
'
' Spreadsheet is checked for 3 sheets, named "Submodel", "Entity" and "Attribute". Below is the expected format for
' each of these sheets.
'
' FORMAT OF "SUBMODEL" SHEET:
'
'	1.  Model Name - Required - String
'	2.  Submodel Name - Required - String
'	3.  Enterprise DD Name - Required - String
'	4.  Attachment Type Name - Required - String
'	5.  Attachment Name - Required - String
'	6.  Override Value - (Optional) - String
'
' FORMAT OF "ENTITY" SHEET:
'
'	1.  Model Name - Required - String
'	2.  Entity Name - Required - String
'	3.  Enterprise DD Name - Required - String
'	4.  Attachment Type Name - Required - String
'	5.  Attachment Name - Required - String
'	6.  Override Value - (Optional) - String
'
' FORMAT OF "ATTRIBUTE" SHEET:
'
'	1.  Model Name - Required - String
'	2.  Entity Name - Required - String
'	3.  Attribute Name - Required - String
'	4.  Enterprise DD Name - Required - String
'	5.  Attachment Type Name - Required - String
'	6.  Attachment Name - Required - String
'	7.  Override Value - (Optional) - String
'
' Expected Format:
'
' Date Type: 			MM/DD/YYYY
' Time Type:            hh:mmAMPM
'
' Version: 1.0
'***********************************************************************************

Option Explicit

' Constants
Const BOOLEAN_TYPE As Integer = 1
Const DATE_TYPE As Integer = 2
Const EXTERNAL_FILE_PATH_TYPE As Integer = 3
Const NUMERIC_TYPE As Integer = 4
Const TEXT_TYPE As Integer = 5
Const TEXT_LIST_TYPE As Integer = 6
Const TIME_TYPE As Integer = 7


' Global Variables
Dim Excel As Object
Dim sheet As Object
Dim range As Object
Dim ListDictNames() As String

Sub Main

	On Error GoTo errHandler


	Begin Dialog UserDialog 520,118,"Enterprise Import Attachment Bindings From Excel",.DialogFunc ' %GRID:10,7,1,1
		Text 10,20,210,14,"Path to binding info spreadsheet:",.Text1
		TextBox 20,48,400,21,.edFileName
		PushButton 430,48,60,21,"Browse",.Browse
		PushButton 150,90,90,21,"Import",.btnImport
		PushButton 260,90,90,21,"Exit",.btnExit
	End Dialog
	Dim dlg As UserDialog

	Debug.Clear

	Dialog dlg

	Exit Sub

errHandler:
	If (Not Excel Is Nothing) Then
		Excel.Quit
	End If
	
	MsgBox Err.Description, vbCritical

End Sub

Rem See DialogFunc help topic for more information.
Private Function DialogFunc(DlgItem$, Action%, SuppValue&) As Boolean

	Dim fileName As String

	Select Case Action%
		Case 1 ' Dialog box initialization
	
			'make diagram and file name text blocks read only
	
			'populate the diagram and file name text blocks with the appropiate data.
	
		Case 2 ' Value changing or button pressed

			Select Case DlgItem

				Case "Browse"
					'browse to excel file if used pushes browse button.  Put path in text box.
					fileName = GetFilePath(,"xls;xlsx;*",,"Open SpreadSheetle", 0)
			
					If (fileName <> "") Then
						DlgText("edFileName", fileName)
					End If
		
					DialogFunc = True

				Case "btnImport"

	                fileName = DlgText("edFileName")
	
	                If (fileName = "") Then
	                        MsgBox "You must specify a file."
	                        DialogFunc = True
	                        Exit Function
	                Else
	                	If (Not FileExists(fileName)) Then
	                        MsgBox "Specified file does not exist."
	                        DialogFunc = True
	                        Exit Function
	                    End If
	                End If


	                doImport(fileName)
	
	                DialogFunc = False
	                Exit Function

	            Case "btnExit"

                    DialogFunc = False
                    Exit Function

			End Select


			Rem DialogFunc = True ' Prevent button press from closing the dialog box
		Case 3 ' TextBox or ComboBox text changed
		Case 4 ' Focus changed
		Case 5 ' Idle
			Rem DialogFunc = True ' Continue getting idle actions
		Case 6 ' Function key
	End Select

End Function

Function GetSubmodel(theModel As Model, ByVal nameValue As String) As SubModel
	Dim theSubmodel As SubModel
	Dim isLogical As Boolean

	Set GetSubmodel = Nothing

	Set theSubmodel = theModel.SubModels(nameValue)
	If (theSubmodel Is Nothing) Then
		For Each theSubmodel In theModel.SubModels
			If (StrComp(theSubmodel.Name, nameValue, vbUseCompareOption) = 0) Then
				Set GetSubmodel = theSubmodel
				Exit For
			End If
		Next theSubmodel
	Else
		Set GetSubmodel = theSubmodel
	End If

End Function

Function GetEntity(theModel As Model, ByVal nameValue As String) As Entity
	Dim theEntity As Entity
	Dim isLogical As Boolean

	Set GetEntity = Nothing

	isLogical = theModel.Logical
	Set theEntity = theModel.Entities(nameValue)
	If (theEntity Is Nothing) Then
		For Each theEntity In theModel.Entities
			If (StrComp(IIf(isLogical, theEntity.EntityName, theEntity.TableName), nameValue, vbUseCompareOption) = 0) Then
				Set GetEntity = theEntity
				Exit For
			End If
		Next theEntity
	Else
		Set GetEntity = theEntity
	End If

End Function

Function GetAttribute(isLogical As Boolean, theEntity As Entity, nameValue As String) As AttributeObj
	Dim theAttribute As AttributeObj

	Set GetAttribute = Nothing

	Set theAttribute = theEntity.Attributes(nameValue)
	If (Not theAttribute Is Nothing) Then
		Set GetAttribute = theAttribute
		Exit Function
	End If

	For Each theAttribute In theEntity.Attributes
		If (StrComp(IIf(isLogical, theAttribute.AttributeName, theAttribute.ColumnName), nameValue, vbUseCompareOption) = 0) Then
			Set GetAttribute = theAttribute
			Exit Function
		End If
	Next theAttribute

End Function

Function doImport(fileName As String)

	Dim theDiagram As Diagram

	Set theDiagram = DiagramManager.ActiveDiagram
	If (theDiagram Is Nothing) Then
		MsgBox "Please open a .dm1 file first.", vbExclamation
		End
	End If


	'initialize excel object and make visible
	Set Excel = CreateObject("Excel.Application")

	Excel.workbooks.open fileName

	ImportSubmodelAttachments theDiagram
	ImportEntityAttachments theDiagram
	ImportAttributeAttachments theDiagram

	Excel.Quit

	MsgBox "Import Complete.", vbInformation

End Function

Function ImportSubmodelAttachments(theDiag As Diagram)

	Dim Model_Name As String
	Dim Prev_Model_Name As String
	Dim Submodel_Name As String
	Dim Prev_Submodel_Name As String
	Dim EDD_Name As String
	Dim Prev_EDD_Name As String
	Dim Attachment_Type As String
	Dim Attachment_Name As String
	Dim Override_value As String
	Dim theModel As Model
	Dim theSubmodel As SubModel
	Dim binding_count As Integer
	Dim success_count As Integer
	Dim start_range As Integer
	Dim theDictionary As Dictionary
	Dim theAttachmentType As AttachmentType
	Dim theAttachment As Attachment
	Dim theBoundAttachment As BoundAttachment
	Dim I As Integer

	Prev_Model_Name = ""
	Prev_Submodel_Name = ""
	Prev_EDD_Name = ""

	'get sheet info from excel object
	Set sheet = Excel.worksheets("Submodel")
	If (sheet Is Nothing) Then
		MsgBox "Submodel sheet not found; skipping.", vbExclamation
		Exit Function
	End If

	Set range = sheet.usedrange


	'get count for loop
	success_count = 0
	binding_count = range.rows.Count
	start_range = 2						'ignore first row of sheet.

	For I = start_range To binding_count

		'initialize string variables with data from spread sheet.
		Model_Name = Trim(range.cells(I,1).Value)
		Submodel_Name = Trim(range.cells(I,2).Value)
		EDD_Name = Trim(range.cells(I,3).Value)
		Attachment_Type = Trim(range.cells(I,4).Value)
		Attachment_Name = Trim(range.cells(I,5).Value)
		Override_value = Trim( CStr(range.cells(I,6).Value ) )

		If Prev_Model_Name <> Model_Name Then
			Set theModel = theDiag.Models(Model_Name)
			If theModel Is Nothing Then
				Debug.Print "Model : " & Model_Name & " not found."
				GoTo ContinueLoop
			End If
			Prev_Model_Name = Model_Name
		End If

		If Prev_Submodel_Name <> Submodel_Name Then
			Set theSubmodel = GetSubmodel(theModel, Submodel_Name)
			If theSubmodel Is Nothing Then
				Debug.Print "Submodel : " & Submodel_Name & " not found."
				GoTo ContinueLoop
			End If
			Prev_Submodel_Name = Submodel_Name
		End If

		If Prev_EDD_Name <> EDD_Name Then
			Set theDictionary = GetDict( EDD_Name )
			If theDictionary Is Nothing Then
				Debug.Print "Data Dictionary : " & EDD_Name & " not found."
				GoTo ContinueLoop
			End If
			Prev_EDD_Name = EDD_Name
		End If

		Set theAttachmentType = theDictionary.AttachmentTypes(Attachment_Type)
		If (theAttachmentType Is Nothing) Then
			Debug.Print "Attachment type: " & Attachment_Type & " does not exist."
			GoTo continueLoop
		End If

		Set theAttachment = theAttachmentType.Attachments(Attachment_Name)
		If theAttachment Is Nothing Then
			Debug.Print "Attachment : " & Attachment_Name & " does not exist."
			GoTo continueLoop
		End If


		Set theBoundAttachment = theSubmodel.BoundAttachments(theAttachment.ID)


		If (theBoundAttachment Is Nothing) Then
			Set theBoundAttachment = theSubmodel.BoundAttachments.Add(theAttachment.ID)
		End If

		If (Override_value <> "") Then
			theBoundAttachment.ValueOverride = GetValueforAttachment( theAttachment, Override_value )
			If CheckOp() Then
				success_count = success_count + 1
			End If
		End If

		continueLoop:

	Next I

	MsgBox "Found " & binding_count-1 & " submodel attachments, successfully imported " & success_count & "."
End Function

Function ImportEntityAttachments(theDiag As Diagram)

	Dim Model_Name As String
	Dim Prev_Model_Name As String
	Dim Entity_Name As String
	Dim Prev_Entity_Name As String
	Dim EDD_Name As String
	Dim Prev_EDD_Name As String
	Dim Attachment_Type As String
	Dim Attachment_Name As String
	Dim Override_value As String
	Dim theModel As Model
	Dim theEntity As Entity
	Dim binding_count As Integer
	Dim success_count As Integer
	Dim start_range As Integer
	Dim theDictionary As Dictionary
	Dim theAttachmentType As AttachmentType
	Dim theAttachment As Attachment
	Dim theBoundAttachment As BoundAttachment
	Dim I As Integer

	Prev_Model_Name = ""
	Prev_Entity_Name = ""
	Prev_EDD_Name = ""

	'get sheet info from excel object
	Set sheet = Excel.worksheets("Entity")
	If (sheet Is Nothing) Then
		MsgBox "Entity sheet not found; skipping.", vbExclamation
		Exit Function
	End If

	Set range = sheet.usedrange


	'get count for loop
	success_count = 0
	binding_count = range.rows.Count
	start_range = 2						'ignore first row of sheet.

	For I = start_range To binding_count

		'initialize string variables with data from spread sheet.
		Model_Name = Trim(range.cells(I,1).Value)
		Entity_Name = Trim(range.cells(I,2).Value)
		EDD_Name = Trim(range.cells(I,3).Value)
		Attachment_Type = Trim(range.cells(I,4).Value)
		Attachment_Name = Trim(range.cells(I,5).Value)
		Override_value = Trim( CStr(range.cells(I,6).Value ) )

		If Prev_Model_Name <> Model_Name Then
			Set theModel = theDiag.Models(Model_Name)
			If theModel Is Nothing Then
				Debug.Print "Model : " & Model_Name & " not found."
				GoTo ContinueLoop
			End If
			Prev_Model_Name = Model_Name
		End If

		If Prev_Entity_Name <> Entity_Name Then
			Set theEntity = GetEntity(theModel, Entity_Name)
			If theEntity Is Nothing Then
				Debug.Print "Entity/Table : " & Entity_Name & " not found."
				GoTo ContinueLoop
			End If
			Prev_Entity_Name = Entity_Name
		End If

		If Prev_EDD_Name <> EDD_Name Then
			Set theDictionary = GetDict( EDD_Name )
			If theDictionary Is Nothing Then
				Debug.Print "Data Dictionary : " & EDD_Name & " not found."
				GoTo ContinueLoop
			End If
			Prev_EDD_Name = EDD_Name
		End If

		Set theAttachmentType = theDictionary.AttachmentTypes(Attachment_Type)
		If (theAttachmentType Is Nothing) Then
			Debug.Print "Attachment type: " & Attachment_Type & " does not exist."
			GoTo continueLoop
		End If

		Set theAttachment = theAttachmentType.Attachments(Attachment_Name)
		If theAttachment Is Nothing Then
			Debug.Print "Attachment : " & Attachment_Name & " does not exist."
			GoTo continueLoop
		End If


		Set theBoundAttachment = theEntity.BoundAttachments(theAttachment.ID)


		If (theBoundAttachment Is Nothing) Then
			Set theBoundAttachment = theEntity.BoundAttachments.Add(theAttachment.ID)
		End If

		If (Override_value <> "") Then
			theBoundAttachment.ValueOverride = GetValueforAttachment( theAttachment, Override_value )
			If CheckOp() Then
				success_count = success_count + 1
			End If
		End If

		continueLoop:

	Next I

	MsgBox "Found " & binding_count-1 & " entity attachments, successfully imported " & success_count & "."
End Function

Function ImportAttributeAttachments(theDiag As Diagram)

	Dim Model_Name As String
	Dim Prev_Model_Name As String
	Dim Entity_Name As String
	Dim Prev_Entity_Name As String
	Dim EDD_Name As String
	Dim Prev_EDD_Name As String
	Dim Attribute_Name As String
	Dim Prev_Attribute_Name As String
	Dim Attachment_Type As String
	Dim Attachment_Name As String
	Dim Override_value As String
	Dim theModel As Model
	Dim theEntity As Entity
	Dim theAttribute As AttributeObj
	Dim binding_count As Integer
	Dim success_count As Integer
	Dim start_range As Integer
	Dim theDictionary As Dictionary
	Dim theAttachmentType As AttachmentType
	Dim theAttachment As Attachment
	Dim theBoundAttachment As BoundAttachment
	Dim I As Integer

	Prev_Model_Name = ""
	Prev_Entity_Name = ""
	Prev_Attribute_Name = ""
	Prev_EDD_Name = ""


	'get sheet info from excel object
	Set sheet = Excel.worksheets("Attribute")
	If (sheet Is Nothing) Then
		MsgBox "Entity sheet not found; skipping.", vbExclamation
		Exit Function
	End If

	Set range = sheet.usedrange


	'get count for loop
	success_count = 0
	binding_count = range.rows.Count
	start_range = 2						'ignore first row of sheet.

	For I = start_range To binding_count

		'initialize string variables with data from spread sheet.
		Model_Name = Trim(range.cells(I,1).Value)
		Entity_Name = Trim(range.cells(I,2).Value)
		Attribute_Name = Trim(range.cells(I, 3).Value)
		EDD_Name = Trim(range.cells(I,4).Value)
		Attachment_Type = Trim(range.cells(I,5).Value)
		Attachment_Name = Trim(range.cells(I,6).Value)
		Override_value = Trim( CStr(range.cells(I,7).Value ) )

		If Prev_Model_Name <> Model_Name Then
			Set theModel = theDiag.Models(Model_Name)
			If theModel Is Nothing Then
				Debug.Print "Model : " & Model_Name & " not found."
				GoTo ContinueLoop
			End If
			Prev_Model_Name = Model_Name
		End If

		If Prev_Entity_Name <> Entity_Name Then
			Set theEntity = GetEntity(theModel, Entity_Name)
			If theEntity Is Nothing Then
				Debug.Print "Entity/Table : " & Entity_Name & " not found."
				GoTo ContinueLoop
			End If
			Prev_Entity_Name = Entity_Name
		End If

		If Prev_Attribute_Name <> Attribute_Name Then
			Set theAttribute = GetAttribute(theModel.Logical, theEntity, Attribute_Name)
			If (theAttribute Is Nothing) Then
				Debug.Print "Attribute/Column : " & Attribute_Name & " not found."
				GoTo ContinueLoop
			End If
			Prev_Attribute_Name = Attribute_Name
		End If

		If Prev_EDD_Name <> EDD_Name Then
			Set theDictionary = GetDict( EDD_Name )
			If theDictionary Is Nothing Then
				Debug.Print "Data Dictionary : " & EDD_Name & " not found."
				GoTo ContinueLoop
			End If
			Prev_EDD_Name = EDD_Name
		End If

		Set theAttachmentType = theDictionary.AttachmentTypes(Attachment_Type)
		If (theAttachmentType Is Nothing) Then
			Debug.Print "Attachment type: " & Attachment_Type & " does not exist."
			GoTo ContinueLoop
		End If

		Set theAttachment = theAttachmentType.Attachments(Attachment_Name)
		If (theAttachment Is Nothing) Then
			Debug.Print "Attachment : " & Attachment_Name & " does not exist."
			GoTo ContinueLoop
		End If


		Set theBoundAttachment = theAttribute.BoundAttachments(theAttachment.ID)
		If (theBoundAttachment Is Nothing) Then

			Set theBoundAttachment = theAttribute.BoundAttachments.Add(theAttachment.ID)
		End If

		If (Override_value <> "") Then
			theBoundAttachment.ValueOverride = GetValueforAttachment( theAttachment, Override_value )

			If CheckOp() Then
				success_count = success_count + 1
			End If
		End If

		ContinueLoop:
	Next I

	MsgBox "Found " & binding_count-1 & " attribute attachments, successfully imported " & success_count & "."

End Function
Function CheckOp()
	Dim errorCode As Integer

	Err.Clear

	CheckOp = True

	errorCode = DiagramManager.GetLastErrorCode()
	If (errorCode <> 0) Then
		Err.Raise Number := vbObjectError + errorCode, _
			Source := "Macro error handler", _
			Description := DiagramManager.GetLastErrorString()
		CheckOp = False
	End If
End Function
Function FileExists(ByVal fileName As String) As Boolean

	FileExists = (Dir(fileName) <> "")

End Function
Function GetValueforAttachment(theAttachment As Attachment, ByVal value As String) As String

	Dim dt, tm As Date
	Dim S As String

	GetValueforAttachment = ""

	If (theAttachment Is Nothing) Or (value = "") Then
		Exit Function
	End If


	Select Case theAttachment.Datatype
		Case BOOLEAN_TYPE
			GetValueforAttachment = CStr(CBool(value))
		Case EXTERNAL_FILE_PATH_TYPE
			GetValueforAttachment = value
			CheckOp
		Case NUMERIC_TYPE
			GetValueforAttachment = CStr(CInt(value))
			CheckOp
		Case TEXT_TYPE
			GetValueforAttachment = value
			CheckOp
		Case DATE_TYPE
			dt = CStr(CDate(value))
			S = Format(dt, "MM/DD/YYYY")
			GetValueforAttachment = S
		Case TIME_TYPE
			tm = CStr(CDate(value)) ' Type checking through casting
			S = Format(tm, "hh:mmAMPM") ' Expected ER/Studio format
			GetValueforAttachment = S
		Case TEXT_LIST_TYPE
			GetValueforAttachment = value
	End Select
End Function

Function GetDict(NameOfDictionary As String) As Dictionary
	Dim theDiagram As Diagram
	Dim theDict As Dictionary

	Set theDiagram = DiagramManager.ActiveDiagram
	Set theDict = theDiagram.EnterpriseDataDictionaries(NameOfDictionary)
	If theDict Is Nothing Then
		Set GetDict = theDiagram.Dictionary
	Else
		Set GetDict = theDict
	End If
End Function
