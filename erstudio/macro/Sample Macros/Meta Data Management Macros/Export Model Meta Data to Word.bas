'MACRO TITLE: EXPORT MODEL META DATA TO WORD
'This macro generates a mini report for the selected entities in the active model.
' REQUIREMENT: You must have Word 97 installed.

Sub Main
	' Dim MS Word variables zzz.

	Dim Word As Object
	Dim Docs As Object
	Dim WordBasic As Object
	Dim ActiveDoc As Object
	
	' Dim ER/Studio variables.
	
	Dim diag As Diagram
	Dim mdl As Model
	Dim sm As SubModel
	Dim so As SelectedObject
	Dim id As Integer
	Dim ent As Entity
	Dim attr As AttributeObj

	' Start MS Word and make it visible.

	Set Word = CreateObject("Word.Application")
	Word.Visible = True
	Word.Options.CheckGrammarAsYouType = False
	Word.Options.CheckSpellingAsYouType = False
	Word.Documents.Add
	Set ActiveDoc = Word.Documents(1)
	ActiveDoc.Activate

	' Init the ER/Studio variables.
	
	Set diag = DiagramManager.ActiveDiagram
	Set mdl = diag.ActiveModel
	Set sm = mdl.ActiveSubModel

	' You need the submodel when determining what's selected.
    ' Iterate through all selected objects and process only entities.
    
    For Each so In sm.SelectedObjects
		If so.Type = 1 Then		' Entities are Type 1 objects.
			
			' Get the entity.
			
			id = so.ID
			Set ent = mdl.Entities.Item(id)
			
			Word.Selection.TypeText Text:=ent.EntityName & vbCrLf
			
			For Each attr In ent.Attributes
				Word.Selection.TypeText Text:=vbTab & attr.ColumnName & " [" & attr.Datatype & "]" & vbCrLf
			Next
			
			Word.Selection.TypeText Text:= vbCrLf
		End If
    Next
End Sub
