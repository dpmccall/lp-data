' Macro: Import Columns From Excel.bas
'
' This macro will import entities and columns using information provided in Excel spreadsheet.
'
' The Excel spreadsheet ought to have the following column format:
'
'1. Entity Name
'2. Table Name
'3. Attribute Name
'4. Column Name
'5. Data type
'6. Table Definition
'7. Attribute Definition
'8. Nullability - 'NULL' or 'NOT NULL'
'9. Primary Key - 'True' or 'False'
'
' Version 1.0
'*******************************************************************************************************


Sub Main


	Begin Dialog UserDialog 720,105,"Import Attributes/Columns From Excel Spreadsheet",.DlgFunc ' %GRID:10,7,1,1
		Text 10,21,120,14,"Excel File Path:",.txtFile
		TextBox 150,21,450,21,.edFileName
		PushButton 610,21,90,21,"Select File",.btnSelectFile
		PushButton 260,70,90,21,"Import",.btnImport
		CancelButton 380,70,90,21,.btnExit
	End Dialog
	Dim dlg As UserDialog


	If (DiagramManager.ActiveDiagram Is Nothing) Then
		MsgBox "Please open a diagram file before running the macro.", vbExclamation
		Exit Sub
	End If

	If Dialog(dlg) = 0 Then Exit Sub



End Sub

Rem See DialogFunc help topic for more information.
Private Function DlgFunc(DlgItem$, Action%, SuppValue&) As Boolean
	Dim fileName As String
	Dim fileExt As String

	Select Case Action%
	Case 1 ' Dialog box initialization
	Case 2 ' Value changing or button pressed

		Select Case DlgItem$
	        Case "btnSelectFile"
	
					fileName = GetFilePath(,"xls;xlsx",,"Open File", 0)
	
					If (fileName <> "") Then
						DlgText("edFileName", fileName)
					End If
	
	                DlgFunc = True
	                Exit Function

            Case "btnImport"

                    fileName = DlgText("edFileName")

                    If Len(fileName) = 0 Then
                            MsgBox "You must specify a file."
                            DlgFunc = True
                            Exit Function
                    Else
                    	If Not FileExists(fileName) Then
                            MsgBox "Specified file does not exist."
                            DlgFunc = True
                            Exit Function
                        Else
							fileExt =  Right$(fileName, Len(fileName) - InStrRev(fileName, "."))
							If (LCase(Left(fileExt, 3)) <> "xls") Then
								MsgBox("You can only select Excel spreadsheets.", vbExclamation)
								Exit Function
							End If
                        End If
                    End If

                    doImport(fileName)

                    DlgFunc = False
                    Exit Function

            Case "btnExit"
                    DlgFunc = False
                    Exit Function
        End Select
	Case 3 ' TextBox or ComboBox text changed
	Case 4 ' Focus changed
	Case 5 ' Idle
		Rem DlgFunc = True ' Continue getting idle actions
	Case 6 ' Function key
	End Select
End Function
Function FileExists(ByVal fileName As String) As Boolean

	FileExists = (Dir(fileName) <> "")

End Function
Function doImport(ByVal fileName As String)

	Dim theDiagram As Diagram
	Dim theModel As Model
	Dim theEntity As Entity
	Dim theAttribute As AttributeObj

	'define excel variables
	Dim Excel As Object
	Dim sheet As Object
	Dim range As Object
	Dim rowCount As Integer
	Dim I As Integer
	Dim entity_name As String
	Dim table_name As String
	Dim attribute_name As String
	Dim column_name As String
	Dim table_definition As String
	Dim attr_definition As String
	Dim datatype As String
	Dim nullability As String
	Dim datawidth As Integer
	Dim nPos As Integer
	Dim sDataType As String
	Dim s As String
	Dim nSepPos As Integer
	Dim datascale As Integer
	Dim bPrimaryKey As Boolean

	On Error GoTo errHandler


	'initialize excel object
	Set Excel = Nothing
	Set Excel = CreateObject("Excel.Application")
	Excel.workbooks.open fileName

	'get sheet info from excel object
	Set sheet = Excel.worksheets(1)
	Set range = sheet.usedrange
	rowCount = range.rows.Count

	DiagramManager.EnableScreenUpdateEx(False, True)
	Set theDiagram = DiagramManager.ActiveDiagram
	Set theModel = theDiagram.ActiveModel
	For I = 2 To rowCount
		entity_name = Trim(range.cells(I,1).Value)
		table_name = Trim(range.cells(I,2).Value)
		attribute_name = Trim(range.cells(I,3).Value)
		column_name = Trim(range.cells(I,4).Value)
		sDataType = Trim(range.cells(I,5).Value)
		table_definition = Trim(range.cells(I,6).Value)
		attr_definition =  Trim(range.cells(I,7).Value)
		nullability =  UCase(Trim(range.cells(I,8).Value))
		bPrimaryKey = IIf(UCase(Trim(range.cells(I,9).Value)) = "TRUE", True, False)
		nPos = InStr(sDataType, "(")
		If nPos = 0 Then
			datatype = sDataType
			datawidth = 0
		Else
			datatype = Mid(sDataType, 1, nPos - 1)
			sDataType = Mid(sDataType, nPos +1, Len(sDataType))
			nPos = InStr(sDataType, ")")
			s = Mid(sDataType, 1, nPos - 1)
			nSepPos = InStr(s, ",")
			If nSepPos = 0 Then
				datawidth = Mid(s, 1, nPos - 1)
				datascale = 0
			Else
				datawidth = Mid(s, 1, nSepPos - 1)
				datascale = Mid(s, nSepPos + 1, Len(sDataType))
			End If
		End If

		Set theEntity = theModel.Entities(entity_name)
		If theEntity Is Nothing Then
			Set theEntity = theModel.Entities.Add(0, 0)
			CheckOp
			theEntity.EntityName = entity_name
			theEntity.TableName = table_name
			theEntity.Definition = table_definition
		End If

		Set theAttribute = theEntity.Attributes(attribute_name)
		If theAttribute Is Nothing Then
			Set theAttribute = theEntity.Attributes.Add(attribute_name, False)
			CheckOp
		End If

		theAttribute.ColumnName = column_name
		theAttribute.AttributeName = attribute_name
		theAttribute.Definition = attr_definition
		theAttribute.Datatype = datatype
		theAttribute.DataLength = datawidth
		theAttribute.DataScale = datascale
		Debug.Print theAttribute.AttributeName & " " & nullability
		If nullability = "NULL" Or nullability = "NOT NULL" Then
			theAttribute.NullOption = nullability
		End If
		If bPrimaryKey Then
			theAttribute.PrimaryKey = True
		End If
	Next I

	DiagramManager.EnableScreenUpdateEx(True, True)

	If Not Excel Is Nothing Then
			Excel.Quit
	End If

	MsgBox "Import Complete!", vbInformation
	
	Exit Function

errHandler:
	
	DiagramManager.EnableScreenUpdateEx(True, True)

	If Not Excel Is Nothing Then
			Excel.Quit
	End If

	MsgBox Err.Description, vbCritical
	
	End

End Function
Function CheckOp()
	Dim errorCode As Integer

	Err.Clear

	errorCode = DiagramManager.GetLastErrorCode()
	If (errorCode <> 0) Then
		Err.Raise Number := vbObjectError + errorCode, _
			Source := "Macro error handler", _
			Description := DiagramManager.GetLastErrorString()
	End If
End Function
