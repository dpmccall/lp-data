'TITLE:  VISUAL DATA LINEAGE IMPORT FROM EXCEL.BAS
'DESCRIPTION:  This macro imports visual data lineage from Excel.  Each record in the
'	Excel sheet should reference a tranformation in a data flow.

'	To get a sample of the spreadsheet, run the macro and select "Get Sample Sheet" or
'	run the "Visual Data Lineage Export to Excel.bas" macro.



'ER/Studio Variables
Dim diag As Diagram
Dim mdl As Model
Dim ent As Entity
Dim attr As AttributeObj
Dim dlsmdl As DataLineageSource
Dim dlstable As DataLineageTable
Dim dlscolumn As DataLineageColumn
Dim tran As Transformation
Dim tranfield As TransformationField
Dim trandisplay As TransformationDisplay
Dim dflow As DataFlow
Dim lincomp As LineageComponent
Dim lincompdisp As LineageComponentDisplay
Dim dstream As DataStream
Dim vw As View
Dim vwcolumn As ViewField

Dim error_log As String

' Dim MS Excel variables.
	
Dim Excel As Object
Dim curRow As Integer
Dim curCol As Integer
Dim sheet As Object
Dim range As Object

Public Const CLR_WHITE = RGB(255, 255, 255)
Public Const CLR_BLACK = RGB(0, 0, 0)
Public Const CLR_GREY = RGB(192, 192, 192)
Public Const CLR_TEAL = RGB(0, 128, 128)

Option Explicit

Sub Main
	
	Dim Excel As Object



	Debug.Clear

	Set diag = DiagramManager.ActiveDiagram

	Begin Dialog UserDialog 630,196,"Import Domains Bindings From Excel",.DialogFunc ' %GRID:10,7,1,1
		Text 30,21,210,14,"Path to binding info spreadsheet:",.Text1
		TextBox 70,42,440,21,.Path
		OKButton 350,147,110,28
		CancelButton 480,147,110,28
		PushButton 530,42,60,21,"Browse",.Browse
		CheckBox 30,91,320,21,"Log errors to c:\vdl_mappings_import_log.txt",.log_errors_chbx
		PushButton 30,140,180,35,"Get Sample",.getsample
	End Dialog


	Dim dlg As UserDialog

	'initialize dialog defaults, 0 = unchecked, 1 = checked
	dlg.log_errors_chbx = 1

	'start_dialog:
	
	'start dialog
	If Dialog(dlg) = -1 Then

		'initialize excel object and make visible
		Set Excel = CreateObject("Excel.Application")
		'excel.Visible = True
	
		'this error is for an errant file path, dialog will be restarted
		'On Error GoTo Error_handle
			
		Excel.workbooks.open dlg.Path

		'range variables for loop
		Dim binding_count As Integer
		Dim start_range As Integer
	
		'get sheet info from excel object
		Set sheet = Excel.worksheets(1)
		Set range = sheet.usedrange
	
		'get count for loop
		binding_count = range.rows.Count
		start_range = 2			'ignore header row of sheet
		curCol = 1  'start at the first column of the spread sheet

		'input diagram and model name in error log
		error_log = "Diagram Name:       " & diag.ProjectName & vbCrLf
		error_log = error_log & "File Name:          " & diag.FileName & vbCrLf & vbCrLf & vbCrLf & vbCrLf

		Dim i As Integer
		Dim dflowNameStr As String
		Dim tranNameStr As String
		Dim tranBizDefStr As String
		Dim tranCodeDefStr As String
		Dim tranTypeStr As String
		Dim inputColumnStr As String
		Dim outputColumnStr As String
		Dim tranHorizontalPosition As Integer
		Dim tranVerticalPosition As Integer
		Dim tranHorizontalSize As Integer
		Dim tranVerticalSize As Integer

		For i = start_range To binding_count

			curCol = 1
			curRow = i

			'initialize string variables with data from spread sheet.
			dflowNameStr = GetCell(range, curRow, curCol, 0, 1)
			tranNameStr = GetCell(range, curRow, curCol, 0, 1)
			tranTypeStr = udfTransformationType(GetCell(range, curRow, curCol, 0, 1))
			inputColumnStr = GetCell(range, curRow, curCol, 0, 1)
			tranBizDefStr = GetCell(range, curRow, curCol, 0, 1)
			tranCodeDefStr = GetCell(range, curRow, curCol, 0, 1)
			outputColumnStr = GetCell(range, curRow, curCol, 0, 1)
			tranHorizontalPosition = 100
			tranVerticalPosition = 100
			tranHorizontalSize = 100
			tranVerticalSize = 100


			Set dflow = diag.DataFlows.Item(dflowNameStr)

			If dflow Is Nothing Then

				'data flow doesn't exist so create it
				Set dflow = diag.DataFlows.Add(dflowNameStr)
	
			End If
	
			If dflow Is Nothing Then

				'if the object is still not initialized something happened when creating it.  log error to log file
				error_log = error_log & "Row < " & i & " > -  Data Flow  <" & dflowNameStr & "> could not be created. ERROR: " & DiagramManager.GetLastErrorString & vbCrLf & vbCrLf
	
			Else
	
				'after the data flow is created, create the transformation
				Set trandisplay = dflow.TransformationDisplays.Item(tranNameStr)
				Set tran = dflow.Transformations.Item(tranNameStr)
	
  				If tran Is Nothing And trandisplay Is Nothing Then
	
					'if neither the display or object exist add it to the data flow
					Set tran = dflow.Transformations.Add(tranHorizontalPosition, tranVerticalPosition)

					If tran Is Nothing Then

						'log missing transformation in the error string
						error_log = error_log & "Row < " & i & " > -  Transformation  <" & tranNameStr & "> could not be created.  ERROR: "  & DiagramManager.GetLastErrorString & vbCrLf & vbCrLf

					Else

						'set the name and display object
						tran.Name = tranNameStr
						Set trandisplay = dflow.TransformationDisplays.Item(tranNameStr)

						'set the other transformation properties
						tran.Type = tranTypeStr
						tran.BusinessDefinition = tranBizDefStr
						tran.CodeDefinition = tranCodeDefStr
	
						'add the input lineage components to dataflow
						 AddTransformationLineageComponents inputColumnStr, dflow, tran, True

						'add the output lineage component to dataflow
						 AddTransformationLineageComponents outputColumnStr, dflow, tran, False

					End If	  'transformation existence check

				 End If 'tran and tran display check
	
			End If		  'data flow existence check
	
		Next

		Excel.workbooks.Close
		'excel.visible = False

		If dlg.log_errors_chbx = 1 Then

			Open "c:\vdl_mappings_import_log.txt" For Output As #1
			Print #1, error_log
			Close #1

		End If

		MsgBox("Import of source and target mappings complete.",vbOkOnly,"ERStudio")
	
		Exit Sub
	
		'Error_handle:
		'	MsgBox("Please enter a valid path.",,"Error!")
		'	GoTo start_dialog

	End If

End Sub

Sub AddTransformationLineageComponents ( currMappings As String, currDflow As DataFlow, currTran As Transformation, isInput As Boolean )

	Dim m_column_mappings As String
	Dim m_comma_position As Integer
	Dim m_dot_position As Integer
	Dim m_model_name As String
	Dim m_table_name As String
	Dim m_column_name As String

	m_column_mappings = currMappings

	'this gets rid of any trailing commas
	If Right(m_column_mappings, 1) = "," Then

		m_column_mappings = Left(m_column_mappings, Len(m_column_mappings)-1)

	End If

	If m_column_mappings <> "" Then

		Do  'need to parse mapping string

			'get model name
			m_dot_position = InStr(m_column_mappings,".")
			m_model_name = Left(m_column_mappings, m_dot_position - 1)
			m_column_mappings = Right(m_column_mappings, Len(m_column_mappings) - m_dot_position)
		
			'get table name
			m_dot_position = InStr(m_column_mappings,".")
			m_table_name = Left(m_column_mappings, m_dot_position - 1)
			m_column_mappings = Right(m_column_mappings, Len(m_column_mappings) - m_dot_position)
		
			'move to next mapping
			m_comma_position = InStr(m_column_mappings,",")
		
			'move to next mapping
			If m_comma_position <> 0 Then
				m_column_name = Left(m_column_mappings, m_comma_position - 1)
				m_column_mappings = Right(m_column_mappings, Len(m_column_mappings) - m_comma_position)
			Else
				m_column_name = m_column_mappings
			End If
		
			m_model_name = Trim(m_model_name)
			m_table_name = Trim(m_table_name)
			m_column_name = Trim(m_column_name)
		
			'now get the lineage components from the model objects
			Set mdl = diag.Models.Item(m_model_name)

			If mdl Is Nothing Then

				'look in the data lineage sources for the model and entity
				Set dlsmdl = diag.DataLineageSources.Item(m_model_name)
				
				If dlsmdl Is Nothing Then

					'log to error file
					error_log = error_log & "Row < " & curRow & " > -  Data Lineage Model  <" & m_model_name & "> could not be found.  ERROR: "  & DiagramManager.GetLastErrorString & vbCrLf & vbCrLf

				Else

					Set dlstable = dlsmdl.DataLineageTables.Item(m_table_name)

					If dlstable Is Nothing Then

						'log to error file
							error_log = error_log & "Row < " & curRow & " > - Data Lineage Table <" & m_table_name & "> could not be found in data flow <" & currDflow.Name & ">.  ERROR: "  & DiagramManager.GetLastErrorString & vbCrLf & vbCrLf

					Else


						'add the data lineage table to the data flow
						Set lincomp = currDflow.LineageComponents.Add(1, dlstable.ID, 100, 100)

						'see if the lineage component exists in the data flow
						Set lincomp = currDflow.LineageComponents.ItemBySource(1, dlstable.ID)


						'determine if we're adding an input or an output to the transformation
						If isInput = True Then
							Set dstream = currDflow.DataStreams.Add(currTran.Name,lincomp.ID,True)
						Else
							Set dstream = currDflow.DataStreams.Add(currTran.Name,lincomp.ID,False)
						End If

						If dstream Is Nothing And Not(lincomp Is Nothing) Then
							'log to error file
							error_log = error_log & "Row < " & curRow & " > -  Data Stream between  <" & lincomp.Name & "> and <" & currTran.Name & "> could not be added in data flow <" & currDflow.Name & ">.  ERROR: "  & DiagramManager.GetLastErrorString & vbCrLf & vbCrLf
						End If

						'now add the input or output column
						Set dlscolumn = dlstable.DataLineageColumns.Item(m_column_name)

						If dlscolumn Is Nothing Then

							'log to error file if view column doesn't exist
							error_log = error_log & "Row < " & curRow & " > -  Data Lineage Column  <" & m_column_name & "> could not be found in data flow <" & currDflow.Name & ">.  ERROR: "  & DiagramManager.GetLastErrorString & vbCrLf & vbCrLf


						Else

							'set input or output column based on view column
							If isInput = True Then
								Set tranfield = currTran.LineageInputs.Add(5,dlscolumn.ID)
							Else
								Set tranfield = currTran.LineageOutputs.Add(5,dlscolumn.ID)
							End If


						End If

					End If

				End If

			Else

				'if model exists then set the entity based on table_name
				Set ent = mdl.Entities.Item(m_table_name)
				
				If ent Is Nothing Then

					'if entity is nothing try views
					Set vw = mdl.Views.Item(m_table_name)

					If vw Is Nothing Then

						'log to error file
						error_log = error_log & "Row < " & curRow & " > -  Model Entity or View  <" & m_table_name & "> could not be found in data flow <" & currDflow.Name & ">.  ERROR: "  & DiagramManager.GetLastErrorString & vbCrLf & vbCrLf

					Else

						'add the view to the data flow
						Set lincomp = currDflow.LineageComponents.Add(16, vw.ID, 100, 100)

						'see if the lineage component exists in the data flow
						Set lincomp = currDflow.LineageComponents.ItemBySource(16, vw.ID)

						'determine if we're adding an input or an output to the transformation
						If isInput = True Then
							Set dstream = currDflow.DataStreams.Add(currTran.Name,lincomp.ID,True)
						Else
							Set dstream = currDflow.DataStreams.Add(currTran.Name,lincomp.ID,False)
						End If

						If dstream Is Nothing And Not(lincomp Is Nothing) Then
							'log to error file
							error_log = error_log & "Row < " & curRow & " > -  Data Stream between  <" & lincomp.Name & "> and <" & currTran.Name & "> could not be added in data flow <" & currDflow.Name & ">.  ERROR: "  & DiagramManager.GetLastErrorString & vbCrLf & vbCrLf
						End If


						'now add the input or output column
						Set vwcolumn = vw.ViewFields.Item(m_column_name)

						If vwcolumn Is Nothing Then

							'log to error file if view column doesn't exist
							error_log = error_log & "Row < " & curRow & " > -  View Column  <" & m_column_name & "> could not be found in data flow <" & currDflow.Name & ">.  ERROR: "  & DiagramManager.GetLastErrorString & vbCrLf & vbCrLf

						Else

							'set input or output column based on view column
							If isInput = True Then
								Set tranfield = currTran.LineageInputs.Add(17,vwcolumn.ID)
							Else
								Set tranfield = currTran.LineageOutputs.Add(17,vwcolumn.ID)
							End If

						End If
					
					End If

				Else

					'add the table to the data flow
					Set lincomp = currDflow.LineageComponents.Add(1, ent.ID, 100, 100)

					'see if the lineage component exists in the data flow
					Set lincomp = currDflow.LineageComponents.ItemBySource(1, ent.ID)


					'determine if we're adding an input or an output to the transformation
					If isInput = True Then
						Set dstream = currDflow.DataStreams.Add(currTran.Name,lincomp.ID,True)
					Else
						Set dstream = currDflow.DataStreams.Add(currTran.Name,lincomp.ID,False)
					End If

					If dstream Is Nothing And Not(lincomp Is Nothing) Then
						'log to error file
						error_log = error_log & "Row < " & curRow & " > -  Data Stream between  <" & lincomp.Name & "> and <" & currTran.Name & "> could not be added in data flow <" & currDflow.Name & ">.  ERROR: "  & DiagramManager.GetLastErrorString & vbCrLf & vbCrLf
					End If


					'now add the input or output column
					Set attr = ent.Attributes.Item(m_column_name)

					If attr Is Nothing Then

						'log to error file if column doesn't exist
						error_log = error_log & "Row < " & curRow & " > -  Model Column  <" & m_column_name & "> could not be found in data flow <" & currDflow.Name & ">.  ERROR: "  & DiagramManager.GetLastErrorString & vbCrLf & vbCrLf


					Else

						'set input or output column based on attribute
						If isInput = True Then
							Set tranfield = currTran.LineageInputs.Add(5,attr.ID)
						Else
							Set tranfield = currTran.LineageOutputs.Add(5,attr.ID)
						End If

						If tranfield Is Nothing Then
							'log to error file
							error_log = error_log & "Row < " & curRow & " > -  Transformation Input/Output  <" & m_column_name & "> between Lineage Component <" & lincomp.Name & "> and Transformation <" & tran.Name & "> could not be added in data flow <" & currDflow.Name & ">.  ERROR: "  & DiagramManager.GetLastErrorString & vbCrLf & vbCrLf
						End If

					End If  'attribute existence check

				End If  'entity/table existence check

			End If   'model existence check

		Loop While m_comma_position <> 0

	End If 'mappings exist check

End Sub


Function udfTransformationType ( inType As String ) As String

	If inType = "[default]" Then
		udfTransformationType = ""
	Else
		udfTransformationType = inType
	End If

End Function

Function IsPhysicalModel( mName As String ) As Boolean

	Dim m As Model

	Set m = diag.Models.Item(mName)

	If m Is Nothing Then

		IsPhysicalModel = False

	Else

		IsPhysicalModel = True

	End If

End Function

Sub PrintSampleSheet (  )

	curRow = 1
	curCol = 1

	PrintCell "Data Flow", curRow, curCol, 0, 1
	PrintCell "Transformation Name", curRow, curCol, 0, 1
	PrintCell "Transformation Type", curRow, curCol, 0, 1
	PrintCell "Input Columns", curRow, curCol, 0, 1
	PrintCell "Transformation Definition", curRow, curCol, 0, 1
	PrintCell "Transformation Code", curRow, curCol, 0, 1
	PrintCell "Output Columns", curRow, curCol, 0, 1

	curRow = 2
	curCol = 1

	PrintCell "String - Required", curRow, curCol, 0, 1
	PrintCell "String - Required", curRow, curCol, 0, 1
	PrintCell "String - Optional", curRow, curCol, 0, 1
	PrintCell "format should be <model or source name>.<view or table name>.<view field or column name> without the < >.", curRow, curCol, 0, 1
	PrintCell "String - Optional", curRow, curCol, 0, 1
	PrintCell "String - Optional", curRow, curCol, 0, 1
	PrintCell "format should be <model or source name>.<view or table name>.<view field or column name> without the < >.", curRow, curCol, 0, 1

	Excel.Range("A1:G1").interior.Color = CLR_GREY

	Excel.columns("A:C").autofit
	Excel.columns("E:F").autofit
    Excel.Columns("D:D").columnwidth = 60
    Excel.Columns("D:D").WrapText = True
    Excel.Columns("G:G").columnwidth = 60
    Excel.Columns("G:G").WrapText = True

	Excel.Rows("1:1").Font.Bold = True
    Excel.Rows("2:2").Font.Italic = True


End Sub

Function GetCell (range As Object, row As Integer, col As Integer, rowInc As Integer, colInc As Integer) As String

	GetCell = Trim(range.cells(row,col).Value)
	curRow = curRow + rowInc
	curCol = curCol + colInc

End Function

' Print a cell

Sub PrintCell(value As String, row As Integer, col As Integer, rowInc As Integer, colInc As Integer)
	
	'sample
	'	PrintCell ent.EntityName, curRow, curCol, 0, 1, clrFore, clrBack, 10, False

	Excel.Cells(row, col).Value = value

	curRow = curRow + rowInc
	curCol = curCol + colInc

End Sub

Rem See DialogFunc help topic for more information.
Private Function DialogFunc(DlgItem$, Action%, SuppValue&) As Boolean
	Select Case Action%
	Case 1 ' Dialog box initialization

	Case 2 ' Value changing or button pressed


		If DlgItem = "Browse" Then
			'browse to excel file if used pushes browse button.  Put path in text box.
			DlgText "path", GetFilePath(,"xlsx;xls",,"Open SpreadSheet")
			DialogFunc = True
		ElseIf DlgItem = "OK" And DlgText("path") = "" Then
			'don't exit dialog if a path is not specified
			MsgBox("Please enter a valid path.",,"Error!")
			DialogFunc = True
		ElseIf DlgItem = "getsample" Then
			Set Excel = CreateObject("Excel.Application")
			Excel.Visible = True
			Excel.Workbooks.Add
			PrintSampleSheet
			DialogFunc = True
		End If


		Rem DialogFunc = True ' Prevent button press from closing the dialog box
	Case 3 ' TextBox or ComboBox text changed
	Case 4 ' Focus changed
	Case 5 ' Idle
		Rem DialogFunc = True ' Continue getting idle actions
	Case 6 ' Function key
	End Select
End Function
