'#Language "WWB-COM"
'
' Macro Title: BatchConvertErwin10.bas
'
' This macro can be used to batch import erwin diagrams.
'
'
' Version: 1.0
' Date: 9/26/2014
'***********************************************************************************
Option Explicit

Dim BridgeID As String
Dim ConsistencyCheck As Integer
Dim DimensionalProperties As Boolean
Dim TargetDatabaseID As Integer
Dim EncodingID As Integer
Dim GenerateLogFile As Integer
Dim extER1 As Boolean
Dim extXML As Boolean
Dim sDir As String
Dim currentDrive As String
Dim currentDir As String
Dim ERWinFileDrive As String
Dim ERWinFileDir As String


Sub Main()
Dim ListFileType(3) As String
Dim ListConsistenyCheck(3) As String
Dim ListDimensionalProperties(2) As String
Dim ListTargetDatabase() As String
Dim ListEncoding() As String
Dim ListGenerateLogFile(3) As String


        ListFileType(0) = "CA AllFusion ERwin 4.x Data Modeler"
        ListFileType(1) = "CA AllFusion ERwin 7.x Data Modeler"
        ListFileType(2) = "CA AllFusion ERwin 9.x Data Modeler"


        ListConsistenyCheck(0) = "No Consistency Check"
        ListConsistenyCheck(1) = "Basic Consistency Check"
        ListConsistenyCheck(2) = "Detailed Consistency Check"

        ListDimensionalProperties(0) = "False"
        ListDimensionalProperties(1) = "True"

        ListGenerateLogFile(0) = "Always Generate"
        ListGenerateLogFile(1) = "Only Generate on Warnings and Errors"
        ListGenerateLogFile(2) = "Only Generate on Errors"

        LoadTargetDatabaseList ListTargetDatabase
        LoadEncodingList ListEncoding

	Begin Dialog UserDialog 650,427,"ERwin Conversion",.ConvertDialogFunc ' %GRID:10,7,1,1
		Text 10,14,60,14,"&Directory:",.txtDirectory
		TextBox 80,14,410,21,.edDir
		PushButton 510,14,120,21,"Choose Directory",.btnChooseDir
		GroupBox 10,63,450,63,"File Extensions",.gbFileExt
		CheckBox 20,84,420,14,"ERwin Model (Requires ERwin to be installed on the machine)",.cbER1
		CheckBox 20,105,90,14,".XML",.cbXML
		PushButton 200,399,90,21,"Convert",.btnConvert
		PushButton 310,399,90,21,"Exit",.btnExit
		Text 10,147,90,14,"File &Type",.txtFileType
		DropListBox 160,147,320,49,ListFileType(),.lbFileType
		Text 10,182,130,14,"Consistency Check",.txtConsistencyCheck
		DropListBox 160,182,320,70,ListConsistenyCheck(),.lbConsistencyCheck
		Text 10,217,200,14,"Export Dimensional Properties:",.txtDimensionalProperties
		DropListBox 220,217,260,70,ListDimensionalProperties(),.lbDimensionalProperties
		Text 10,259,120,14,"Target Database:",.txtTargetDatabase
		DropListBox 160,259,320,70,ListTargetDatabase(),.lbTargetDatabase
		Text 10,301,90,14,"Encoding",.txtEncoding
		DropListBox 160,301,320,70,ListEncoding(),.lbEncoding
		Text 10,336,120,14,"Generate Log File:",.txtGenerateLogFile
		DropListBox 160,336,320,56,ListGenerateLogFile(),.lbGenerateLogFile
	End Dialog
        Dim dlg As UserDialog


        dlg.cbER1 = 1
        dlg.cbXML = 0
        dlg.lbEncoding = 22
        dlg.lbGenerateLogFile = 0
        dlg.lbFileType = 1

        Debug.Clear
        Dialog dlg

End Sub
Private Function ConvertDialogFunc(DlgItem$, Action%, SuppValue&) As Boolean
        Dim n As Integer
        Dim retval As Long  ' return value

        Select Case Action%
        Case 1 ' Dialog box initialization


        Case 2 ' Value changing or button pressed
                Select Case DlgItem$
                        Case "btnChooseDir"


                        	currentDrive = Left$(CurDir, 1)
                            currentDir = Mid$(CurDir, 3)
							sDir = ShellBrowseForFolder()
							If (sDir <> "") Then
                            	DlgText "edDir", sDir
                            	ERWinFileDrive = Left$(sDir, 1)
                                ERWinFileDir = Mid$(sDir, 3)
                                If (ERWinFileDrive <> currentDrive) Then
                                	ChDrive ERWinFileDrive
                                End If
                                If (ERWinFileDir <> currentDir) Then
                                    ChDir ERWinFileDir$
                                End If
                            End If

                                ConvertDialogFunc = True
                                Exit Function
                        Case "btnConvert"
                                If (DlgValue("cbER1") = 0) And (DlgValue("cbXML") = 0) Then
                                        MsgBox "You must choose a file extension."
                                        ConvertDialogFunc = True
                                        Exit Function
                                End If

                                extER1 = (DlgValue("cbER1") = 1)

                                extXML = (DlgValue("cbXML") = 1)


                                sDir = DlgText("edDir")
                                If Len(sDir) = 0 Then
                                        MsgBox "You must specify a directory."
                                        ConvertDialogFunc = True
                                        Exit Function
                                ElseIf Not DirExists(sDir) Then
                                        MsgBox "Specified directory does not exist."
                                        ConvertDialogFunc = True
                                        Exit Function
                                End If

                                ERWinFileDrive = Left$(sDir, 1)
                                ERWinFileDir = Mid$(sDir, 3)
                                If (ERWinFileDrive <> currentDrive) Then
                                        ChDrive ERWinFileDrive
                                End If
                                If (ERWinFileDir <> currentDir) Then
                                        ChDir ERWinFileDir$
                                End If


                                ' BridgeID
                                Select Case DlgValue("lbFileType")
                                	Case 0
                                        BridgeID = "CaErwin4Xml"        'CA AllFusion ERwin 4.x Data Modeler ... 14
                                	Case 1
                                		BridgeID = "CaErwin7Xml"   'CA AllFusion ERwin 7.x Data Modeler ... 15
                                	Case 2
                                		BridgeID = "CaErwin9Xml"        'CA AllFusion ERwin 4.x Data Modeler ... 14
                                End Select

                                ' ConsistencyCheck
                                ConsistencyCheck = DlgValue("lbConsistencyCheck") + 1 'values range from 1-3

                                ' DimensionalProperties
                                n = DlgValue("lbDimensionalProperties")
                                If n = 0 Then
                                        DimensionalProperties = False
                                ElseIf n = 1 Then
                                        DimensionalProperties = True
                                End If

                                ' TargetDatabaseID
                                TargetDatabaseID = DlgValue("lbTargetDatabase")

                                ' EncodingID
                                EncodingID = DlgValue("lbEncoding")

                                ' GenerateLogFile
                                GenerateLogFile = DlgValue("lbGenerateLogFile")

                                doImport
                                MsgBox ("Done")
                                ConvertDialogFunc = True
                                Exit Function
                        Case "btnExit"
                                ConvertDialogFunc = False
                                Exit Function
                End Select
        End Select
End Function
' Source: http://www.freevbcode.com/ShowCode.asp?ID=501
Function DirExists(ByVal DName As String) As Boolean
        Dim sDummy As String

        On Error Resume Next

        If Right(DName, 1) <> "\" Then
                DName = DName & "\"
        End If
        sDummy = Dir$(DName & "*.*", vbDirectory)
        DirExists = Not (sDummy = "")
End Function
Sub LoadTargetDatabaseList(ListTargetDatabase() As String)

        ReDim ListTargetDatabase(0 To 58)

        ListTargetDatabase(0) = "Auto Detect"
        ListTargetDatabase(1) = "As/400 2"
        ListTargetDatabase(2) = "As/400 3"
        ListTargetDatabase(3) = "As/400 4.x"
        ListTargetDatabase(4) = "DB2/MVS 2"
        ListTargetDatabase(5) = "DB2/MVS 3"
        ListTargetDatabase(6) = "DB2/MVS 4"
        ListTargetDatabase(7) = "DB2/390 5"
        ListTargetDatabase(8) = "DB2/CS 2"
        ListTargetDatabase(9) = "DB2/UDB 5"
        ListTargetDatabase(10) = "Informix 4.x"
        ListTargetDatabase(11) = "Informix 5.x"
        ListTargetDatabase(12) = "Informix 7.x"
        ListTargetDatabase(13) = "Informix 9.x"
        ListTargetDatabase(14) = "Ingres 6.4"
        ListTargetDatabase(15) = "Ingres 1.x"
        ListTargetDatabase(16) = "Ingres 2.x"
        ListTargetDatabase(17) = "InterBase"
        ListTargetDatabase(18) = "Oracle 7.0"
        ListTargetDatabase(19) = "Oracle 7.1"
        ListTargetDatabase(20) = "Oracle 7.2"
        ListTargetDatabase(21) = "Oracle 7.3"
        ListTargetDatabase(22) = "Oracle 8.x"
        ListTargetDatabase(23) = "Progress 7.0"
        ListTargetDatabase(24) = "Progress 8.x"
        ListTargetDatabase(25) = "Rdb 4"
        ListTargetDatabase(26) = "Rdb 6"
        ListTargetDatabase(27) = "Rdb 7"
        ListTargetDatabase(28) = "Red Brick 3.0"
        ListTargetDatabase(29) = "Red Brick 4.0"
        ListTargetDatabase(30) = "Red Brick 5.x"
        ListTargetDatabase(31) = "SQL Server 4.x"
        ListTargetDatabase(32) = "SQL Server 6.x"
        ListTargetDatabase(33) = "SQL Server 7.x"
        ListTargetDatabase(34) = "SQLBase 5.0"
        ListTargetDatabase(35) = "SQLBase 6.x"
        ListTargetDatabase(36) = "Sybase 4.2"
        ListTargetDatabase(37) = "Sybase 10"
        ListTargetDatabase(38) = "Sybase 11.x"
        ListTargetDatabase(39) = "Teradata 1"
        ListTargetDatabase(40) = "Teradata 2.x"
        ListTargetDatabase(41) = "Watcom 3"
        ListTargetDatabase(42) = "Watcom 4"
        ListTargetDatabase(43) = "SQL Anywhere 5"
        ListTargetDatabase(44) = "Access 1.1"
        ListTargetDatabase(45) = "Access 2.0"
        ListTargetDatabase(46) = "Access 7"
        ListTargetDatabase(47) = "Access 97"
        ListTargetDatabase(48) = "Clipper"
        ListTargetDatabase(49) = "DBase 3"
        ListTargetDatabase(50) = "DBase 4"
        ListTargetDatabase(51) = "FoxPro"
        ListTargetDatabase(52) = "Paradox 4.x"
        ListTargetDatabase(53) = "Paradox 5.x"
        ListTargetDatabase(54) = "Paradox 7.x"
        ListTargetDatabase(55) = "HiRDB"
        ListTargetDatabase(56) = "ODBC 2.0"
        ListTargetDatabase(57) = "ODBC 3.0"
        ListTargetDatabase(58) = "SAS"

End Sub
Sub LoadEncodingList(ListEncoding() As String)

        ReDim ListEncoding(0 To 22)

        ListEncoding(0) = "No Encoding Conversion"
        ListEncoding(1) = "Central And Eastern European (ISO 8859-2)"
        ListEncoding(2) = "Central And Eastern European (Windows 1250)"
        ListEncoding(3) = "Chinese Traditional (Big5)"
        ListEncoding(4) = "Chinese Simplified"
        ListEncoding(5) = "Cyrillic (ISO 8859-5)"
        ListEncoding(6) = "Cyrillic (Windows 1251)"
        ListEncoding(7) = "DOS"
        ListEncoding(8) = "Greek (ISO 8859-7)"
        ListEncoding(9) = "Greek (Windows 1253)"
        ListEncoding(10) = "Hebrew (ISO 8859-8)"
        ListEncoding(11) = "Hebrew (Windows 1255)"
        ListEncoding(12) = "Japanese"
        ListEncoding(13) = "Korean"
        ListEncoding(14) = "Thai (TIS 620)"
        ListEncoding(15) = "Thai (Windows 874)"
        ListEncoding(16) = "Turkish (ISO 8859-1)"
        ListEncoding(17) = "Turkish (Windows 1254)"
        ListEncoding(18) = "UTF 8"
        ListEncoding(19) = "Western European (ISO 8859-1)"
        ListEncoding(20) = "Western European (ISO 8859-15)"
        ListEncoding(21) = "Western European (Windows 1259)"
        ListEncoding(22) = "Locale Encoding"
End Sub
Sub doImport()
    Dim objMetaImport As MetaImportObject

    Set objMetaImport = DiagramManager.MetaImportObject
    objMetaImport.BridgeIDName = BridgeID
    objMetaImport.ConsistencyCheck = ConsistencyCheck
    objMetaImport.DimensionalProperties = DimensionalProperties
    objMetaImport.EncodingID = EncodingID
    objMetaImport.GenerateLogFile = GenerateLogFile
    objMetaImport.TargetDatabaseID = TargetDatabaseID

    If extER1 Then
    	If (StrComp(BridgeID, "CaErwin4Xml", vbTextCompare) = 0) Then
        	ProcessFiles objMetaImport, "*.ER1"
        ElseIf (StrComp(BridgeID, "CaErwin7Xml", vbTextCompare) = 0) Or (StrComp(BridgeID, "CaErwin9Xml", vbTextCompare) = 0) Then
            ProcessFiles objMetaImport, "*.ERWIN"
        End If
	End If

    If extXML Then
    	ProcessFiles objMetaImport, "*.XML"
    End If

End Sub
Sub ProcessFiles(objMetaImport As MetaImportObject, fileExt As String)
        Dim F As String
        Dim theDiagram As Diagram
        Dim currentDir As String
        Dim dm1FileName As String
        Dim fileName As String
        Dim extPos As Integer
        Dim logFileName As String


        currentDir = CurDir()
        If (Right(currentDir, 1) <> "\") Then
                currentDir = currentDir + "\"
        End If

        currentDir = CurDir()
        If (Right(currentDir, 1) <> "\") Then
                currentDir = currentDir + "\"
        End If

        F$ = Dir$(fileExt)
        While F$ <> ""
		        Debug.Print "Converting """ & F$ & """"
		        objMetaImport.FileNameAndPath = currentDir & F$
			      extPos = InStrRev(F$, ".")
			      dm1FileName = Mid(F$, 1, extPos)
			      logFileName = dm1FileName & "log"
			      dm1FileName = dm1FileName & "dm1"
			    objMetaImport.ImportToFileNameAndPath = currentDir & dm1FileName
				objMetaImport.LogFileLocation = currentDir & logFileName
		        objMetaImport.DoImport
		        Debug.Print "Log file """ & objMetaImport.LogFileLocation & """ written."

		        Set theDiagram = DiagramManager.ActiveDiagram
		        If Not theDiagram Is Nothing Then
						'DeleteAttachments
		                'extPos = InStrRev(F$, ".")
		                'dm1FileName = Mid(F$, 1, extPos)
		                'dm1FileName = dm1FileName & "dm1"
		                fileName = dm1FileName
		                        If (UCase(theDiagram.FileName) = UCase(fileName)) Then
		                                dm1FileName = currentDir & dm1FileName
		                        theDiagram.SaveFile(dm1FileName)
		                        Debug.Print "Saved File """ & dm1FileName & """"
		                        DiagramManager.CloseDiagram (fileName)
		                End If
		            End If
		        DoEvents
		        F$ = Dir$()
        Wend
End Sub
Sub  DeleteAttachments()

	  Dim dict As Dictionary
      Dim attach As Attachment
      Dim atttype As AttachmentType

      Set dict = DiagramManager.ActiveDataDictionary
      For Each atttype In dict.AttachmentTypes
            For Each attach In atttype.Attachments
                  atttype.Attachments.Remove(attach.ID)
            Next
            dict.AttachmentTypes.Remove(atttype.ID)
      Next

End Sub
Function ShellBrowseForFolder() As String

	Const BIF_RETURNONLYFSDIRS As Long = &H1

    Dim objShell As Object
    Dim objFolder As Object

    Set objShell = CreateObject("shell.application")
    Set objFolder = objShell.BrowseForFolder(0, "SQL File Folder",  BIF_RETURNONLYFSDIRS, "")
    If (Not objFolder Is Nothing) Then
    	ShellBrowseForFolder = objFolder.Items.Item.Path
    Else
    	ShellBrowseForFolder = ""
    End If
End Function
