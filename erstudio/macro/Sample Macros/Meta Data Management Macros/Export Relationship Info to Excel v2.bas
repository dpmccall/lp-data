'MACRO TITLE: EXPORT RELATIONSHIP INFO TO EXCEL v2.BAS
' This macro generates a report on relationships.
' LAST UPDATE:  1/21/2015


Dim curRow As Integer
Dim curCol As Integer
Dim clrBack As Variant
Dim clrFore As Variant
Dim clrTitleBack As Variant
Dim clrTitleFore As Variant

' Dim MS Excel variables.
	
Dim Excel As Object
	
' Dim ER/Studio variables.

Public Const CLR_WHITE = RGB(255, 255, 255)
Public Const CLR_BLACK = RGB(0, 0, 0)
Public Const CLR_GREY = RGB(192, 192, 192)
Public Const CLR_TEAL = RGB(0, 128, 128)

Sub Main

	Dim theDiagram As Diagram
	Dim theModel As Model


	If (DiagramManager.DiagramCount = 0) Then
		MsgBox("Please open a model before running macro..", vbExclamation)
		Exit Sub
	End If

	Set theDiagram = DiagramManager.ActiveDiagram
	Set theModel = theDiagram.ActiveModel
	If (theModel.Relationships.Count = 0) Then
		MsgBox("There are no relationships in this model.", vbExclamation)
		Exit Sub
	End If


	' Create Excel workbook.
	Set Excel = CreateObject("Excel.Application")
	Excel.Workbooks.Add

	' Init the ER/Studio variables.
	curRow = 1
	curCol = 1

	clrBack = CLR_WHITE
	clrFore = CLR_BLACK
	clrTitleBack = CLR_GREY
	clrTitleFore = CLR_BLACK

	' Export
	PrintColumnHeader
	PrintData(theModel)
	AutofitAllUsed()
	MsgBox("Export Complete!", vbInformation,"ER/Studio")

	'make Excel visible
	Excel.Visible = True

End Sub

Function PrintData(theModel As Model)
	Dim theRelationship As Relationship
	Dim theFKColPair As FKColumnPair
	Dim S As String
	Dim theUserComment As UserComment

	For Each theRelationship In theModel.Relationships
		PrintCell theRelationship.Name, curRow, curCol, 0, 1, clrTitleFore, clrTitleBack, 10, False
		PrintCell theRelationship.BusinessName, curRow, curCol, 0, 1, clrTitleFore, clrTitleBack, 10, False
		PrintCell theRelationship.Definition, curRow, curCol, 0, 1, clrTitleFore, clrTitleBack, 10, False
		PrintCell theRelationship.ParentEntity.TableName, curRow, curCol, 0, 1, clrTitleFore, clrTitleBack, 10, False
		PrintCell theRelationship.ChildEntity.TableName, curRow, curCol, 0, 1, clrTitleFore, clrTitleBack, 10, False

		S = ""
		For Each theFKColPair In theRelationship.FKColumnPairs
			S = S + theFKColPair.ParentAttribute.ColumnName + ";"
		Next theFKColPair
		If (S <> "") Then S = Left(S, Len(S) - 1)
		PrintCell S, curRow, curCol, 0, 1, clrTitleFore, clrTitleBack, 10, False

		PrintCell theRelationship.VerbPhrase, curRow, curCol, 0, 1, clrTitleFore, clrTitleBack, 10, False
		PrintCell theRelationship.InversePhrase, curRow, curCol, 0, 1, clrTitleFore, clrTitleBack, 10, False

		Select Case theRelationship.CardinalityValue
			Case 0
				If theRelationship.OptionalityValue = 0 Then
					PrintCell "One to Zero or More" ,curRow, curCol, 0, 1, clrTitleFore, clrTitleBack, 10, False
				ElseIf theRelationship.OptionalityValue = 1 Then
					PrintCell "Zero or One to Zero or More" ,curRow, curCol, 0, 1, clrTitleFore, clrTitleBack, 10, False
				End If
			Case 1
				If theRelationship.OptionalityValue = 0 Then
					PrintCell "One to One or More",curRow, curCol, 0, 1, clrTitleFore, clrTitleBack, 10, False
				ElseIf theRelationship.OptionalityValue = 1 Then
					PrintCell "Zero or One to One or More",curRow, curCol, 0, 1, clrTitleFore, clrTitleBack, 10, False
				End If
			Case 2
				If theRelationship.OptionalityValue = 0 Then
					PrintCell "One to Zero or One",curRow, curCol, 0, 1, clrTitleFore, clrTitleBack, 10, False
				ElseIf theRelationship.OptionalityValue = 1 Then
					PrintCell "Zero or One to Zero or One" ,curRow, curCol, 0, 1, clrTitleFore, clrTitleBack, 10, False
				End If
			Case 3
				PrintCell "One to Exactly " + CStr(theRelationship.CardinalityValue) ,curRow, curCol, 0, 1, clrTitleFore, clrTitleBack, 10, False
		End Select

		Select Case theRelationship.Type
			Case 0
				PrintCell "Identifying", curRow, curCol, 0, 1, clrTitleFore, clrTitleBack, 10, False
			Case 1
				PrintCell "Non-Identifying", curRow, curCol, 0, 1, clrTitleFore, clrTitleBack, 10, False
			Case 2
				PrintCell "Non-Specific", curRow, curCol, 0, 1, clrTitleFore, clrTitleBack, 10, False
			Case 3
				PrintCell "Non-Identifying Optional",curRow, curCol, 0, 1, clrTitleFore, clrTitleBack, 10, False
		End Select

		If theRelationship.OptionalityValue = 0 Then
			PrintCell "Optional", curRow, curCol, 0, 1, clrTitleFore, clrTitleBack, 10, False
		ElseIf theRelationship.OptionalityValue = 1 Then
			PrintCell "Mandatory" ,curRow, curCol, 0, 1, clrTitleFore, clrTitleBack, 10, False
		End If

		S = ""
		S = "A " + theRelationship.ParentEntity.TableName + " " + theRelationship.VerbPhrase + " " + theRelationship.ChildEntity.TableName + "(s). " + _
			"An " + theRelationship.ChildEntity.TableName + " " + theRelationship.InversePhrase + " " + theRelationship.ParentEntity.TableName + "."
		PrintCell S, curRow, curCol, 0, 1, clrTitleFore, clrTitleBack, 10, False

		curRow = curRow + 1
		curCol = 1
	Next theRelationship
End Function
Sub PrintColumnHeader

		PrintCell "Parent Relationship FK Name", curRow, curCol, 0, 1, clrTitleFore, clrTitleBack, 10, True

		PrintCell "Business Name", curRow, curCol, 0, 1, clrTitleFore, clrTitleBack, 10, True

		PrintCell "Definition", curRow, curCol, 0, 1, clrTitleFore, clrTitleBack, 10, True

		PrintCell "Parent Table Name", curRow, curCol, 0, 1, clrTitleFore, clrTitleBack, 10, True

		PrintCell "Child Table Name", curRow, curCol, 0, 1, clrTitleFore, clrTitleBack, 10, True

		PrintCell "Parent Migrated Column Name", curRow, curCol, 0, 1, clrTitleFore, clrTitleBack, 10, True

		PrintCell "Verb Phrase", curRow, curCol, 0, 1, clrTitleFore, clrTitleBack, 10, True

		PrintCell "Inverse Verb Phrase", curRow, curCol, 0, 1, clrTitleFore, clrTitleBack, 10, True

		PrintCell "Cardinality", curRow, curCol, 0, 1, clrTitleFore, clrTitleBack, 10, True

		PrintCell "Relationship Type", curRow, curCol, 0, 1, clrTitleFore, clrTitleBack, 10, True

		PrintCell "Existence", curRow, curCol, 0, 1, clrTitleFore, clrTitleBack, 10, True

		PrintCell "Relationship Statement Rule", curRow, curCol, 0, 1, clrTitleFore, clrTitleBack, 10, True

		curRow = curRow + 1
		curCol = 1

End Sub
Sub PrintCell(value As String, row As Integer, col As Integer, rowInc As Integer, colInc As Integer, clrFore As Variant, clrBack As Variant, szFont As Integer, bBold As Boolean)
	Excel.Cells(row, col).Value = value

	Excel.Cells(row, col).Font.Bold = bBold
	Excel.Cells(row, col).Font.Color = clrFore
	Excel.Cells(row, col).Font.Size = szFont

	curRow = curRow + rowInc
	curCol = curCol + colInc
End Sub
Sub AutofitAllUsed()
	Dim x As Long

	For x = 1 To Excel.ActiveSheet.UsedRange.Columns.Count
    	Excel.ActiveSheet.UsedRange.Columns(x).EntireColumn.autofit
	Next x
End Sub
