Option Explicit
' Macro: Import Relationships From Excel.bas
'
' This macro will import relationships using information provided in Excel spreadsheet.
'
' The Excel spreadsheet ought to have the following column format:
'
'1. Relationship Name - Optional
'2. Parent Table Name - Mandatory
'3. Child Table Name  - Mandatory
'4. Relationship Type - Mandatory. It needs to be one of the following numeric values:
'    Identifying             => 0
'    NonIdentifying          => 1
'    NonSpecific             => 2
'    NonIdentifying Optional => 3
'    Subtype Cluster         => 99 (If added, next cell, can be Complete 0 and InComplete - 1)
'
' The first row in the spreadsheet is skipped as it can be the column titles.
' Version 1.0
'*******************************************************************************************************


Sub Main


	Begin Dialog UserDialog 720,105,"Import Relationships From Excel ",.DlgFunc ' %GRID:10,7,1,1
		Text 10,21,120,14,"Excel File Path:",.txtFile
		TextBox 150,21,450,21,.edFileName
		PushButton 610,21,90,21,"Select File",.btnSelectFile
		PushButton 260,70,90,21,"Import",.btnImport
		CancelButton 380,70,90,21,.btnExit
	End Dialog
	Dim dlg As UserDialog


	If (DiagramManager.ActiveDiagram Is Nothing) Then
		MsgBox "Please open a diagram file before running the macro.", vbExclamation
		Exit Sub
	End If

	If Dialog(dlg) = 0 Then Exit Sub

End Sub

Rem See DialogFunc help topic for more information.
Private Function DlgFunc(DlgItem$, Action%, SuppValue&) As Boolean
	Dim fileName As String
	Dim fileExt As String

	Select Case Action%
	Case 1 ' Dialog box initialization
	Case 2 ' Value changing or button pressed

		Select Case DlgItem$
	        Case "btnSelectFile"

					fileName = GetFilePath(,"xls;xlsx",,"Open File", 0)

					If (fileName <> "") Then
						DlgText("edFileName", fileName)
					End If

	                DlgFunc = True
	                Exit Function

            Case "btnImport"

                    fileName = DlgText("edFileName")

                    If Len(fileName) = 0 Then
                            MsgBox "You must specify a file."
                            DlgFunc = True
                            Exit Function
                    Else
                    	If Not FileExists(fileName) Then
                            MsgBox "Specified file does not exist."
                            DlgFunc = True
                            Exit Function
                        Else
							fileExt =  Right$(fileName, Len(fileName) - InStrRev(fileName, "."))
							If (LCase(Left(fileExt, 3)) <> "xls") Then
								MsgBox("You can only select Excel spreadsheets.", vbExclamation)
								Exit Function
							End If
                        End If
                    End If

                    doImport(fileName)

                    DlgFunc = False
                    Exit Function

            Case "btnExit"
                    DlgFunc = False
                    Exit Function
        End Select
	Case 3 ' TextBox or ComboBox text changed
	Case 4 ' Focus changed
	Case 5 ' Idle
		Rem DlgFunc = True ' Continue getting idle actions
	Case 6 ' Function key
	End Select
End Function
Function FileExists(ByVal fileName As String) As Boolean

	FileExists = (Dir(fileName) <> "")

End Function
Function doImport(ByVal fileName As String)

	Dim theDiagram As Diagram
	Dim theModel As Model

	Dim Excel As Object
	Dim sheet As Object
	Dim range As Object
	Dim rowCount As Integer
	Dim I As Integer
	Dim parentEntName As String
	Dim childEntName As String
	Dim relName As String
	Dim theRel As Relationship
	Dim TypeOfSubTypeCluster As Integer
	Dim relType As Integer
	Dim theSubTypeCluster As SubTypeCluster

	Dim logFile As String
	Dim fileHandle As Long
	Dim errMsg As String

	On Error GoTo errHandler

	'initialize excel object and make visible
	Set Excel = CreateObject("Excel.Application")
	Excel.workbooks.open fileName
	'get sheet info from excel object
	Set sheet = Excel.worksheets(1)
	Set range = sheet.usedrange
	rowCount = range.rows.Count


	' Log file
	logFile = GetUserMyDocumentDirectory()
	If Right(logFile, 1) <> "\" Then logFile = logFile & "\"
	logFile = logFile & "Import_Relationship_Log.txt"
	fileHandle = FreeFile
	Open logFile For Output As #fileHandle

	Set theDiagram = DiagramManager.ActiveDiagram
	Set theModel = theDiagram.ActiveModel
	DiagramManager.EnableScreenUpdateEx(False, True)
	For I = 2 To rowCount
		relName = Trim(range.cells(I,1).Value)
		parentEntName = Trim(range.cells(I,2).Value)
		childEntName = Trim(range.cells(I,3).Value)
		relType = CInt(Trim(range.cells(I,4).Value))

		Set theRel = Nothing

		If relType >= 0 And relType <= 3 Then
			Set theRel = theModel.Relationships.Add(parentEntName, childEntName, relType)
		ElseIf relType = 99 Then
			TypeOfSubTypeCluster = CInt(Trim(range.cells(I,5).Value))
			Set theSubTypeCluster = theModel.SubTypeClusters.Add(parentEntName, childEntName, TypeOfSubTypeCluster)
		End If

		errMsg = CheckOp()
		If errMsg <> "" Then
			errMsg = "Row: " & CStr(I) & " - Error: " & errMsg & ". When creating relationship from " & parentEntName & " to " & childEntName
			Print #fileHandle,
		Else
			If Not theRel Is Nothing Then
				theRel.Name = relName
			End If
		End If
	Next I
	Close #fileHandle
	DiagramManager.EnableScreenUpdateEx(True, True)
	Excel.Quit

	MsgBox "Import Complete!" & vbCrLf & "Log: " & logFile, vbInformation

	Exit Function

errHandler:

	DiagramManager.EnableScreenUpdateEx(True, True)

	If Not Excel Is Nothing Then
			Excel.Quit
	End If

	MsgBox Err.Description, vbCritical

	Reset

	End

End Function
Function CheckOp() As String
	Dim errorCode As Integer

	CheckOp = ""
	errorCode = DiagramManager.GetLastErrorCode()
	If (errorCode <> 0) Then
		CheckOp = DiagramManager.GetLastErrorString()
	End If
End Function
Function GetUserMyDocumentDirectory() As String
	' http://msdn.microsoft.com/en-us/library/0ea7b5xe.aspx
	Dim WshShell As Object
	Set WshShell = CreateObject("WScript.Shell")
	GetUserMyDocumentDirectory = WshShell.SpecialFolders("MyDocuments")
End Function
