' Macro: RepoBatchAddDiagrams.bas
'
' Purpose: This macro can add diagrams in the specified directory to repository in batch.
'
' Usage:
'
'   Directory:         Specify the location of SQL files. Use Choose Directory button to select folder.
'
'   Date: 8/30/2014
'
'============================================================================================
Option Explicit
Dim repoDiagramsInRepoDB As StringObjects
Sub Main

	If (DiagramManager.CurrentUser = "") Then
		MsgBox("You must log to repository before running this macro.", vbExclamation)
		Exit Sub
	End If

	Set repoDiagramsInRepoDB = DiagramManager.RepoDiagrams

	Begin Dialog UserDialog 670,98,"Bacth Add Diagrams to Repository",.FolderPathDialogFunc ' %GRID:10,7,1,1
		Text 10,14,60,14,"&Directory:",.txtDirectory
		TextBox 90,14,430,21,.edDir
		PushButton 540,14,120,21,"Choose Directory",.btnChooseDir
		PushButton 210,56,90,21,"Ok",.btnOk
		CancelButton 320,56,90,21
	End Dialog
    Dim dlg As UserDialog

    Debug.Clear
    If (Dialog(dlg) = -1) Then End

End Sub
Function IsInRepoDB(fileName As String) As Boolean
	Dim diagName As StringObject

	IsInRepoDB = False

	For Each diagName In repoDiagramsInRepoDB
		If (StrComp(diagName.StringValue, fileName, vbTextCompare) = 0) Then
			IsInRepoDB = True
			Exit Function
		End If
	Next diagName
End Function
Function ShellBrowseForFolder() As String
	Const BIF_RETURNONLYFSDIRS As Long = &H1

    Dim objShell As Object
    Dim objFolder As Object

    Set objShell = CreateObject("shell.application")
    Set objFolder = objShell.BrowseForFolder(0, "SQL File Folder",  BIF_RETURNONLYFSDIRS, "")
    If (Not objFolder Is Nothing) Then
    	ShellBrowseForFolder = objFolder.Items.Item.Path
    Else
    	ShellBrowseForFolder = ""
    End If
End Function
Private Function FolderPathDialogFunc(DlgItem$, Action%, SuppValue&) As Boolean
        Dim n As Integer
        Dim retval As Long  ' return value
        Dim fileExtensions As String
        Dim dbPlatform As String
        Dim sDir As String
        Dim saveClose As Boolean

        Select Case Action%
        Case 1 ' Dialog box initialization


        Case 2 ' Value changing or button pressed
                Select Case DlgItem$
                        Case "btnChooseDir"

							sDir = ShellBrowseForFolder()
							If (sDir <> "") Then DlgText("edDir", sDir)

                            FolderPathDialogFunc = True
                            Exit Function

                        Case "btnOk"

                            sDir = DlgText("edDir")
                            If Len(sDir) = 0 Then
                                    MsgBox "You must specify a directory."
                                    FolderPathDialogFunc = True
                                    Exit Function
                            ElseIf Not DirExists(sDir) Then
                                    MsgBox "Specified directory does not exist."
                                    FolderPathDialogFunc = True
                                    Exit Function
                            End If


                            AddDiagramsToRepository(sDir)
                            FolderPathDialogFunc = True
                            Exit Function
                End Select
        End Select
End Function
Function DirExists(ByVal DName As String) As Boolean
        Dim sDummy As String

        On Error Resume Next

        If Right(DName, 1) <> "\" Then
                DName = DName & "\"
        End If
        sDummy = Dir$(DName & "*.*", vbDirectory)
        DirExists = Not (sDummy = "")
End Function
Function AddDiagramsToRepository(ByVal sDir As String)
	Dim theDiagram As Diagram
	Dim logFile As String
	Dim logFileHandle As Long
	Dim logEntry As String
	Dim fileName As String
	Dim diagramPath As String
	Dim CurrentDir As String


	On Error GoTo errHandler

	CurrentDir = CurDir()
	ChDir(sDir)

	diagramPath = sDir
	If (Right(diagramPath, 1) <> "\") Then diagramPath = diagramPath + "\"


	' Create and open Log file
	logFile = GetUserMyDocumentDirectory()
	If (Right(logFile, 1) <> "\") Then logFile = logFile & "\"
	logFile = logFile & "Import_SQL_Log.txt"
	logFileHandle = FreeFile
	Open logFile For Output As #logFileHandle

	fileName$ = Dir$("*.dm1")
	While (fileName$ <> "")

		logEntry = "Processing: " & fileName$
	    Print #logFileHandle, logEntry ; vbCrLf

		If IsInRepoDB(fileName$) Then
			logEntry = "Repo Add Diagram Error. Add Diagram operation failed." & vbCrLf & _
				"ER/Studio Data Architect is unable to add this diagram to the Repository because the Repository already has a diagram with the same name."
			Print #logFileHandle, logEntry ; vbCrLf
			GoTo continueLoop
		End If

	    Set theDiagram = DiagramManager.OpenFile(fileName)
	    logEntry = CheckOp()
		If (logEntry <> "") Then
			Print #logFileHandle, logEntry ; vbCrLf
			GoTo continueLoop:
		End If

	    theDiagram.RepoAddDiagram
	    logEntry = CheckOp()
		If (logEntry <> "") Then
			Print #logFileHandle, logEntry ; vbCrLf
			GoTo continueLoop:
		End If

		DiagramManager.CloseDiagram(fileName$)
	    logEntry = CheckOp()
		If (logEntry <> "") Then
			Print #logFileHandle, logEntry ; vbCrLf
			GoTo continueLoop:
		End If


		continueLoop:
        fileName$ = Dir$()
	Wend

	ChDir(CurrentDir)
	Close #logFileHandle
	MsgBox("Operation Complete." + vbCrLf + "Log File: " + logFile, vbInformation)


	Exit Function

errHandler:
	If (CurrentDir <> "") Then ChDir(CurrentDir)
	MsgBox(Err.Description, vbCritical)
End Function
Function CheckOp() As String
	Dim errorCode As Integer

	CheckOp = ""
	errorCode = DiagramManager.GetLastErrorCode()
	If (errorCode <> 0) Then
		CheckOp = DiagramManager.GetLastErrorString()
	End If
End Function
Function GetUserMyDocumentDirectory() As String
	' http://msdn.microsoft.com/en-us/library/0ea7b5xe.aspx
	Dim WshShell As Object
	Set WshShell = CreateObject("WScript.Shell")
	GetUserMyDocumentDirectory = WshShell.SpecialFolders("MyDocuments")
End Function
