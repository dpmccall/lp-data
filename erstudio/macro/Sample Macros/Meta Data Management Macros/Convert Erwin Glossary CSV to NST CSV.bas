'TITLE:  Convert Erwin Glossary CSV to NST CSV

'DESCRIPTION:  This macro is used to import value references from an ERwin
'	glossary csv exported file to a ERStudio Naming Standards Template abbreviation
'	compatible csv file. Read the below ReadMe for the valid values of each
'	column in the spreadsheet.

'AUTHOR:  Luis Sucar
'CREATED: 1/12/2015


'README:  This defines datatypes and constraints on each column of the spreadsheet.
'	The first row of the output spread sheet is reserved for column headers.
'	In order to keep a correct conversion some colums included on
'	ERWin CSV (	Alt Abbreviation, M2 and Definition) will be ignored.
'	The specified column order is required for proper import.
'	All columns are required in the spreadsheet, but some may have null values.
'	You will find the resultant file stored in the same folder of the original ERwin glossary csv file.
'	The resultant file name will has this structure ERWinFileName+"_Converted.csv".
'	The resultant file will be a ERStudio Naming Standards Template abbreviation compatible file.

'FORMAT OF SPREADSHEET:
'	The source file must be a Comma Separated Value CSV
'	(Column Order Number) Column Name - Datatype - Description

' (1) Word/Words - String - This is required in the spreadsheet.
'		This is the reference value.

' (2) Abbreviation - String - This is required in the spreadsheet.
'		This Is the physical Name For the reference value.  This can�t be Null.

' (3) Alt Abbreviation - String - This is not required in the spreadsheet.
'		This can be Null. This value will be ignored.

' (4) P - String - The valid values are True if checked with "X" value or
'		False if the value is left in blank with "". This can be null.

' (5) M1 - String - The valid values are True if checked with "X" value or
'		False if the value is left in blank with "". This can be null.

' (6) M2 - String - The valid values are True if checked with "X" value or
'		False if the value is left in blank with "". This can be null.
'		This value will be ignored.

' (7) C - String - The valid values are True if checked with "X" value or
'		False if the value is left in blank with "". This can be null.

' (8) Definition - String - This value is not required. This can be null.
'		This value will be ignored.

Dim diag As Diagram
Dim mdl As Model
Dim excel As Object
Dim errorlog As String
Dim pathDestiny As String
Dim pathDestinyWarning As String
Dim wbIn As Variant
Dim wbOut As Variant
Dim wsIn As Variant
Dim wsOut As Variant

Sub Main
	
	Dim refval As ReferenceValue

	Set diag = DiagramManager.ActiveDiagram
	Set mdl = diag.ActiveModel
	
	Begin Dialog UserDialog 510,210,"Convert Erwin Glossary CSV to NST CSV",.DialogFunc ' %GRID:10,7,1,1
		Text 40,14,240,14,"Path to external glossary CSV file:",.Text1
		TextBox 70,35,340,21,.Path
		OKButton 200,168,110,21
		CancelButton 350,168,110,21
		PushButton 430,35,60,21,"Browse",.Browse
		Text 50,70,400,84,"Note:  Glossary values info must be in Sheet1."+ vbCrLf + vbCrLf +"IMPORTANT:If exists a previous exported file it will be overwrited."+ vbCrLf + vbCrLf + "See ReadMe for the format of the sheet.  Edit macro to view ReadMe.",.Text2
	End Dialog

	Dim dlg As UserDialog

	start_dialog:
	If Dialog(dlg) = -1 Then
	'initialize excel object
	Set excel = CreateObject("Excel.Application")
	'silence excel dialogs
	excel.Application.DisplayAlerts = False
	'initialize excel resultatn sheet
	Set wbOut = excel.workbooks.Add
	'this Error Is For an errant file path, Dialog will be restarted
	On Error GoTo Error_handle
	Set wbIn = excel.workbooks.open (dlg.Path, ,True)
	'Select sheets needed
	Set wsIn = wbIn.activesheet
	Set wsOut = wbOut.activesheet
	'Output file will be store on this path
	pathDestiny = dlg.Path
	pathDestinyWarning = pathDestiny
	pathDestinyWarning = Replace (pathDestinyWarning, ".csv","_Converted.csv")
	
	If Dir(pathDestinyWarning) <> "" Then
  		Dim Sure As Integer
		Sure = MsgBox("A previously converted file exists."+ vbCrLf + "Pressing Ok will cause it to be overwritten."+ vbCrLf + "Do you want to continue?", vbOkCancel)
		If Sure = vbOK Then
			'Input file processed and Output file generated
			ImportGlossary
		Else
			GoTo start_dialog
		End If
	Else
		'Input file processed and Output file generated
  		ImportGlossary
	End If

	
	'Closing wookbos and excel application
	wbIn.Close
	wbOut.Close
	excel.Application.Quit
	
	MsgBox("Import Completed!",,"ER/Studio")

	Exit Sub

	Error_handle:
		MsgBox("Please enter a valid path.",,"Error!")
		GoTo start_dialog

	End If

End Sub

Sub ImportGlossary

	Dim range As Object
    Dim RVCount As Long
	Dim refval As ReferenceValue
	Dim currentRV As String
	Dim lastRV As String
	Dim RVvalue As ValuePair
	Dim rowOrigin
	Dim rowDestiny

	'Sheet header inicialization
	wsOut.cells(1,1).Value = "Logical"
	wsOut.cells(1,2).Value = "Physical"
	wsOut.cells(1,3).Value = "Prime"
	wsOut.cells(1,4).Value = "Qualifier"
	wsOut.cells(1,5).Value = "Class"
	wsOut.cells(1,6).Value = "Illegal"
	wsOut.cells(1,7).Value = "Priority"

	Set range = wsIn.usedrange
	range.select

	RVCount = range.rows.Count
	Debug.Print "Reference Value Count = " & RVCount

	'If Input file has content the macro continues
	If RVCount >= 1 Then

		For rowOrigin = 1 To RVCount
			'Output file already has the firts row intialized
			rowDestiny=rowOrigin+1
			If range.cells(rowOrigin,1).Value <> "" Then
				'Values copied to respective cells in output wokbook
				wsOut.cells(rowDestiny,1).Value = range.cells(rowOrigin,1).Value
				wsOut.cells(rowDestiny,2).Value = range.cells(rowOrigin,2).Value
	
				If range.cells(rowOrigin,4).Value = "X" Then
					wsOut.cells(rowDestiny,3).Value = "Yes"
				Else
					wsOut.cells(rowDestiny,3).Value = "No"
				End If
		
				If range.cells(rowOrigin,5).Value = "X" Then
					wsOut.cells(rowDestiny,4).Value = "Yes"
				Else
					wsOut.cells(rowDestiny,4).Value = "No"
				End If
		
				If range.cells(rowOrigin,6).Value  = "X" Then
					wsOut.cells(rowDestiny,5).Value = "Yes"
				Else
					wsOut.cells(rowDestiny,5).Value = "No"
				End If
	
				wsOut.cells(rowDestiny,6).Value = "No"
				wsOut.cells(rowDestiny,7).Value = "Primary"
	
			End If
		Next
	'Storing the output file on CSV format
	pathDestiny = Replace (pathDestiny, ".csv","_Converted.csv")
	wbOut.SaveAs Filename:= pathDestiny,CreateBackup:=False, ConflictResolution:= xlLocalSessionChanges, FileFormat:= 6
	
	End If

End Sub

Rem See DialogFunc help topic for more information.
Private Function DialogFunc(DlgItem$, Action%, SuppValue&) As Boolean
	Select Case Action%
	Case 1 ' Dialog box initialization
	Case 2 ' Value changing or button pressed

		If DlgItem = "Browse" Then
			'browse to excel file if used pushes browse button.  Put path in text box.
			DlgText "path", GetFilePath(,"csv",,"Open External CSV")
			DialogFunc = True
		ElseIf DlgItem = "OK" And DlgText("path") = "" Then
			'don't exit dialog if a path is not specified
			MsgBox("Please enter a valid path.",,"Error!")
			DialogFunc = True
		End If

		Rem DialogFunc = True ' Prevent button press from closing the dialog box
	Case 3 ' TextBox or ComboBox text changed
	Case 4 ' Focus changed
	Case 5 ' Idle
		Rem DialogFunc = True ' Continue getting idle actions
	Case 6 ' Function key
	End Select
End Function
