' Macro: Drop Tables.bas
'
' This macro will generate a SQL file with DROP TABLE statements in dependency order.
'
' Version: 1.0
'***********************************************************************************

Option Explicit

Dim sqlFileName As String

'ER.Studio variables
Dim theDiagram As Diagram
Dim theModel As Model
Dim theSubModel As SubModel
Dim optionTableSelection As Integer

'Array variables
Dim theSelectedTableCollection() As Integer
Dim theRelCollection() As Integer
Dim theOrderedCollection() As Integer
Dim theConstraintCollection() As Integer
Sub Main()


	Begin Dialog UserDialog 650,231,"Drop Table DDL Generation",.DDLDialogFunc ' %GRID:10,7,1,1
		Text 10,14,60,14,"&Directory:",.txtDirectory
		TextBox 80,14,410,21,.edDir
		PushButton 510,14,120,21,"Choose File Path",.btnChooseDir
		PushButton 200,182,90,21,"Generate",.btnGenerate
		GroupBox 20,56,470,105,"Table Selection",.grpBoxTableSelection
		OptionGroup .opGroupBoxTableSelection
			OptionButton 50,84,220,14,"Selected Tables",.opSelectedTables
			OptionButton 50,119,90,14,"All Tables",.opAllTables
		CancelButton 320,182,90,21,.btnExit
	End Dialog

    Debug.Clear

    Dim dlg As UserDialog
	dlg.opGroupBoxTableSelection = 1

	Set theDiagram = DiagramManager.ActiveDiagram
	Set theModel = theDiagram.ActiveModel
	Set theSubModel = theModel.ActiveSubModel

   	If Dialog(dlg) = 0 Then
   		End
   	End If

End Sub
Function DDLDialogFunc(DlgItem$, Action%, SuppValue&) As Boolean
        Dim physpath As String

        Debug.Print DlgItem$ & " " & Action% & " " & SuppValue&

        Select Case Action%
        Case 1 ' Dialog box initialization


        Case 2 ' Value changing or button pressed
                Select Case DlgItem$
                        Case "btnChooseDir"

								sqlFileName = GetFilePath(theDiagram.FileName & "." & theModel.Name & ".sql", "sql;ddl;*",,"Save File As", 3)

								If (sqlFileName <> "") Then
									DlgText("edDir", sqlFileName)
								End If

                                DDLDialogFunc = True
                                'Exit Function

                        Case "btnGenerate"

                                physpath = DlgText("edDir")
                                optionTableSelection = DlgValue("opGroupBoxTableSelection")

                                If (physpath = "") Then
                                        MsgBox "You must specify a file."
                                        DDLDialogFunc = True
                                        Exit Function
                                Else
                                	physpath = Left(physpath, InStrRev(physpath, "\") )
                                	If ( Not DirExists(physpath) ) Then
                                        MsgBox "Specified directory does not exist."
                                        DDLDialogFunc = True
                                        Exit Function
                                    End If
                                End If

                                doGenerate()

                                DDLDialogFunc = False
                        Case "btnExit"
                                DDLDialogFunc = False
                End Select
            Case 5

        End Select
End Function
' Source: http://www.freevbcode.com/ShowCode.asp?ID=501
Function DirExists(ByVal DName As String) As Boolean
        Dim sDummy As String

        On Error Resume Next

        sDummy = Dir$(DName, vbDirectory)
        DirExists = Not (sDummy = "")
End Function
Function AddArrNoDup(theCollection() As Integer, value As Integer)
	Dim I As Integer
	Dim nCount As Integer
	Dim bFound As Boolean

	If (value = 0) Then
		Exit Function
	End If

	Err.Clear
	On Error Resume Next
	nCount = UBound(theCollection)
	If ( Err.Number <> 0 ) Then
		Exit Function
	End If

	bFound = False
	For I = LBound(theCollection) To nCount
		If ( theCollection(I) = value ) Then
			bFound = True
			Exit For
		End If
	Next I

	If Not bFound Then
		For I = LBound(theCollection) To nCount
			If (theCollection(I) = 0) Then
				theCollection(I) = value
				Exit For
			End If
		Next I
	End If
End Function
Function GetIndexOf(collection() As Integer, value As Integer) As Integer
	Dim I As Integer
	Dim nCount As Integer

	GetIndexOf = -1

	Err.Clear
	On Error Resume Next
	nCount = UBound(collection)
	If (Err.Number <> 0) Then
		Exit Function
	End If

	For I = LBound(collection) To nCount
		If (collection(I) = value) Then
			GetIndexOf = I
			Exit Function
		End If
	Next I
End Function
Function AddToCollection(collection() As Integer, value As Integer)
	Dim nCount As Integer

	Err.Clear
	On Error Resume Next
	nCount = UBound(collection)
	If (Err.Number <> 0) Then
		nCount = 1
		ReDim collection(nCount)
	Else
		nCount = nCount + 1
		ReDim Preserve collection(nCount)
	End If
	collection(nCount) = value
End Function
Function ResizeCollection(theCollection As Variant)
	Dim I As Integer
	Dim nCount As Integer

	On Error Resume Next
	If (UBound(theCollection) < 1) Then
		Exit Function
	End If

	For I = LBound(theCollection) To UBound(theCollection)
		If ( (theCollection(I) = 0) Or (theCollection(I) = "") ) Then
			nCount = I
			Exit For
		End If
	Next I

	If (nCount > 1) Then
		ReDim Preserve theCollection(nCount - 1)
	End If

End Function
Function GetSelectedTables()
	Dim theCurEntity As Entity
	Dim so As SelectedObject
	Dim nCount As Integer
	Dim I As Integer

	Select Case optionTableSelection
		Case 0
			For Each so In theSubModel.SelectedObjects
				If (so.Type = 1) Then
					Set theCurEntity = theModel.Entities(so.ID)
					AddArrNoDup(theSelectedTableCollection, theCurEntity.ID)
				End If
			Next so
		Case 1
			For Each theCurEntity In theModel.Entities
				AddArrNoDup(theSelectedTableCollection, theCurEntity.ID)
			Next theCurEntity
	End Select

	ResizeCollection(theSelectedTableCollection) 

End Function
Function GetSelectedRelationships()
	Dim I As Integer
	Dim theCurEntity As Entity
	Dim theCurRel As Relationship

	On Error Resume Next
	If (UBound(theSelectedTableCollection) < 1) Then
		Exit Function
	End If
	
	For I = LBound(theSelectedTableCollection) To UBound(theSelectedTableCollection)
		Set theCurEntity = theModel.Entities(theSelectedTableCollection(I))
		If Not theCurEntity Is Nothing Then
			If (theCurEntity.ParentRelationships.Count > 0) Then
				For Each theCurRel In theCurEntity.ParentRelationships
					AddArrNoDup(theRelCollection, theCurRel.ID)
				Next theCurRel
			End If

			If (theCurEntity.ChildRelationships.Count > 0) Then
				For Each theCurRel In theCurEntity.ChildRelationships
					AddArrNoDup(theRelCollection, theCurRel.ID)
				Next theCurRel
			End If

		End If
	Next I

	ResizeCollection(theRelCollection)
End Function
Function OrderTableCollection()
	Dim theRel As Relationship
	Dim id As Integer
	Dim I As Integer

	On Error Resume Next
	If (UBound(theRelCollection) < 1) Then
		For I = LBound(theSelectedTableCollection) To UBound(theSelectedTableCollection)
			id = theSelectedTableCollection(I)
			AddToCollection(theOrderedCollection(), id)
		Next I
		Exit Function
	End If

	For I = LBound(theRelCollection) To UBound(theRelCollection)
		Set theRel = theModel.Relationships(theRelCollection(I))
		If Not theRel Is Nothing Then
			' Recursively get all subtrees of children
			GetChildren(theRel)
		End If
	Next I
	ResizeCollection(theConstraintCollection())

	' Get all parent tables and tables that are not in any relation
	' GetChildren has all the children in the selected table list in ordered collection at this point.
	' What is left to see what selected tables are not in the collection and add them to it.
	For I = LBound(theSelectedTableCollection) To UBound(theSelectedTableCollection)
		id = theSelectedTableCollection(I)
		If (GetIndexOf(theSelectedTableCollection(), id) <> -1) And _
			(GetIndexOf(theOrderedCollection(), id) = -1) Then
				AddToCollection(theOrderedCollection(), id)
		End If
	Next I

End Function
Function GetChildren(theRel As Relationship)
	Dim theCurEntity As Entity
	Dim theCurRel As Relationship

	If (theRel.ParentEntity.ID = theRel.ChildEntity.ID) Then
		AddArrNoDup(theConstraintCollection(), theRel.ID)
		Exit Function
	End If

	Set theCurEntity = theRel.ChildEntity

	' Move down the tree
	For Each theCurRel In theCurEntity.ParentRelationships
		' Beware of self-referencing relationships
		If (theCurEntity.ID <> theRel.ParentEntity.ID) Then
			GetChildren(theCurRel)
		End If
	Next theCurRel

	' If the node is in the selected table list added it to the ordered collection
	If (GetIndexOf(theSelectedTableCollection(), theCurEntity.ID) <> -1) And _
		(GetIndexOf(theOrderedCollection(), theCurEntity.ID) = -1) Then
		AddToCollection(theOrderedCollection(), theCurEntity.ID)
	End If


End Function
Function doGenerate()

	If (theModel.Entities.Count > 0) Then
		ReDim theSelectedTableCollection(theModel.Entities.Count)
		GetSelectedTables()
	End If

	If (theModel.Relationships.Count > 0) Then
		ReDim theRelCollection(theModel.Relationships.Count)
		ReDim theConstraintCollection(theModel.Relationships.Count)
		GetSelectedRelationships()
	End If

	OrderTableCollection()

	ResizeCollection(theConstraintCollection)

	GenerateDDL()

End Function
Function GenerateDDL()
	Dim I As Integer
	Dim theEntity As Entity
	Dim fileHandle As Long
	Dim openEnclose As String
	Dim closeEnclose As String
	Dim sqlStmt As String
	Dim terminator As String
	Dim upperBound As Integer
	Dim theRel As Relationship
	Dim tableName As String

	If (InStr(UCase(theModel.DatabasePlatform), "MICROSOFT SQL SERVER") > 0) Then
		openEnclose = "["
		closeEnclose = "]"
		terminator = "GO"
	Else
		openEnclose = """"
		closeEnclose = """"
		terminator = ";"
	End If

	fileHandle = FreeFile
	Open sqlFileName For Output As #fileHandle

	On Error Resume Next
	upperBound = UBound(theConstraintCollection)
	If (Err.Number = 0) Then
		For I = LBound(theConstraintCollection) To upperBound
			Set theRel = theModel.Relationships(theConstraintCollection(I))
			If (Not theRel Is Nothing) Then
				Set theEntity = theRel.ChildEntity
				tableName = IIf(theEntity.Owner = "", "", openEnclose & theEntity.Owner & closeEnclose & ".")
				tableName = tableName & openEnclose & theEntity.TableName & closeEnclose
				sqlStmt = "ALTER TABLE " & tableName & " DROP CONSTRAINT " & theRel.Name & vbCrLf & terminator
				Print #fileHandle, sqlStmt
			End If
		Next I
	Else
		Err.Clear
	End If

	On Error Resume Next
	upperBound = UBound(theOrderedCollection)
	If (Err.Number = 0) Then
		For I = LBound(theOrderedCollection) To upperBound
			Set theEntity = theModel.Entities(theOrderedCollection(I))
			If Not theEntity Is Nothing Then
				tableName = IIf(theEntity.Owner = "", "", openEnclose & theEntity.Owner & closeEnclose & ".")
				tableName = tableName & openEnclose & theEntity.TableName & closeEnclose
				sqlStmt = "DROP TABLE " & tableName & vbCrLf & terminator
				Print #fileHandle, sqlStmt
			End If
		Next I
	End If

	Close #fileHandle

    MsgBox "Done. Generated file: " & sqlFileName, vbInformation
End Function
