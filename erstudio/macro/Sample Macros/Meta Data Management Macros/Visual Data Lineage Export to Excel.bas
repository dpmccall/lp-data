
'MACRO TITLE: VISUAL DATA LINEAGE EXPORT TO EXCEL.BAS
' This macro will output the visual data lineage information to excel.  If
' the format of the spreadsheet is unchanged you can update the
' mappings and import them back into the model.
' REQUIREMENT: You must have MS Excel 97 or later installed




' dim dialog variables
Dim data_flow_UIchoice As Integer
Dim data_flow_txtchoice As String
Dim data_flow_choice As Integer


' Dim MS Excel variables.
	
Dim Excel As Object
Dim curRow As Integer
Dim curCol As Integer

' Dim ER/Studio variables.
	
Dim diag As Diagram
Dim mdl As Model
Dim submdl As SubModel
Dim so As SelectedObject
Dim ent As Entity
Dim vw As View
Dim vwcolumn As ViewField
Dim attr As AttributeObj
Dim rel As Relationship
Dim entdisplay As EntityDisplay
Dim dflow As DataFlow
Dim dlmdl As DataLineageSource
Dim dltable As DataLineageTable
Dim dlcolumn As DataLineageColumn
Dim dlview As DataLineageView
Dim dlviewfield As DataLineageViewField

Public Const CLR_WHITE = RGB(255, 255, 255)
Public Const CLR_BLACK = RGB(0, 0, 0)
Public Const CLR_GREY = RGB(192, 192, 192)
Public Const CLR_TEAL = RGB(0, 128, 128)

Option Explicit

Sub Main

	DiagramManager.EnableScreenUpdate(False)


	' Init the ER/Studio variables.
	Set diag = DiagramManager.ActiveDiagram

	'fill array for model drop down in the dialog
	Dim dfcount As Integer
	dfcount = diag.DataFlows.Count

	If dfcount = 0 Then
		MsgBox("There is no DataFlow in this Diagram!",,"ER/Studio")
	Else
		ReDim DFListArray (0 To dfcount) As String
		
		Dim i As Integer
		i = 0
	
		For Each dflow In diag.DataFlows
	
			DFListArray(i) = dflow.Name
			i = i + 1
	
		Next
	
		curRow = 2
		curCol = 1
	
		Debug.Clear
	
		' Prompt the user.
		
		Begin Dialog UserDialog 390,287,"Data Lineage Export Options",.ExportHandler ' %GRID:10,7,1,1
			GroupBox 20,21,350,168,"Choose Export Scope",.GroupBox3
			OptionGroup .object_sel
				OptionButton 60,56,200,14,"Active Data Flow",.OptionButton0
				OptionButton 60,84,140,14,"All Data Flows",.OptionButton1
				OptionButton 60,112,220,14,"Choose Data Flow:",.OptionButton2
			OKButton 20,238,160,35
			CancelButton 220,238,150,35
			DropListBox 90,140,260,147,DFListArray(),.DataFlowList,2
		End Dialog
		Dim dlg As UserDialog
	
	
		' Print the spread sheet.
	
		If Dialog(dlg) = -1 Then
	
			' Start Excel and create a workbook
			Set Excel = CreateObject("Excel.Application")
			Excel.Workbooks.Add
			Excel.Visible = True
	
			Debug.Print dlg.dataflowlist
	
			' initialize Excel Workbook header
		    PrintColumnHeader
		    
		    Debug.Print data_flow_choice
	
		    'print lineage data to Excel
		    Select Case (data_flow_choice)
	
				Case 0
					
					Set dflow = diag.ActiveDataFlow
					PrintData(dflow)
	
				Case 1
	
					For Each dflow In diag.DataFlows
						PrintData(dflow)
					Next
	
				Case 2
	
					Set dflow = diag.DataFlows.Item(data_flow_txtchoice)
					PrintData(dflow)
	
		    End Select
	
			'Format Excel Worksheet
			Excel.rows.autofit
			Excel.columns("A:C").autofit
			Excel.columns("E:E").columnwidth = 20
			Excel.columns("E:E").WrapText = True
			Excel.columns("D:D").WrapText = True
			Excel.columns("G:G").WrapText = True
			Excel.columns("F:F").autofit
	
			MsgBox("Data Lineage export complete.",vbOkOnly)
	
		End If 'dialog
	
	End If 'check if DataFlow Exist

	DiagramManager.EnableScreenUpdate(True)

End Sub

Sub PrintData ( df As DataFlow )

	If df.Transformations.Count > 0 Then
		Dim tran As Transformation
	
		For Each tran In df.Transformations
	
			PrintCell df.Name, curRow, curCol, 0, 1
			PrintCell tran.Name, curRow, curCol, 0, 1
			PrintCell udfTransformationType(tran), curRow, curCol, 0, 1
			PrintCell udfTransformationColumns(tran, "INPUT"), curRow, curCol, 0, 1
			PrintCell tran.BusinessDefinition, curRow, curCol, 0, 1
			PrintCell tran.CodeDefinition, curRow, curCol, 0, 1
			PrintCell udfTransformationColumns(tran, "OUTPUT"), curRow, curCol, 0, 1
	
			curRow = curRow + 1
			curCol = 1
		
		Next
	End If

End Sub


' Print the column header.  Only print headers when value is true in options array.

Sub PrintColumnHeader (  )

	curRow = 1
	curCol = 1

	PrintCell "Data Flow", curRow, curCol, 0, 1
	PrintCell "Transformation Name", curRow, curCol, 0, 1
	PrintCell "Transformation Type", curRow, curCol, 0, 1
	PrintCell "Input Columns", curRow, curCol, 0, 1
	PrintCell "Transformation Definition", curRow, curCol, 0, 1
	PrintCell "Transformation Code", curRow, curCol, 0, 1
	PrintCell "Output Columns", curRow, curCol, 0, 1

	Excel.Range("A1:G1").interior.Color = CLR_GREY

	Excel.Rows("1:1").Select
    Excel.Selection.AutoFilter
    Excel.Columns("D:D").columnwidth = 50
    Excel.Columns("G:G").columnwidth = 50

	curRow = curRow + 1
	curCol = 1

End Sub



' Print a cell

Sub PrintCell(value As String, row As Integer, col As Integer, rowInc As Integer, colInc As Integer)
	
	Excel.Cells(row, col).Value = value

	curRow = curRow + rowInc
	curCol = curCol + colInc

End Sub

Function udfTransformationType ( currTran As Transformation ) As String

	If currTran.Type = "" Then
		udfTransformationType = "[default]"
	Else
		udfTransformationType = currTran.Type
	End If

End Function

Function udfTransformationColumns ( currTran As Transformation, ComponentType As String ) As String

	Dim ColumnListStr As String
	Dim TransFieldObj As TransformationField
	Dim ModelNameStr As String
	Dim TableNameStr As String
	Dim ColumnNameStr As String
	Dim TableDotColumnStr As String

	Select Case ComponentType

	Case "INPUT"

		ColumnListStr = ""

		If currTran.LineageInputs.Count = 0 Then

			udfTransformationColumns = ColumnListStr

		Else

			For Each TransFieldObj In currTran.LineageInputs

				Set mdl = diag.Models.Item(TransFieldObj.SourceModelID)

				If mdl Is Nothing Then

					Set dlmdl = diag.DataLineageSources.Item(TransFieldObj.SourceModelID)
					ModelNameStr = dlmdl.Name

					Select Case TransFieldObj.SourceObjectType

					Case 5 ' trans field is an attribute

						Set dltable = dlmdl.DataLineageTables.Item(TransFieldObj.SourceParentID)
						TableNameStr = dltable.Name

						Set dlcolumn = dltable.DataLineageColumns.Item(TransFieldObj.SourceObjectID)
						ColumnNameStr = dlcolumn.ColumnName

					Case 17 ' trans field is a view column

						Set dlview = dlmdl.DataLineageViews.Item(TransFieldObj.SourceParentID)
						TableNameStr = dlview.Name

						Set dlviewfield = dlview.DataLineageViewFields.Item(TransFieldObj.SourceObjectID)
						ColumnNameStr = dlviewfield.ParentColumnName
					
					Case Else

						Debug.Print "ERROR:  Output type not defined."
					
					End Select

				Else

					ModelNameStr = mdl.Name

					Select Case TransFieldObj.SourceObjectType

					Case 5 'transfield is an attribute

						Set ent = mdl.Entities.Item(TransFieldObj.SourceParentID)
						TableNameStr = ent.TableName

						Set attr = ent.Attributes.Item(TransFieldObj.SourceObjectID)
						ColumnNameStr = attr.ColumnName

					Case 17 ' transformation field is a view column

						Set vw = mdl.Views.Item(TransFieldObj.SourceParentID)
						TableNameStr = vw.Name

						Set vwcolumn = vw.ViewFields.Item(TransFieldObj.SourceObjectID)
						ColumnNameStr = vwcolumn.ParentColumnName

					Case Else

						Debug.Print "ERROR:  Output type not defined."

					End Select

				End If

				'ColumnListStr = ColumnListStr & ModelNameStr & "." & TransFieldObj.Name & "," & " "
				ColumnListStr = ColumnListStr & ModelNameStr & "." & TableNameStr & "." & ColumnNameStr & "," & " "

			Next

			ColumnListStr = Trim(ColumnListStr)

			If Right(ColumnListStr, 1) = "," Then

				ColumnListStr = Left(ColumnListStr, Len(ColumnListStr)-1)

			End If

			udfTransformationColumns = ColumnListStr
		
		End If

	Case "OUTPUT"

		ColumnListStr = ""

		If currTran.LineageOutputs.Count = 0 Then

			udfTransformationColumns = ColumnListStr

		Else

			For Each TransFieldObj In currTran.LineageOutputs

				Set mdl = diag.Models.Item(TransFieldObj.SourceModelID)

				If mdl Is Nothing Then

					Set dlmdl = diag.DataLineageSources.Item(TransFieldObj.SourceModelID)
					ModelNameStr = dlmdl.Name

					Select Case TransFieldObj.SourceObjectType

					Case 5 ' Transformation Field is an attribute

						Set dltable = dlmdl.DataLineageTables.Item(TransFieldObj.SourceParentID)
						TableNameStr = dltable.Name

						Set dlcolumn = dltable.DataLineageColumns.Item(TransFieldObj.SourceObjectID)
						ColumnNameStr = dlcolumn.ColumnName

					Case 17 ' transformation field is a view column

						Set dlview = dlmdl.DataLineageViews.Item(TransFieldObj.SourceParentID)
						TableNameStr = dlview.Name

						Set dlviewfield = dlview.DataLineageViewFields.Item(TransFieldObj.SourceObjectID)
						ColumnNameStr = dlviewfield.ParentColumnName

					Case Else

						Debug.Print "ERROR:  Output type not defined."

					End Select

				Else

					ModelNameStr = mdl.Name

					Select Case TransFieldObj.SourceObjectType

					Case 5 ' transformation field is an attribute

						Set ent = mdl.Entities.Item(TransFieldObj.SourceParentID)
						TableNameStr = ent.TableName

						Set attr = ent.Attributes.Item(TransFieldObj.SourceObjectID)
						ColumnNameStr = attr.ColumnName

					Case 17 ' transformation field is a view column

						Set vw = mdl.Views.Item(TransFieldObj.SourceParentID)
						TableNameStr = vw.Name

						Set vwcolumn = vw.ViewFields.Item(TransFieldObj.SourceObjectID)
						ColumnNameStr = vwcolumn.ParentColumnName

					Case Else

						Debug.Print "ERROR:  Output type not defined."

					End Select

				End If


				'ColumnListStr = ColumnListStr & ModelNameStr & "." & TransFieldObj.Name  & "," & " "
				ColumnListStr = ColumnListStr & ModelNameStr & "." & TableNameStr & "." & ColumnNameStr & "," & " "

			Next

			ColumnListStr = Trim(ColumnListStr)

			If Right(ColumnListStr, 1) = "," Then

				ColumnListStr = Left(ColumnListStr, Len(ColumnListStr)-1)

			End If

			udfTransformationColumns = ColumnListStr
		
		End If

	Case Else

		ColumnListStr = "TABLE_ERROR"
		udfTransformationColumns = ColumnListStr

	End Select

End Function

Function udfGetTransformationObjectName (currTransField As TransformationField ) As String

	Dim ObjectName As String

	'determine the whether the lineage component is a view or table
	Select Case currTransField.SourceObjectType

	Case 5	'SourceObjectType = 5 is the ID of an attribute/column (refer to object browser > help for Transformation object)

		Set mdl = diag.Models.Item(currTransField.SourceModelID)
		Set ent = mdl.Entities.Item(currTransField.SourceParentID)
		Set attr = ent.Attributes.Item(currTransField.SourceObjectID)

		ObjectName = ent.TableName & "." & attr.ColumnName

	Case 17 'SourceObjectType = 17 is the ID of an view column (refer to object browser > help for Transformation object)

		Set mdl = diag.Models.Item(currTransField.SourceModelID)
		Set vw = mdl.Views.Item(currTransField.SourceParentID)
		Set vwcolumn = vw.ViewFields.Item(currTransField.SourceObjectID)

		ObjectName = vw.Name & "." & vwcolumn.ParentColumnName

	Case Else

		ObjectName = "COLUMN_ERROR"

	End Select

	udfGetTransformationObjectName = ObjectName

End Function


Rem See DialogFunc help topic for more information.
Private Function ExportHandler(DlgItem$, Action%, SuppValue&) As Boolean
	
	Select Case Action%
	
	Case 1 ' Dialog box initialization

		DlgEnable "DataFlowList", False
		data_flow_txtchoice = DlgText("DataFlowlist")
		Debug.Print data_flow_txtchoice

	Case 2 ' Value changing or button pressed

		If DlgItem = "OptionButton2" Then

			DlgEnable "DataFlowList", True
			data_flow_choice = DlgValue("object_sel")
			data_flow_txtchoice = DlgText ("DataFlowlist")
			data_flow_UIchoice = DlgValue("DataFlowlist")

		End If

		If DlgItem = "object_sel" Then

			data_flow_choice = DlgValue("object_sel")

	  		Select Case data_flow_choice
	        
	        Case 0
				DlgEnable "DataFlowList", False
	        Case 1
				DlgEnable "DataFlowList", False
	        Case 2
				DlgEnable "DataFlowList", True
	        End Select

		'	data_flow_txtchoice = DlgText("DataFlowlist")
			data_flow_UIchoice = DlgValue("object_sel")

			Debug.Print DlgValue("object_sel")
			Debug.Print "data_flow_choice = " & data_flow_choice
			Debug.Print "data_flow_txtchoice = " & data_flow_txtchoice
			Debug.Print "data_flow_UIchoice = " & data_flow_UIchoice

		End If

		If DlgItem = "DataFlowList" Then

			data_flow_txtchoice = DlgText ("DataFlowlist")
			data_flow_UIchoice = DlgValue("DataFlowlist")
			Debug.Print data_flow_txtchoice
			Debug.Print data_flow_UIchoice

		End If


		Rem ExportHandler = True ' Prevent button press from closing the dialog box
	Case 3 ' TextBox or ComboBox text changed
	Case 4 ' Focus changed
	Case 5 ' Idle
		Rem ExportHandler = True ' Continue getting idle actions
	Case 6 ' Function key
	End Select
End Function

