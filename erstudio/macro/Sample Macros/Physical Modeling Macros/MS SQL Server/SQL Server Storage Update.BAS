Sub Main
	Dim ind As Index
	Dim mo As Model
	Dim diag As Diagram
	Dim ent As Entity


	Debug.Clear

	' Set ER variables

    Set diag = DiagramManager.ActiveDiagram
    Set mo = diag.ActiveModel

	' Debug.Print mo.MajorPlatform

	' Check that active model is a physical model and SQL Server model

	If mo.MajorPlatform = "Microsoft SQL Server" And mo.Logical = False Then

	Begin Dialog UserDialog 400,189,"SQL Server Storage Update Macro" ' %GRID:10,7,1,1
		CheckBox 50,14,190,14,"Update Table Parameter",.tablechckbx
		Text 80,42,70,14,"File Group:",.Text1
		Text 80,105,80,14,"File Group:",.Text2
		TextBox 170,35,160,21,.TBLFGInput
		CheckBox 50,77,230,14,"Update Index Parameters",.IndexChbx
		TextBox 170,98,160,21,.INXFGInput
		Text 80,133,80,14,"Fill Factor:",.Text3
		TextBox 170,126,160,21,.INXFFInput
		OKButton 50,154,130,21
		CancelButton 210,154,130,21
	End Dialog
	Dim dlg As UserDialog

		If Dialog(dlg) = -1 Then

			For Each ent In mo.Entities

				'set table File group if option is checked
				If dlg.tablechckbx = 1 Then
					ent.StorageLocation = dlg.TBLFGInput
				End If

				For Each ind In ent.Indexes

					'set index file group and fill factor if option is checked
					If dlg.indexchbx = 1 Then
						ind.Location = dlg.INXFGInput
						ind.FillfactorPercent = CLng(dlg.INXFFInput)
					End If

				Next ind
			Next ent

		End If 'dialog

	Else

		MsgBox("Active Model is not a SQL Server Physical Model.")

	End If 'DB platform check


End Sub
