'SELECTIVELY UPDATE ORACLE TABLE STORAGE PARAMETERS.BAS
'This Macro updates the Oracle Table Storage Parameters selectively.
'To update lasso a group of tables in an Oracle physical model and
'hit the execute button.
'MODIFY DATE: 3/13/2002


'array variables
Dim tblspaces() As String
Dim MaxExts() As String
Dim BufferPool() As String



'ER/Studio variables
Dim diag As Diagram
Dim mdl As Model
Dim submdl As SubModel
Dim ent As Entity
Dim so As SelectedObject

Sub setBP

	ReDim BufferPool (0 To 4) As String

	BufferPool(0) = "NONE"
	BufferPool(1) = "DEFAULT"
	BufferPool(2) = "KEEP"
	BufferPool(3) = "RECYCLE"

End Sub

Sub setMaxExts

	ReDim MaxExts(0 To 1) As String


	MaxExts(1) = "UNLIMITED"

End Sub


Sub gettblspaces

	Dim i, count As Integer
	Dim oratblspc As OracleTablespace


	count = mdl.OracleTablespaces.Count
	i = 0

	If count = 0 Then

		ReDim tblspaces(0)

	Else

		ReDim tblspaces (0 To count - 1)


		For Each oratblspc In mdl.OracleTablespaces

			tblspaces(i) = oratblspc.Name
			i = i + 1

		Next

	End If


End Sub


Sub Main

	Set diag = DiagramManager.ActiveDiagram
	Set mdl = diag.ActiveModel
	Set submdl = mdl.ActiveSubModel

	If mdl.MajorPlatform = "Oracle" Then

	gettblspaces
	setBP
	setMaxExts


	Begin Dialog UserDialog 820,322,"Table Storage Parameters" ' %GRID:10,7,1,1
		OKButton 550,280,110,28
		GroupBox 20,14,250,140,"Organization",.GroupBox1
		GroupBox 280,14,210,140,"Physical",.GroupBox2
		GroupBox 20,161,250,105,"Segment",.GroupBox3
		DropListBox 80,203,170,91,tblspaces(),.tblspcsdd,1
		GroupBox 280,161,210,105,"Group",.GroupBox4
		GroupBox 500,7,300,259,"Storage",.GroupBox5
		CheckBox 290,161,70,14,"Parallel",.Parallelchbx
		OptionGroup .organizeopt
			OptionButton 50,49,90,14,"Heap",.OptionButton1
			OptionButton 50,77,140,14,"Index Organized",.OptionButton2
		CheckBox 160,49,90,14,"Cache",.cachechbx
		Text 300,42,70,14,"Pct Free:",.PctFree
		Text 300,70,70,14,"Pct Used:",.PctUsed
		Text 300,98,90,14,"Initial Trans:",.initrans
		Text 300,126,90,14,"Max Trans:",.maxtrans
		TextBox 380,35,90,21,.pctfreetxt
		TextBox 380,63,90,21,.pctusedtxt
		TextBox 380,91,90,21,.initranstxt
		TextBox 380,119,90,21,.maxtranstxt
		Text 40,182,90,14,"Tablespace:",.tblspc
		Text 300,189,90,14,"Degrees:",.degrees
		Text 300,224,90,14,"Instances",.Text7
		TextBox 380,182,90,21,.degreestxt
		TextBox 380,217,90,21,.instancestxt
		CheckBox 80,238,110,14,"No Logging",.nologgingchbx
		Text 540,42,90,14,"Initial Extent:",.Text8
		Text 540,70,90,14,"Next Extent:",.Text9
		Text 540,98,90,14,"Pct Increase:",.Text10
		Text 540,126,90,14,"Min Extents:",.Text11
		TextBox 660,35,100,21,.iniextenttxt
		TextBox 660,63,100,21,.NextExtenttxt
		Text 540,182,90,14,"Free Lists:",.Text13
		TextBox 660,91,100,21,.pctincreasetxt
		TextBox 660,119,100,21,.minextentstxt
		DropListBox 660,147,120,49,MaxExts(),.maxextentsdd,1
		TextBox 660,175,100,21,.freeliststxt
		Text 540,210,120,14,"Free List Groups:",.Text14
		TextBox 660,203,100,21,.freelistgroupstxt
		Text 540,238,90,14,"Buffer Pool:",.Text15
		Text 540,154,90,14,"Max Extents:",.Text12
		Text 770,42,20,14,"KB",.Text1
		Text 770,70,20,14,"KB",.Text2
		DropListBox 660,231,100,70,BufferPool(),.bufferpooldd
		CancelButton 690,280,100,28
		Text 50,280,330,28,"NOTE:  Setting fields to -1 will set the parameters in the index editors to NULL.",.Text3
	End Dialog
	Dim dlg As UserDialog
	

	If Dialog(dlg) = -1 Then

		For Each so In submdl.SelectedObjects

			If so.Type = 1 Then

				Set ent = mdl.Entities.Item(so.ID)

				If IsNumeric(dlg.freelistgroupstxt) = True Then
					ent.FreeListGroups = CLng(dlg.FreeListGroupstxt)
				End If

				If IsNumeric(dlg.freeliststxt) = True Then
					ent.FreeLists = CLng(dlg.FreeListstxt)
				End If

				If IsNumeric(dlg.iniextenttxt) = True Then
					ent.InitialExtent = CLng(dlg.iniextenttxt)
				End If

				If IsNumeric(dlg.initranstxt) = True Then
					ent.InitTransactions = CLng(dlg.initranstxt)
				End If

				If dlg.maxextentsdd <> "" Then
					ent.MaxExtents = CStr(dlg.maxextentsdd)
				End If

				If IsNumeric(dlg.maxtranstxt) = True Then
					ent.MaxTransactions = CLng(dlg.maxtranstxt)
				End If

				If IsNumeric(dlg.minextentstxt) = True Then
					ent.MinExtents = CLng(dlg.minextentstxt)
				End If

				If IsNumeric(dlg.nextextenttxt) = True Then
					ent.NextExtent = CLng(dlg.nextextenttxt)
				End If

				
				If dlg.NoLoggingchbx = 1 Then
					ent.NoLogging = True
				Else
					ent.NoLogging = False
				End If

				ent.OracleBufferPool = BufferPool(dlg.bufferpooldd)

				If dlg.organizeopt = 0 Then
					ent.OracleIndex = False
				Else
					ent.OracleIndex = True
				End If

				If dlg.cachechbx = 1 Then
					ent.OracleCache = True
				Else
					ent.OracleCache = False
				End If

				If dlg.parallelchbx = 1 Then
					ent.OracleParallel = True
					ent.OracleDegrees = CStr(dlg.degreestxt)
					ent.OracleInstances = CStr(dlg.instancestxt)
				Else
					ent.OracleParallel = False
				End If

				If IsNumeric(dlg.PctFreetxt) = True Then
					ent.PctFree = CLng(dlg.pctfreetxt)
				End If

				If IsNumeric(dlg.pctincreasetxt) = True Then
					ent.PctIncrease = CLng(dlg.pctincreasetxt)
				End If

				If IsNumeric(dlg.pctusedtxt) = True Then
					ent.PctUsed = CLng(dlg.pctusedtxt)
				End If


				ent.StorageLocation = dlg.tblspcsdd

			End If


		Next


	End If

	Else

		MsgBox("Active Model must be an Oracle Physical Model.",,"ERROR!")

	End If


	
End Sub
