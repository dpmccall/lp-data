'#Language "WWB-COM"

Option Explicit

Function cleanName(inDirtyName As String) As String
	cleanName = LCase(inDirtyName)
End Function



Sub Main
	' *******************************************************************
	' Postgres Permissions
	' Uses physical model name as prefix for 3 permission groups.
	' *  <physical model>_ro:  read only permissions
	' *  <physical model>_rw:  read/write permissions
	' *  <physical model>_adm:  administrator permissions
	' Executes against all the tables in the model (not just selected ones).
	' *******************************************************************


	If DiagramManager.ActiveDiagram.ActiveModel.Logical = True Then
		Dim target_model As Model
		Set target_model = DiagramManager.ActiveDiagram.ActiveModel

		Dim current_entity As Entity
		For Each current_entity In target_model.Entities
			Debug.Print "...." + current_entity.TableName

			Dim clean_table_name As String
			clean_table_name = Replace(LCase(current_entity.EntityName), " ", "_")
			current_entity.TableName = clean_table_name
		Next

		MsgBox "Done"
	Else
		Err.Raise(-20000, "This macro acts on logical model only.")
	End If
End Sub
