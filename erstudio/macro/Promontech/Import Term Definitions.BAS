'#Reference {00000600-0000-0010-8000-00AA006D2EA4}#2.8#0#C:\Program Files\Common Files\System\ado\msadox28.tlb#Microsoft ADO Ext. 2.8 for DDL and Security#ADOX
'#Reference {B691E011-1797-432E-907A-4D8C69339129}#6.1#0#C:\Program Files\Common Files\System\ado\msado15.dll#Microsoft ActiveX Data Objects 6.1 Library#ADODB
'#Reference {E0E270C2-C0BE-11D0-8FE4-00A0C90A6341}#1.5#0#C:\Windows\System32\simpdata.tlb#Microsoft OLE DB Simple Provider 1.5 Library#MSDAOSP
'#Reference {00000600-0000-0010-8000-00AA006D2EA4}#6.0#0#C:\Program Files\Common Files\System\ado\msadox.dll#Microsoft ADO Ext. 6.0 for DDL and Security#ADOX
'#Language "WWB-COM"

Option Explicit
Public Const DSN As String = "dddev"
Public Const USERNAME As String = "postgres"
Public Const PASSWORD As String = "lO3XUV8l"

Private Type termRec
	termKey As String
	termName As String
	termDefinition As String
End Type


Function termKeysFromAnnotation(inAnnotationText As String) As String()
	If Len(inAnnotationText) > 0 Then
		Dim starting As Integer
		Dim ending As Integer

		starting = InStr(1, inAnnotationText, "{")
		ending = InStr(1, inAnnotationText, "}")

		If starting > 0 And ending > 0 And ending > starting Then
			termKeysFromAnnotation = Split(Mid(inAnnotationText, starting + 1, ending - starting - 1), ",")
		End If
	End If
End Function



Function findTerm(inTermKey As String) As termRec
	Dim cn As New ADODB.Connection
	Dim rs As New ADODB.Recordset

	'Open the connection
	cn.Open "DSN=" + DSN + ";UID=" + USERNAME + ";PWD=" + PASSWORD + ";"

	Dim sql_text As String
	sql_text = "SELECT term_name, term_definition FROM public.term_v WHERE term_key = '" + inTermKey + "' AND term_name IS NOT NULL"

	'Open the recordset
	rs.Open sql_text, cn

	'Loop though the recordset print the results
	Dim result As termRec
	While Not rs.EOF
		result.termKey = inTermKey
		result.termName = rs!term_name
		result.termDefinition = rs!term_definition
		rs.MoveNext
	Wend

	'Cleanup
	If rs.State <> adStateClosed Then rs.Close
	Set rs = Nothing
	If cn.State <> adStateClosed Then cn.Close
	Set cn = Nothing

	findTerm = result
End Function


Function notes(inDefinition As String) As String
	Dim result As String

	Dim term_keys() As String
	term_keys = termKeysFromAnnotation(inDefinition)

 	result = "<table border='1'><tr><td bgcolor='#FF0000'><font size=2 face='Arial'><b>Term Key</b></font></td><td bgcolor='#FF0000'><font size=2 face='Arial'><b>Term Name</b></font></td><td bgcolor='#FF0000'><font size=2 face='Arial'><b>Term Definition</b></font></td></tr>"
 	Dim x As Integer
 	For x = LBound(term_keys) To UBound(term_keys)
 		Dim term As termRec
 		term = findTerm(term_keys(x))

		result = result + "<tr><td><font size=2 face='Arial'>" + term.termKey + "</font></td><td><font size=2 face='Arial'>" + term.termName + "</font></td><td><font size=2 face='Arial'>" + term.termDefinition + "</font></td></tr>"
 	Next
	result = result + "</table>"

	notes = result
End Function







'
'  loop through the annotated dictionary elements
'
Sub Main
	'  set the diagram
	Dim domain_diagram As Diagram
	Set domain_diagram = DiagramManager.ActiveDiagram


	'  set the definition in the domains
	Dim current_domain As Domain
	For Each current_domain In domain_diagram.Dictionary.Domains
		If Mid(current_domain.Definition, 1, 18) = "@DictionaryElement" Then
			current_domain.Note = notes(current_domain.Definition)
		End If
	Next


	'  set the definition in the attributes
	Dim current_entity As Entity
	For Each current_entity In DiagramManager.ActiveDiagram.ActiveModel.Entities
		Dim current_attribute As AttributeObj
		For Each current_attribute In current_entity.Attributes
			If current_attribute.IsOverrideEnabled(5) Then
				current_attribute.EnableOverride(6, True)
				current_attribute.Notes	= notes(current_attribute.Definition)
			End If
		Next
	Next


	MsgBox "Done"
End Sub
