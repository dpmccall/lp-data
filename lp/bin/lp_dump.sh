#!/bin/bash
#
#  source common script
#
my_dir=$(dirname $0)
source ${my_dir}/lp_common.sh


#
#  usage
#
usage() {
    inMessage=${1}

    echo "${inMessage}"
    echo ""
    echo "usage: ${0} [options] [actions]"
    echo "General options:"
    echo "-?, --help        Show this message"
    echo "-v, --verbose     Verbose"
    echo "-c, --config      Configuration file name"
}


#
#  verbose
#
verbose() {
    appender_setLevel stderr TRACE
    logger_trace "Verbose output enabled."
}


#
#  dumpAggregate
#
dumpAggregate() {
    inConfig=${1}
    source ${inConfig}
    logger_info `set | grep "aggregate_"`
   
    PGPASSWORD=${aggregate_password}
    pg_dump --host=${aggregate_host} --port=${aggregate_port} --dbname=${aggregate_database} --data-only > ${aggregate_dump_directory}/aggregate.sql
    if [ $? -ne 0 ]
    then
        logger_error "Failed exporting aggregate"
        exit -1
    fi
}


#
#  dumpEventStore
#
dumpEventStore() {
    inConfig=${1}
    source ${inConfig}
    logger_info `set | grep "eventstore_"`
    
    mongoexport --host=${eventstore_host} --port=${eventstore_port} --username=${eventstore_username} --password=${eventstore_password} --db=${eventstore_database} --collection=${eventstore_collection} --out=${eventstore_dump_directory}/${eventstore_collection}.json
    if [ $? -ne 0 ]
    then
        logger_error "Failed exporting eventstore"
        exit -2
    fi
}


#
#  dump
#
dump() {
    inConfig=${1}    
    logger_info "dump():  inConfig=${inConfig}"
    
    dumpAggregate ${inConfig}
    dumpEventStore ${inConfig}
}



#
#  main
#


#  Handle environment variables.  If .pgpass file does not exist, the script will request password for each call to psql.
logger_debug "lp_export"


#  Apply defaults
if [ -z "${port}" ]
then
    port="5432"
fi
if [ -z "${username}" ]
then
    username="postgres"
fi
if [ -z "${schema}" ]
then
    schema="public"
fi


#  Parse command line parameters.  Supports short and long parameters.
logger_debug "Parsing parameters."
while getopts 'vc:-' OPTION
do
    case "$OPTION" in
        v  ) verbose;;
        c  ) config="$OPTARG";;
        -  )
            [ $OPTIND -ge 1 ] && optind=$(expr $OPTIND - 1 ) || optind=$OPTIND
            eval OPTION="\$$optind"
            OPTARG=$(echo $OPTION | cut -d'=' -f2)
            OPTION=$(echo $OPTION | cut -d'=' -f1)
            case $OPTION in
                --verbose   ) verbose;;
                --help      ) usage "Requested help"; exit 0;;
                --config    ) config="$OPTARG";;
                *           ) usage "Invalid argument.  $OPTION"; exit -1;;
            esac
            OPTIND=1
            shift
            ;;
        ?  ) usage "Requested help"; exit 0;;
        *  ) usage "Invalid parameter = $OPTION"; exit -1;;
    esac
done
cat ${config} | logger_debug 


#  Process the requested action verb.
action=${@:$OPTIND:1}
case "${action}" in
    dump )
        dump ${config};;
    * )
        usage "Unknown action = ${action}"; exit -1;;
esac




