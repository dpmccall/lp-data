#!/bin/bash
#
#  source common script
#
my_dir=$(dirname $0)
source ${my_dir}/dd_common.sh


#
#  usage
#
usage() {
    inMessage=${1}

    echo "${inMessage}"
    echo ""
    echo "usage: ${0} [options] [actions]"
    echo "Options:"
    echo "-?, --help        Show this message"
    echo "-v, --verbose     Verbose"
    echo "-d, --directory   location of files for parsing"
    echo ""
    echo "Actions:"
    echo "findAndParseAnnotations   extract Java tags from code"
}


#
#  verbose
#
verbose() {
    appender_setLevel stderr TRACE
    logger_trace "Verbose output enabled."
}


#
#  parseAnnotation
#
parseAnnotationForTermKeys() {
    inAnnotation=${1}
    logger_trace "inAnnotation=${inAnnotation}"
    term_key_tokens=`echo ${inAnnotation} | cut -f2 -d'(' | cut -f1 -d')' | cut -f2 -d '=' | tr -d '{' | tr -d '}' | tr -d ' '`                       
    logger_trace "term_key_tokens=${term_key_tokens}"
    
    IFS=','
    for term_key in $term_key_tokens
    do
        logger_trace ${term_key}
        echo "${term_key}"
    done
}


#
#  annotationToJson
#
annotationToJson() {
    inFileName=${1}
    inAnnotation=${2}
    inFileType=${3}

    json="{\"file\":\"${inFileName}\",\"term_keys\":["
    key_array=""
    number_of_terms=0
    while read term_key
    do
        let number_of_terms=number_of_terms+1
        key_array="${key_array},${term_key}"
    done < <(parseAnnotationForTermKeys "${inAnnotation}")
    key_array=`echo ${key_array} | cut -c2-`
    json="${json}${key_array}"
    json="${json}], \"number_of_terms\":${number_of_terms}},"
    json="${json}], \"file_type\":\"${inFileType}\"}"
    echo "${json}"
}


#
#  findAndParseAnnotations
#
findAndParseAnnotations() {
    inDirectory=${1}
    inFilePattern=${2}
    
    
    grep -R --include="${inFilePattern}" "@DictionaryElement" ${inDirectory} | while read grep_stdout
    do
        file_name=`echo ${grep_stdout} | cut -f1 -d':'`
        annotation=`echo ${grep_stdout} | cut -f2 -d':'`
        
        annotationToJson "${file_name}" "${annotation}" "${inFilePattern}"
    done
}





#
#  main
#

#  Handle environment variables.  If .pgpass file does not exist, the script will request password for each call to psql.
#  Parse command line parameters.  Supports short and long parameters.
logger_debug "Parsing parameters."
while getopts 'vd:' OPTION
do
    case "$OPTION" in
        v  ) verbose;;
        d  ) directory="$OPTARG";;
        -  )
            [ $OPTIND -ge 1 ] && optind=$(expr $OPTIND - 1 ) || optind=$OPTIND
            eval OPTION="\$$optind"
            OPTARG=$(echo $OPTION | cut -d'=' -f2)
            OPTION=$(echo $OPTION | cut -d'=' -f1)
            case $OPTION in
                --verbose   ) verbose;;
                --help      ) usage "Requested help"; exit 0;;
                --directory ) directory="$OPTARG";;
                *           ) usage "Invalid argument.  $OPTION"; exit -1;;
            esac
            OPTIND=1
            shift
            ;;
        ?  ) usage "Requested help"; exit 0;;
        *  ) usage "Invalid parameter = $OPTION"; exit -1;;
    esac
done
logger_trace "directory=${directory}"


#  Process the requested action verb.
action=${@:$OPTIND:1}
case "${action}" in
    findAndParseAnnotations )
        findAndParseAnnotations ${directory};;
    * )
        usage "Unknown action = ${action}"; exit -1;;
esac




