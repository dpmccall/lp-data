#!/bin/bash
#
#  source common script
#
my_dir=$(dirname $0)
source ${my_dir}/pg_common.sh


#
#  usage
#
usage() {
    inMessage=${1}

    echo "${inMessage}"
    echo ""
    echo "usage: ${0} [options] [actions]"
    echo "Options:"
    echo "-?, --help        Show this message"
    echo "-v, --verbose     Verbose"
    echo "-h, --host=       database server host, overrides PGHOST"
    echo "-p, --port=       database server port (default: 5432), overrides PGPORT"
    echo "-d, --dbname=     database name, overrides PGDATABASE"
    echo "-U, --username=   database user name (default: postgres), overrides PGUSER"
    echo "-s, --schema=     database schema (default: public)"
    echo "-t, --table=      table name"
    echo ""
    echo "Actions:"
    echo "tables            list tables"
    echo "dropTables        drop all tables in schema"
    echo "makeDropTables    create SQL to drop tables in a schema"
    echo "makeGrantTables   generate SQL to grant permissions to ro and rw roles"
    echo "schemas           list schemas"
    echo "columns           list columns"
    echo "makeInsert        generate the SQL to insert a row"
    echo "makeCountTable    generate SQL to count the number of rows in a table"
    echo "countTable        show the number of rows in a table"
    echo "countTables       show the number of rows in every table of a schema"
    echo "makeSelect        generate the SQL to select from the table or view"
}


#
#  verbose
#
verbose() {
    appender_setLevel stderr TRACE
    logger_trace "Verbose output enabled."
}


#
#  columns
#
columns() {
    inHost=${1}
    inPort=${2}
    inDatabase=${3}
    inUsername=${4}
    inSchema=${5}
    inTable=${6}
    
    psql \
        --host=${inHost} \
        --port=${inPort} \
        --username=${inUsername} \
        --dbname=${inDatabase} \
        --quiet \
        --tuples-only \
        --field-separator-zero \
        --record-separator-zero \
        --command="SELECT c.column_name FROM information_schema.columns c WHERE c.table_catalog = '${inDatabase}' AND c.table_schema = '${inSchema}' AND c.table_name = '${inTable}' ORDER BY c.ordinal_position ASC" | sed -e '$d'
    if [ ${PIPESTATUS[0]} -ne 0 ]
    then
        logger_error "Table list returned non-zero error code."
        exit -1
    fi    
}


#
#  makeInsert
#
makeInsert() {
    inHost=${1}
    inPort=${2}
    inDatabase=${3}
    inUsername=${4}
    inSchema=${5}
    inTable=${6}
    
    echo "INSERT INTO ${inSchema}.${inTable}"
    echo "("
    #  list columns
    while read row_data
    do
        column_name=`echo ${row_data} | cut -d"|" -f1 | tr -d '[[:space:]]'`
        ordinal_position=`echo ${row_data} | cut -d"|" -f2 | tr -d '[[:space:]]'`
        min_ordinal_position=`echo ${row_data} | cut -d"|" -f3 | tr -d '[[:space:]]'`
        max_ordinal_position=`echo ${row_data} | cut -d"|" -f4 | tr -d '[[:space:]]'`
        
        logger_debug "column_name=${column_name} ordinal_position=${ordinal_position} min_ordinal_position=${min_ordinal_position} max_ordinal_position=${max_ordinal_position}"
        
        
        if [[ ${ordinal_position} -ne ${max_ordinal_position} ]]
        then
            echo "    ${column_name},"
        else 
            echo "    ${column_name}"
        fi
    done < <(    \
        psql \
            --host=${inHost} \
            --port=${inPort} \
            --username=${inUsername} \
            --dbname=${inDatabase} \
            --quiet \
            --tuples-only \
            --field-separator-zero \
            --record-separator-zero \
            --command=" \
                SELECT  
                    c.column_name, 
                    c.ordinal_position, 
                    c_min.min_ordinal_position, 
                    c_min.max_ordinal_position 
                FROM information_schema.columns c 
                INNER JOIN 
                ( 
                    SELECT 
                        c2.table_catalog, 
                        c2.table_schema, 
                        c2.table_name, 
                        MIN(c2.ordinal_position) min_ordinal_position, 
                        MAX(c2.ordinal_position) max_ordinal_position 
                    FROM information_schema.columns c2 
                    GROUP BY 
                        c2.table_catalog, 
                        c2.table_schema, 
                        c2.table_name 
                ) c_min 
                    ON c.table_catalog = c_min.table_catalog 
                    AND c.table_schema = c_min.table_schema 
                    AND c.table_name = c_min.table_name 
                WHERE c.table_catalog = '${inDatabase}' 
                AND c.table_schema = '${inSchema}' 
                AND c.table_name = '${inTable}' 
                ORDER BY 
                    c.ordinal_position ASC;" | sed -e '$d')
    echo ")"                   

    
    echo "VALUES"
    echo "("
    #  list values
    while read row_data
    do
        column_name=`echo ${row_data} | cut -d"|" -f1 | tr -d '[[:space:]]'`
        ordinal_position=`echo ${row_data} | cut -d"|" -f2 | tr -d '[[:space:]]'`
        min_ordinal_position=`echo ${row_data} | cut -d"|" -f3 | tr -d '[[:space:]]'`
        max_ordinal_position=`echo ${row_data} | cut -d"|" -f4 | tr -d '[[:space:]]'`
        
        logger_debug "column_name=${column_name} ordinal_position=${ordinal_position} min_ordinal_position=${min_ordinal_position} max_ordinal_position=${max_ordinal_position}"
        
        
        if [[ ${ordinal_position} -ne ${max_ordinal_position} ]]
        then
            echo "    NULL,  -- ${column_name}"
        else 
            echo "    NULL   -- ${column_name}"
        fi
    done < <( \
        psql \
            --host=${inHost} \
            --port=${inPort} \
            --username=${inUsername} \
            --dbname=${inDatabase} \
            --quiet \
            --tuples-only \
            --field-separator-zero \
            --record-separator-zero \
            --command=" \
                SELECT  
                    c.column_name, 
                    c.ordinal_position, 
                    c_min.min_ordinal_position, 
                    c_min.max_ordinal_position 
                FROM information_schema.columns c 
                INNER JOIN 
                ( 
                    SELECT 
                        c2.table_catalog, 
                        c2.table_schema, 
                        c2.table_name, 
                        MIN(c2.ordinal_position) min_ordinal_position, 
                        MAX(c2.ordinal_position) max_ordinal_position 
                    FROM information_schema.columns c2 
                    GROUP BY 
                        c2.table_catalog, 
                        c2.table_schema, 
                        c2.table_name 
                ) c_min 
                    ON c.table_catalog = c_min.table_catalog 
                    AND c.table_schema = c_min.table_schema 
                    AND c.table_name = c_min.table_name 
                WHERE c.table_catalog = '${inDatabase}' 
                AND c.table_schema = '${inSchema}' 
                AND c.table_name = '${inTable}' 
                ORDER BY 
                    c.ordinal_position ASC;" | sed -e '$d')
    echo ");"                       
}


#
#  tables
#
tables() {
    inHost=${1}
    inPort=${2}
    inDatabase=${3}
    inUsername=${4}
    inSchema=${5}

    psql \
        --host=${inHost} \
        --port=${inPort} \
        --username=${inUsername} \
        --dbname=${inDatabase} \
        --quiet \
        --tuples-only \
        --field-separator-zero \
        --record-separator-zero \
        --command="SELECT table_name FROM information_schema.tables WHERE table_catalog = '${inDatabase}' AND table_schema = '${inSchema}' AND table_type = 'BASE TABLE' ORDER BY table_name ASC" | sed -e '$d'
    if [ ${PIPESTATUS[0]} -ne 0 ]
    then
        logger_error "Table list returned non-zero error code."
        exit -1
    fi
}


#
#  makeDropTables
#
makeDropTables() {
    inHost=${1}
    inPort=${2}
    inDatabase=${3}
    inUsername=${4}
    inSchema=${5}


    while read table_name
    do
        echo "DROP TABLE ${inSchema}.${table_name} CASCADE;"
    done < <(tables ${inHost} ${inPort} ${inDatabase} ${inUsername} ${inSchema})
}


#
#  dropTables
#
dropTables() {
    inHost=${1}
    inPort=${2}
    inDatabase=${3}
    inUsername=${4}
    inSchema=${5}


    
    psql \
        --host=${inHost} \
        --port=${inPort} \
        --username=${inUsername} \
        --dbname=${inDatabase} \
        < <(makeDropTables ${inHost} ${inPort} ${inDatabase} ${inUsername} ${inSchema})
    if [ $? -ne 0 ]
    then
        logger_error "Failed dropping tables."
        exit -1
    fi
}


#
#  makeGrantTables
#
makeGrantTables() {
    inHost=${1}
    inPort=${2}
    inDatabase=${3}
    inUsername=${4}
    inSchema=${5}
    
    
    while read table_name
    do
        echo "GRANT SELECT ON ${inSchema}.${table_name} TO ${inDatabase}_ro;"
        echo "GRANT SELECT, INSERT, UPDATE, DELETE ON ${inSchema}.${table_name} TO ${inDatabase}_rw;"
        echo "GRANT ALL ON ${inSchema}.${table_name} TO ${inDatabase}_adm;"
    done < <(tables ${inHost} ${inPort} ${inDatabase} ${inUsername} ${inSchema})
}


#
#  grantTables
#
grantTables() {
    inHost=${1}
    inPort=${2}
    inDatabase=${3}
    inUsername=${4}
    inSchema=${5}

    
    psql \
        --host=${inHost} \
        --port=${inPort} \
        --username=${inUsername} \
        --dbname=${inDatabase} \
        < <(makeGrantTables ${inHost} ${inPort} ${inDatabase} ${inUsername} ${inSchema})
    if [ $? -ne 0 ]
    then
        logger_error "Failed granting tables."
        exit -1
    fi
}


#
#  schemas
#
schemas() {
    inHost=${1}
    inPort=${2}
    inDatabase=${3}
    inUsername=${4}

    
    psql \
        --host=${inHost} \
        --port=${inPort} \
        --username=${inUsername} \
        --dbname=${inDatabase} \
        --quiet \
        --tuples-only \
        --field-separator-zero \
        --record-separator-zero \
        --command="SELECT schema_name FROM information_schema.schemata WHERE schema_name NOT IN ('information_schema', 'pg_catalog') ORDER BY schema_name ASC" | sed -e '$d'
    if [ ${PIPESTATUS[0]} -ne 0 ]
    then
        logger_error "Schema list returned non-zero code."
        exit -1
    fi    
}


#
#  makeCountTable
#
makeCountTable() {
    inHost=${1}
    inPort=${2}
    inDatabase=${3}
    inUsername=${4}
    inSchema=${5}
    inTable=${6}
    
    echo "SELECT '${inTable}' table_name, COUNT(*) num_rows FROM ${inSchema}.${inTable}"
}    


#
#  countTable
#
countTable() {
    inHost=${1}
    inPort=${2}
    inDatabase=${3}
    inUsername=${4}
    inSchema=${5}
    inTable=${6}

    psql \
        --host=${inHost} \
        --port=${inPort} \
        --username=${inUsername} \
        --dbname=${inDatabase} \
        < <(makeCountTable ${inHost} ${inPort} ${inDatabase} ${inUsername} ${inSchema} ${inTable})
    if [ ${PIPESTATUS[0]} -ne 0 ]
    then
        logger_error "Count returned non-zero code."
        exit -1
    fi        
}


#
#  makeCountTables
#
makeCountTables() {
    inHost=${1}
    inPort=${2}
    inDatabase=${3}
    inUsername=${4}
    inSchema=${5}
    
    sql_text=""
    while read table_name
    do
        sql_text="${sql_text} `makeCountTable ${inHost} ${inPort} ${inDatabase} ${inUsername} ${inSchema} ${table_name}`\n"
        sql_text="${sql_text} UNION ALL\n"
    done < <(tables ${inHost} ${inPort} ${inDatabase} ${inUsername} ${inSchema})
    echo -e $sql_text | sed '$ d' | sed '$ d'
}


#
#  countTables
#
countTables() {
    inHost=${1}
    inPort=${2}
    inDatabase=${3}
    inUsername=${4}
    inSchema=${5}
 
    psql \
        --host=${inHost} \
        --port=${inPort} \
        --username=${inUsername} \
        --dbname=${inDatabase} \
        < <(makeCountTables ${inHost} ${inPort} ${inDatabase} ${inUsername} ${inSchema})
    if [ ${PIPESTATUS[0]} -ne 0 ]
    then
        logger_error "Schema list returned non-zero code."
        exit -1
    fi            
}


#
#  makeSelect
#
makeSelect() {
    inHost=${1}
    inPort=${2}
    inDatabase=${3}
    inUsername=${4}
    inSchema=${5}
    inTable=${6}
    
    
    sql_text="SELECT \
                  c.column_name, \
                  c.is_nullable, 
                  c.data_type, \
                  c.ordinal_position, \
                  c_max.min_ordinal_position, \
                  c_max.max_ordinal_position \
              FROM information_schema.columns c \
              INNER JOIN \
              ( \
                  SELECT \
                      c2.table_catalog, \
                      c2.table_schema, \
                      c2.table_name, \
                      MIN(c2.ordinal_position) min_ordinal_position, \
                      MAX(c2.ordinal_position) max_ordinal_position \
                  FROM information_schema.columns c2 \
                  GROUP BY \
                      c2.table_catalog, \
                      c2.table_schema, \
                      c2.table_name \
              ) c_max \
                  ON c.table_catalog = c_max.table_catalog \
                  AND c.table_schema = c_max.table_schema \
                  AND c.table_name = c_max.table_name \
              WHERE c.table_catalog = '${inDatabase}' \
              AND c.table_schema = '${inSchema}' \
              AND c.table_name = '${inTable}' \
              ORDER BY  \
                  c.table_catalog ASC"    
    logger_trace "makeSelect(): ${sql_text}"
    
    
    IFS=
    echo 'SELECT'
    while read row_data
    do
        column_name=$(echo ${row_data} | cut -f1 -d'|' | sed 's/^[ \t]*//;s/[ \t]*$//')
        is_nullable=$(echo ${row_data} | cut -f2 -d'|' | sed 's/^[ \t]*//;s/[ \t]*$//')
        data_type=$(echo ${row_data} | cut -f3 -d'|' | sed 's/^[ \t]*//;s/[ \t]*$//')
        ordinal_position=$(echo ${row_data} | cut -f4 -d'|' | sed 's/^[ \t]*//;s/[ \t]*$//')
        min_ordinal_position=$(echo ${row_data} | cut -f5 -d'|' | sed 's/^[ \t]*//;s/[ \t]*$//')
        max_ordinal_position=$(echo ${row_data} | cut -f6 -d'|'| sed 's/^[ \t]*//;s/[ \t]*$//')        
        logger_trace "column_name=${column_name} is_nullable=${is_nullable} data_type=${data_type} ordinal_position=${ordinal_position} min_ordinal_position=${min_ordinal_position} max_ordinal_position=${max_ordinal_position}"
        
        column_output="    a.${column_name}"
        #  last column does not need a comma
        if [ ${ordinal_position} == ${max_ordinal_position} ]
        then       
            column_output="${column_output},"
        fi
        
        column_output="${column_output}    --  ${data_type}"
        
        #  nullable
        if [ ${is_nullable} == "NO" ]
        then 
            column_output="${column_output} NOT NULL"
        fi
        
        echo ${column_output}
    done < <(\
        psql \
            --host=${inHost} \
            --port=${inPort} \
            --username=${inUsername} \
            --dbname=${inDatabase} \
            --quiet \
            --tuples-only \
            --field-separator-zero \
            --record-separator-zero \
            --command="${sql_text}" | sed -e '$d')
    echo "FROM ${inSchema}.${inTable} a"
}








#
#  main
#


#  Handle environment variables.  If .pgpass file does not exist, the script will request password for each call to psql.
logger_debug "Postgres environment variables.  PGHOST=${PGHOST} PGPORT=${PGPORT} PGDATABASE=${PGDATABASE} PGUSER=${PGUSER}"
host=${PGHOST}
port=${PGPORT}
database=${PGDATABASE}
username=${PGUSER}
schema=public


#  Apply defaults
if [ -z "${port}" ]
then
    port="5432"
fi
if [ -z "${username}" ]
then
    username="postgres"
fi
if [ -z "${schema}" ]
then
    schema="public"
fi


#  Parse command line parameters.  Supports short and long parameters.
logger_debug "Parsing parameters."
while getopts 'vh:p:-U:d:s:' OPTION
do
    case "$OPTION" in
        v  ) verbose;;
        h  ) host="$OPTARG";;
        p  ) port="$OPTARG";;
        d  ) database="$OPTARG";;
        U  ) username="$OPTARG";;
        s  ) schema="$OPTARG";;
        t  ) table="$OPTARG";;
        -  )
            [ $OPTIND -ge 1 ] && optind=$(expr $OPTIND - 1 ) || optind=$OPTIND
            eval OPTION="\$$optind"
            OPTARG=$(echo $OPTION | cut -d'=' -f2)
            OPTION=$(echo $OPTION | cut -d'=' -f1)
            case $OPTION in
                --verbose   ) verbose;;
                --help      ) usage "Requested help"; exit 0;;
                --host      ) host="$OPTARG";;
                --port      ) port="$OPTARG";;
                --username  ) username="$OPTARG";;
                --dbname    ) database="$OPTARG";;
                --schema    ) schema="$OPTARG";;
                --table     ) table="$OPTARG";;
                *           ) usage "Invalid argument.  $OPTION"; exit -1;;
            esac
            OPTIND=1
            shift
            ;;
        ?  ) usage "Requested help"; exit 0;;
        *  ) usage "Invalid parameter = $OPTION"; exit -1;;
    esac
done
logger_debug "Connection options.  host=${host} port=${port} database=${database} username=${username} schema=${schema} table=${table}"


#  Process the requested action verb.
action=${@:$OPTIND:1}
case "${action}" in
    tables )
        tables ${host} ${port} ${database} ${username} ${schema};;
    dropTables )
        dropTables ${host} ${port} ${database} ${username} ${schema};;
    makeDropTables )
        makeDropTables ${host} ${port} ${database} ${username} ${schema};;
    makeGrantTables )
        makeGrantTables ${host} ${port} ${database} ${username} ${schema};;
    grantTables )
        grantTables ${host} ${port} ${database} ${username} ${schema};;
    schemas )
        schemas ${host} ${port} ${database} ${username};;
    columns )
        columns ${host} ${port} ${database} ${username} ${schema} ${table};;
    makeInsert )
        makeInsert ${host} ${port} ${database} ${username} ${schema} ${table};;    
    makeCountTable )
        makeCountTable ${host} ${port} ${database} ${username} ${schema} ${table};;
    countTable )
        countTable ${host} ${port} ${database} ${username} ${schema} ${table};;
    makeCountTables )
        makeCountTables ${host} ${port} ${database} ${username} ${schema};;        
    countTables )
        countTables ${host} ${port} ${database} ${username} ${schema};;
    makeSelect )
        makeSelect ${host} ${port} ${database} ${username} ${schema} ${table};;
    * )
        usage "Unknown action = ${action}"; exit -1;;
esac




