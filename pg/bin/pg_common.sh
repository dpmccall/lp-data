#!/bin/bash
#
#  script home
#
base_dir="`dirname $0`"
export PG_HOME="`(cd \"$base_dir/..\"; pwd)`"

#
#  configure logging
#
LOG4SH_CONFIGURATION='none'
source ${PG_HOME}/bin/log4sh
log4sh_resetConfiguration

#  root logger set to INFO
logger_setLevel INFO

#  console appender
logger_addAppender console
appender_setLevel console INFO
appender_setLayout console PatternLayout
appender_setType console ConsoleAppender
appender_setPattern console "[%d{ISODATE}][%p][%r][%t] %m%n"
appender_activateOptions console


#  standard error
logger_addAppender stderr
appender_setLevel stderr ERROR
appender_setLayout stderr PatternLayout
appender_setType stderr FileAppender
appender_setPattern stderr "[%d{ISODATE}][%p][%r][%t] %m%n"
appender_file_setFile stderr STDERR
appender_activateOptions stderr


#  file appender
mkdir -p "$PG_HOME/log"
today=`date +%Y-%m-%d`
logger_addAppender rolling_file
appender_setLevel rolling_file TRACE
appender_setLayout rolling_file PatternLayout
appender_setType rolling_file RollingFileAppender
appender_setPattern rolling_file "[%d{ISODATE}][%p][%r][%t] %m%n"
appender_file_setFile rolling_file "${PG_HOME}/log/${today}_pg.log"
appender_file_setMaxFileSize rolling_file 10MiB
appender_activateOptions rolling_file

