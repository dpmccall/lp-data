\connect price

--
--  view
--
DROP VIEW public.rate_sheet_v;


--
--  table
--
DROP TABLE public.credit_profile_price CASCADE;
DROP TABLE public.dti_price CASCADE;
DROP TABLE public.loan_amount_price CASCADE;
DROP TABLE public.ltv_price CASCADE;
DROP TABLE public.mortgage_insurance_rate CASCADE;
DROP TABLE public.product_type_price CASCADE;
DROP TABLE public.property_usage_type_price CASCADE;

