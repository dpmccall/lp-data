\connect price

--
-- ER/Studio Data Architect SQL Code Generation
-- Project :      price.public.DM1
--
-- Date Created : Tuesday, June 21, 2016 10:38:11
-- Target DBMS : PostgreSQL 9.x
--

-- 
-- TABLE: credit_profile_price 
--

CREATE TABLE credit_profile_price(
    credit_profile_price_key          int4             NOT NULL,
    credit_profile_upper_limit        int4             NOT NULL,
    credit_profile_lower_limit        int4             NOT NULL,
    credit_profile_rate_adjustment    numeric(8, 7)    NOT NULL,
    CONSTRAINT credit_profile_price_pk PRIMARY KEY (credit_profile_price_key)
)
;
GRANT SELECT ON public.credit_profile_price TO price_ro;
GRANT SELECT, INSERT, UPDATE, DELETE ON public.credit_profile_price TO price_rw;
GRANT ALL ON public.credit_profile_price TO price_adm;


-- 
-- TABLE: dti_price 
--

CREATE TABLE dti_price(
    dti_price_key          int4              NOT NULL,
    dti_upper_limit        numeric(10, 5)    NOT NULL,
    dti_lower_limit        numeric(10, 5)    NOT NULL,
    dti_rate_adjustment    numeric(8, 7)     NOT NULL,
    CONSTRAINT dti_price_pk PRIMARY KEY (dti_price_key)
)
;
GRANT SELECT ON public.dti_price TO price_ro;
GRANT SELECT, INSERT, UPDATE, DELETE ON public.dti_price TO price_rw;
GRANT ALL ON public.dti_price TO price_adm;


-- 
-- TABLE: loan_amount_price 
--

CREATE TABLE loan_amount_price(
    loan_amount_price_key          int4              NOT NULL,
    loan_amount_upper_limit        numeric(15, 2)    NOT NULL,
    loan_amount_lower_limit        numeric(15, 2)    NOT NULL,
    loan_amount_rate_adjustment    numeric(8, 7)     NOT NULL,
    CONSTRAINT loan_amount_price_pk PRIMARY KEY (loan_amount_price_key)
)
;




-- 
-- TABLE: ltv_price 
--

CREATE TABLE ltv_price(
    ltv_price_key          int4              NOT NULL,
    ltv_upper_limit        numeric(10, 5)    NOT NULL,
    ltv_lower_limit        numeric(10, 5)    NOT NULL,
    ltv_rate_adjustment    numeric(8, 7)     NOT NULL,
    CONSTRAINT ltv_price_pk PRIMARY KEY (ltv_price_key)
)
;
GRANT SELECT ON public.ltv_price TO price_ro;
GRANT SELECT, INSERT, UPDATE, DELETE ON public.ltv_price TO price_rw;
GRANT ALL ON public.ltv_price TO price_adm;


-- 
-- TABLE: mortgage_insurance_rate 
--

CREATE TABLE mortgage_insurance_rate(
    mortgage_insurance_rate_key       int4              NOT NULL,
    product_type                      varchar(50)       NOT NULL,
    ltv_upper_limit                   numeric(10, 5)    NOT NULL,
    ltv_lower_limit                   numeric(10, 5)    NOT NULL,
    credit_profile_upper_limit        int4              NOT NULL,
    credit_profile_lower_limit        int4              NOT NULL,
    mortgage_insurance_coverage       numeric(8, 7)     NOT NULL,
    annual_mortgage_insurance_rate    numeric(8, 7)     NOT NULL,
    CONSTRAINT mortgage_insurance_rate_pk PRIMARY KEY (mortgage_insurance_rate_key)
)
;
GRANT SELECT ON public.mortgage_insurance_rate TO price_ro;
GRANT SELECT, INSERT, UPDATE, DELETE ON public.mortgage_insurance_rate TO price_rw;
GRANT ALL ON public.mortgage_insurance_rate TO price_adm;


-- 
-- TABLE: product_type_price 
--

CREATE TABLE product_type_price(
    product_type_price_key            int4             NOT NULL,
    product_type                      varchar(50)      NOT NULL,
    loan_amortization_period_type     varchar(50)      NOT NULL,
    loan_amortization_period_count    int4             NOT NULL,
    product_type_rate_adjustment      numeric(8, 7)    NOT NULL,
    CONSTRAINT product_type_price_pk PRIMARY KEY (product_type_price_key)
)
;
GRANT SELECT ON public.product_type_price TO price_ro;
GRANT SELECT, INSERT, UPDATE, DELETE ON public.product_type_price TO price_rw;
GRANT ALL ON public.product_type_price TO price_adm;


-- 
-- TABLE: property_usage_type_price 
--

CREATE TABLE property_usage_type_price(
    property_usage_type_price_key          int4             NOT NULL,
    property_usage_type                    varchar(50)      NOT NULL,
    property_usage_type_rate_adjustment    numeric(8, 7)    NOT NULL,
    CONSTRAINT property_usage_type_price_pk PRIMARY KEY (property_usage_type_price_key)
)
;
GRANT SELECT ON public.property_usage_type_price TO price_ro;
GRANT SELECT, INSERT, UPDATE, DELETE ON public.property_usage_type_price TO price_rw;
GRANT ALL ON public.property_usage_type_price TO price_adm;


-- 
-- VIEW: rate_sheet_v 
--

CREATE OR REPLACE VIEW public.rate_sheet_v AS
SELECT
    cpp.credit_profile_price_key,
    cpp.credit_profile_upper_limit,
    cpp.credit_profile_lower_limit,
    cpp.credit_profile_rate_adjustment,
--    dp.dti_price_key,
--    dp.dti_upper_limit,
--    dp.dti_lower_limit,
--    dp.dti_rate_adjustment,
    lp.ltv_price_key,
    lp.ltv_upper_limit,
    lp.ltv_lower_limit,
    lp.ltv_rate_adjustment,
    ptp.product_type_price_key,
    ptp.product_type,
    ptp.loan_amortization_period_type,
    ptp.loan_amortization_period_count,
    ptp.product_type_rate_adjustment,
    putp.property_usage_type_price_key,
    putp.property_usage_type,
    putp.property_usage_type_rate_adjustment,
    lap.loan_amount_price_key,
    lap.loan_amount_upper_limit,
    lap.loan_amount_lower_limit,
    lap.loan_amount_rate_adjustment,
    cpp.credit_profile_rate_adjustment 
--        + dp.dti_rate_adjustment 
        + lp.ltv_rate_adjustment 
        + ptp.product_type_rate_adjustment 
        + putp.property_usage_type_rate_adjustment
        + lap.loan_amount_rate_adjustment rate_adjustment
FROM
    public.credit_profile_price cpp,
--    public.dti_price dp,
    public.ltv_price lp,
    public.product_type_price ptp,
    public.property_usage_type_price putp,
    public.loan_amount_price lap;
    
    
GRANT SELECT ON public.rate_sheet_v TO price_ro;
GRANT SELECT ON public.rate_sheet_v TO price_rw;
GRANT ALL ON public.rate_sheet_v TO price_adm;

-- 
-- INDEX: credit_profile_price_x1 
--

CREATE UNIQUE INDEX credit_profile_price_x1 ON credit_profile_price(credit_profile_upper_limit, credit_profile_lower_limit)
;
-- 
-- INDEX: dti_price_x1 
--

CREATE UNIQUE INDEX dti_price_x1 ON dti_price(dti_upper_limit, dti_lower_limit)
;
-- 
-- INDEX: loan_amount_price_x1 
--

CREATE INDEX loan_amount_price_x1 ON loan_amount_price(loan_amount_upper_limit)
;
-- 
-- INDEX: ltv_price_x1 
--

CREATE UNIQUE INDEX ltv_price_x1 ON ltv_price(ltv_upper_limit, ltv_lower_limit)
;
-- 
-- INDEX: product_type_price_x1 
--

CREATE UNIQUE INDEX product_type_price_x1 ON product_type_price(product_type)
;
-- 
-- INDEX: property_usage_type_price_x1 
--

CREATE UNIQUE INDEX property_usage_type_price_x1 ON property_usage_type_price(property_usage_type)
;
