\connect price

DELETE FROM public.credit_profile_price;
DELETE FROM public.dti_price;
DELETE FROM public.loan_amount_price;
DELETE FROM public.ltv_price;
DELETE FROM public.mortgage_insurance_rate;
DELETE FROM public.product_type_price;
DELETE FROM public.property_usage_type_price;
 