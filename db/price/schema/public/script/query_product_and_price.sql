SELECT
    four.* 
FROM
(
    SELECT
        three.principal_and_interest_payment_amount 
            + three.proposed_monthly_property_taxes_payment_amount 
            + three.proposed_monthly_property_hazard_insurance_premium_amount total_monthly_piti_payment_amount,
        three.principal_and_interest_payment_amount 
            + three.mi_initial_premium_amount
            + three.proposed_monthly_property_taxes_payment_amount 
            + three.proposed_monthly_property_hazard_insurance_premium_amount
            + three.estimated_monthly_hoa_payment total_monthly_pitia_payment_amount,
        three.*
    FROM
    (
        SELECT
            ROUND
            (
                (
                    two.adjusted_monthly_rate / 
                    (1 - ((1 + two.adjusted_monthly_rate) ^ (-1 * two.loan_amortization_period_count)))
                ) * two.loan_amount,
                2
            ) principal_and_interest_payment_amount,
            two.*
        FROM
        (
            SELECT
                ROUND(one.adjusted_annual_rate / one.number_of_months_in_year, 8) adjusted_monthly_rate,
                one.*
            FROM
            (
                SELECT
                    p.*,
                    r.rate_adjustment,    
                    r.product_type,
                    r.loan_amortization_period_type,
                    r.loan_amortization_period_count,
                    COALESCE(mi.annual_mortgage_insurance_rate, 0) annual_mortgage_insurance_rate,
                    r.rate_adjustment + p.annual_rate adjusted_annual_rate,  
                    (r.rate_adjustment + p.annual_rate) * p.apr_multiplier apr,
                    ROUND(p.property_value_amount * p.monthly_property_tax_multiplier, 2) proposed_monthly_property_taxes_payment_amount,
                    ROUND(p.loan_amount * p.closing_cost_multiplier, 2) estimated_closing_costs_amount, 
                    ROUND(p.loan_amount * (COALESCE(mi.annual_mortgage_insurance_rate, 0) / p.number_of_months_in_year), 2) mi_initial_premium_amount
                FROM
                (
                    SELECT
                        ROUND(CAST(i.loan_amount AS DECIMAL) / CAST(i.property_value_amount AS DECIMAL), 4) ltv,
                        i.annual_property_tax_multiplier / i.number_of_months_in_year monthly_property_tax_multiplier,
                        i.*
                    FROM
                    (
                        SELECT
                            .0375 annual_rate,
                            250000 loan_amount,
                            .25 mortgage_insurance_coverage,
                            720 credit_profile,
                            CAST('PrimaryResidence' AS VARCHAR) property_usage_type,
                            .0106 annual_property_tax_multiplier,
                            .015 closing_cost_multiplier,
                            12 number_of_months_in_year,
                            79.33 proposed_monthly_property_hazard_insurance_premium_amount,
                            291000 property_value_amount,
                            125.00 estimated_monthly_hoa_payment,
                            1.1678 apr_multiplier
                    ) i
                ) p
                INNER JOIN rate_sheet_v r
                    ON r.credit_profile_lower_limit <= p.credit_profile
                    AND r.credit_profile_upper_limit >= p.credit_profile
                    AND r.ltv_lower_limit <= p.ltv
                    AND r.ltv_upper_limit >= p.ltv
                    AND r.property_usage_type = p.property_usage_type
                    AND r.loan_amount_lower_limit <= p.loan_amount      
                    AND r.loan_amount_upper_limit >= p.loan_amount     
                LEFT OUTER JOIN mortgage_insurance_rate mi
                    ON r.product_type = mi.product_type
                    AND mi.mortgage_insurance_coverage = p.mortgage_insurance_coverage
                    AND mi.credit_profile_lower_limit <= p.credit_profile
                    AND mi.credit_profile_upper_limit >= p.credit_profile
                    AND mi.ltv_lower_limit <= p.ltv
                    AND mi.ltv_upper_limit >= p.ltv
            ) one
        ) two
    ) three
) four;





