\connect price

INSERT INTO public.credit_profile_price
(
    credit_profile_price_key,
    credit_profile_upper_limit,
    credit_profile_lower_limit,
    credit_profile_rate_adjustment
)
VALUES
(
    4,  -- credit_profile_price_key
    9999,  -- credit_profile_upper_limit
    720,  -- credit_profile_lower_limit
    0   -- credit_profile_rate_adjustment
);


INSERT INTO public.credit_profile_price
(
    credit_profile_price_key,
    credit_profile_upper_limit,
    credit_profile_lower_limit,
    credit_profile_rate_adjustment
)
VALUES
(
    1,  -- credit_profile_price_key
    719,  -- credit_profile_upper_limit
    680,  -- credit_profile_lower_limit
    .00125   -- credit_profile_rate_adjustment
);


INSERT INTO public.credit_profile_price
(
    credit_profile_price_key,
    credit_profile_upper_limit,
    credit_profile_lower_limit,
    credit_profile_rate_adjustment
)
VALUES
(
    2,  -- credit_profile_price_key
    679,  -- credit_profile_upper_limit
    620,  -- credit_profile_lower_limit
    .00250   -- credit_profile_rate_adjustment
);



INSERT INTO public.credit_profile_price
(
    credit_profile_price_key,
    credit_profile_upper_limit,
    credit_profile_lower_limit,
    credit_profile_rate_adjustment
)
VALUES
(
    3,  -- credit_profile_price_key
    619,  -- credit_profile_upper_limit
    580,  -- credit_profile_lower_limit
    .0100   -- credit_profile_rate_adjustment
);


INSERT INTO public.dti_price
(
    dti_price_key,
    dti_upper_limit,
    dti_lower_limit,
    dti_rate_adjustment
)
VALUES
(
    1,  -- dti_price_key
    .50,  -- dti_upper_limit
    .4500,  -- dti_lower_limit
    .00250   -- dti_rate_adjustment
);


INSERT INTO public.dti_price
(
    dti_price_key,
    dti_upper_limit,
    dti_lower_limit,
    dti_rate_adjustment
)
VALUES
(
    2,  -- dti_price_key
    .4499,  -- dti_upper_limit
    .43,  -- dti_lower_limit
    .00125   -- dti_rate_adjustment
);


INSERT INTO public.dti_price
(
    dti_price_key,
    dti_upper_limit,
    dti_lower_limit,
    dti_rate_adjustment
)
VALUES
(
    3,  -- dti_price_key
    .4299,  -- dti_upper_limit
    0,  -- dti_lower_limit
    0   -- dti_rate_adjustment
);


INSERT INTO public.loan_amount_price
(
    loan_amount_price_key,
    loan_amount_upper_limit,
    loan_amount_lower_limit,
    loan_amount_rate_adjustment
)
VALUES
(
    1,  -- loan_amount_price_key
    417000,  -- loan_amount_upper_limit
    0,  -- loan_amount_lower_limit
    0   -- loan_amount_rate_adjustment
);


INSERT INTO public.loan_amount_price
(
    loan_amount_price_key,
    loan_amount_upper_limit,
    loan_amount_lower_limit,
    loan_amount_rate_adjustment
)
VALUES
(
    2,  -- loan_amount_price_key
    9999999999999.99,  -- loan_amount_upper_limit
    417000.01,  -- loan_amount_lower_limit
    0.005   -- loan_amount_rate_adjustment
);
INSERT INTO public.ltv_price
(
    ltv_price_key,
    ltv_upper_limit,
    ltv_lower_limit,
    ltv_rate_adjustment
)
VALUES
(
    1,  -- ltv_price_key
    .97,  -- ltv_upper_limit
    .9501,  -- ltv_lower_limit
    .005   -- ltv_rate_adjustment
);


INSERT INTO public.ltv_price
(
    ltv_price_key,
    ltv_upper_limit,
    ltv_lower_limit,
    ltv_rate_adjustment
)
VALUES
(
    2,  -- ltv_price_key
    .95,  -- ltv_upper_limit
    .9001,  -- ltv_lower_limit
    .00250   -- ltv_rate_adjustment
);


INSERT INTO public.ltv_price
(
    ltv_price_key,
    ltv_upper_limit,
    ltv_lower_limit,
    ltv_rate_adjustment
)
VALUES
(
    3,  -- ltv_price_key
    .90,  -- ltv_upper_limit
    .8001,  -- ltv_lower_limit
    .00125   -- ltv_rate_adjustment
);


INSERT INTO public.ltv_price
(
    ltv_price_key,
    ltv_upper_limit,
    ltv_lower_limit,
    ltv_rate_adjustment
)
VALUES
(
    4,  -- ltv_price_key
    .80,  -- ltv_upper_limit
    .7001,  -- ltv_lower_limit
    0   -- ltv_rate_adjustment
);



INSERT INTO public.ltv_price
(
    ltv_price_key,
    ltv_upper_limit,
    ltv_lower_limit,
    ltv_rate_adjustment
)
VALUES
(
    5,  -- ltv_price_key
    .70,  -- ltv_upper_limit
    .6001,  -- ltv_lower_limit
    -.00125   -- ltv_rate_adjustment
);



INSERT INTO public.ltv_price
(
    ltv_price_key,
    ltv_upper_limit,
    ltv_lower_limit,
    ltv_rate_adjustment
)
VALUES
(
    6,  -- ltv_price_key
    .60,  -- ltv_upper_limit
    0,  -- ltv_lower_limit
    -.0025   -- ltv_rate_adjustment
);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(1, '30YearFixed', 0.97000, 0.95010, 999, 760, 0.3500000, 0.0055000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(2, '30YearFixed', 0.97000, 0.95010, 759, 740, 0.3500000, 0.0075000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(3, '30YearFixed', 0.97000, 0.95010, 739, 720, 0.3500000, 0.0095000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(4, '30YearFixed', 0.97000, 0.95010, 719, 700, 0.3500000, 0.0115000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(5, '30YearFixed', 0.97000, 0.95010, 699, 680, 0.3500000, 0.0140000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(6, '30YearFixed', 0.97000, 0.95010, 679, 660, 0.3500000, 0.0190000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(7, '30YearFixed', 0.97000, 0.95010, 659, 640, 0.3500000, 0.0205000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(8, '30YearFixed', 0.97000, 0.95010, 639, 620, 0.3500000, 0.0225000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(9, '30YearFixed', 0.97000, 0.95010, 999, 760, 0.2500000, 0.0044000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(10, '30YearFixed', 0.97000, 0.95010, 759, 740, 0.2500000, 0.0063000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(11, '30YearFixed', 0.97000, 0.95010, 739, 720, 0.2500000, 0.0077000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(12, '30YearFixed', 0.97000, 0.95010, 719, 700, 0.2500000, 0.0093000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(13, '30YearFixed', 0.97000, 0.95010, 699, 680, 0.2500000, 0.0115000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(14, '30YearFixed', 0.97000, 0.95010, 679, 660, 0.2500000, 0.0152000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(15, '30YearFixed', 0.97000, 0.95010, 659, 640, 0.2500000, 0.0167000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(16, '30YearFixed', 0.97000, 0.95010, 639, 620, 0.2500000, 0.0181000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(17, '30YearFixed', 0.97000, 0.95010, 999, 760, 0.1800000, 0.0037000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(18, '30YearFixed', 0.97000, 0.95010, 759, 740, 0.1800000, 0.0054000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(19, '30YearFixed', 0.97000, 0.95010, 739, 720, 0.1800000, 0.0065000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(20, '30YearFixed', 0.97000, 0.95010, 719, 700, 0.1800000, 0.0078000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(21, '30YearFixed', 0.97000, 0.95010, 699, 680, 0.1800000, 0.0098000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(22, '30YearFixed', 0.97000, 0.95010, 679, 660, 0.1800000, 0.0118000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(23, '30YearFixed', 0.97000, 0.95010, 659, 640, 0.1800000, 0.0133000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(24, '30YearFixed', 0.97000, 0.95010, 639, 620, 0.1800000, 0.0143000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(25, '30YearFixed', 0.95000, 0.90010, 999, 760, 0.3000000, 0.0041000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(26, '30YearFixed', 0.95000, 0.90010, 759, 740, 0.3000000, 0.0059000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(27, '30YearFixed', 0.95000, 0.90010, 739, 720, 0.3000000, 0.0073000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(28, '30YearFixed', 0.95000, 0.90010, 719, 700, 0.3000000, 0.0087000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(29, '30YearFixed', 0.95000, 0.90010, 699, 680, 0.3000000, 0.0108000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(30, '30YearFixed', 0.95000, 0.90010, 679, 660, 0.3000000, 0.0142000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(31, '30YearFixed', 0.95000, 0.90010, 659, 640, 0.3000000, 0.0150000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(32, '30YearFixed', 0.95000, 0.90010, 639, 620, 0.3000000, 0.0161000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(33, '30YearFixed', 0.95000, 0.90010, 999, 760, 0.2500000, 0.0037000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(34, '30YearFixed', 0.95000, 0.90010, 759, 740, 0.2500000, 0.0052000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(35, '30YearFixed', 0.95000, 0.90010, 739, 720, 0.2500000, 0.0064000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(36, '30YearFixed', 0.95000, 0.90010, 719, 700, 0.2500000, 0.0075000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(37, '30YearFixed', 0.95000, 0.90010, 699, 680, 0.2500000, 0.0094000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(38, '30YearFixed', 0.95000, 0.90010, 679, 660, 0.2500000, 0.0121000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(39, '30YearFixed', 0.95000, 0.90010, 659, 640, 0.2500000, 0.0128000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(40, '30YearFixed', 0.95000, 0.90010, 639, 620, 0.2500000, 0.0137000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(41, '30YearFixed', 0.95000, 0.90010, 999, 760, 0.1600000, 0.0032000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(42, '30YearFixed', 0.95000, 0.90010, 759, 740, 0.1600000, 0.0044000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(43, '30YearFixed', 0.95000, 0.90010, 739, 720, 0.1600000, 0.0054000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(44, '30YearFixed', 0.95000, 0.90010, 719, 700, 0.1600000, 0.0064000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(45, '30YearFixed', 0.95000, 0.90010, 699, 680, 0.1600000, 0.0078000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(46, '30YearFixed', 0.95000, 0.90010, 679, 660, 0.1600000, 0.0102000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(47, '30YearFixed', 0.95000, 0.90010, 659, 640, 0.1600000, 0.0110000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(48, '30YearFixed', 0.95000, 0.90010, 639, 620, 0.1600000, 0.0120000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(49, '30YearFixed', 0.90000, 0.85010, 999, 760, 0.2500000, 0.0030000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(50, '30YearFixed', 0.90000, 0.85010, 759, 740, 0.2500000, 0.0041000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(51, '30YearFixed', 0.90000, 0.85010, 739, 720, 0.2500000, 0.0050000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(52, '30YearFixed', 0.90000, 0.85010, 719, 700, 0.2500000, 0.0060000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(53, '30YearFixed', 0.90000, 0.85010, 699, 680, 0.2500000, 0.0073000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(54, '30YearFixed', 0.90000, 0.85010, 679, 660, 0.2500000, 0.0100000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(55, '30YearFixed', 0.90000, 0.85010, 659, 640, 0.2500000, 0.0105000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(56, '30YearFixed', 0.90000, 0.85010, 639, 620, 0.2500000, 0.0110000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(57, '30YearFixed', 0.90000, 0.85010, 999, 760, 0.1200000, 0.0023000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(58, '30YearFixed', 0.90000, 0.85010, 759, 740, 0.1200000, 0.0030000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(59, '30YearFixed', 0.90000, 0.85010, 739, 720, 0.1200000, 0.0036000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(60, '30YearFixed', 0.90000, 0.85010, 719, 700, 0.1200000, 0.0041000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(61, '30YearFixed', 0.90000, 0.85010, 699, 680, 0.1200000, 0.0050000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(62, '30YearFixed', 0.90000, 0.85010, 679, 660, 0.1200000, 0.0065000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(63, '30YearFixed', 0.90000, 0.85010, 659, 640, 0.1200000, 0.0069000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(64, '30YearFixed', 0.90000, 0.85010, 639, 620, 0.1200000, 0.0077000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(65, '30YearFixed', 0.85000, 0.00000, 999, 760, 0.1200000, 0.0019000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(66, '30YearFixed', 0.85000, 0.00000, 759, 740, 0.1200000, 0.0020000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(67, '30YearFixed', 0.85000, 0.00000, 739, 720, 0.1200000, 0.0023000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(68, '30YearFixed', 0.85000, 0.00000, 719, 700, 0.1200000, 0.0027000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(69, '30YearFixed', 0.85000, 0.00000, 699, 680, 0.1200000, 0.0032000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(70, '30YearFixed', 0.85000, 0.00000, 679, 660, 0.1200000, 0.0041000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(71, '30YearFixed', 0.85000, 0.00000, 659, 640, 0.1200000, 0.0043000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(72, '30YearFixed', 0.85000, 0.00000, 639, 620, 0.1200000, 0.0045000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(73, '30YearFixed', 0.85000, 0.00000, 999, 760, 0.0600000, 0.0018000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(74, '30YearFixed', 0.85000, 0.00000, 759, 740, 0.0600000, 0.0019000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(75, '30YearFixed', 0.85000, 0.00000, 739, 720, 0.0600000, 0.0022000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(76, '30YearFixed', 0.85000, 0.00000, 719, 700, 0.0600000, 0.0026000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(77, '30YearFixed', 0.85000, 0.00000, 699, 680, 0.0600000, 0.0031000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(78, '30YearFixed', 0.85000, 0.00000, 679, 660, 0.0600000, 0.0040000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(79, '30YearFixed', 0.85000, 0.00000, 659, 640, 0.0600000, 0.0042000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(80, '30YearFixed', 0.85000, 0.00000, 639, 620, 0.0600000, 0.0043000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(81, '15YearFixed', 0.97000, 0.95010, 999, 760, 0.3500000, 0.0055000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(82, '15YearFixed', 0.97000, 0.95010, 759, 740, 0.3500000, 0.0075000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(83, '15YearFixed', 0.97000, 0.95010, 739, 720, 0.3500000, 0.0095000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(84, '15YearFixed', 0.97000, 0.95010, 719, 700, 0.3500000, 0.0115000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(85, '15YearFixed', 0.97000, 0.95010, 699, 680, 0.3500000, 0.0140000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(86, '15YearFixed', 0.97000, 0.95010, 679, 660, 0.3500000, 0.0190000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(87, '15YearFixed', 0.97000, 0.95010, 659, 640, 0.3500000, 0.0205000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(88, '15YearFixed', 0.97000, 0.95010, 639, 620, 0.3500000, 0.0225000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(89, '15YearFixed', 0.97000, 0.95010, 999, 760, 0.2500000, 0.0044000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(90, '15YearFixed', 0.97000, 0.95010, 759, 740, 0.2500000, 0.0063000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(91, '15YearFixed', 0.97000, 0.95010, 739, 720, 0.2500000, 0.0077000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(92, '15YearFixed', 0.97000, 0.95010, 719, 700, 0.2500000, 0.0093000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(93, '15YearFixed', 0.97000, 0.95010, 699, 680, 0.2500000, 0.0115000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(94, '15YearFixed', 0.97000, 0.95010, 679, 660, 0.2500000, 0.0152000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(95, '15YearFixed', 0.97000, 0.95010, 659, 640, 0.2500000, 0.0167000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(96, '15YearFixed', 0.97000, 0.95010, 639, 620, 0.2500000, 0.0181000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(97, '15YearFixed', 0.97000, 0.95010, 999, 760, 0.1800000, 0.0037000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(98, '15YearFixed', 0.97000, 0.95010, 759, 740, 0.1800000, 0.0054000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(99, '15YearFixed', 0.97000, 0.95010, 739, 720, 0.1800000, 0.0065000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(100, '15YearFixed', 0.97000, 0.95010, 719, 700, 0.1800000, 0.0078000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(101, '15YearFixed', 0.97000, 0.95010, 699, 680, 0.1800000, 0.0098000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(102, '15YearFixed', 0.97000, 0.95010, 679, 660, 0.1800000, 0.0118000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(103, '15YearFixed', 0.97000, 0.95010, 659, 640, 0.1800000, 0.0133000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(104, '15YearFixed', 0.97000, 0.95010, 639, 620, 0.1800000, 0.0143000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(105, '15YearFixed', 0.95000, 0.90010, 999, 760, 0.3500000, 0.0041000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(106, '15YearFixed', 0.95000, 0.90010, 759, 740, 0.3500000, 0.0059000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(107, '15YearFixed', 0.95000, 0.90010, 739, 720, 0.3500000, 0.0073000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(108, '15YearFixed', 0.95000, 0.90010, 719, 700, 0.3500000, 0.0087000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(109, '15YearFixed', 0.95000, 0.90010, 699, 680, 0.3500000, 0.0108000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(110, '15YearFixed', 0.95000, 0.90010, 679, 660, 0.3500000, 0.0142000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(111, '15YearFixed', 0.95000, 0.90010, 659, 640, 0.3500000, 0.0150000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(112, '15YearFixed', 0.95000, 0.90010, 639, 620, 0.3500000, 0.0161000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(113, '15YearFixed', 0.95000, 0.90010, 999, 760, 0.2500000, 0.0037000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(114, '15YearFixed', 0.95000, 0.90010, 759, 740, 0.2500000, 0.0052000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(115, '15YearFixed', 0.95000, 0.90010, 739, 720, 0.2500000, 0.0064000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(116, '15YearFixed', 0.95000, 0.90010, 719, 700, 0.2500000, 0.0075000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(117, '15YearFixed', 0.95000, 0.90010, 699, 680, 0.2500000, 0.0094000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(118, '15YearFixed', 0.95000, 0.90010, 679, 660, 0.2500000, 0.0121000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(119, '15YearFixed', 0.95000, 0.90010, 659, 640, 0.2500000, 0.0128000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(120, '15YearFixed', 0.95000, 0.90010, 639, 620, 0.2500000, 0.0137000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(121, '15YearFixed', 0.95000, 0.90010, 999, 760, 0.1600000, 0.0032000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(122, '15YearFixed', 0.95000, 0.90010, 759, 740, 0.1600000, 0.0044000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(123, '15YearFixed', 0.95000, 0.90010, 739, 720, 0.1600000, 0.0054000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(124, '15YearFixed', 0.95000, 0.90010, 719, 700, 0.1600000, 0.0064000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(125, '15YearFixed', 0.95000, 0.90010, 699, 680, 0.1600000, 0.0078000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(126, '15YearFixed', 0.95000, 0.90010, 679, 660, 0.1600000, 0.0102000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(127, '15YearFixed', 0.95000, 0.90010, 659, 640, 0.1600000, 0.0110000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(128, '15YearFixed', 0.95000, 0.90010, 639, 620, 0.1600000, 0.0120000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(129, '15YearFixed', 0.90000, 0.85010, 999, 760, 0.2500000, 0.0030000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(130, '15YearFixed', 0.90000, 0.85010, 759, 740, 0.2500000, 0.0041000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(131, '15YearFixed', 0.90000, 0.85010, 739, 720, 0.2500000, 0.0050000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(132, '15YearFixed', 0.90000, 0.85010, 719, 700, 0.2500000, 0.0060000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(133, '15YearFixed', 0.90000, 0.85010, 699, 680, 0.2500000, 0.0073000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(134, '15YearFixed', 0.90000, 0.85010, 679, 660, 0.2500000, 0.0100000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(135, '15YearFixed', 0.90000, 0.85010, 659, 640, 0.2500000, 0.0105000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(136, '15YearFixed', 0.90000, 0.85010, 639, 620, 0.2500000, 0.0110000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(137, '15YearFixed', 0.90000, 0.85010, 999, 760, 0.1200000, 0.0023000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(138, '15YearFixed', 0.90000, 0.85010, 759, 740, 0.1200000, 0.0030000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(139, '15YearFixed', 0.90000, 0.85010, 739, 720, 0.1200000, 0.0036000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(140, '15YearFixed', 0.90000, 0.85010, 719, 700, 0.1200000, 0.0041000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(141, '15YearFixed', 0.90000, 0.85010, 699, 680, 0.1200000, 0.0050000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(142, '15YearFixed', 0.90000, 0.85010, 679, 660, 0.1200000, 0.0065000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(143, '15YearFixed', 0.90000, 0.85010, 659, 640, 0.1200000, 0.0069000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(144, '15YearFixed', 0.90000, 0.85010, 639, 620, 0.1200000, 0.0077000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(145, '15YearFixed', 0.85000, 0.00000, 999, 760, 0.1200000, 0.0019000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(146, '15YearFixed', 0.85000, 0.00000, 759, 740, 0.1200000, 0.0020000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(147, '15YearFixed', 0.85000, 0.00000, 739, 720, 0.1200000, 0.0023000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(148, '15YearFixed', 0.85000, 0.00000, 719, 700, 0.1200000, 0.0027000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(149, '15YearFixed', 0.85000, 0.00000, 699, 680, 0.1200000, 0.0032000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(150, '15YearFixed', 0.85000, 0.00000, 679, 660, 0.1200000, 0.0041000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(151, '15YearFixed', 0.85000, 0.00000, 659, 640, 0.1200000, 0.0043000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(152, '15YearFixed', 0.85000, 0.00000, 639, 620, 0.1200000, 0.0045000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(153, '15YearFixed', 0.85000, 0.00000, 999, 760, 0.0600000, 0.0018000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(154, '15YearFixed', 0.85000, 0.00000, 759, 740, 0.0600000, 0.0019000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(155, '15YearFixed', 0.85000, 0.00000, 739, 720, 0.0600000, 0.0022000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(156, '15YearFixed', 0.85000, 0.00000, 719, 700, 0.0600000, 0.0026000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(157, '15YearFixed', 0.85000, 0.00000, 699, 680, 0.0600000, 0.0031000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(158, '15YearFixed', 0.85000, 0.00000, 679, 660, 0.0600000, 0.0040000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(159, '15YearFixed', 0.85000, 0.00000, 659, 640, 0.0600000, 0.0042000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(160, '15YearFixed', 0.85000, 0.00000, 639, 620, 0.0600000, 0.0043000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(161, '5/1Arm', 0.97000, 0.95010, 999, 760, 0.3500000, 0.0069000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(162, '5/1Arm', 0.97000, 0.95010, 759, 740, 0.3500000, 0.0094000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(163, '5/1Arm', 0.97000, 0.95010, 739, 720, 0.3500000, 0.0119000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(164, '5/1Arm', 0.97000, 0.95010, 719, 700, 0.3500000, 0.0144000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(165, '5/1Arm', 0.97000, 0.95010, 699, 680, 0.3500000, 0.0175000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(166, '5/1Arm', 0.97000, 0.95010, 679, 660, 0.3500000, 0.0238000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(167, '5/1Arm', 0.97000, 0.95010, 659, 640, 0.3500000, 0.0256000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(168, '5/1Arm', 0.97000, 0.95010, 639, 620, 0.3500000, 0.0281000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(169, '5/1Arm', 0.97000, 0.95010, 999, 760, 0.2500000, 0.0055000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(170, '5/1Arm', 0.97000, 0.95010, 759, 740, 0.2500000, 0.0079000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(171, '5/1Arm', 0.97000, 0.95010, 739, 720, 0.2500000, 0.0096000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(172, '5/1Arm', 0.97000, 0.95010, 719, 700, 0.2500000, 0.0116000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(173, '5/1Arm', 0.97000, 0.95010, 699, 680, 0.2500000, 0.0144000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(174, '5/1Arm', 0.97000, 0.95010, 679, 660, 0.2500000, 0.0190000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(175, '5/1Arm', 0.97000, 0.95010, 659, 640, 0.2500000, 0.2090000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(176, '5/1Arm', 0.97000, 0.95010, 639, 620, 0.2500000, 0.0226000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(177, '5/1Arm', 0.97000, 0.95010, 999, 760, 0.1800000, 0.0046000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(178, '5/1Arm', 0.97000, 0.95010, 759, 740, 0.1800000, 0.0068000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(179, '5/1Arm', 0.97000, 0.95010, 739, 720, 0.1800000, 0.0081000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(180, '5/1Arm', 0.97000, 0.95010, 719, 700, 0.1800000, 0.0098000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(181, '5/1Arm', 0.97000, 0.95010, 699, 680, 0.1800000, 0.0123000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(182, '5/1Arm', 0.97000, 0.95010, 679, 660, 0.1800000, 0.0156000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(183, '5/1Arm', 0.97000, 0.95010, 659, 640, 0.1800000, 0.0175000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(184, '5/1Arm', 0.97000, 0.95010, 639, 620, 0.1800000, 0.0188000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(185, '5/1Arm', 0.95000, 0.90010, 999, 760, 0.3000000, 0.0051000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(186, '5/1Arm', 0.95000, 0.90010, 759, 740, 0.3000000, 0.0074000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(187, '5/1Arm', 0.95000, 0.90010, 739, 720, 0.3000000, 0.0091000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(188, '5/1Arm', 0.95000, 0.90010, 719, 700, 0.3000000, 0.0109000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(189, '5/1Arm', 0.95000, 0.90010, 699, 680, 0.3000000, 0.0135000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(190, '5/1Arm', 0.95000, 0.90010, 679, 660, 0.3000000, 0.0179000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(191, '5/1Arm', 0.95000, 0.90010, 659, 640, 0.3000000, 0.0195000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(192, '5/1Arm', 0.95000, 0.90010, 639, 620, 0.3000000, 0.0216000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(193, '5/1Arm', 0.95000, 0.90010, 999, 760, 0.2500000, 0.0046000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(194, '5/1Arm', 0.95000, 0.90010, 759, 740, 0.2500000, 0.0065000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(195, '5/1Arm', 0.95000, 0.90010, 739, 720, 0.2500000, 0.0080000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(196, '5/1Arm', 0.95000, 0.90010, 719, 700, 0.2500000, 0.0094000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(197, '5/1Arm', 0.95000, 0.90010, 699, 680, 0.2500000, 0.0118000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(198, '5/1Arm', 0.95000, 0.90010, 679, 660, 0.2500000, 0.0155000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(199, '5/1Arm', 0.95000, 0.90010, 659, 640, 0.2500000, 0.0169000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(200, '5/1Arm', 0.95000, 0.90010, 639, 620, 0.2500000, 0.0185000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(201, '5/1Arm', 0.95000, 0.90010, 999, 760, 0.1600000, 0.0040000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(202, '5/1Arm', 0.95000, 0.90010, 759, 740, 0.1600000, 0.0055000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(203, '5/1Arm', 0.95000, 0.90010, 739, 720, 0.1600000, 0.0068000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(204, '5/1Arm', 0.95000, 0.90010, 719, 700, 0.1600000, 0.0080000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(205, '5/1Arm', 0.95000, 0.90010, 699, 680, 0.1600000, 0.0098000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(206, '5/1Arm', 0.95000, 0.90010, 679, 660, 0.1600000, 0.0128000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(207, '5/1Arm', 0.95000, 0.90010, 659, 640, 0.1600000, 0.0138000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(208, '5/1Arm', 0.95000, 0.90010, 639, 620, 0.1600000, 0.0150000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(209, '5/1Arm', 0.90000, 0.85010, 999, 760, 0.2500000, 0.0038000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(210, '5/1Arm', 0.90000, 0.85010, 759, 740, 0.2500000, 0.0051000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(211, '5/1Arm', 0.90000, 0.85010, 739, 720, 0.2500000, 0.0063000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(212, '5/1Arm', 0.90000, 0.85010, 719, 700, 0.2500000, 0.0075000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(213, '5/1Arm', 0.90000, 0.85010, 699, 680, 0.2500000, 0.0091000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(214, '5/1Arm', 0.90000, 0.85010, 679, 660, 0.2500000, 0.0125000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(215, '5/1Arm', 0.90000, 0.85010, 659, 640, 0.2500000, 0.0135000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(216, '5/1Arm', 0.90000, 0.85010, 639, 620, 0.2500000, 0.0149000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(217, '5/1Arm', 0.90000, 0.85010, 999, 760, 0.1200000, 0.0030000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(218, '5/1Arm', 0.90000, 0.85010, 759, 740, 0.1200000, 0.0038000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(219, '5/1Arm', 0.90000, 0.85010, 739, 720, 0.1200000, 0.0045000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(220, '5/1Arm', 0.90000, 0.85010, 719, 700, 0.1200000, 0.0051000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(221, '5/1Arm', 0.90000, 0.85010, 699, 680, 0.1200000, 0.0063000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(222, '5/1Arm', 0.90000, 0.85010, 679, 660, 0.1200000, 0.0081000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(223, '5/1Arm', 0.90000, 0.85010, 659, 640, 0.1200000, 0.0086000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(224, '5/1Arm', 0.90000, 0.85010, 639, 620, 0.1200000, 0.0096000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(225, '5/1Arm', 0.85000, 0.00000, 999, 760, 0.1200000, 0.0024000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(226, '5/1Arm', 0.85000, 0.00000, 759, 740, 0.1200000, 0.0025000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(227, '5/1Arm', 0.85000, 0.00000, 739, 720, 0.1200000, 0.0029000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(228, '5/1Arm', 0.85000, 0.00000, 719, 700, 0.1200000, 0.0034000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(229, '5/1Arm', 0.85000, 0.00000, 699, 680, 0.1200000, 0.0040000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(230, '5/1Arm', 0.85000, 0.00000, 679, 660, 0.1200000, 0.0051000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(231, '5/1Arm', 0.85000, 0.00000, 659, 640, 0.1200000, 0.0056000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(232, '5/1Arm', 0.85000, 0.00000, 639, 620, 0.1200000, 0.0061000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(233, '5/1Arm', 0.85000, 0.00000, 999, 760, 0.0600000, 0.0023000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(234, '5/1Arm', 0.85000, 0.00000, 759, 740, 0.0600000, 0.0024000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(235, '5/1Arm', 0.85000, 0.00000, 739, 720, 0.0600000, 0.0028000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(236, '5/1Arm', 0.85000, 0.00000, 719, 700, 0.0600000, 0.0033000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(237, '5/1Arm', 0.85000, 0.00000, 699, 680, 0.0600000, 0.0039000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(238, '5/1Arm', 0.85000, 0.00000, 679, 660, 0.0600000, 0.0050000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(239, '5/1Arm', 0.85000, 0.00000, 659, 640, 0.0600000, 0.0053000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(240, '5/1Arm', 0.85000, 0.00000, 639, 620, 0.0600000, 0.0053000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(241, '7/1Arm', 0.97000, 0.95010, 999, 760, 0.3500000, 0.0069000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(242, '7/1Arm', 0.97000, 0.95010, 759, 740, 0.3500000, 0.0094000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(243, '7/1Arm', 0.97000, 0.95010, 739, 720, 0.3500000, 0.0119000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(244, '7/1Arm', 0.97000, 0.95010, 719, 700, 0.3500000, 0.0144000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(245, '7/1Arm', 0.97000, 0.95010, 699, 680, 0.3500000, 0.0175000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(246, '7/1Arm', 0.97000, 0.95010, 679, 660, 0.3500000, 0.0238000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(247, '7/1Arm', 0.97000, 0.95010, 659, 640, 0.3500000, 0.0256000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(248, '7/1Arm', 0.97000, 0.95010, 639, 620, 0.3500000, 0.0281000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(249, '7/1Arm', 0.97000, 0.95010, 999, 760, 0.2500000, 0.0055000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(250, '7/1Arm', 0.97000, 0.95010, 759, 740, 0.2500000, 0.0079000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(251, '7/1Arm', 0.97000, 0.95010, 739, 720, 0.2500000, 0.0096000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(252, '7/1Arm', 0.97000, 0.95010, 719, 700, 0.2500000, 0.0116000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(253, '7/1Arm', 0.97000, 0.95010, 699, 680, 0.2500000, 0.0144000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(254, '7/1Arm', 0.97000, 0.95010, 679, 660, 0.2500000, 0.0190000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(255, '7/1Arm', 0.97000, 0.95010, 659, 640, 0.2500000, 0.2090000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(256, '7/1Arm', 0.97000, 0.95010, 639, 620, 0.2500000, 0.0226000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(257, '7/1Arm', 0.97000, 0.95010, 999, 760, 0.1800000, 0.0046000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(258, '7/1Arm', 0.97000, 0.95010, 759, 740, 0.1800000, 0.0068000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(259, '7/1Arm', 0.97000, 0.95010, 739, 720, 0.1800000, 0.0081000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(260, '7/1Arm', 0.97000, 0.95010, 719, 700, 0.1800000, 0.0098000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(261, '7/1Arm', 0.97000, 0.95010, 699, 680, 0.1800000, 0.0123000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(262, '7/1Arm', 0.97000, 0.95010, 679, 660, 0.1800000, 0.0156000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(263, '7/1Arm', 0.97000, 0.95010, 659, 640, 0.1800000, 0.0175000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(264, '7/1Arm', 0.97000, 0.95010, 639, 620, 0.1800000, 0.0188000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(265, '7/1Arm', 0.95000, 0.90010, 999, 760, 0.3000000, 0.0051000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(266, '7/1Arm', 0.95000, 0.90010, 759, 740, 0.3000000, 0.0074000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(267, '7/1Arm', 0.95000, 0.90010, 739, 720, 0.3000000, 0.0091000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(268, '7/1Arm', 0.95000, 0.90010, 719, 700, 0.3000000, 0.0109000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(269, '7/1Arm', 0.95000, 0.90010, 699, 680, 0.3000000, 0.0135000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(270, '7/1Arm', 0.95000, 0.90010, 679, 660, 0.3000000, 0.0179000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(271, '7/1Arm', 0.95000, 0.90010, 659, 640, 0.3000000, 0.0195000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(272, '7/1Arm', 0.95000, 0.90010, 639, 620, 0.3000000, 0.0216000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(273, '7/1Arm', 0.95000, 0.90010, 999, 760, 0.2500000, 0.0046000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(274, '7/1Arm', 0.95000, 0.90010, 759, 740, 0.2500000, 0.0065000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(275, '7/1Arm', 0.95000, 0.90010, 739, 720, 0.2500000, 0.0080000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(276, '7/1Arm', 0.95000, 0.90010, 719, 700, 0.2500000, 0.0094000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(277, '7/1Arm', 0.95000, 0.90010, 699, 680, 0.2500000, 0.0118000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(278, '7/1Arm', 0.95000, 0.90010, 679, 660, 0.2500000, 0.0155000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(279, '7/1Arm', 0.95000, 0.90010, 659, 640, 0.2500000, 0.0169000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(280, '7/1Arm', 0.95000, 0.90010, 639, 620, 0.2500000, 0.0185000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(281, '7/1Arm', 0.95000, 0.90010, 999, 760, 0.1600000, 0.0040000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(282, '7/1Arm', 0.95000, 0.90010, 759, 740, 0.1600000, 0.0055000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(283, '7/1Arm', 0.95000, 0.90010, 739, 720, 0.1600000, 0.0068000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(284, '7/1Arm', 0.95000, 0.90010, 719, 700, 0.1600000, 0.0080000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(285, '7/1Arm', 0.95000, 0.90010, 699, 680, 0.1600000, 0.0098000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(286, '7/1Arm', 0.95000, 0.90010, 679, 660, 0.1600000, 0.0128000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(287, '7/1Arm', 0.95000, 0.90010, 659, 640, 0.1600000, 0.0138000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(288, '7/1Arm', 0.95000, 0.90010, 639, 620, 0.1600000, 0.0150000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(289, '7/1Arm', 0.90000, 0.85010, 999, 760, 0.2500000, 0.0038000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(290, '7/1Arm', 0.90000, 0.85010, 759, 740, 0.2500000, 0.0051000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(291, '7/1Arm', 0.90000, 0.85010, 739, 720, 0.2500000, 0.0063000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(292, '7/1Arm', 0.90000, 0.85010, 719, 700, 0.2500000, 0.0075000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(293, '7/1Arm', 0.90000, 0.85010, 699, 680, 0.2500000, 0.0091000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(294, '7/1Arm', 0.90000, 0.85010, 679, 660, 0.2500000, 0.0125000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(295, '7/1Arm', 0.90000, 0.85010, 659, 640, 0.2500000, 0.0135000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(296, '7/1Arm', 0.90000, 0.85010, 639, 620, 0.2500000, 0.0149000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(297, '7/1Arm', 0.90000, 0.85010, 999, 760, 0.1200000, 0.0030000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(298, '7/1Arm', 0.90000, 0.85010, 759, 740, 0.1200000, 0.0038000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(299, '7/1Arm', 0.90000, 0.85010, 739, 720, 0.1200000, 0.0045000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(300, '7/1Arm', 0.90000, 0.85010, 719, 700, 0.1200000, 0.0051000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(301, '7/1Arm', 0.90000, 0.85010, 699, 680, 0.1200000, 0.0063000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(302, '7/1Arm', 0.90000, 0.85010, 679, 660, 0.1200000, 0.0081000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(303, '7/1Arm', 0.90000, 0.85010, 659, 640, 0.1200000, 0.0086000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(304, '7/1Arm', 0.90000, 0.85010, 639, 620, 0.1200000, 0.0096000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(305, '7/1Arm', 0.85000, 0.00000, 999, 760, 0.1200000, 0.0024000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(306, '7/1Arm', 0.85000, 0.00000, 759, 740, 0.1200000, 0.0025000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(307, '7/1Arm', 0.85000, 0.00000, 739, 720, 0.1200000, 0.0029000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(308, '7/1Arm', 0.85000, 0.00000, 719, 700, 0.1200000, 0.0034000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(309, '7/1Arm', 0.85000, 0.00000, 699, 680, 0.1200000, 0.0040000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(310, '7/1Arm', 0.85000, 0.00000, 679, 660, 0.1200000, 0.0051000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(311, '7/1Arm', 0.85000, 0.00000, 659, 640, 0.1200000, 0.0056000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(312, '7/1Arm', 0.85000, 0.00000, 639, 620, 0.1200000, 0.0061000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(313, '7/1Arm', 0.85000, 0.00000, 999, 760, 0.0600000, 0.0023000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(314, '7/1Arm', 0.85000, 0.00000, 759, 740, 0.0600000, 0.0024000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(315, '7/1Arm', 0.85000, 0.00000, 739, 720, 0.0600000, 0.0028000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(316, '7/1Arm', 0.85000, 0.00000, 719, 700, 0.0600000, 0.0033000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(317, '7/1Arm', 0.85000, 0.00000, 699, 680, 0.0600000, 0.0039000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(318, '7/1Arm', 0.85000, 0.00000, 679, 660, 0.0600000, 0.0050000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(319, '7/1Arm', 0.85000, 0.00000, 659, 640, 0.0600000, 0.0053000);
INSERT INTO public.mortgage_insurance_rate(mortgage_insurance_rate_key, product_type, ltv_upper_limit, ltv_lower_limit, credit_profile_upper_limit, credit_profile_lower_limit, mortgage_insurance_coverage, annual_mortgage_insurance_rate)
  VALUES(320, '7/1Arm', 0.85000, 0.00000, 639, 620, 0.0600000, 0.0053000);
INSERT INTO public.product_type_price
(
    product_type_price_key,
    product_type,
    loan_amortization_period_type,
    loan_amortization_period_count,
    product_type_rate_adjustment
)
VALUES
(
    1,  -- product_type_price_key
    '15YearFixed',  -- product_type
    'Monthly',
    180,
    -0.0075   -- product_type_rate_adjustment
);


INSERT INTO public.product_type_price
(
    product_type_price_key,
    product_type,
    loan_amortization_period_type,
    loan_amortization_period_count,
    product_type_rate_adjustment
)
VALUES
(
    2,  -- product_type_price_key
    '30YearFixed',  -- product_type
    'Monthly',
    360,
    0   -- product_type_rate_adjustment
);



INSERT INTO public.product_type_price
(
    product_type_price_key,
    product_type,
    loan_amortization_period_type,
    loan_amortization_period_count,
    product_type_rate_adjustment
)
VALUES
(
    3,  -- product_type_price_key
    '5/1Arm',  -- product_type
    'Monthly',
    360,
    -0.0075   -- product_type_rate_adjustment
);



INSERT INTO public.product_type_price
(
    product_type_price_key,
    product_type,
    loan_amortization_period_type,
    loan_amortization_period_count,
    product_type_rate_adjustment
)
VALUES
(
    4,  -- product_type_price_key
    '7/1Arm',  -- product_type
    'Monthly',
    360,
    -0.005   -- product_type_rate_adjustment
);

INSERT INTO public.property_usage_type_price
(
    property_usage_type_price_key,
    property_usage_type,
    property_usage_type_rate_adjustment
)
VALUES
(
    1,  -- property_usage_type_price_key
    'Investor',  -- property_usage_type
    0.005   -- property_usage_type_rate_adjustment
);


INSERT INTO public.property_usage_type_price
(
    property_usage_type_price_key,
    property_usage_type,
    property_usage_type_rate_adjustment
)
VALUES
(
    2,  -- property_usage_type_price_key
    'PrimaryResidence',  -- property_usage_type
    0   -- property_usage_type_rate_adjustment
);



INSERT INTO public.property_usage_type_price
(
    property_usage_type_price_key,
    property_usage_type,
    property_usage_type_rate_adjustment
)
VALUES
(
    3,  -- property_usage_type_price_key
    'SecondHome',  -- property_usage_type
    0.0025   -- property_usage_type_rate_adjustment
);
