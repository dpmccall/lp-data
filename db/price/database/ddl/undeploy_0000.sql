--
--  database
--
DROP DATABASE price;


--
--  role
--
DROP ROLE price_ro;
DROP ROLE price_rw;
DROP ROLE price_adm;


