--
--  database
--
CREATE DATABASE price;


--
--  role
--
CREATE ROLE price_ro LOGIN;
CREATE ROLE price_rw LOGIN;
CREATE ROLE price_adm LOGIN;


--
--  service account
--  
DO
$$
BEGIN
    IF NOT EXISTS (SELECT * FROM pg_catalog.pg_user WHERE usename = 'svcjava') THEN
        CREATE USER svcjava PASSWORD 'default';
    END IF;
END
$$;
GRANT price_rw TO svcjava;


--  
--  schema
--
\connect price
--  Do not create public schema.  It already exists.
GRANT ALL ON SCHEMA public TO price_adm WITH GRANT OPTION;


--  Keep schema is used for undo.
CREATE SCHEMA IF NOT EXISTS keep;
GRANT ALL ON SCHEMA keep TO price_adm WITH GRANT OPTION;






