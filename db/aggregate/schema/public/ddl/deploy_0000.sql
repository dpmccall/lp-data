--
-- ER/Studio Data Architect SQL Code Generation
-- Project :      aggregate.public.dm1
--
-- Date Created : Thursday, June 30, 2016 16:16:33
-- Target DBMS : PostgreSQL 9.x
--


\connect aggregate






-- 
-- TABLE: public.applicant 
--

CREATE TABLE public.applicant(
    applicant_id    uuid     NOT NULL,
    tenant_id       uuid     NOT NULL,
    data            jsonb,
    deleted_date    date,
    CONSTRAINT applicant_pk PRIMARY KEY (applicant_id) USING INDEX TABLESPACE pg_default 
)
;
GRANT SELECT ON public.applicant TO aggregate_ro;
GRANT SELECT, INSERT, UPDATE, DELETE ON public.applicant TO aggregate_rw;
GRANT ALL ON public.applicant TO aggregate_adm;

COMMENT ON COLUMN public.applicant.applicant_id IS '@Term(id = 888)'
;
COMMENT ON COLUMN public.applicant.tenant_id IS '@Term(id = 889)'
;
COMMENT ON TABLE public.applicant IS 'Storage and retrieval of JSON aggregate representation for Applicants.'
;

-- 
-- TABLE: public.applicant_reo 
--

CREATE TABLE public.applicant_reo(
    applicant_id    uuid     NOT NULL,
    reo_id          uuid     NOT NULL,
    tenant_id       uuid     NOT NULL,
    data            jsonb    NOT NULL,
    CONSTRAINT applicant_reo_pk PRIMARY KEY (applicant_id, reo_id)
)
;
GRANT SELECT ON public.applicant_reo TO aggregate_ro;
GRANT SELECT, INSERT, UPDATE, DELETE ON public.applicant_reo TO aggregate_rw;
GRANT ALL ON public.applicant_reo TO aggregate_adm;

COMMENT ON COLUMN public.applicant_reo.applicant_id IS '@Term(id = 888)'
;
COMMENT ON COLUMN public.applicant_reo.tenant_id IS '@Term(id = 889)'
;

-- 
-- TABLE: public.asset 
--

CREATE TABLE public.asset(
    asset_id               uuid     NOT NULL,
    loan_transaction_id    uuid     NOT NULL,
    tenant_id              uuid     NOT NULL,
    data                   jsonb,
    deleted_date           date,
    CONSTRAINT asset_pk PRIMARY KEY (asset_id) USING INDEX TABLESPACE pg_default 
)
;
GRANT SELECT ON public.asset TO aggregate_ro;
GRANT SELECT, INSERT, UPDATE, DELETE ON public.asset TO aggregate_rw;
GRANT ALL ON public.asset TO aggregate_adm;

COMMENT ON COLUMN public.asset.tenant_id IS '@Term(id = 889)'
;

-- 
-- TABLE: public.declaration 
--

CREATE TABLE public.declaration(
    party_id        uuid     NOT NULL,
    loan_id         uuid     NOT NULL,
    tenant_id       uuid     NOT NULL,
    deleted_date    date,
    data            jsonb,
    CONSTRAINT declaration_pk PRIMARY KEY (party_id, loan_id)
)
;
GRANT SELECT ON public.declaration TO aggregate_ro;
GRANT SELECT, INSERT, UPDATE, DELETE ON public.declaration TO aggregate_rw;
GRANT ALL ON public.declaration TO aggregate_adm;

COMMENT ON COLUMN public.declaration.tenant_id IS '@Term(id = 889)'
;

-- 
-- TABLE: public.document_descriptor 
--

CREATE TABLE public.document_descriptor(
    document_descriptor_id    uuid     NOT NULL,
    tenant_id                 uuid     NOT NULL,
    loan_transaction_id       uuid     NOT NULL,
    data                      jsonb,
    deleted_date              date,
    CONSTRAINT document_descriptor_pk PRIMARY KEY (document_descriptor_id) USING INDEX TABLESPACE pg_default 
)
;
GRANT SELECT ON public.document_descriptor TO aggregate_ro;
GRANT SELECT, INSERT, UPDATE, DELETE ON public.document_descriptor TO aggregate_rw;
GRANT ALL ON public.document_descriptor TO aggregate_adm;

COMMENT ON COLUMN public.document_descriptor.tenant_id IS '@Term(id = 889)'
;

-- 
-- TABLE: public.government_monitoring 
--

CREATE TABLE public.government_monitoring(
    party_id        uuid     NOT NULL,
    loan_id         uuid     NOT NULL,
    tenant_id       uuid     NOT NULL,
    data            jsonb    NOT NULL,
    deleted_date    date,
    CONSTRAINT government_monitoring_pk PRIMARY KEY (party_id, loan_id)
)
;
GRANT SELECT ON public.government_monitoring TO aggregate_ro;
GRANT SELECT, INSERT, UPDATE, DELETE ON public.government_monitoring TO aggregate_rw;
GRANT ALL ON public.government_monitoring TO aggregate_adm;

COMMENT ON COLUMN public.government_monitoring.tenant_id IS '@Term(id = 889)'
;

-- 
-- TABLE: public.income 
--

CREATE TABLE public.income(
    income_id       uuid     NOT NULL,
    tenant_id       uuid     NOT NULL,
    applicant_id    uuid     NOT NULL,
    data            jsonb    NOT NULL,
    deleted_date    date,
    CONSTRAINT income_pk PRIMARY KEY (income_id) USING INDEX TABLESPACE pg_default 
)
;
GRANT SELECT ON public.income TO aggregate_ro;
GRANT SELECT, INSERT, UPDATE, DELETE ON public.income TO aggregate_rw;
GRANT ALL ON public.income TO aggregate_adm;

COMMENT ON COLUMN public.income.tenant_id IS '@Term(id = 889)'
;
COMMENT ON COLUMN public.income.applicant_id IS '@Term(id = 888)'
;

-- 
-- TABLE: public.liability 
--

CREATE TABLE public.liability(
    party_id        uuid     NOT NULL,
    loan_id         uuid     NOT NULL,
    tenant_id       uuid     NOT NULL,
    deleted_date    date,
    data            jsonb,
    CONSTRAINT liability_pk PRIMARY KEY (party_id, loan_id)
)
;
GRANT SELECT ON public.liability TO aggregate_ro;
GRANT SELECT, INSERT, UPDATE, DELETE ON public.liability TO aggregate_rw;
GRANT ALL ON public.liability TO aggregate_adm;

COMMENT ON COLUMN public.liability.tenant_id IS '@Term(id = 889)'
;

-- 
-- TABLE: public.loan 
--

CREATE TABLE public.loan(
    loan_id                uuid     NOT NULL,
    loan_transaction_id    uuid     NOT NULL,
    tenant_id              uuid     NOT NULL,
    data                   jsonb    NOT NULL,
    deleted_date           date,
    CONSTRAINT loan_pk PRIMARY KEY (loan_id) USING INDEX TABLESPACE pg_default 
)
;
GRANT SELECT ON public.loan TO aggregate_ro;
GRANT SELECT, INSERT, UPDATE, DELETE ON public.loan TO aggregate_rw;
GRANT ALL ON public.loan TO aggregate_adm;

COMMENT ON COLUMN public.loan.tenant_id IS '@Term(id = 889)'
;

-- 
-- TABLE: public.loan_transaction 
--

CREATE TABLE public.loan_transaction(
    loan_transaction_id    uuid     NOT NULL,
    tenant_id              uuid     NOT NULL,
    data                   jsonb,
    deleted_date           date,
    CONSTRAINT loan_transaction_pk PRIMARY KEY (loan_transaction_id) USING INDEX TABLESPACE pg_default 
)
;
GRANT SELECT ON public.loan_transaction TO aggregate_ro;
GRANT SELECT, INSERT, UPDATE, DELETE ON public.loan_transaction TO aggregate_rw;
GRANT ALL ON public.loan_transaction TO aggregate_adm;

COMMENT ON COLUMN public.loan_transaction.tenant_id IS '@Term(id = 889)'
;

-- 
-- TABLE: public.online_sale 
--

CREATE TABLE public.online_sale(
    loan_id         uuid     NOT NULL,
    tenant_id       uuid,
    data            jsonb    NOT NULL,
    deleted_date    date,
    CONSTRAINT online_sale_pk PRIMARY KEY (loan_id)
)
;
GRANT SELECT ON public.online_sale TO aggregate_ro;
GRANT SELECT, INSERT, UPDATE, DELETE ON public.online_sale TO aggregate_rw;
GRANT ALL ON public.online_sale TO aggregate_adm;

COMMENT ON COLUMN public.online_sale.tenant_id IS '@Term(id = 889)'
;

-- 
-- TABLE: public.party 
--

CREATE TABLE public.party(
    party_id        uuid     NOT NULL,
    tenant_id       uuid     NOT NULL,
    data            jsonb    NOT NULL,
    deleted_date    date,
    CONSTRAINT party_pk PRIMARY KEY (party_id)
)
;
GRANT SELECT ON public.party TO aggregate_ro;
GRANT SELECT, INSERT, UPDATE, DELETE ON public.party TO aggregate_rw;
GRANT ALL ON public.party TO aggregate_adm;

COMMENT ON COLUMN public.party.tenant_id IS '@Term(id = 889)'
;

-- 
-- TABLE: public.product_and_pricing 
--

CREATE TABLE public.product_and_pricing(
    loan_id         uuid     NOT NULL,
    tenant_id       uuid,
    data            jsonb    NOT NULL,
    deleted_date    date,
    CONSTRAINT product_and_pricing_pk PRIMARY KEY (loan_id)
)
;
GRANT SELECT ON public.product_and_pricing TO aggregate_ro;
GRANT SELECT, INSERT, UPDATE, DELETE ON public.product_and_pricing TO aggregate_rw;
GRANT ALL ON public.product_and_pricing TO aggregate_adm;

COMMENT ON COLUMN public.product_and_pricing.tenant_id IS '@Term(id = 889)'
;

-- 
-- TABLE: public.property 
--

CREATE TABLE public.property(
    property_id     uuid     NOT NULL,
    tenant_id       uuid     NOT NULL,
    data            jsonb,
    deleted_date    date,
    CONSTRAINT property_pk PRIMARY KEY (property_id) USING INDEX TABLESPACE pg_default 
)
;
GRANT SELECT ON public.property TO aggregate_ro;
GRANT SELECT, INSERT, UPDATE, DELETE ON public.property TO aggregate_rw;
GRANT ALL ON public.property TO aggregate_adm;

COMMENT ON COLUMN public.property.tenant_id IS '@Term(id = 889)'
;

-- 
-- TABLE: public.reo 
--

CREATE TABLE public.reo(
    reo_id          uuid     NOT NULL,
    tenant_id       uuid     NOT NULL,
    data            jsonb,
    deleted_date    date,
    CONSTRAINT reo_pk PRIMARY KEY (reo_id) USING INDEX TABLESPACE pg_default 
)
;
GRANT SELECT ON public.reo TO aggregate_ro;
GRANT SELECT, INSERT, UPDATE, DELETE ON public.reo TO aggregate_rw;
GRANT ALL ON public.reo TO aggregate_adm;

COMMENT ON COLUMN public.reo.tenant_id IS '@Term(id = 889)'
;

-- 
-- TABLE: public.subject_property 
--

CREATE TABLE public.subject_property(
    loan_transaction_id    uuid     NOT NULL,
    tenant_id              uuid     NOT NULL,
    property_id            uuid,
    data                   jsonb,
    deleted_date           date,
    CONSTRAINT subject_property_pk PRIMARY KEY (loan_transaction_id) USING INDEX TABLESPACE pg_default 
)
;
GRANT SELECT ON public.subject_property TO aggregate_ro;
GRANT SELECT, INSERT, UPDATE, DELETE ON public.subject_property TO aggregate_rw;
GRANT ALL ON public.subject_property TO aggregate_adm;

COMMENT ON COLUMN public.subject_property.tenant_id IS '@Term(id = 889)'
;

-- 
-- TABLE: public.tenant 
--

CREATE TABLE public.tenant(
    tenant_id    uuid    NOT NULL,
    CONSTRAINT tenant_pk PRIMARY KEY (tenant_id)
)
;
GRANT SELECT ON public.tenant TO aggregate_ro;
GRANT SELECT, INSERT, UPDATE, DELETE ON public.tenant TO aggregate_rw;
GRANT ALL ON public.tenant TO aggregate_adm;

COMMENT ON COLUMN public.tenant.tenant_id IS '@Term(id = 889)'
;

-- 
-- INDEX: applicant_reo_x1 
--

CREATE INDEX applicant_reo_x1 ON public.applicant_reo(reo_id)
;
-- 
-- INDEX: asset_x1 
--

CREATE INDEX asset_x1 ON public.asset(loan_transaction_id)
;
-- 
-- INDEX: declaration_x1 
--

CREATE INDEX declaration_x1 ON public.declaration(loan_id)
;
-- 
-- INDEX: document_descriptor_x1 
--

CREATE INDEX document_descriptor_x1 ON public.document_descriptor(loan_transaction_id)
;
-- 
-- INDEX: government_monitoring_x1 
--

CREATE INDEX government_monitoring_x1 ON public.government_monitoring(loan_id)
;
-- 
-- INDEX: income_x1 
--

CREATE INDEX income_x1 ON public.income(applicant_id)
;
-- 
-- INDEX: liability_x1 
--

CREATE INDEX liability_x1 ON public.liability(loan_id)
;
-- 
-- INDEX: loan_x1 
--

CREATE INDEX loan_x1 ON public.loan(loan_transaction_id)
;
-- 
-- INDEX: subject_property_x1 
--

CREATE INDEX subject_property_x1 ON public.subject_property(loan_transaction_id)
;


-- 
-- TABLE: public.user_summary 
--

CREATE TABLE public.user_summary(
    user_id                uuid     NOT NULL,
    loan_transaction_id    uuid     NOT NULL,
    tenant_id              uuid     NOT NULL,
    deleted_date           date,
    data                   jsonb    NOT NULL,
    CONSTRAINT user_summary_pk PRIMARY KEY (user_id, loan_transaction_id)
)
;
GRANT SELECT ON public.user_summary TO aggregate_ro;
GRANT SELECT, INSERT, UPDATE, DELETE ON public.user_summary TO aggregate_rw;
GRANT ALL ON public.user_summary TO aggregate_adm;


-- 
-- INDEX: user_summary_x1 
--

CREATE INDEX user_summary_x1 ON public.user_summary(loan_transaction_id)
;
-- 
-- INDEX: user_summary_x2 
--

CREATE INDEX user_summary_x2 ON public.user_summary(tenant_id)
;

















CREATE OR REPLACE VIEW public.document_descriptor_v AS
SELECT
    dd.document_descriptor_id,
    CAST(dd.data -> 'lastMetaData' ->> 'event-id' AS UUID) event_id,
    dd.data -> 'lastMetaData' ->> 'username' user_name,
    CAST(dd.data -> 'lastMetaData' ->> 'user-identifier' AS UUID) user_id,
    CAST(dd.data -> 'lastMetaData' ->> 'command-identifier' AS UUID) command_id,
    CAST(dd.data -> 'lastMetaData' ->> 'correlation-identifier' AS UUID) correlation_id,
    dd.tenant_id,
    dd.loan_transaction_id,
    dd.data ->> 'contentType' content_type,
    dd.data ->> 'uploaderUserId' uploader_user_id
FROM public.document_descriptor dd;

GRANT SELECT ON public.document_descriptor_v TO aggregate_ro;
GRANT SELECT ON public.document_descriptor_v TO aggregate_rw;
GRANT SELECT ON public.document_descriptor_v TO aggregate_adm;


CREATE OR REPLACE VIEW public.document_v AS
SELECT
    d document_id,
    dd.document_descriptor_id
FROM public.document_descriptor dd, JSON_ARRAY_ELEMENTS_TEXT(dd.data::JSON -> 'documentIds') d
WHERE (dd.data -> 'documentIds') IS NOT NULL;

GRANT SELECT ON public.document_v TO aggregate_ro;
GRANT SELECT ON public.document_v TO aggregate_rw;
GRANT SELECT ON public.document_v TO aggregate_adm;



CREATE OR REPLACE VIEW public.employment_v AS
SELECT
    p.party_id,
    CAST(p.data -> 'lastMetaData' ->> 'event-id' AS UUID) event_id,
    p.data -> 'lastMetaData' ->> 'username' user_name,
    CAST(p.data -> 'lastMetaData' ->> 'user-identifier' AS UUID) user_id,
    CAST(p.data -> 'lastMetaData' ->> 'command-identifier' AS UUID) command_id,
    CAST(p.data -> 'lastMetaData' ->> 'correlation-identifier' AS UUID) correlation_id,
    CAST(e ->> 'id' AS UUID) employment_id,
    CAST(e ->> 'coEmployer' AS BOOLEAN) co_employer_indicator,                                   --  867
    e ->> 'employerName' employer_name,                                         --  392
    CAST(e ->> 'selfEmployed' AS BOOLEAN) employment_applicant_self_employed_indicator,          --  397                   
    e ->> 'coEmployerName' co_employer_name,                                    --  868
    e ->> 'coEmployerPhone' co_employer_phone,                                  --  869
    e -> 'employerAddress' ->> 'city' employer_city,                            --  390
    e -> 'employerPhone' ->> 'number' employer_phone,
    CAST(e -> 'employerAddress' ->> 'validated' AS BOOLEAN) employer_address_validated_indicator,          
    e -> 'employerAddress' ->> 'postalCode' employer_postal_code,               --  393
    e -> 'employerAddress' ->> 'countryCode' employer_country,                  --  391
    e -> 'employerAddress' ->> 'stateProvince' employer_state,                  --  394 
    e -> 'employerAddress' ->> 'streetAddress1' employer_street_address_1,      --  395
    e -> 'employerAddress' ->> 'streetAddress2' employer_street_address_2,
    e -> 'employerAddress' ->> 'postalCodePlusFour' employer_plus_four_zip_code, 
    TO_DATE(e ->> 'employerEndDate', 'YYYY-MM-DD') employer_end_date,
    e ->> 'employerWebSite' employer_url,                                       --  866
    e ->> 'employerPosition' employer_position_description,                     --  399                                 
    TO_DATE(e ->> 'employerStartDate', 'YYYY-MM-DD') employment_start_date,                            --  618
    CAST(e ->> 'employedByRelative' AS BOOLEAN) employed_by_relative_indicator,                  --  877                            
    e ->> 'employerTypeOfBusiness' employer_business_description,               --  865
    TO_NUMBER(e ->> 'selfEmployedPercentOwnership', '9D9999999') self_employed_ownership_percent        --  878
FROM public.party p, JSON_ARRAY_ELEMENTS(p.data::JSON -> 'employerData') e;


GRANT SELECT ON public.employment_v TO aggregate_ro;
GRANT SELECT ON public.employment_v TO aggregate_rw;
GRANT SELECT ON public.employment_v TO aggregate_adm;



CREATE OR REPLACE VIEW public.loan_transaction_v AS
SELECT
    lt.loan_transaction_id,
    lt.tenant_id,
    CAST(lt.data -> 'lastMetaData' ->> 'event-id' AS UUID) event_id,
    lt.data -> 'lastMetaData' ->> 'username' user_name,
    CAST(lt.data -> 'lastMetaData' ->> 'user-identifier' AS UUID) user_id,
    CAST(lt.data -> 'lastMetaData' ->> 'command-identifier' AS UUID) command_id,
    CAST(lt.data -> 'lastMetaData' ->> 'correlation-identifier' AS UUID) correlation_id,
    lt.data ->> 'subjectPropertySet' subject_property_indicator
FROM public.loan_transaction lt;


GRANT SELECT ON public.loan_transaction_v TO aggregate_ro;
GRANT SELECT ON public.loan_transaction_v TO aggregate_rw;
GRANT SELECT ON public.loan_transaction_v TO aggregate_adm;


CREATE OR REPLACE VIEW public.loan_v AS
SELECT
    l.loan_id,
    l.loan_transaction_id,
    l.tenant_id,
    l.deleted_date,
    l.data ->> 'loanNumber' loan_number,
    l.data ->> 'loanPurpose' loan_purpose,
    CAST(l.data -> 'lastMetaData' ->> 'event-id' AS UUID) event_id,
    l.data -> 'lastMetaData' ->> 'username' user_name,
    CAST(l.data -> 'lastMetaData' ->> 'user-identifier' AS UUID) user_id,
    CAST(l.data -> 'lastMetaData' ->> 'command-identifier' AS UUID) command_id,
    CAST(l.data -> 'lastMetaData' ->> 'correlation-identifier' AS UUID) correlation_id,
    l.data
FROM public.loan l;


GRANT SELECT ON public.loan_v TO aggregate_ro;
GRANT SELECT ON public.loan_v TO aggregate_rw;
GRANT SELECT ON public.loan_v TO aggregate_adm;


CREATE OR REPLACE VIEW public.party_contact_point_telephone_v AS
SELECT
    p.party_id,
    ptn ->> 'number' contact_point_telephone_number,
    ptn ->> 'extension' contact_point_extension,
    ptn ->> 'phoneType' telephone_type,
    p.data 
FROM public.party p, JSON_ARRAY_ELEMENTS(p.data::JSON -> 'telephoneNumbers') ptn
WHERE (p.data ->> 'telephoneNumbers') IS NOT NULL;


GRANT SELECT ON public.party_contact_point_telephone_v TO aggregate_ro;
GRANT SELECT ON public.party_contact_point_telephone_v TO aggregate_rw;
GRANT SELECT ON public.party_contact_point_telephone_v TO aggregate_adm;


CREATE OR REPLACE VIEW public.party_v AS
SELECT
    p.party_id,                                                                                               
    CAST(p.data -> 'lastMetaData' ->> 'event-id' AS UUID) event_id,
    p.data -> 'lastMetaData' ->> 'username' user_name,
    CAST(p.data -> 'lastMetaData' ->> 'user-identifier' AS UUID) user_id,
    CAST(p.data -> 'lastMetaData' ->> 'command-identifier' AS UUID) command_id,
    CAST(p.data -> 'lastMetaData' ->> 'correlation-identifier' AS UUID) correlation_id,
    p.tenant_id,                                                                                              
    p.data ->> 'type' party_type,                                                                             
    p.data -> 'mailingAddress' ->> 'city' mail_to_city,                                                       
    p.data -> 'mailingAddress' ->> 'validated' mail_to_validated,                                       
    p.data -> 'mailingAddress' ->> 'postalCode' mail_to_postal_code,                                          
    p.data -> 'mailingAddress' ->> 'countryCode' mail_to_country,                                             
    p.data -> 'mailingAddress' ->> 'stateProvince' mail_to_state,                                             
    p.data -> 'mailingAddress' ->> 'streetAddress1' mail_to_street_address_1,                                 
    p.data -> 'mailingAddress' ->> 'streetAddress2' mail_to_street_address_2,                                 
    p.data -> 'mailingAddress' ->> 'postalCodePlusFour' applicant_mail_to_zip_plus_four,                        
    p.data -> 'individualParty' -> 'name' ->> 'last' individual_last_name,                                    
    p.data -> 'individualParty' -> 'name' ->> 'first' individual_first_name,                                  
    p.data -> 'individualParty' -> 'name' ->> 'middle' individual_middle_name,                                  
    p.data -> 'individualParty' -> 'name' ->> 'suffix' individual_suffix,                                        
    p.data
FROM public.party p;


GRANT SELECT ON public.party_v TO aggregate_ro;
GRANT SELECT ON public.party_v TO aggregate_rw;
GRANT SELECT ON public.party_v TO aggregate_adm;


CREATE OR REPLACE VIEW public.applicant_v AS
SELECT
    loan.loan_transaction_id,
    loan.loan_id,
    p.party_id,
    p.event_id,
    p.user_name,
    p.user_id,
    p.command_id,
    p.correlation_id,
    p.tenant_id,
    p.party_type,
    p.mail_to_city,
    p.mail_to_validated,
    p.mail_to_postal_code,
    p.mail_to_country,
    p.mail_to_state,
    p.mail_to_street_address_1,
    p.mail_to_street_address_2,
    p.applicant_mail_to_zip_plus_four,
    p.individual_last_name,
    p.individual_first_name,
    p.individual_middle_name,
    p.individual_suffix,
    p.data -> 'applicantParty' -> 'ssn' ->> 'value' ssn,
    p.data -> 'applicantParty' ->> 'visaType' visa_type,
    TO_DATE(p.data -> 'applicantParty' ->> 'birthDate', 'YYYY-MM-DD') birth_date,
    p.data -> 'applicantParty' ->> 'veteranStatus' veteran_status,
    p.data -> 'applicantParty' ->> 'yearsOfSchool' years_of_school,
    TO_DATE(p.data -> 'applicantParty' -> 'currentAddress' ->> 'start', 'YYYY-MM-DD') current_residence_start_date,
    TO_DATE(p.data -> 'applicantParty' -> 'currentAddress' ->> 'end', 'YYYY-MM-DD') current_residence_end_date,
    p.data -> 'applicantParty' -> 'currentAddress' ->> 'city' current_residence_city,
    p.data -> 'applicantParty' -> 'currentAddress' ->> 'validated' current_residence_validated,
    p.data -> 'applicantParty' -> 'currentAddress' ->> 'postalCode' current_residence_postal_code,
    p.data -> 'applicantParty' -> 'currentAddress' ->> 'countryCode' current_residence_country_code,
    p.data -> 'applicantParty' -> 'currentAddress' ->> 'stateProvince' current_residence_state,
    p.data -> 'applicantParty' -> 'currentAddress' ->> 'streetAddress1' current_residence_street_address_1,
    p.data -> 'applicantParty' -> 'currentAddress' ->> 'streetAddress2' current_residence_street_address_2,
    p.data -> 'applicantParty' -> 'currentAddress' ->> 'postalCodePlusFour' current_residence_zip_plus_four,
    p.data -> 'applicantParty' -> 'currentAddress' ->> 'residencyBasisType' current_residence_residency_basis_type,
    p.data -> 'applicantParty' ->> 'citizenshipStatus' citizenship_status,
    p.data -> 'applicantParty' ->> 'relationshipStatus' relationship_status,
    p.data -> 'applicantParty' ->> 'employerLineOfWorkYears' employer_line_of_work_years,
    p.data -> 'applicantParty' ->> 'mailingAddressSameAsCurrentAddress' mailing_address_same_as_current_address,
    p.data -> 'applicantParty' ->> 'employmentBorrowerSelfEmployedIndicator' employment_borrower_self_employed_indicator,
    TO_DATE(p.data ->> 'retireDate', 'YYYY-MM-DD') retirement_start_date,                                                 
    p.data ->> 'militaryStatus' military_status,                                                                 
    p.data ->> 'employmentStatus' employment_status_type,                                                       
    p.data ->> 'employerLineOfWorkYears' employer_line_of_work_years_todo,
    p.data ->> 'employmentBorrowerSelfEmployedIndicator' employment_borrower_self_employed_indicator_todo,
    p.data
FROM 
(
    SELECT
        l.loan_transaction_id,
        l.loan_id,
        CAST(lp AS UUID) party_id
    FROM public.loan l, JSON_ARRAY_ELEMENTS_TEXT(l.data::JSON -> 'applicantIds') lp
) loan
INNER JOIN public.party_v p
    ON loan.party_id = p.party_id;


GRANT SELECT ON public.applicant_v TO aggregate_ro;
GRANT SELECT ON public.applicant_v TO aggregate_rw;
GRANT SELECT ON public.applicant_v TO aggregate_adm;


CREATE OR REPLACE VIEW public.property_v AS
SELECT
    p.property_id,
    CAST(p.data -> 'lastMetaData' ->> 'event-id' AS UUID) event_id,
    p.data -> 'lastMetaData' ->> 'username' user_name,
    CAST(p.data -> 'lastMetaData' ->> 'user-identifier' AS UUID) user_id,
    CAST(p.data -> 'lastMetaData' ->> 'command-identifier' AS UUID) command_id,
    CAST(p.data -> 'lastMetaData' ->> 'correlation-identifier' AS UUID) correlation_id,
    p.tenant_id,
    p.data -> 'address' ->> 'city' property_city,
    p.data -> 'address' ->> 'validated' property_address_validated_indicator,
    p.data -> 'address' ->> 'postalCode' property_postal_code,
    p.data -> 'address' ->> 'countryCode' property_country,
    p.data -> 'address' ->> 'stateProvince' property_state,
    p.data -> 'address' ->> 'streetAddress1' property_address_1,
    p.data -> 'address' ->> 'streetAddress2' property_address_2,
    p.data -> 'address' ->> 'postalCodePlusFour' property_zip_plus_four,
    p.data
FROM public.property p;

GRANT SELECT ON public.property_v TO aggregate_ro;
GRANT SELECT ON public.property_v TO aggregate_rw;
GRANT SELECT ON public.property_v TO aggregate_adm;



CREATE OR REPLACE VIEW public.reo_v AS
SELECT
    r.reo_id,
    CAST(r.data -> 'lastMetaData' ->> 'event-id' AS UUID) event_id,
    r.data -> 'lastMetaData' ->> 'username' user_name,
    CAST(r.data -> 'lastMetaData' ->> 'user-identifier' AS UUID) user_id,
    CAST(r.data -> 'lastMetaData' ->> 'command-identifier' AS UUID) command_id,
    CAST(r.data -> 'lastMetaData' ->> 'correlation-identifier' AS UUID) correlation_id,
    r.tenant_id,
    CAST(r.data ->> 'loanTransactionId' AS UUID) loan_transaction_id,
    TO_NUMBER(r.data -> 'taxes' ->> 'amount', '9999999999999.99') property_taxes_amount,
    r.data ->> 'county' reo_property_county,
    TO_NUMBER(r.data -> 'insurance' ->> 'amount', '9999999999999.99') property_hazard_insurance_amount,
    r.data ->> 'reoCreator' created_user_id,
    TO_NUMBER(r.data -> 'totalLeins' ->> 'amount', '9999999999999.99') reo_property_lien_upb_amount,
    r.data ->> 'expenseType' expense_type,                                      --  don't know the term for this
    TO_NUMBER(r.data -> 'marketValue' ->> 'amount', '9999999999999.99') reo_property_market_value_amount,
    r.data ->> 'propertyType' reo_property_gse_property_type,
    TO_NUMBER(r.data -> 'firstMortgage' ->> 'amount', '9999999999999.99') property_first_mortgage_amount,
    TO_NUMBER(r.data -> 'hoaOrCondoFees' ->> 'amount', '9999999999999.99') property_hoa_or_condo_dues_amount,
    TO_NUMBER(r.data -> 'secondMortgage' ->> 'amount', '9999999999999.99') property_second_mortgage_amount,
    r.data ->> 'dispositionType' reo_property_disposition_status_type,
    CAST(r.data ->> 'subjectProperty' AS BOOLEAN) reo_property_subject_indicator,
    CAST(r.data ->> 'currentResidence' AS BOOLEAN) reo_property_current_residence_indicator,
    TO_NUMBER(r.data -> 'otherExpenseAmount' ->> 'amount', '9999999999999.99') reo_property_other_expense_amount,  -- don't know the term for this
    CAST(r.data ->> 'reoPropertyReference' AS UUID) property_id,
    TO_NUMBER(r.data -> 'monthlyGrossRentalReceived' ->> 'amount', '9999999999999.99') reo_property_rental_income_gross_amount,
    r.data ->> 'otherExpenseTypeDescription' reo_property_other_expense_type_description,
    r.data
FROM public.reo r;


GRANT SELECT ON public.reo_v TO aggregate_ro;
GRANT SELECT ON public.reo_v TO aggregate_rw;
GRANT SELECT ON public.reo_v TO aggregate_adm;


CREATE OR REPLACE VIEW public.subject_property_v AS
SELECT
    sp.loan_transaction_id,
    sp.tenant_id,
    CAST(sp.data ->> 'propertyId' AS UUID) property_id,
    CAST(sp.data -> 'lastMetaData' ->> 'event-id' AS UUID) event_id,
    sp.data -> 'lastMetaData' ->> 'username' user_name,
    CAST(sp.data -> 'lastMetaData' ->> 'user-identifier' AS UUID) user_id,
    CAST(sp.data -> 'lastMetaData' ->> 'command-identifier' AS UUID) command_id,
    CAST(sp.data -> 'lastMetaData' ->> 'correlation-identifier' AS UUID) correlation_id,
    sp.data ->> 'county' county,
    sp.data ->> 'acreage' acreage,
    sp.data -> 'address' ->> 'city' subject_property_city,
    sp.data -> 'address' ->> 'validated' subject_property_address_validated,
    sp.data -> 'address' ->> 'countryCode' subject_property_country,
    sp.data -> 'address' ->> 'stateProvince' subject_property_state,
    sp.data -> 'address' ->> 'streetAddress1' subject_property_street_address_1,
    sp.data -> 'address' ->> 'streetAddress2' subject_property_street_address_2,
    sp.data -> 'address' ->> 'postalCodePlusFour' subject_property_plus_four_zip,
    sp.data -> 'address' ->> 'postalCode' subject_property_postal_code,
    sp.data ->> 'yearBuilt' property_structure_built_year,
    sp.data ->> 'loanPurpose' loan_purpose_type,
    sp.data ->> 'numberOfUnits' property_financed_number_of_units,
    sp.data ->> 'occupancyType' intent_to_occupy_type,
    sp.data ->> 'subjectPropertyType' gse_property_type,
    sp.data
FROM public.subject_property sp;


GRANT SELECT ON public.subject_property_v TO aggregate_ro;
GRANT SELECT ON public.subject_property_v TO aggregate_rw;
GRANT SELECT ON public.subject_property_v TO aggregate_adm;


CREATE OR REPLACE VIEW public.asset_owner_v AS
SELECT
    a.asset_id,
    CAST(o ->> 'partyId' AS UUID) party_id 
FROM public.asset a, JSON_ARRAY_ELEMENTS(a.data::JSON -> 'owners') o
WHERE (a.data ->> 'partyId') IS NOT NULL;

GRANT SELECT ON public.asset_owner_v TO aggregate_ro;
GRANT SELECT ON public.asset_owner_v TO aggregate_rw;
GRANT SELECT ON public.asset_owner_v TO aggregate_adm;


CREATE OR REPLACE VIEW public.asset_v AS
SELECT 
    a.asset_id,
    a.loan_transaction_id,
    a.tenant_id,
    CAST(a.data -> 'value' ->> 'amount' AS DECIMAL) asset_cash_or_market_value,
    a.data ->> 'assetType' asset_type,
    a.data ->> 'holderName' holder_name,
    a.data ->> 'accountNumber' account_number,
    a.data ->> 'assetCategory' asset_category
FROM public.asset a;

GRANT SELECT ON public.asset_v TO aggregate_ro;
GRANT SELECT ON public.asset_v TO aggregate_rw;
GRANT SELECT ON public.asset_v TO aggregate_adm;


CREATE OR REPLACE VIEW public.income_v AS
SELECT
    i.income_id,
    i.tenant_id,
    i.applicant_id party_id,
    i.data ->> 'type' income_type,
    CAST(i.data -> 'amount' ->> 'amount' AS DECIMAL) income_amount,
    i.data ->> 'category' income_category,
    i.data ->> 'frequency' income_frequency_type
FROM public.income i;

GRANT SELECT ON public.income_v TO aggregate_ro;
GRANT SELECT ON public.income_v TO aggregate_rw;
GRANT SELECT ON public.income_v TO aggregate_adm;



CREATE OR REPLACE VIEW public.status_bar_v AS
SELECT
    ticks.*,
    ticks.registration_ticks +
        ticks.applicant_ticks +
        ticks.employment_ticks +
        ticks.income_ticks +
        ticks.asset_ticks +
        ticks.liability_ticks +
        ticks.reo_ticks + 
        ticks.subject_property_ticks +
        ticks.document_ticks complete_ticks
FROM
(
    SELECT
        loan.loan_transaction_id,
        loan.loan_id,
        loan.num_applicants,
        1 registration_ticks,
        CASE
            WHEN complete_applicants.num_applicants = loan.num_applicants THEN 4
            ELSE 0
        END applicant_ticks,
        CASE
            WHEN complete_employment.num_applicants = loan.num_applicants THEN 4
            ELSE 0
        END employment_ticks,
        CASE
            WHEN complete_income.num_applicants = loan.num_applicants THEN 2
            ELSE 0
        END income_ticks,
        CASE
            WHEN complete_asset.num_applicants = loan.num_applicants THEN 4
            ELSE 0
        END asset_ticks,
        CASE
            WHEN complete_liability.num_applicants = loan.num_applicants THEN 2
            ELSE 0
        END liability_ticks,
        3 reo_ticks,
        CASE
            WHEN sp1.property_id IS NOT NULL THEN 2
            ELSE 0
        END subject_property_ticks,
        5 document_ticks,
        25 possible_ticks
    FROM  
    (
        SELECT
            a1.loan_transaction_id,
            a1.loan_id,
            COUNT(*) num_applicants
        FROM public.applicant_v a1
        GROUP BY
            a1.loan_transaction_id,
            a1.loan_id
    ) loan
    --
    --  applicant
    --
    LEFT OUTER JOIN
    (
        SELECT
            a2.loan_transaction_id,
            a2.loan_id,
            COUNT(*) num_applicants
        FROM public.applicant_v a2
        WHERE a2.individual_first_name IS NOT NULL
        AND a2.individual_last_name IS NOT NULL
        AND a2.birth_date IS NOT NULL
        --  AND a2.ssn IS NOT NULL   --  removed at Simon's request
        AND a2.veteran_status IS NOT NULL
        AND a2.citizenship_status IS NOT NULL
        AND a2.current_residence_street_address_1 IS NOT NULL
        AND a2.current_residence_city IS NOT NULL
        AND a2.current_residence_state IS NOT NULL
        AND a2.current_residence_postal_code IS NOT NULL
        AND a2.current_residence_country_code IS NOT NULL
        AND a2.current_residence_start_date IS NOT NULL
        AND a2.mail_to_street_address_1 IS NOT NULL
        AND a2.mail_to_city IS NOT NULL
        AND a2.mail_to_state IS NOT NULL
        AND a2.mail_to_postal_code IS NOT NULL
        AND a2.mail_to_country IS NOT NULL
        AND a2.relationship_status IS NOT NULL
        --  residency_basis_type
        AND EXISTS
        (
            SELECT 1
            FROM public.party_contact_point_telephone_v pcpt
            WHERE a2.party_id = pcpt.party_id
        )
        GROUP BY
            a2.loan_transaction_id,
            a2.loan_id
    ) complete_applicants
        ON loan.loan_id = complete_applicants.loan_id
    --
    --  employment
    --
    LEFT OUTER JOIN
    (
        SELECT
            a3.loan_transaction_id,
            a3.loan_id,
            COUNT(*) num_applicants
        FROM public.applicant_v a3
        WHERE a3.retirement_start_date <= CURRENT_DATE - INTERVAL '2 YEARS'
        OR EXISTS 
        (
            SELECT 1
            FROM public.employment_v e1
            WHERE e1.employment_start_date IS NOT NULL
            AND e1.employer_phone IS NOT NULL
            AND e1.employed_by_relative_indicator IS NOT NULL
            AND e1.employer_name IS NOT NULL
            AND e1.employer_street_address_1 IS NOT NULL
            AND e1.employer_city IS NOT NULL
            AND e1.employer_state IS NOT NULL
            AND e1.employer_postal_code IS NOT NULL
            AND e1.employment_applicant_self_employed_indicator IS NOT NULL
            AND e1.employer_position_description IS NOT NULL
            AND a3.party_id = e1.party_id
        )
        GROUP BY
            a3.loan_transaction_id,
            a3.loan_id
    ) complete_employment
        ON loan.loan_id = complete_employment.loan_id
    --
    --  income
    --
    LEFT OUTER JOIN
    (
        SELECT
            a4.loan_transaction_id,
            a4.loan_id,
            COUNT(*) num_applicants
        FROM public.applicant_v a4
        WHERE EXISTS
        (
            SELECT 1
            FROM public.income_v i
            WHERE a4.party_id = i.party_id
        )
        GROUP BY
            a4.loan_transaction_id,
            a4.loan_id
    ) complete_income
        ON loan.loan_id = complete_income.loan_id
    --
    --  asset
    --
    LEFT OUTER JOIN
    (
        SELECT
            a5.loan_transaction_id,
            a5.loan_id,
            COUNT(*) num_applicants
        FROM public.applicant_v a5
        WHERE EXISTS
        (
            SELECT 1
            FROM public.asset_owner_v ao
            WHERE a5.party_id = ao.party_id
        )
        GROUP BY
            a5.loan_transaction_id,
            a5.loan_id
    ) complete_asset
        ON loan.loan_id = complete_asset.loan_id
    --
    --  liability
    --
    LEFT OUTER JOIN
    (
        SELECT
            a6.loan_transaction_id,
            a6.loan_id,
            COUNT(*) num_applicants
        FROM public.applicant_v a6
        WHERE EXISTS
        (
            SELECT 1
            FROM public.liability l
            WHERE a6.party_id = l.party_id
        )
        GROUP BY
            a6.loan_transaction_id,
            a6.loan_id    
    ) complete_liability
        ON loan.loan_id = complete_liability.loan_id
    --
    --  subject_property
    --
    LEFT OUTER JOIN public.subject_property_v sp1
        ON loan.loan_transaction_id = sp1.loan_transaction_id
        AND sp1.gse_property_type IS NOT NULL
        AND sp1.intent_to_occupy_type IS NOT NULL
        AND sp1.subject_property_street_address_1 IS NOT NULL
        AND sp1.subject_property_city IS NOT NULL
        AND sp1.subject_property_state IS NOT NULL
        AND sp1.subject_property_postal_code IS NOT NULL
        AND sp1.property_financed_number_of_units IS NOT NULL
        AND sp1.property_structure_built_year IS NOT NULL
        AND sp1.county IS NOT NULL
        AND sp1.acreage IS NOT NULL
) ticks;

GRANT SELECT ON public.status_bar_v TO aggregate_ro;
GRANT SELECT ON public.status_bar_v TO aggregate_rw;
GRANT SELECT ON public.status_bar_v TO aggregate_adm;




