\connect aggregate


DROP VIEW public.status_bar_v;
DROP VIEW public.subject_property_v;
DROP VIEW public.reo_v;
DROP VIEW public.property_v;
DROP VIEW public.applicant_v;
DROP VIEW public.party_v;
DROP VIEW public.party_contact_point_telephone_v;
DROP VIEW public.loan_v;
DROP VIEW public.loan_transaction_v;
DROP VIEW public.employment_v;
DROP VIEW public.document_v;
DROP VIEW public.document_descriptor_v;
DROP VIEW public.asset_v;
DROP VIEW public.asset_owner_v;
DROP VIEW public.income_v;


DROP TABLE public.applicant CASCADE;
DROP TABLE public.applicant_reo CASCADE;
DROP TABLE public.asset CASCADE;
DROP TABLE public.declaration CASCADE;
DROP TABLE public.document_descriptor CASCADE;
DROP TABLE public.government_monitoring CASCADE;
DROP TABLE public.income CASCADE;
DROP TABLE public.liability CASCADE;
DROP TABLE public.loan CASCADE;
DROP TABLE public.loan_transaction CASCADE;
DROP TABLE public.party CASCADE;
DROP TABLE public.property CASCADE;
DROP TABLE public.reo CASCADE;
DROP TABLE public.subject_property CASCADE;
DROP TABLE public.tenant CASCADE;
DROP TABLE public.online_sale CASCADE;
DROP TABLE public.product_and_pricing CASCADE;
DROP TABLE public.user_summary CASCADE;
