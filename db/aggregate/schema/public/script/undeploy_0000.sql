\connect aggregate

DELETE FROM public.reo;
DELETE FROM public.applicant_reo;
DELETE FROM public.asset;
DELETE FROM public.document_descriptor;
DELETE FROM public.income;
DELETE FROM public.property;
DELETE FROM public.subject_property;
DELETE FROM public.applicant;
DELETE FROM public.loan;
DELETE FROM public.loan_transaction;
DELETE FROM public.party;
