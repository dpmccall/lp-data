--
--  database
--
CREATE DATABASE aggregate;


--
--  role
--
CREATE ROLE aggregate_ro LOGIN;
CREATE ROLE aggregate_rw LOGIN;
CREATE ROLE aggregate_adm LOGIN;


--
--  service accounts
--  
DO
$$
BEGIN
    IF NOT EXISTS (SELECT * FROM pg_catalog.pg_user WHERE usename = 'svcjava') THEN
        CREATE USER svcjava PASSWORD 'default';
    END IF;
    
    IF NOT EXISTS (SELECT * FROM pg_catalog.pg_user WHERE usename = 'svcjasper') THEN
        CREATE USER svcjasper PASSWORD 'reports';
    END IF;    
END
$$;
GRANT aggregate_rw TO svcjava;
GRANT aggregate_adm TO postgres;
GRANT aggregate_ro TO svcjasper;


--  
--  schema
--
\connect aggregate
--  Do not create public schema.  It already exists.
GRANT ALL ON SCHEMA public TO aggregate_adm WITH GRANT OPTION;


--  Keep schema is used for undo.
CREATE SCHEMA IF NOT EXISTS keep;
GRANT ALL ON SCHEMA keep TO aggregate_adm WITH GRANT OPTION;






