--
--  database
--
DROP DATABASE aggregate;


--
--  role
--
DROP ROLE aggregate_ro;
DROP ROLE aggregate_rw;
DROP ROLE aggregate_adm;


