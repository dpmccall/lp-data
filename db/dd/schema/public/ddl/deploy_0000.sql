-- Project :      dd.public.DM1
--
-- Date Created : Tuesday, July 19, 2016 09:49:12
-- Target DBMS : PostgreSQL 9.x
--


\connect dd

-- 
-- TABLE: dictionary_workbook 
--

CREATE TABLE dictionary_workbook(
    dictionary_workbook_key    int4            NOT NULL,
    sheet_name                 varchar(200)    NOT NULL,
    loaded_on_date             timestamp       NOT NULL,
    data                       jsonb,
    CONSTRAINT dictionary_workbook_pk PRIMARY KEY (dictionary_workbook_key)
)
;
GRANT SELECT ON public.dictionary_workbook TO dd_ro;
GRANT SELECT, INSERT, UPDATE, DELETE ON public.dictionary_workbook TO dd_rw;
GRANT ALL ON public.dictionary_workbook TO dd_adm;


-- 
-- TABLE: term 
--

CREATE TABLE term(
    term_key                           int4             NOT NULL,
    term_name                          varchar(200),
    term_description                   varchar(4000),
    related_term_key                   varchar(200),
    story_introduction                 varchar(1000),
    calculated_term_indicator          varchar(200),
    data_type                          varchar(200),
    data_length                        varchar(200),
    data_precision                     varchar(200),
    data_rounding                      varchar(200),
    nullable                           varchar(200),
    default_value                      varchar(200),
    sample_data                        varchar(1000),
    shown_values                       varchar(200),
    reasonable_limit                   varchar(200),
    reasonable_check                   varchar(200),
    mismo_ldd_name                     varchar(1000),
    business_owner                     varchar(200),
    it_owner                           varchar(200),
    npi_security_level                 varchar(200),
    critical_value_level               varchar(200),
    customizable_indicator             varchar(200),
    key_words                          varchar(200),
    investor_indicator                 varchar(200),
    servicing_indicator                varchar(200),
    compliance_indicator               varchar(200),
    categories                         varchar(200),
    reference_values                   varchar(200),
    source_preference                  varchar(1000),
    manual_entry_allowed_indicator     varchar(200),
    manual_change_allowed_indicator    varchar(200),
    mismo_categories                   varchar(1000),
    CONSTRAINT term_pk PRIMARY KEY (term_key)
)
;
GRANT SELECT ON public.term TO dd_ro;
GRANT SELECT, INSERT, UPDATE, DELETE ON public.term TO dd_rw;
GRANT ALL ON public.term TO dd_adm;


-- 
-- TABLE: term_enumeration 
--

CREATE TABLE term_enumeration(
    term_key                              int4             NOT NULL,
    term_enumeration_sequence             varchar(200)     NOT NULL,
    term_enumeration_short_description    varchar(200),
    term_enumeration_long_description     varchar(4000),
    term_enumeration_label                varchar(1000),
    term_enumeration_sort_order           varchar(200),
    term_enumeration_enabled              varchar(200),
    term_enumeration_deactivated          varchar(200),
    mismo_enumeration                     varchar(1000),
    CONSTRAINT term_enumeration_pk PRIMARY KEY (term_key, term_enumeration_sequence)
)
;
GRANT SELECT ON public.term_enumeration TO dd_ro;
GRANT SELECT, INSERT, UPDATE, DELETE ON public.term_enumeration TO dd_rw;
GRANT ALL ON public.term_enumeration TO dd_adm;


-- 
-- VIEW: term_enumeration_v 
--

CREATE OR REPLACE VIEW public.term_enumeration_v AS
SELECT 
    j ->> 'termRefId' term_key,
    j ->> 'shortDescription' term_enumeration_short_description,
    j ->> 'longDescription' term_enumeration_long_description,
    j ->> 'order' term_enumeration_sort_order,
    j ->> 'termRefId' term_enumeration_sequence
FROM dictionary_workbook ts, JSON_ARRAY_ELEMENTS(ts.data::json) j
WHERE ts.sheet_name = 'Reference Data';
;


GRANT SELECT ON public.term_enumeration_v TO dd_ro;
GRANT SELECT, INSERT, UPDATE, DELETE ON public.term_enumeration_v TO dd_rw;
GRANT ALL ON public.term_enumeration_v TO dd_adm
;

-- 
-- VIEW: term_v 
--

CREATE OR REPLACE VIEW public.term_v AS
SELECT 
    j ->> 'termId' term_key,
    j ->> 'term' term_name,
    j ->> 'definition' term_definition,
    j ->> 'relatedToTermId' realted_term_key,  
    j ->> 'storyIntroduction' story_introduction,
    j ->> 'calcFlag' calc_flag,
    j ->> 'newDataType' data_type,
    j ->> 'length' data_length,
    j ->> 'precision' data_precision,
    j ->> 'rounding' data_rounding,
    j ->> 'nullable' nullable,
    j ->> 'defaultValue' default_value,
    j ->> 'sampleData' sample_data,
    j ->> 'shownValues' shown_values,
    j ->> 'reasonableLimits' reasonable_limit,
    j ->> 'mismoFieldName' mismo_ldd_name,
    j ->> 'promontechBusinessOwner' business_owner,
    j ->> 'promontechItOwner' it_owner,
    j ->> 'npiSecurityLevel' npi_security_level,
    j ->> 'criticalValueLevel' critical_value_level,
    j ->> 'investorFieldFlag' investor_indicator,
    j ->> 'servicingFieldFlag' servicing_indicator,
    j ->> 'complianceFieldFlag' compliance_indicator,
    j ->> 'promontechUseCategories' categories,
    j ->> 'sourceReference' source_reference,
    j ->> 'allowManualEntry' manual_entry_allowed_indicator,
    j ->> 'allowManualChange' manual_change_allowed_indicator,
    j ->> 'mismoCategories' mismo_categories
FROM dictionary_workbook ts, JSON_ARRAY_ELEMENTS(ts.data::json) j
;

GRANT SELECT ON public.term_v TO dd_ro;
GRANT SELECT, INSERT, UPDATE, DELETE ON public.term_v TO dd_rw;
GRANT ALL ON public.term_v TO dd_adm
;

-- 
-- INDEX: term_x1 
--

CREATE UNIQUE INDEX term_x1 ON term(term_name)
;
-- 
-- TABLE: term_enumeration 
--

ALTER TABLE term_enumeration ADD CONSTRAINT term_enumeration_fk_1 
    FOREIGN KEY (term_key)
    REFERENCES term(term_key)
;

