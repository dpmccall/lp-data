--
--  database
--
DROP DATABASE dd;


--
--  role
--
DROP ROLE dd_ro;
DROP ROLE dd_rw;
DROP ROLE dd_adm;


