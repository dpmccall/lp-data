--
--  database
--
CREATE DATABASE dd;


--
--  role
--
CREATE ROLE dd_ro LOGIN;
CREATE ROLE dd_rw LOGIN;
CREATE ROLE dd_adm LOGIN;


--
--  service account
--  
DO
$$
BEGIN
    IF NOT EXISTS (SELECT * FROM pg_catalog.pg_user WHERE usename = 'svcjava') THEN
        CREATE USER svcjava PASSWORD 'default';
    END IF;
END
$$;
GRANT dd_rw TO svcjava;


--  
--  schema
--
\connect dd
--  Do not create public schema.  It already exists.
GRANT ALL ON SCHEMA public TO dd_adm WITH GRANT OPTION;


--  Keep schema is used for undo.
CREATE SCHEMA IF NOT EXISTS keep;
GRANT ALL ON SCHEMA keep TO dd_adm WITH GRANT OPTION;






